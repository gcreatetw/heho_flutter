//
//  FLNativeView.swift
//  Runner
//
//  Created by 洪茂榜 on 2022/3/10.
//

import Flutter
import UIKit
import iMFAD

class FLNativeViewFactory: NSObject, FlutterPlatformViewFactory {
    private var messenger: FlutterBinaryMessenger

    init(messenger: FlutterBinaryMessenger) {
        self.messenger = messenger
        super.init()
    }

    func create(
        withFrame frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?
    ) -> FlutterPlatformView {
        return FLNativeView(
            frame: frame,
            viewIdentifier: viewId,
            arguments: args,
            binaryMessenger: messenger)
    }
}

class FLNativeView: NSObject, FlutterPlatformView {
    private var _view: UIView
    var banner:MFBannerView?

    init(
        frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?,
        binaryMessenger messenger: FlutterBinaryMessenger?
    ) {
        _view = UIView()
        super.init()
        // iOS views can be created here
        createNativeView(view: _view)
    }

    func view() -> UIView {
        return _view
    }

    func createNativeView(view _view: UIView){
//        _view.backgroundColor = UIColor.blue
        
//        let nativeLabel = UILabel()
//        nativeLabel.text = "Native text from iOS"
//        nativeLabel.textColor = UIColor.white
//        nativeLabel.textAlignment = .center
//        nativeLabel.frame = CGRect(x: 0, y: 0, width: 180, height: 48.0)
        
        print("ios");
        let point = CGPoint(x: ((UIScreen.main.bounds.width)/2)-150, y: (self.view().frame.size.height)/2)
        banner = MFBannerView.init(adSize: MFAdSize300X250, origin: point);
        banner?.bannerId = "13282";
        banner?.delegate = self;
        banner?.setAdAutoRefresh(true);
        banner?.requestAd();

        _view.addSubview(banner!)
    }
}

extension FLNativeView :MFBannerDelegate{
    
    func requestAdSuccess() {
        //顯示banner廣告
        banner?.show();
    }
    
    func requestAdFail() {
        print("請求廣告失敗");
    }
    
    func onClickAd() {
        print("Click Ad");
    }
}

