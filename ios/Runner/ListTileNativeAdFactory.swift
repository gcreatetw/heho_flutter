//
//  ListTileNativeAdFactory.swift
//  Runner
//
//  Created by 洪茂榜 on 2022/3/30.
//

import google_mobile_ads

// TODO: Implement ListTileNativeAdFactory
class ListTileNativeAdFactory : FLTNativeAdFactory {

    func createNativeAd(_ nativeAd: GADNativeAd,
                        customOptions: [AnyHashable : Any]? = nil) -> GADNativeAdView? {
        let nibView = Bundle.main.loadNibNamed("ListTileNativeAdView", owner: nil, options: nil)!.first
        let nativeAdView = nibView as! GADNativeAdView

        func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
            let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
                label.numberOfLines = 2
                label.lineBreakMode = NSLineBreakMode.byWordWrapping
                label.font = font
                label.text = text
                label.sizeToFit()
            return label.frame.height
        }
        var height = heightForView(text: nativeAd.headline!, font: UIFont(name: "HelveticaNeue", size: 18)!, width: (UIScreen.main.bounds.width - ((UIScreen.main.bounds.width - 40)/3 + 24) - 16))
        let adTitle = UILabel()
        adTitle.numberOfLines = 2
        adTitle.frame = CGRect(x: ((UIScreen.main.bounds.width - 40)/3 + 24), y: 0,width: (UIScreen.main.bounds.width - ((UIScreen.main.bounds.width - 40)/3 + 24) - 16),height: height)
        adTitle.text = nativeAd.headline
        
//        (nativeAdView.bodyView as! UILabel).text = nativeAd.body
//        nativeAdView.bodyView!.isHidden = nativeAd.body == nil
        let adImage = UIImageView()
        adImage.frame = CGRect(x: 16, y: 0, width: (UIScreen.main.bounds.width - 40)/3, height: (UIScreen.main.bounds.width - 40)/3/120*63)
        adImage.image = nativeAd.images![0].image
        adImage.isHidden = nativeAd.images![0].image == nil

        nativeAdView.callToActionView?.isUserInteractionEnabled = false

        nativeAdView.nativeAd = nativeAd
        nativeAdView.addSubview(adTitle)
        nativeAdView.addSubview(adImage)

        return nativeAdView
    }
}
