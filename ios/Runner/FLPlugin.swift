//
//  FLPlugin.swift
//  Runner
//
//  Created by 洪茂榜 on 2022/3/10.
//

import Flutter
import UIKit

class FLPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let factory = FLNativeViewFactory(messenger: registrar.messenger())
        registrar.register(factory, withId: "ClickForce300250")
    }
}

