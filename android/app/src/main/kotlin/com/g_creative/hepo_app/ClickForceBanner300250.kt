package com.g_creative.hepo_app

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import com.clickforce.ad.AdManage
import com.clickforce.ad.AdSize
import com.clickforce.ad.AdView
import com.google.android.gms.ads.nativead.NativeAdView
import io.flutter.plugin.platform.PlatformView
import com.clickforce.ad.Listener.AdViewListener




class ClickForceBanner300250(context: Context, id: Int, creationParams: Map<String?, Any?>?) : PlatformView {
    private val box = LayoutInflater.from(context)
        .inflate(R.layout.click_force_banner_300250, null)
    private val ad = box.findViewById<AdView>(R.id.ad)
    override fun getView(): View {

        ad.getAd(13282, AdSize.MA300X250,true)

        ad.setOnAdViewLoaded(object : AdViewListener {
            override fun OnAdViewLoadSuccess() {
                Log.d("Ad Response Result", "成功請求廣告")
                ad.show() //顯示banner廣告
            }

            override fun OnAdViewLoadFail() {
                Log.d("Ad Response Result", "請求廣告失敗")
            }

            override fun OnAdViewClickToAd() {
                Log.d("Ad Click", "點擊廣告")
            }
        })
        return box
    }

    override fun dispose() {
        ad.releaseAd();
    }
}