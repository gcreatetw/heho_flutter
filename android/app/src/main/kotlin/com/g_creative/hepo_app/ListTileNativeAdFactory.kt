// TODO: Implement ListTileNativeAdFactory class

package com.g_creative.hepo_app

import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import io.flutter.plugins.googlemobileads.GoogleMobileAdsPlugin

class ListTileNativeAdFactory(val context: Context) : GoogleMobileAdsPlugin.NativeAdFactory {

    override fun createNativeAd(
        nativeAd: NativeAd,
        customOptions: MutableMap<String, Any>?
    ): NativeAdView {
        val nativeAdView = LayoutInflater.from(context)
            .inflate(R.layout.list_tile_native_ad, null) as NativeAdView

        with(nativeAdView) {
            val attributionViewSmall =
                findViewById<TextView>(R.id.tv_list_tile_native_ad_attribution_small)
            val attributionViewLarge =
                findViewById<TextView>(R.id.tv_list_tile_native_ad_attribution_large)

            val width = Resources.getSystem().displayMetrics.widthPixels/Resources.getSystem().displayMetrics.density
            val iconView = findViewById<ImageView>(R.id.iv_list_tile_native_ad_icon)
            val iconParams: ViewGroup.LayoutParams = iconView.layoutParams
            iconParams.width = (((width - 40)/3)*Resources.getSystem().displayMetrics.density).toInt()
            iconParams.height = ((((width - 40)/3)/120*63)*Resources.getSystem().displayMetrics.density).toInt()
            iconView.layoutParams = iconParams
            val icon = nativeAd.images[0]
            if (icon != null) {
                print(123)
                attributionViewSmall.visibility = View.VISIBLE
                attributionViewLarge.visibility = View.INVISIBLE
                iconView.setImageDrawable(icon.drawable)
            } else {
                attributionViewSmall.visibility = View.INVISIBLE
                attributionViewLarge.visibility = View.VISIBLE
            }
            this.iconView = iconView

            val headlineView = findViewById<TextView>(R.id.tv_list_tile_native_ad_headline)
            val param = headlineView.layoutParams as ViewGroup.MarginLayoutParams
            param.leftMargin = (((width - 40)/3)*Resources.getSystem().displayMetrics.density).toInt() + 8*Resources.getSystem().displayMetrics.density.toInt()
            headlineView.layoutParams = param
            headlineView.text = nativeAd.headline
            this.headlineView = headlineView


//            val bodyView = findViewById<TextView>(R.id.tv_list_tile_native_ad_body)
//            with(bodyView) {
//                text = nativeAd.body
//                visibility = if (nativeAd.body.isNotEmpty()) View.VISIBLE else View.INVISIBLE
//            }
            this.bodyView = bodyView

            setNativeAd(nativeAd)
        }

        return nativeAdView
    }
}