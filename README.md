# Hepo App

## 如何執行

### 開發環境
 - 作業系統：macOS 12.0.1 20G165 darwin-x64,
 - Flutter v2.5.3 Stable Version
 - Dart v2.12 (支援 Null-Safety)
 - Android Platform android-31, build-tools 31.0.0
 - Java version OpenJDK Runtime Environment (build 11.0.10+0-b96-7281165)
 - Xcode 13.0, Build version 13A233
 - CocoaPods version 1.11.2

### 執行 Flutter

```bash
    flutter run
```

## 命令集

### 生成資源檔類別

若未安裝 [spider](https://pub.dev/packages/spider)，請先安裝套件

```bash
    pub global activate spider
```

執行生成指令

```bash
    spider build
```

### 生成 JSON 序列化類別成員

執行生成指令

```bash
    flutter packages pub run build_runner build --delete-conflicting-outputs 
```

執行生成指令(監聽模式)

```bash
    flutter packages pub run build_runner watch --delete-conflicting-outputs 
```

### 建置版本

#### Android

編譯 bundle app (現行 Play 商店要求格式)

```bash
    flutter build appbundle --no-tree-shake-icons
```

並將安裝檔 `build/app/outputs/bundle/release/app-release.aab` 上傳至 Play Store Console

(限定 macOS 作業系統)

#### iOS

```bash
    bash ./scripts/ios_release.sh 
```
