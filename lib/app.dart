import 'package:firebase_messaging_platform_interface/src/remote_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hepo_app/config/constants.dart';
import 'package:hepo_app/models/hospital_meta_data.dart';
import 'package:hepo_app/models/phone_area_code.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/home/hospital_search_page.dart';
import 'package:hepo_app/pages/home/news_content_page.dart';
import 'package:hepo_app/pages/home/symptom_search_page.dart';
import 'package:hepo_app/pages/main_page.dart';
import 'package:hepo_app/pages/welcome_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/utils/app_tracking_transparency_utils.dart';
import 'package:hepo_app/utils/firebase_analytics_utils.dart';
import 'package:hepo_app/utils/firebase_message_utils.dart';
import 'package:hepo_app/utils/firebase_remote_config_utils.dart';
import 'package:hepo_app/utils/preferences.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import 'models/app_meta_data.dart';
import 'models/collection_data.dart';
import 'models/news_data.dart';
import 'models/user_location.dart';
import 'models/weather_data.dart';

extension _DateEx on DateTime {
  bool isSameDay(DateTime dateTime) {
    return dateTime.year == year &&
        dateTime.month == month &&
        dateTime.day == day;
  }
}

GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late bool isSameDay;

  UserData userData = UserData();

  UserLocation location = UserLocation();

  AppMetaData metaData = AppMetaData();

  CollectionData collectionData = CollectionData.init();

  PhoneAreaCodeData phoneAreaCodeData = PhoneAreaCodeData.init();

  WeatherData weatherData = WeatherData.init();

  bool hasMessage = false;

  @override
  void initState() {
    Future<void>.microtask(() async {
      phoneAreaCodeData.fetch();
      metaData.fetch();
      collectionData.fetch();
      await AppTrackingUtils.instance.checkPermissionStatus();
      FirebaseMessagingUtils.instance.init(onClick: _onclick);
      FirebaseRemoteConfigUtils.instance.fetchAndActivate();
    });
    _checkIsSameDay();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<UserData>.value(value: userData),
        ChangeNotifierProvider<UserLocation>.value(value: location),
        ChangeNotifierProvider<AppMetaData>.value(value: metaData),
        ChangeNotifierProvider<CollectionData>.value(value: collectionData),
        ChangeNotifierProvider<WeatherData>.value(value: weatherData)
      ],
      child: MaterialApp(
        navigatorKey: navigatorKey,
        title: 'Hepo App',
        builder: (BuildContext context, Widget? child) => MediaQuery(
          data: MediaQuery.of(context).copyWith(
            textScaleFactor: 1.0,
          ),
          child: child!,
        ),
        theme: ThemeData(
          primarySwatch: Colors.blue,
          accentColor: AppColors.muddyGreen,
        ),
        onGenerateRoute: (RouteSettings settings) {
          switch (settings.name) {
            case MainPage.routeName:
              return PageTransition<void>(
                child: const MainPage(),
                type: PageTransitionType.fade,
              );
            case HospitalSearchPage.routeName:
              return MaterialPageRoute<void>(
                builder: (_) => ChangeNotifierProvider<HospitalMetaData>(
                  create: (_) => HospitalMetaData.init(),
                  child: const HospitalSearchPage(),
                ),
              );
            case SymptomSearchPage.routeName:
              return MaterialPageRoute<void>(
                builder: (_) => const SymptomSearchPage(),
              );
            default:
              return MaterialPageRoute<void>(
                builder: (_) => Constants.showWelcomePage
                    ? const WelcomePage()
                    : const MainPage(),
              );
          }
        },
        localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          DefaultCupertinoLocalizations.delegate,
        ],
        supportedLocales: const <Locale>[
          Locale('zh', 'TW'),
        ],
        navigatorObservers: <NavigatorObserver>[
          if (FirebaseAnalyticsUtils.isSupported)
            FirebaseAnalyticsObserver(
              analytics: FirebaseAnalyticsUtils.instance.analytics!,
            ),
        ],
      ),
    );
  }

  void _checkIsSameDay() {
    final int lastVisitTime = Preferences.getInt(
      Constants.lastVisitTime,
      DateTime.now().subtract(const Duration(days: 1)).millisecondsSinceEpoch,
    );
    isSameDay = DateTime.fromMillisecondsSinceEpoch(lastVisitTime).isSameDay(
      DateTime.now(),
    );
    Preferences.setInt(
      Constants.lastVisitTime,
      DateTime.now().millisecondsSinceEpoch,
    );
  }

  void _onclick(RemoteMessage? message) {
    if (message != null) {
      hasMessage = true;
      Navigator.of(navigatorKey.currentState!.context).pushNamedAndRemoveUntil(
          MainPage.routeName, ModalRoute.withName(MainPage.routeName));
      Navigator.of(navigatorKey.currentState!.context).push(
        MaterialPageRoute<void>(
          builder: (_) => ChangeNotifierProvider<News>.value(
            value: News(
                imageUrl: '',
                type: NewsType.normal,
                link: message.data['url'].toString(),
                id: message.data['id'].toString(),
                dateTime: DateTime.now(),
                title: ''),
            child: const NewsContentPage(),
          ),
        ),
      );
    }
  }
}
