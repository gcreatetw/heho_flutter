import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/config/constants.dart';
import 'package:hepo_app/models/advertisement_data.dart';
import 'package:hepo_app/models/api/action_type.dart';
import 'package:hepo_app/models/api/advance_search_response.dart';
import 'package:hepo_app/models/api/advertisement_response.dart';
import 'package:hepo_app/models/api/article_response.dart';
import 'package:hepo_app/models/api/collection_type.dart';
import 'package:hepo_app/models/api/expert_data_response.dart';
import 'package:hepo_app/models/api/external_link_response.dart';
import 'package:hepo_app/models/api/forget_password_response.dart';
import 'package:hepo_app/models/api/get_hot_disease_response.dart';
import 'package:hepo_app/models/api/health_life_news_response.dart';
import 'package:hepo_app/models/api/health_life_type_response.dart';
import 'package:hepo_app/models/api/heho_advertisement_response.dart';
import 'package:hepo_app/models/api/home_auto_text_response.dart';
import 'package:hepo_app/models/api/home_category_response.dart';
import 'package:hepo_app/models/api/home_keyword_response.dart';
import 'package:hepo_app/models/api/hospital_content_response.dart';
import 'package:hepo_app/models/api/hospital_list_response.dart';
import 'package:hepo_app/models/api/hospital_option_response.dart';
import 'package:hepo_app/models/api/keys_response.dart';
import 'package:hepo_app/models/api/login_response.dart';
import 'package:hepo_app/models/api/media_health_categories_response.dart';
import 'package:hepo_app/models/api/member_collection_response.dart';
import 'package:hepo_app/models/api/meta_data_response.dart';
import 'package:hepo_app/models/api/order_list_response.dart';
import 'package:hepo_app/models/api/phone_area_code_response.dart';
import 'package:hepo_app/models/api/recommend_article_response.dart';
import 'package:hepo_app/models/api/recommend_text_response.dart';
import 'package:hepo_app/models/api/remove_search_response.dart';
import 'package:hepo_app/models/api/symptom_category_response.dart';
import 'package:hepo_app/models/api/update_member_data_response.dart';
import 'package:hepo_app/models/api/user_hobby_response.dart';
import 'package:hepo_app/models/api/weather_info_response.dart';
import 'package:hepo_app/models/general_callback.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/member/comments_page.dart';
import 'package:hepo_app/utils/preferences.dart';
import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:uuid/uuid.dart';

export 'package:hepo_app/models/general_callback.dart';

extension MapEx on Map<String, String> {
  String get postData {
    String rawString = '';
    forEach((String key, String value) {
      if (rawString != '') rawString += '&';
      rawString += '$key=$value';
    });
    return rawString;
  }
}

extension _StringEx on List<String> {
  String get apiFormat {
    String rawString = '';
    forEach((String element) {
      if (rawString.isNotEmpty) rawString += ',';
      rawString += element;
    });
    return rawString;
  }
}

class Helper {
  static const String _operateSystemTypeKeyName = 'cmd';
  static const String _uidKeyName = 'uid';
  static const String _tokenKeyName = 't';
  static const String _appVersionKeyName = 'ver';
  static const String _deviceIdKeyName = 'gid';
  static const String _sessionKeyName = 'session';

  static const String targetUrl = 'targetUrl';

  static const String _defaultUid = 'Default';
  static const String _defaultToken =
      //ignore: lines_longer_than_80_chars
      'QF4ZxSIzLfJSftx2ctsrmVL4GsMguRfO9ZNzyIm0SPnI0k1snwAJGphWKYEOS6oXVYzsL8VKcmgZInwKr0MzWcnu0anFTxZZGNKxI4yQouClPG5Jpcphhhsv7auzoT7W9JaqOZOsnMNIMZikmeehdVYXC2RwQYkqRH0huQ8BydOCiY3eoe5orqp2aCiUZmQtK0f7ONg5a1ux915r5ikaxx4Uqwgw2S6f0IRDL55enUjWW6bRLkWoHgJZvDbc6uGb';

  static const String host =
      Constants.alpha ? 'oc-uat.heho.com.tw' : 'myhope.com.tw';

  static Helper? _instance;

  // ignore: prefer_constructors_over_static_methods
  static Helper get instance {
    return _instance ??= Helper();
  }

  static const String webviewJumpUrl =
      'https://$host/index.php?route=api/app/jumpToMyHope';

  Helper() {
    dio.options = BaseOptions(
      baseUrl: 'https://$host/',
      connectTimeout: 10000,
      receiveTimeout: 10000,
      contentType: Headers.formUrlEncodedContentType,
    );
    if (kIsWeb) {
      operateSystemType = 'web';
    } else if (Platform.isAndroid) {
      operateSystemType = 'android';
    } else if (Platform.isIOS) {
      operateSystemType = 'ios';
    } else if (Platform.isWindows) {
      operateSystemType = 'windows';
    } else if (Platform.isMacOS) {
      operateSystemType = 'mac';
    } else {
      operateSystemType = 'unknown';
    }
  }

  Dio dio = Dio();

  late String operateSystemType;

  late String appVersion;

  String? uid;

  String? token;

  String deviceId = '';

  String? session;

  Future<void> init() async {
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    appVersion = packageInfo.version;
    String? uuid = Preferences.getStringSecurity(Constants.deviceId, null);
    if (uuid == null) {
      uuid = const Uuid().v4();
      await Preferences.setStringSecurity(Constants.deviceId, uuid);
    }
    deviceId = uuid;
    final bool isLogin = Preferences.getBool(Constants.isLogin, false);
    if (isLogin) {
      token = Preferences.getStringSecurity(Constants.token, null);
      session = Preferences.getStringSecurity(Constants.session, null);
      uid = Preferences.getStringSecurity(Constants.id, null);
    }
    if (kDebugMode) {
      debugPrint('deviceId = $deviceId');
    }
  }

  void _loadTestToken() {
    uid = '76';
    token =
        'WzDvrL1r7qglaVY9w6U1R1bnyNmcc4I9gnkFn3mZKmlJ7Uw5PesEPBVoOOdp3dsKsB93euSNLQvD8rjuDCDHaxrFVdWVpSdd6Uw6XEMxXge3AUnNLQvB0HGIZlxJWXy6Ri5ANFvkphr3YROT1VJtN43t7kEm0YZ0DtfTFcBupEXnGlpk4vHQ9L7PWDMyc2AX3VERMJJTFhblpfiOh9WzCtikQh442mejYQK9fiPkLLVNAY5fsDGYoFcBtm1e23ya';
  }

  Map<String, String> generateMap({
    Map<String, String>? data,
    String? uid,
    String? token,
    bool ignoreUid = false,
    bool ignoreToken = false,
    bool ignoreDeviceId = false,
    bool ignoreSession = true,
  }) {
    return <String, String>{
      _operateSystemTypeKeyName: operateSystemType,
      if (!ignoreUid) _uidKeyName: uid ?? this.uid ?? '',
      if (!ignoreToken) _tokenKeyName: token ?? this.token ?? '',
      _appVersionKeyName: appVersion,
      if (!ignoreDeviceId) _deviceIdKeyName: deviceId,
      if (!ignoreSession) _sessionKeyName: session ?? '',
      if (data != null) ...data,
    };
  }

  String _generateRoute(String name) => 'index.php?route=$name';

  Future<void> getMetaConfig({
    required GeneralCallback<MetaDataResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.get(
        _generateRoute('api/app/getUpdate'),
        queryParameters: generateMap(
          ignoreUid: true,
          ignoreToken: true,
          ignoreDeviceId: true,
        ),
      );
      if (response.isSuccessful) {
        final MetaDataResponse data = MetaDataResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getExternalLink({
    required GeneralCallback<ExternalLinkResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getExternalLink'),
        data: generateMap(
          token: _defaultToken,
          uid: _defaultUid,
        ),
      );
      if (response.isSuccessful) {
        final ExternalLinkResponse data =
            ExternalLinkResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getWeatherInfo({
    required GeneralCallback<WeatherInfoResponse> callback,
    double? latitude,
    double? longitude,
  }) async {
    try {

      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getWeather'),
        data: generateMap(
          ignoreUid: true,
          ignoreToken: true,
          ignoreDeviceId: true,
          data: <String, String>{
            'lat': latitude?.toString() ?? '',
            'lon': longitude?.toString() ?? '',
          },
        ),
      );
      if (response.isSuccessful) {
        final WeatherInfoResponse data =
            WeatherInfoResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  ///2021/10/13 首頁搜尋api
  Future<void> getHomeKeyWordInfo({
    required GeneralCallback<HomeKeywordResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getSearchPage'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final HomeKeywordResponse data =
            HomeKeywordResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  ///2021/10/13 首頁搜尋自動拼字
  Future<void> getHomeAutoText({
    required GeneralCallback<HomeAutoTextResponse> callback,
    required String query,
    required String searchType,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.get(
        _generateRoute('api/app/getAutocomplete'),
        queryParameters: generateMap(
          data: <String, String>{
            'query': query,
            'searchType': searchType,
          },
        ),
      );
      if (response.isSuccessful) {
        final HomeAutoTextResponse data =
            HomeAutoTextResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  ///2021/10/15 首頁搜尋結果
  Future<void> getAdvanceSearch({
    required GeneralCallback<AdvanceSearchResponse> callback,
    String? query,
    required String log,
    required String sort,
    required int page,
    required int limit,
  }) async {
    try {
      final Response<Map<String, dynamic>> response =
          await dio.post(_generateRoute('api/app/search'),
              data: generateMap(data: <String, String>{
                'q': query ?? '',
                'log': log,
                'sort': sort,
                'limit': limit.toString(),
                'page': page.toString(),
              }));
      if (response.isSuccessful) {
        final AdvanceSearchResponse data =
            AdvanceSearchResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  ///刪除搜尋紀錄
  Future<void> getRemoveSearch({
    required GeneralCallback<RemoveSearchResponse> callback,
    required String keyword,
  }) async {
    try {
      final Response<Map<String, dynamic>> response =
          await dio.post(_generateRoute('api/app/delUserRecentSearch'),
              data: generateMap(data: <String, String>{
                'keyword': keyword,
              }));
      if (response.isSuccessful) {
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getHomeAdvertisement({
    required GeneralCallback<AdvertisementData?> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getSplash'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final AdvertisementResponse data =
            AdvertisementResponse.fromJson(response.data!);
        final AdvertisementBean? dataBean = data.data?.first;
        if (dataBean != null) {
          AdvertisementType type = AdvertisementType.square;
          switch (dataBean.splashType) {
            case '1':
              type = AdvertisementType.rectangle;
              break;
            case '2':
              type = AdvertisementType.square;
              break;
            case '3':
              type = AdvertisementType.fullScreen;
              break;
          }
          callback.onSuccess(
            AdvertisementData(
              type: type,
              imageUrl: dataBean.imageUrl,
              linkUrl: dataBean.clickUrl,
            ),
          );
        } else {
          callback.onSuccess(null);
        }
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getHomeCategories({
    required GeneralCallback<HomeCategoryResponse> callback,
    required String version,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getCategory'),
        data: generateMap(
          data: <String, String>{
            'ver':version,
            'limit': '10',
            'page': '1',
          },
        ),
      );
      if (response.isSuccessful) {
        final HomeCategoryResponse data =
            HomeCategoryResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getHomeArticles({
    required String id,
    required int page,
    required int limit,
    required GeneralCallback<ArticleResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getCategoryPost'),
        data: generateMap(
          data: <String, String>{
            'cat_id': id,
            'page': page.toString(),
            'limit': limit.toString(),
          },
        ),
      );
      if (response.isSuccessful) {
        final ArticleResponse data = ArticleResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getNewsContentAdvertisement({
    required GeneralCallback<HehoAdvertisementResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio
          .post(_generateRoute('api/app/getPostAdv'), data: generateMap());
      if (response.isSuccessful) {
        final HehoAdvertisementResponse data =
            HehoAdvertisementResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getHotDisease({
    required GeneralCallback<GetHotDiseaseResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getHotDisease'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final GetHotDiseaseResponse data =
            GetHotDiseaseResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getAdvertisement({
    required GeneralCallback<HehoAdvertisementResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getAdv'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final HehoAdvertisementResponse data =
            HehoAdvertisementResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getMemberAdvertisement({
    required GeneralCallback<HehoAdvertisementResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getMemberAdv'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final HehoAdvertisementResponse data =
        HehoAdvertisementResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getRecommendArticle({
    required GeneralCallback<RecommendArticleResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getRecommand'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final RecommendArticleResponse data =
            RecommendArticleResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getSymptomCategory({
    required GeneralCallback<SymptomCategoryResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getDiseaseCategory'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final SymptomCategoryResponse data =
            SymptomCategoryResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getSymptomAdvertisement({
    required GeneralCallback<HehoAdvertisementResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getSymptomAdv'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final HehoAdvertisementResponse data =
            HehoAdvertisementResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getHospitalOption({
    required GeneralCallback<HospitalOptionResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getHospitalSearchOption'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final HospitalOptionResponse data =
            HospitalOptionResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  ///types: 特約類別
  ///department: 診療科別
  Future<void> getHospitalList({
    required GeneralCallback<HospitalListResponse> callback,
    required int page,
    required int limit,
    double? latitude,
    double? longitude,
    List<String>? types,
    List<String>? departments,
    List<String>? countries,
    String? keyword,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/searchHospitalPage'),
        data: generateMap(
          data: <String, String>{
            'page': page.toString(),
            'limit': limit.toString(),
            'lat': latitude?.toString() ?? '',
            'lon': longitude?.toString() ?? '',
            if (types != null) 'category': types.apiFormat,
            if (departments != null) 'service': departments.apiFormat,
            if (countries != null) 'county': countries.apiFormat,
            if (keyword != null) 'q': keyword,
          },
        ),
      );
      if (response.isSuccessful) {
        final HospitalListResponse data =
            HospitalListResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getHospitalContent({
    required String id,
    required GeneralCallback<HospitalContentResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getHospitalInfo'),
        data: generateMap(
          data: <String, String>{
            'hid': id,
          },
        ),
      );
      if (response.isSuccessful) {
        final HospitalContentResponse data =
            HospitalContentResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getHealthLifeType({
    required GeneralCallback<HealthLifeTypeResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getHealthyTopic'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final HealthLifeTypeResponse data =
            HealthLifeTypeResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getHealthLifeNews({
    String? id,
    required int page,
    required int limit,
    String? sort,
    String? query,
    required GeneralCallback<HealthLifeNewsResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response =
          await dio.post(_generateRoute('api/app/getHealthyCategoryPost'),
              data: generateMap(
                data: <String, String>{
                  'cat_id': id ?? '',
                  'page': page.toString(),
                  'limit': limit.toString(),
                  'sort': sort ?? '',
                  'q': query ?? '',
                },
              ));
      if (response.isSuccessful) {
        final HealthLifeNewsResponse data =
            HealthLifeNewsResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getMediaHealthCategories({
    required GeneralCallback<MediaHealthCategoriesResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getVideoCategory'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final MediaHealthCategoriesResponse data =
            MediaHealthCategoriesResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getExpertData({
    required GeneralCallback<ExpertDataResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getExpertPage'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final ExpertDataResponse data =
            ExpertDataResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getExpertArticle(
      {required GeneralCallback<ArticleResponse> callback,
      required int limit,
      required int page,
      required String expertId}) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getExpertPostData'),
        data: generateMap(data: <String, String>{
          'limit': limit.toString(),
          'page': page.toString(),
          'wpExpertId': expertId,
        }),
      );
      if (response.isSuccessful) {
        final ArticleResponse data = ArticleResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<String> _getKeys() async {
    final Response<Map<String, dynamic>> response = await dio.post(
      _generateRoute('api/app/getKey'),
      data: generateMap(
        uid: _defaultUid,
        token: _defaultToken,
      ),
    );
    final KeysResponse data = KeysResponse.fromJson(response.data!);
    return data.data;
  }

  Future<void> normalLogin({
    required String username,
    required String password,
    required GeneralCallback<LoginResponse> callback,
  }) async {
    try {
      final String keys = await _getKeys();
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/login'),
        data: generateMap(
          data: <String, String>{
            'email': username,
            'password': password,
            'getKey': keys,
          },
        ),
      );
      if (response.isSuccessful) {
        final LoginResponse data = LoginResponse.fromJson(response.data!);
        _loadToken(data);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> facebookLogin({
    required String accessToken,
    required GeneralCallback<LoginResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/fb'),
        data: generateMap(
          uid: _defaultUid,
          token: _defaultToken,
          data: <String, String>{
            'accessToken': accessToken,
          },
        ),
      );
      if (response.isSuccessful) {
        final LoginResponse data = LoginResponse.fromJson(response.data!);
        _loadToken(data);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> lineLogin({
    required String accessToken,
    required String idToken,
    required GeneralCallback<LoginResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/line'),
        data: generateMap(
          data: <String, String>{
            'code': accessToken,
            'id_token': idToken,
          },
        ),
      );
      if (response.isSuccessful) {
        final LoginResponse data = LoginResponse.fromJson(response.data!);
        _loadToken(data);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> googleLogin({
    required String accessToken,
    required String idToken,
    required GeneralCallback<LoginResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/google'),
        data: generateMap(
          data: <String, String>{
            'code': accessToken,
            'id_token': idToken,
          },
        ),
      );
      if (response.isSuccessful) {
        final LoginResponse data = LoginResponse.fromJson(response.data!);
        _loadToken(data);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> appleLogin({
    required String accessToken,
    required String idToken,
    required GeneralCallback<LoginResponse> callback,
    String? givenName,
    String? familyName,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/apple'),
        data: generateMap(
          data: <String, String>{
            'code': accessToken,
            'id_token': idToken,
            'givenName': givenName ?? '',
            'familyName': familyName ?? '',
          },
        ),
      );
      if (response.isSuccessful) {
        final LoginResponse data = LoginResponse.fromJson(response.data!);
        _loadToken(data);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> sendComments({
    required CommentLevel level,
    required String email,
    required String comments,
    required GeneralCallback<GeneralResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/feedback'),
        data: generateMap(
          data: <String, String>{
            'senderEmail': email,
            'score': (level.index + 1).toString(),
            'content': comments,
          },
        ),
      );
      if (response.isSuccessful) {
        final GeneralResponse data = GeneralResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> sendValidationCode({
    required String cellPhoneNumber,
    required GeneralCallback<ForgetPasswordResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
          _generateRoute('api/app/getRegCode'),
          data: generateMap(
              token: _defaultToken,
              uid: _defaultUid,
              data: <String, String>{
                'phone': cellPhoneNumber,
              }));
      if (response.isSuccessful) {
        final ForgetPasswordResponse data =
            ForgetPasswordResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getUserHobby({
    required GeneralCallback<UserHobbyResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getUserHobby'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final UserHobbyResponse data =
            UserHobbyResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> finishRegister({
    required String? regCode,
    required String? telephone,
    required String userName,
    required DateTime? birthday,
    required String? email,
    required String password,
    required String gender,
    required String hobby,
    required GeneralCallback<LoginResponse> callback,
  }) async {
    try {
      _loadTestToken();
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/register'),
        data: generateMap(
          token: _defaultToken,
          uid: _defaultUid,
          data: <String, String>{
            'regCode': regCode ?? '',
            'telephone': telephone ?? '',
            'userName': userName,
            'birthday': birthday != null
                ? DateFormat('yyyy-MM-dd').format(birthday)
                : '',
            'email': email ?? '',
            'password': password,
            'gender': gender,
            'hobby': hobby,
          },
        ),
      );
      if (response.isSuccessful) {
        final LoginResponse data = LoginResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> sendForgetPassword({
    required String account,
    required GeneralCallback<String> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/forgotten'),
        data: generateMap(
          ignoreUid: true,
          ignoreToken: true,
          data: <String, String>{
            'account': account,
          },
        ),
      );
      if (response.isSuccessful) {
        final ForgetPasswordResponse data =
            ForgetPasswordResponse.fromJson(response.data!);
        callback.onSuccess(data.data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> resetPassword({
    required String code,
    required String account,
    required String password,
    required GeneralCallback<GeneralResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/resetPassword'),
        data: generateMap(
          uid: _defaultUid,
          token: _defaultToken,
          data: <String, String>{
            'code': code,
            'account': account,
            'password': password,
          },
        ),
      );
      if (response.isSuccessful) {
        final GeneralResponse data = GeneralResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  ///2021/10/15 加入更新會員資料api、修改傳入參數
  Future<void> updateMemberData({
    required UpdateType updateType,
    required String updateValue,
    String? code,
    required GeneralCallback<UpdateMemberDataResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/updateMemberData'),
        data: generateMap(data: <String, String>{
          'updateType': updateType.text,
          'updateValue': updateValue,
          'code': code ?? '',
        }),
      );
      if (response.isSuccessful) {
        final UpdateMemberDataResponse data =
            UpdateMemberDataResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getMemberData({
    required GeneralCallback<UpdateMemberDataResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getAccountInfo'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final UpdateMemberDataResponse data =
            UpdateMemberDataResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getRecommendText({
    required GeneralCallback<RecommendTextResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getRecommandText'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final RecommendTextResponse data =
            RecommendTextResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getPhoneAreaCode({
    required GeneralCallback<PhoneAreaCodeResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getCountryCode'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final PhoneAreaCodeResponse data =
            PhoneAreaCodeResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getMemberCollection({
    required GeneralCallback<MemberCollectionResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getUserCollection'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final MemberCollectionResponse data =
            MemberCollectionResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> operateMemberCollection({
    required String value,
    required CollectionType type,
    required ActionType action,
    required GeneralCallback<GeneralResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/userCollection'),
        data: generateMap(
          data: <String, String>{
            'collectionName': type.apiName,
            'action': action.apiName,
            'targetValue': value,
          },
        ),
      );
      if (response.isSuccessful) {
        final GeneralResponse data = GeneralResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getOrderList({
    required GeneralCallback<OrderListResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _generateRoute('api/app/getOrder'),
        data: generateMap(),
      );
      if (response.isSuccessful) {
        final OrderListResponse data =
            OrderListResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  void _loadToken(LoginResponse response) {
    final UserDataBean bean = response.data;
    token = bean.token;
    uid = bean.customer_id;
    session = bean.session;
    Preferences.setStringSecurity(Constants.id, bean.customer_id);
    if(session != null){
      Preferences.setStringSecurity(Constants.session, bean.session!);
    }
    Preferences.setStringSecurity(Constants.token, bean.token);
    Preferences.setBool(Constants.isLogin, true);
  }

  void logout() {
    token = null;
    uid = null;
    session = null;
    Preferences.remove(Constants.id);
    Preferences.remove(Constants.session);
    Preferences.remove(Constants.token);
    Preferences.setBool(Constants.isLogin, false);
  }
}

extension ResponseExtension on Response<Map<String, dynamic>> {
  bool get isSuccessful => data != null && generalResponse.statusCode == '200';

  GeneralResponse get generalResponse => data != null
      ? GeneralResponse.fromJson(data!)
      : GeneralResponse(
          statusCode: '4000',
          message: 'Null Error',
        );
}

extension DioErrorI18nExtension on DioError {
  GeneralResponse get generalResponse {
    switch (type) {
      case DioErrorType.other:
        return GeneralResponse(
          statusCode: '2001',
          message: '無網路連線',
        );
      case DioErrorType.connectTimeout:
      case DioErrorType.receiveTimeout:
      case DioErrorType.sendTimeout:
        return GeneralResponse(
          statusCode: '2002',
          message: '連線逾時請稍後再試',
        );
      case DioErrorType.response:
        if (response != null && response!.data is Map<String, dynamic>) {
          final Map<String, dynamic> json =
              response!.data as Map<String, dynamic>;
          final GeneralResponse data = GeneralResponse.fromJson(json);
          return data;
        } else {
          return GeneralResponse(
            statusCode: '2000',
            message: message,
          );
        }
      case DioErrorType.cancel:
      default:
        return GeneralResponse(
          statusCode: '1000',
          message: '取消',
        );
    }
  }

  bool get isJsonResponse {
    return type == DioErrorType.response &&
        response != null &&
        response!.data is Map<String, dynamic>;
  }
}
