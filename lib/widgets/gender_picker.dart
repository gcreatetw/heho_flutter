import 'package:flutter/cupertino.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/resources/colors.dart';

class GenderPicker extends StatefulWidget {
  final Gender? initialValue;
  final Future<bool> Function()? onWillPop;
  final void Function(Gender)? onConfirmClick;

  const GenderPicker({
    Key? key,
    this.initialValue,
    this.onWillPop,
    this.onConfirmClick,
  }) : super(key: key);

  @override
  _GenderPickerState createState() => _GenderPickerState();
}

class _GenderPickerState extends State<GenderPicker> {
  Gender current = Gender.notSet;

  @override
  void initState() {
    if (widget.initialValue != null) {
      current = widget.initialValue!;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.onWillPop,
      child: Container(
        color: AppColors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                CupertinoButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    '取消',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
                const Spacer(),
                CupertinoButton(
                  onPressed: () {
                    widget.onConfirmClick?.call(current);
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    '確認',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 200.0,
              child: CupertinoPicker(
                itemExtent: 40.0,
                scrollController: FixedExtentScrollController(
                  initialItem: current.index,
                ),
                onSelectedItemChanged: (int value) {
                  current = Gender.values[value];
                },
                children: <Widget>[
                  for (Gender value in Gender.values)
                    Center(
                      child: Text(value.text),
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
