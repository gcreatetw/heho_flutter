import 'package:flutter/material.dart';
import 'package:hepo_app/resources/colors.dart';

class DummyArea extends StatelessWidget {
  final bool ignoring;
  final VoidCallback onIgnoreTap;
  final Widget child;

  const DummyArea({
    Key? key,
    required this.ignoring,
    required this.onIgnoreTap,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ignoring ? onIgnoreTap : null,
      child: Container(
        foregroundDecoration:
            ignoring ? const BoxDecoration(color: AppColors.black12) : null,
        child: IgnorePointer(
          ignoring: ignoring,
          child: child,
        ),
      ),
    );
  }
}
