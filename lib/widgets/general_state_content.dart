import 'package:flutter/material.dart';
import 'package:hepo_app/models/general_state.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/svg_hint_content.dart';

class GeneralStateContent extends StatelessWidget {
  final GeneralState state;
  final String? hint;
  final Widget child;
  final VoidCallback? onRetry;
  final bool useRefreshIndicator;
  final String? tag;

  const GeneralStateContent({
    required this.state,
    required this.child,
    this.hint,
    this.onRetry,
    this.useRefreshIndicator = false,
    this.tag,
  });

  @override
  Widget build(BuildContext context) {
    switch (state) {
      case GeneralState.error:
        return InkWell(
          onTap: () {
            onRetry?.call();
          },
          child: Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(
                maxHeight: 216.0,
                maxWidth: 166,
              ),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  SvgHintContent(
                    ImageAssets.cryCancelLoveNoText,
                    message: hint ?? '',
                  ),
                ],
              ),
            ),
          ),
        );
      case GeneralState.finished:
        return useRefreshIndicator
            ? RefreshIndicator(
                onRefresh: () async {
                  onRetry?.call();
                },
                child: child,
              )
            : child;
      case GeneralState.loading:
      default:
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const CircularProgressIndicator.adaptive(),
              const SizedBox(height: 16.0),
              Text(
                hint ?? '載入中',
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: AppColors.grey33,
                  fontFamily: 'HelveticaNeue',
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        );
    }
  }
}
