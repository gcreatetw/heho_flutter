import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hepo_app/resources/colors.dart';

class OutlineTextField extends StatefulWidget {
  final TextEditingController controller;
  final bool enable;
  final FocusNode? focusNode;
  final FocusNode? nextFocusNode;
  final List<String>? autofillHints;
  final String? hintText;
  final Color? textColor;
  final Color? enabledBorderColor;
  final int? maxLength;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final Function(String)? onChanged;
  final VoidCallback? onEditingComplete;
  final double? height;
  final Widget? prefixIcon;
  final double? prefixSize;
  final Widget? suffixIcon;
  final double? suffixSize;
  final bool? obscureText;
  final double borderRadius;
  final int? maxLines;
  final EdgeInsets? contentPadding;
  final String? errorText;

  const OutlineTextField({
    Key? key,
    required this.controller,
    this.enable = true,
    this.focusNode,
    this.nextFocusNode,
    this.autofillHints,
    this.hintText,
    this.textColor,
    this.enabledBorderColor,
    this.maxLength,
    this.onChanged,
    this.keyboardType,
    this.textInputAction,
    this.onEditingComplete,
    this.height,
    this.prefixIcon,
    this.prefixSize,
    this.suffixIcon,
    this.suffixSize,
    this.obscureText,
    this.borderRadius = 5.0,
    this.maxLines,
    this.contentPadding,
    this.errorText,
  }) : super(key: key);

  @override
  _OutlineTextFieldState createState() => _OutlineTextFieldState();
}

class _OutlineTextFieldState extends State<OutlineTextField> {
  Color get borderColor {
    return widget.errorText?.isNotEmpty ?? false
        ? AppColors.terracota
        : (widget.enabledBorderColor ?? AppColors.greyC6);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        if (widget.maxLength != null) ...<Widget>[
          ValueListenableBuilder<TextEditingValue>(
            valueListenable: widget.controller,
            builder: (_, TextEditingValue value, __) {
              return Text(
                '${value.text.length}/${widget.maxLength}',
                style: const TextStyle(
                  color: AppColors.grey2F,
                  fontSize: 10,
                  fontFamily: 'Montserrat',
                ),
              );
            },
          ),
          const SizedBox(height: 2.0),
        ],
        SizedBox(
          height: widget.height ?? 42.0,
          child: TextField(
            controller: widget.controller,
            enabled: widget.enable,
            focusNode: widget.focusNode,
            obscureText: widget.obscureText ?? false,
            obscuringCharacter: '*',
            onChanged: widget.onChanged,
            keyboardType: widget.keyboardType,
            autofillHints: widget.enable ? widget.autofillHints : null,
            onEditingComplete: () {
              widget.focusNode?.unfocus();
              widget.nextFocusNode?.requestFocus();
              widget.onEditingComplete?.call();
            },
            textInputAction: widget.textInputAction ?? TextInputAction.done,
            cursorColor: AppColors.azure,
            style: TextStyle(
              color: widget.textColor ??
                  (widget.enable ? AppColors.grey2F : AppColors.grey79),
              fontSize: 16,
              fontFamily: 'Montserrat',
            ),
            maxLength: widget.maxLength,
            maxLines: widget.maxLines ?? 1,
            maxLengthEnforcement: MaxLengthEnforcement.none,
            decoration: InputDecoration(
              counter: const Offstage(),
              contentPadding: widget.contentPadding ??
                  const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 12.0,
                  ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(widget.borderRadius),
                borderSide: BorderSide(
                  color: borderColor,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(widget.borderRadius),
                borderSide: BorderSide(
                  color: borderColor,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(widget.borderRadius),
                borderSide: BorderSide(
                  color: borderColor,
                ),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(widget.borderRadius),
                borderSide: const BorderSide(
                  color: Colors.transparent,
                ),
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(widget.borderRadius),
                borderSide: const BorderSide(
                  color: AppColors.terracota,
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
              focusColor: AppColors.white,
              hoverColor: AppColors.azure,
              hintStyle: const TextStyle(
                color: AppColors.grey79,
                fontSize: 14,
                fontFamily: 'Montserrat',
              ),
              hintText: widget.hintText,
              prefixIcon: widget.prefixIcon == null
                  ? null
                  : Padding(
                      padding: const EdgeInsets.only(
                        left: 19.0,
                        right: 15.0,
                      ),
                      child: widget.prefixIcon,
                    ),
              prefixIconConstraints: BoxConstraints(
                maxHeight: widget.prefixSize ?? 14.0,
                maxWidth: (widget.prefixSize ?? 14.0) + 19.0 + 15.0,
              ),
              suffixIcon: Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: widget.suffixIcon,
              ),
              suffixIconConstraints: BoxConstraints(
                maxHeight: widget.suffixSize ?? 14.0,
                maxWidth: (widget.suffixSize ?? 14.0) + 16.0,
              ),
            ),
          ),
        ),
        if (widget.errorText != null)
          Text(
            widget.errorText!,
            style: const TextStyle(
              color: AppColors.terracota,
              fontSize: 10,
              fontFamily: 'SFProText',
              fontWeight: FontWeight.w600,
            ),
          ),
      ],
    );
  }
}
