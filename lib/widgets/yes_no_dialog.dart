import 'package:flutter/material.dart';
import 'package:hepo_app/resources/colors.dart';

class YesNoDialog extends StatelessWidget {
  final String title;
  final String? message;
  final Widget? contentWidget;
  final String? leftActionText;
  final String? rightActionText;
  final VoidCallback? onLeftActionClick;
  final VoidCallback? onRightActionClick;
  final double? width;
  final double? buttonWidth;
  final EdgeInsets? padding;
  final bool hideLeftButton;

  const YesNoDialog({
    Key? key,
    required this.title,
    this.message,
    this.contentWidget,
    this.leftActionText,
    this.rightActionText,
    this.onLeftActionClick,
    this.onRightActionClick,
    this.width,
    this.buttonWidth,
    this.padding,
    this.hideLeftButton = false,
  }) : super(key: key);

  static void show(
    BuildContext context,
    String title, {
    String? message,
    String? leftActionText,
    String? rightActionText,
    VoidCallback? onLeftActionClick,
    VoidCallback? onRightActionClick,
    Color? barrierColor,
    bool? barrierDismissible,
    EdgeInsets? padding,
    bool? hideLeftButton,
    double? width,
    double? buttonWidth,
  }) {
    showDialog(
      context: context,
      barrierColor: barrierColor ?? Colors.transparent,
      barrierDismissible: barrierDismissible ?? true,
      builder: (_) => YesNoDialog(
        title: title,
        message: message,
        leftActionText: leftActionText,
        rightActionText: rightActionText,
        onLeftActionClick: onLeftActionClick,
        onRightActionClick: onRightActionClick,
        padding: padding,
        hideLeftButton: hideLeftButton ?? false,
        width: width,
        buttonWidth: buttonWidth,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          constraints: const BoxConstraints(
            maxWidth: 360.0,
          ),
          width: width ?? 230.0,
          padding: padding ??
              const EdgeInsets.symmetric(
                horizontal: 31.0,
                vertical: 24.0,
              ),
          decoration: BoxDecoration(
            color: AppColors.whiteSmoke,
            borderRadius: BorderRadius.circular(10),
            boxShadow: const <BoxShadow>[
              BoxShadow(
                color: AppColors.black16,
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                title,
                style: const TextStyle(
                  color: AppColors.black,
                  fontSize: 16,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 9.0),
              if (message != null) ...<Widget>[
                Text(
                  message ?? '',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: AppColors.grey79,
                    fontSize: 13,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(height: 9.0),
              ],
              if (contentWidget != null) ...<Widget>[
                contentWidget!,
              ],
              const SizedBox(height: 9.0),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  if (!hideLeftButton) ...<Widget>[
                    GestureDetector(
                      onTap: onLeftActionClick ?? () => Navigator.pop(context),
                      child: Container(
                        height: 30,
                        width: buttonWidth ?? 70.0,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: AppColors.muddyGreen,
                          ),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          leftActionText ?? '取消',
                          style: const TextStyle(
                            color: AppColors.muddyGreen,
                            fontSize: 13,
                            fontFamily: 'SFProText',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    const Spacer(),
                  ],
                  GestureDetector(
                    onTap: onRightActionClick ?? () => Navigator.pop(context),
                    child: Container(
                      height: 30.0,
                      width: buttonWidth ?? (hideLeftButton ? 90.0 : 70.0),
                      decoration: BoxDecoration(
                        color: AppColors.muddyGreen,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        rightActionText ?? '確定',
                        style: const TextStyle(
                          color: AppColors.white,
                          fontSize: 13,
                          fontFamily: 'SFProText',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
