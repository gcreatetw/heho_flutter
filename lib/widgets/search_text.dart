import 'package:flutter/material.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';

class SearchText extends StatelessWidget {
  final String text;
  final VoidCallback? onCancelClick;
  final VoidCallback? onTap;

  const SearchText(
    this.text, {
    Key? key,
    this.onTap,
    this.onCancelClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 6.0),
        child: Row(
          children: <Widget>[
            Image.asset(
              IconAssets.search2x,
              width: 24.0,
              height: 24.0,
            ),
            const SizedBox(width: 9.0),
            Expanded(
              child: Text(
                text,
                style: const TextStyle(
                  color: AppColors.grey75,
                  fontSize: 16,
                  fontFamily: 'HelveticaNeue',
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            InkWell(
              onTap: onCancelClick,
              child: SizedBox(
                width: 24.0,
                height: 24.0,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Image.asset(
                    IconAssets.cancel2x,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
