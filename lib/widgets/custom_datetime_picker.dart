import 'package:flutter/cupertino.dart';
import 'package:hepo_app/resources/colors.dart';

class CustomDatePicker extends StatefulWidget {
  final CupertinoDatePickerMode mode;
  final DateTime? initialDateTime;
  final DateTime? minimumDate;
  final DateTime? maximumDate;
  final void Function(DateTime)? onDateTimeChanged;
  final Future<bool> Function()? onWillPop;
  final void Function(DateTime)? onConfirmClick;

  const CustomDatePicker({
    Key? key,
    this.mode = CupertinoDatePickerMode.date,
    this.initialDateTime,
    this.minimumDate,
    this.maximumDate,
    this.onDateTimeChanged,
    this.onWillPop,
    this.onConfirmClick,
  }) : super(key: key);

  @override
  _CustomDatePickerState createState() => _CustomDatePickerState();
}

class _CustomDatePickerState extends State<CustomDatePicker> {
  DateTime current = DateTime.now();

  @override
  void initState() {
    if (widget.initialDateTime != null) {
      current = widget.initialDateTime!;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.onWillPop,
      child: Container(
        color: AppColors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                CupertinoButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    '取消',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
                const Spacer(),
                CupertinoButton(
                  onPressed: () {
                    widget.onConfirmClick?.call(current);
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    '確認',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 200.0,
              child: CupertinoDatePicker(
                initialDateTime: current,
                minimumDate: widget.minimumDate,
                maximumDate: widget.maximumDate,
                mode: widget.mode,
                onDateTimeChanged: (DateTime value) {
                  current = value;
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
