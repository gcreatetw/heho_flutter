import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';

import 'green_button.dart';

class SvgHintContent extends StatelessWidget {
  ///SVG 檔名
  final String assetName;

  ///訊息
  final String? message;

  ///訊息文字顏色
  final Color? messageTextColor;

  ///按鍵文字，若為 null 則不顯示按鈕
  final String? actionText;

  ///按鍵點擊事件
  final VoidCallback? onActionClick;

  ///最下方追加元件
  final List<Widget>? suffixes;

  const SvgHintContent(
    this.assetName, {
    Key? key,
    this.message,
    this.messageTextColor,
    this.actionText,
    this.onActionClick,
    this.suffixes,
  }) : super(key: key);

  static Widget sample() {
    return const SvgHintContent(
      ImageAssets.cryCancelLoveNoText,
      message: '感謝您的意見 ！\n再麻煩留意您的E-mail',
      actionText: '立即前往',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SvgPicture.asset(
          assetName,
        ),
        if (message != null) ...<Widget>[
          const SizedBox(height: 25.0),
          Text(
            message ?? '',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: messageTextColor ?? AppColors.grey33,
              fontFamily: 'HelveticaNeue',
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
        if (actionText != null) ...<Widget>[
          const SizedBox(height: 8.0),
          SizedBox(
            width: 140.0,
            child: GreenButton(
              text: actionText ?? '',
              onTap: onActionClick,
            ),
          ),
          const SizedBox(height: 12.0),
        ],
        ...suffixes ?? <Widget>[],
      ],
    );
  }
}
