import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';

class FakeTextField extends StatelessWidget {
  final String? icon;
  final String title;
  final Color? textColor;
  final Color? borderColor;
  final VoidCallback? onTap;
  final double? height;
  final double? radius;
  final EdgeInsets? padding;

  const FakeTextField({
    Key? key,
    required this.title,
    this.textColor,
    this.icon,
    this.onTap,
    this.height,
    this.padding,
    this.radius,
    this.borderColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height ?? 48.0,
        padding: const EdgeInsets.symmetric(
          vertical: 6.0,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(radius ?? 31),
          ),
          border: Border.all(
            color: borderColor ?? AppColors.greyC6,
          ),
        ),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                left: 12.0,
                right: 12.0,
              ),
              child: Image.asset(
                icon ?? IconAssets.search,
                color: AppColors.grey7F,
              ),
            ),
            AutoSizeText(
              title,
              maxFontSize: 14.0,
              style: TextStyle(
                color: textColor ?? AppColors.grey79,
                fontSize: 14.0,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
