import 'package:flutter/material.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';

class RoundTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode? focusNode;
  final List<String>? autofillHints;
  final bool? enabled;
  final bool? hasFocus;
  final String? hintText;
  final Function(String)? onChanged;
  final VoidCallback? onEditingComplete;
  final bool enableCancelButton;
  final VoidCallback? onCancelClick;
  final VoidCallback? onClearClick;
  final TextInputAction? textInputAction;
  final Color? focusColor;
  final Color? borderColor;

  const RoundTextField({
    Key? key,
    required this.controller,
    this.focusNode,
    this.autofillHints,
    this.enabled,
    this.hasFocus,
    this.hintText,
    this.onCancelClick,
    this.onChanged,
    this.onEditingComplete,
    this.textInputAction,
    this.enableCancelButton = true,
    this.focusColor,
    this.borderColor, this.onClearClick,
  }) : super(key: key);

  @override
  _RoundTextFieldState createState() => _RoundTextFieldState();
}

class _RoundTextFieldState extends State<RoundTextField> {
  FocusNode? focusNode;

  @override
  void initState() {
    focusNode = widget.focusNode;
    focusNode ??= FocusNode();
    focusNode?.addListener(onFocus);
    super.initState();
  }

  @override
  void dispose() {
    focusNode?.removeListener(onFocus);
    super.dispose();
  }

  void onFocus() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 32.0,
      child: Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              controller: widget.controller,
              cursorColor: AppColors.muddyGreen,
              focusNode: focusNode,
              autofillHints: widget.autofillHints,
              enabled: widget.enabled ?? true,
              onChanged: widget.onChanged ??
                  (String text) {
                    setState(() {});
                  },
              onEditingComplete: () {
                focusNode?.unfocus();
                widget.onEditingComplete?.call();
              },
              style: const TextStyle(
                fontSize: 14.0,
                color: AppColors.grey2F,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w600,
              ),
              textInputAction: widget.textInputAction ?? TextInputAction.search,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(28),
                  borderSide: BorderSide(
                    color: widget.focusColor ?? AppColors.black50,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(28),
                  borderSide: BorderSide(
                    color: widget.borderColor ?? AppColors.greyC6,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(28),
                  borderSide: BorderSide(
                    color: widget.focusColor ?? AppColors.black50,
                  ),
                ),
                filled: true,
                fillColor: AppColors.white,
                focusColor: widget.focusColor ?? AppColors.black50,
                hoverColor: widget.focusColor ?? AppColors.black50,
                prefixIconConstraints: const BoxConstraints(
                  maxHeight: 30.0,
                  maxWidth: 44.0,
                ),
                prefixIcon: Padding(
                  padding: const EdgeInsets.only(
                    left: 11.0,
                    right: 13.0,
                  ),
                  child: Image.asset(
                    IconAssets.search,
                    color: AppColors.grey7F,
                  ),
                ),
                suffixIcon: widget.controller.text.isEmpty
                    ? null
                    : IconButton(
                        onPressed: () {
                          setState(() {
                            widget.controller.text = '';
                          });
                          widget.onClearClick?.call();
                        },
                        icon: Image.asset(
                          IconAssets.clear,
                        ),
                      ),
                hintStyle: const TextStyle(
                  fontSize: 14.0,
                  color: AppColors.greyC6,
                  fontWeight: FontWeight.w600,
                ),
                hintText: widget.hintText,
              ),
            ),
          ),
          if ((focusNode?.hasFocus ?? false) && widget.enableCancelButton)
            InkWell(
              onTap: () {
                focusNode?.unfocus();
                widget.onCancelClick?.call();
              },
              child: const Padding(
                padding: EdgeInsets.only(
                  left: 15.0,
                  top: 4.0,
                  bottom: 4.0,
                ),
                child: Text(
                  '取消',
                  style: TextStyle(
                    fontSize: 14.0,
                    color: AppColors.muddyGreen,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
