import 'package:flutter/cupertino.dart';
import 'package:hepo_app/resources/colors.dart';

class CustomItemPicker extends StatefulWidget {
  final int? initialIndex;
  final int itemCount;
  final String Function(int) itemBuilder;
  final Future<bool> Function()? onWillPop;
  final void Function(int)? onConfirmClick;

  const CustomItemPicker({
    Key? key,
    this.initialIndex,
    required this.itemCount,
    required this.itemBuilder,
    this.onWillPop,
    this.onConfirmClick,
  }) : super(key: key);

  @override
  _CustomItemPickerState createState() => _CustomItemPickerState();
}

class _CustomItemPickerState extends State<CustomItemPicker> {
  int currentIndex = 0;

  @override
  void initState() {
    if (widget.initialIndex != null) {
      currentIndex = widget.initialIndex!;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.onWillPop,
      child: Container(
        color: AppColors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                CupertinoButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    '取消',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
                const Spacer(),
                CupertinoButton(
                  onPressed: () {
                    widget.onConfirmClick?.call(currentIndex);
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    '確認',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 200.0,
              child: CupertinoPicker(
                itemExtent: 40.0,
                scrollController: FixedExtentScrollController(
                  initialItem: currentIndex,
                ),
                onSelectedItemChanged: (int value) {
                  currentIndex = value;
                },
                children: <Widget>[
                  for (int i = 0; i < widget.itemCount; i++)
                    Center(
                      child: Text(widget.itemBuilder(i)),
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
