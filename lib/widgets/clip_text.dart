import 'package:flutter/material.dart';
import 'package:hepo_app/resources/colors.dart';

class ClipText extends StatelessWidget {
  final String text;
  final bool isActive;
  final double? textSize;
  final VoidCallback? onTap;

  const ClipText(
    this.text, {
    Key? key,
    this.isActive = false,
    this.onTap,
    this.textSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5.0,
          horizontal: 6.0,
        ),
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 11.0,
        ),
        decoration: BoxDecoration(
          color: isActive ? AppColors.muddyGreen : AppColors.black6,
          borderRadius: BorderRadius.circular(6),
        ),
        child: Text(
          text,
          style: TextStyle(
            color: isActive ? AppColors.white : AppColors.grey75,
            fontSize: textSize ?? 16,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }
}
