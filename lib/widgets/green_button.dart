import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/resources/colors.dart';

class GreenButton extends StatelessWidget {
  final String text;
  final VoidCallback? onTap;
  final EdgeInsets? padding;
  final double? radius;
  final double? fontSize;

  const GreenButton({
    Key? key,
    required this.text,
    required this.onTap,
    this.padding,
    this.radius,
    this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: padding ??
            const EdgeInsets.symmetric(
              horizontal: 32.0,
              vertical: 7.0,
            ),
        decoration: BoxDecoration(
          color: onTap == null ? AppColors.greyE9 : AppColors.muddyGreen,
          borderRadius: BorderRadius.all(
            Radius.circular(radius ?? 20.0),
          ),
          boxShadow: const <BoxShadow>[
            BoxShadow(
              color: Color(0x29c1c1c1),
              offset: Offset(0, 3),
              blurRadius: 10,
            ),
          ],
        ),
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxHeight: 26.0),
            child: AutoSizeText(
              text,
              minFontSize: 11.0,
              maxFontSize: 17.0,
              style: TextStyle(
                color: onTap == null ? AppColors.grey79 : Colors.white,
                fontSize: fontSize ?? 17,
                fontFamily: 'SFProText',
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
