import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_line_sdk/flutter_line_sdk.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/config/sdk_constants.dart';
import 'package:hepo_app/utils/firebase_message_utils.dart';
import 'package:hepo_app/utils/preferences.dart';
import 'package:hepo_app/utils/time_ago_custom_messages.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'app.dart';
import 'config/aes_constants.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Preferences.init(key: AesConstants.key, iv: AesConstants.iv);
  await Helper.instance.init();
  timeago.setLocaleMessages('zh-TW', CustomMessages());
  timeago.setLocaleMessages('en-US', timeago.EnMessages());
  MobileAds.instance.initialize();
  LineSDK.instance.setup(SdkConstants.lineChannelId);
  if (!kIsWeb && (Platform.isAndroid || Platform.isIOS)) {
    await Firebase.initializeApp();
    FirebaseMessagingUtils.handleBackground();
    if (kDebugMode) {
      await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(false);
      await FirebasePerformance.instance.setPerformanceCollectionEnabled(false);
    } else {
      FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    }
  }
  runApp(MyApp());
}
