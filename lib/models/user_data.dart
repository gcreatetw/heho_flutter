import 'package:flutter/widgets.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/heho_advertisement_response.dart';
import 'package:hepo_app/models/api/login_response.dart';
import 'package:hepo_app/models/general_callback.dart';
import 'package:hepo_app/models/general_response.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/utils/preferences.dart';
import 'package:intl/intl.dart';

import '../config/constants.dart';
import 'api/update_member_data_response.dart';

enum LoginType {
  normal,
  facebook,
  google,
  line,
  apple,
}

extension LoginTypeEx on LoginType {
  String get icon {
    switch (this) {
      case LoginType.facebook:
        return IconAssets.facebookWhite2x;
      case LoginType.google:
        return IconAssets.google2x;
      case LoginType.line:
        return IconAssets.lineWhite2x;
      case LoginType.apple:
        return IconAssets.apple2x;
      case LoginType.normal:
        return IconAssets.lineWhite2x;
    }
  }

  Color get backgroundColor {
    switch (this) {
      case LoginType.facebook:
        return AppColors.mutedBlue;
      case LoginType.google:
        return AppColors.white;
      case LoginType.line:
        return AppColors.leafyGreen;
      case LoginType.apple:
        return AppColors.black;
      case LoginType.normal:
        return AppColors.white;
    }
  }
}

enum Gender {
  notSet,
  male,
  female,
}

extension GenderEx on Gender {
  String get text {
    switch (this) {
      case Gender.notSet:
        return '未設定';
      case Gender.male:
        return '生理性別男';
      case Gender.female:
        return '生理性別女';
    }
  }
}

///2021/10/18 轉換性別為1、2、3
extension GenderNum on Gender {
  String get num {
    switch (this) {
      case Gender.notSet:
        return '3';
      case Gender.male:
        return '1';
      case Gender.female:
        return '2';
    }
  }
}

enum UpdateType {
  name,
  gender,
  birthday,
  phone,
  email,
  password,
}

extension UpdateTypeEx on UpdateType {
  String get text {
    switch (this) {
      case UpdateType.name:
        return 'name';
      case UpdateType.gender:
        return 'gender';
      case UpdateType.birthday:
        return 'birthday';
      case UpdateType.phone:
        return 'phone';
      case UpdateType.email:
        return 'email';
      case UpdateType.password:
        return 'password';
    }
  }
}

class UserData extends ChangeNotifier {
  bool isLogin = false;

  bool hasFollowingData = false;

  String name = '';
  String newName = '';
  Gender gender = Gender.notSet;
  Gender newGender = Gender.notSet;
  DateTime? birthday;
  DateTime? newBirthday;
  String cellphoneNumber = '';
  String newCellphoneNumber = '';
  String email = '';
  String newEmail = '';
  String newPassword = '';
  int status = 0;

  int days = 0;

  String advertisementUrl = '';
  String advertisementLinkUrl = '';

  UserData() {
    isLogin = Preferences.getBool(Constants.isLogin, false);
  }

  bool get hasChanged =>
      name != newName ||
      gender != newGender ||
      birthday != newBirthday ||
      cellphoneNumber != newCellphoneNumber ||
      email != newEmail ||
      newPassword.isNotEmpty;

  Future<void> fetchAdvertisement(BuildContext context) async {
    Helper.instance.getMemberAdvertisement(
      callback: GeneralCallback<HehoAdvertisementResponse>(
        onError: (GeneralResponse e) {
          // e.showToast(context);
        },
        onSuccess: (HehoAdvertisementResponse r) {
          advertisementUrl = r.data.img;
          advertisementLinkUrl = r.data.url;
          notifyListeners();
        },
      ),
    );
  }

  void _loadSample() {
    name = '王曉舟';
    newName = '王曉舟';
    days = 126;
    gender = Gender.female;
    newGender = Gender.female;
    birthday = DateTime(1996, 2, 14);
    newBirthday = DateTime(1996, 2, 14);
    cellphoneNumber = '+886932123123';
    newCellphoneNumber = '+886932123123';
    email = '';
    newEmail = '';
    hasFollowingData = true;
  }

  void logout() {
    isLogin = false;
    name = '';
    newName = '';
    days = 0;
    gender = Gender.female;
    cellphoneNumber = '';
    newCellphoneNumber = '';
    email = '';
    newEmail = '';
    notifyListeners();
  }

  Future<void> setName(String value) async {
    newName = value;
    notifyListeners();
  }

  void setBirthday(DateTime value) {
    newBirthday = value;
    notifyListeners();
  }

  void setGender(Gender value) {
    newGender = value;
    notifyListeners();
  }

  void setCellphoneNumber(String value) {
    newCellphoneNumber = value;
    notifyListeners();
  }

  void updateEmail(String value) {
    newEmail = value;
    notifyListeners();
  }

  void revertData() {
    newName = name;
    newGender = gender;
    newBirthday = birthday;
    newCellphoneNumber = cellphoneNumber;
    newEmail = email;
    newPassword = '';
    notifyListeners();
  }

  ///2021/10/15 修改傳入參數、response
  Future<void> updateData({
    required BuildContext context,
    required UpdateType updateType,
    String? code,
  }) async {
    String value;
    String message = '儲存成功';
    switch (updateType) {
      case UpdateType.name:
        value = newName;
        message = '姓名已更新';
        break;
      case UpdateType.gender:
        value = newGender.num;
        message = '性別已更新';
        break;
      case UpdateType.birthday:
        value = DateFormat('yyyy-MM-dd').format(newBirthday!);
        message = '生日已更新';
        break;
      case UpdateType.phone:
        value = newCellphoneNumber;
        message = '手機號碼已更新';
        break;
      case UpdateType.email:
        value = newEmail;
        break;
      case UpdateType.password:
        value = newPassword;
        message = '密碼已更新';
        break;
    }
    await Helper.instance.updateMemberData(
      updateValue: value,
      updateType: updateType,
      code: code,
      callback: GeneralCallback<UpdateMemberDataResponse>(
        onError: (GeneralResponse r) {
          CenterToast.show(context, r.message);

          ///更新失敗 退回舊值
          revertData();
        },
        onSuccess: (UpdateMemberDataResponse r) {
          ///2021/10/18 更新舊資料
          name = newName;
          gender = newGender;
          birthday = newBirthday;
          cellphoneNumber = newCellphoneNumber;
          email = newEmail;
          newPassword = '';
          if (updateType == UpdateType.email) {
            status = 1;
          }
          notifyListeners();
          CenterToast.show(context, message);
        },
      ),
    );
  }

  Future<void> updatePassword({
    required BuildContext context,
    required String code,
  }) async {
    Helper.instance.resetPassword(
      code: code,
      account: email,
      password: newPassword,
      callback: GeneralCallback<GeneralResponse>(
        onError: (GeneralResponse r) {
          CenterToast.show(context, r.message);
          newPassword = '';
        },
        onSuccess: (GeneralResponse r) {
          CenterToast.show(context, '密碼已更新');
          newPassword = '';
        },
      ),
    );
  }

  Future<void> fetchMemberData(BuildContext context) async {
    Helper.instance.getMemberData(
      callback: GeneralCallback<UpdateMemberDataResponse>(
          onError: (GeneralResponse r) {},
          onSuccess: (UpdateMemberDataResponse r) {
            final Data? data = r.data;
            name = data!.firstname;
            newName = name;
            email = data.email;
            newEmail = email;
            switch (data.gender) {
              case '1':
                gender = Gender.male;
                break;
              case '2':
                gender = Gender.female;
                break;
              case '3':
                gender = Gender.notSet;
                break;
            }
            newGender = gender;
            if (data.birthday != null) {
              if (data.birthday!.isNotEmpty) {
                birthday = DateFormat('yyyy-MM-dd').parse(data.birthday!);
                newBirthday = birthday;
              }
            }
            cellphoneNumber = data.telephone ?? '';
            newCellphoneNumber = cellphoneNumber;
            status = data.status;
            days = int.parse(data.regDays ?? '0');
            notifyListeners();
          }),
    );
  }

  ///2021/10/18 new = old 確保未修改時不會跳出已修改提示
  void loadFromApi(UserDataBean data) {
    name = data.firstname;
    newName = name;
    email = data.email ?? '';
    newEmail = email;
    switch (data.gender) {
      case '1':
        gender = Gender.male;
        break;
      case '2':
        gender = Gender.female;
        break;
      case '3':
        gender = Gender.notSet;
        break;
    }
    newGender = gender;
    if (data.birthday != null) {
      if (data.birthday!.isNotEmpty) {
        birthday = DateFormat('yyyy-MM-dd').parse(data.birthday!);
        newBirthday = birthday;
      }
    }
    cellphoneNumber = data.telephone ?? '';
    newCellphoneNumber = cellphoneNumber;
    status = data.status;
    days = int.parse(data.regDays ?? '0');
    notifyListeners();
  }
}
