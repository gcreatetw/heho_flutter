import 'package:flutter/cupertino.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/expert_data.dart';
import 'package:hepo_app/models/general_state.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:intl/intl.dart';

import 'api/article_response.dart';

const int _kDefaultPage = 1;
const int _kDefaultLimit = 10;
const int _kAdBetween = 8;

class ExpertNewsData extends ChangeNotifier {
  List<News> list;

  String? keyword;

  Expert? expert;

  String? hint;

  int page = _kDefaultPage;

  bool canLoadMore = false;

  bool loading = false;

  int limit = _kDefaultLimit;

  GeneralState state = GeneralState.loading;

  bool get hasResult => list.isNotEmpty;

  ExpertNewsData({
    required this.list,
    this.keyword,
  });

  factory ExpertNewsData.init() {
    return ExpertNewsData(
      list: <News>[],
    );
  }

  Future<void> fetch(
      {bool append = false,
      String? id,
      String? keyword,
      bool expert = true}) async {
    if (!append) {
      list.clear();
      page = _kDefaultPage;
      state = GeneralState.loading;
      hint = null;
      notifyListeners();
    } else if (canLoadMore) {
      page++;
      loading = true;
      notifyListeners();
      canLoadMore = false;
    } else {
      return;
    }
    if (expert) {
      Helper.instance.getExpertArticle(
        expertId: id!,
        page: page,
        limit: limit,
        callback: GeneralCallback<ArticleResponse>(
          onError: (GeneralResponse e) {
            hint = e.message;
            state = GeneralState.error;
            notifyListeners();
          },
          onSuccess: (ArticleResponse r) {
            final List<ArticleBean> dataList =
                r.data?.search ?? <ArticleBean>[];
            for (int i = 0; i < dataList.length; i++) {
              if (list.isNotEmpty && (list.length + 1) % _kAdBetween == 0) {
                list.add(
                  News.advertisement(),
                );
              }
              final News data = News(
                id: dataList[i].postId!,
                type: NewsType.normal,
                title: dataList[i].postTitle,
                imageUrl: dataList[i].imgUrl,
                dateTime: DateFormat('yyyy-MM-dd HH:mm:ss')
                    .parse(dataList[i].postDate),
                link: dataList[i].clickUrl,
                // isCollected: element.save == '1',
              );
              list.add(data);
            }
            canLoadMore = dataList.length == limit;
            state = GeneralState.finished;
            loading = false;
            notifyListeners();
          },
        ),
      );
    } else {
      Helper.instance.getHomeArticles(
        id: id!,
        page: page,
        limit: limit,
        callback: GeneralCallback<ArticleResponse>(
          onError: (GeneralResponse e) {
            hint = e.message;
            state = GeneralState.error;
            notifyListeners();
          },
          onSuccess: (ArticleResponse r) {
            final List<ArticleBean> dataList =
                r.data?.search ?? <ArticleBean>[];
            for (int i = 0; i < dataList.length; i++) {
              if (list.isNotEmpty && (list.length + 1) % _kAdBetween == 0) {
                list.add(
                  News.advertisement(),
                );
              }
              final News data = News(
                id: dataList[i].postId!,
                type: NewsType.normal,
                title: dataList[i].postTitle,
                imageUrl: dataList[i].imgUrl,
                dateTime: DateFormat('yyyy-MM-dd HH:mm:ss')
                    .parse(dataList[i].postDate),
                link: dataList[i].clickUrl,
                // isCollected: element.save == '1',
              );
              list.add(data);
            }
            canLoadMore = dataList.length == limit;
            state = GeneralState.finished;
            loading = false;
            notifyListeners();
          },
        ),
      );
    }
  }
}
