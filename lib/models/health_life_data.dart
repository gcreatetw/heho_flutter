import 'package:flutter/material.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/health_life_type_response.dart';
import 'package:hepo_app/pages/home/health_life_page.dart';

class HealthLifeData extends ChangeNotifier {
  final List<HealthLifeTypeData> list;

  int currentIndex = 0;

  HealthLifeTypeData get current => list[currentIndex];

  HealthLifeData({
    required this.list,
  });

  factory HealthLifeData.init() {
    return HealthLifeData(
      list: <HealthLifeTypeData>[],
    );
  }

  Future<void> fetch(BuildContext context, VoidCallback onSuccess) async {
    Helper.instance.getHealthLifeType(
      callback: GeneralCallback<HealthLifeTypeResponse>(
        onError: (GeneralResponse e) {
          e.showToast(context);
        },
        onSuccess: (HealthLifeTypeResponse r) {
          final List<HealthLifeTypeDataBean> data = r.data;
          list.clear();
          for (int i = 0; i < data.length; i++) {
            if (data[i].isDefault == 'Y') {
              currentIndex = i;
            }
            HealthLifeType type;
            switch (data[i].cat_id) {
              case '24365':
                type = HealthLifeType.food;
                break;
              case '36678':
                type = HealthLifeType.sleep;
                break;
              case '19665':
                type = HealthLifeType.chineseMedicine;
                break;
              case '8':
                type = HealthLifeType.sport;
                break;
              default:
                type = HealthLifeType.food;
                break;
            }
            list.add(
              HealthLifeTypeData(
                id: data[i].cat_id,
                name: data[i].name,
                subCatName: data[i].subCatName,
                type: type,
              ),
            );
          }
          onSuccess.call();
          notifyListeners();
        },
      ),
    );
  }
}

class HealthLifeTypeData {
  final String id;
  final String name;
  final List<String> subCatName;
  final HealthLifeType type;

  HealthLifeTypeData({
    required this.id,
    required this.name,
    required this.subCatName,
    required this.type,
  });
}
