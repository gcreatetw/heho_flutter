import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/action_type.dart';
import 'package:hepo_app/models/api/collection_type.dart';
import 'package:hepo_app/models/collection_data.dart';
import 'package:hepo_app/pages/collect/collection_disease_page.dart';
import 'package:hepo_app/pages/collect/collection_symptom_page.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/toast.dart';
import 'package:provider/provider.dart';

import 'general_callback.dart';

class DiseaseData extends ChangeNotifier {
  String name;
  String subTitle;
  String typeName;
  String introduction;
  bool isCollected;

  DiseaseData({
    required this.name,
    required this.subTitle,
    required this.typeName,
    required this.introduction,
    required this.isCollected,
  });

  factory DiseaseData.sample1() {
    return DiseaseData(
      name: '新冠肺炎',
      typeName: '疾病',
      subTitle: '疾病簡介',
      introduction: '這是一種新興傳染病，'
          'WHO（世界衛生組織）'
          '於2020 年 2 月將其命名為 COVID-19。',
      isCollected: false,
    );
  }

  factory DiseaseData.sample2() {
    return DiseaseData(
      name: '頭痛',
      typeName: '症狀',
      subTitle: '可能疾病',
      introduction:
          '把頭痛分成主要兩大類，第一類是原發性頭痛，亦即沒有其他致病因的頭痛，包括偏頭痛、緊縮型頭痛、叢發性頭痛與其他三叉自律神經頭痛、及其他原發性頭痛；第二類是續發於其他疾病造成的頭痛，如歸因於頭部及頸部外傷之頭痛、歸因於顱部或頸部血管疾患之頭痛、歸因於非血管顱內疾患之頭痛、歸因於物質或物質戒斷之頭痛、歸因於感染之頭痛、歸因於體內失恆之頭痛、歸因於頭顱顏面等結構之頭痛、及歸因於精神疾患之頭痛。把頭痛分成主要兩大類，第一類是原發性頭痛，亦即沒有其他致病因的頭痛，包括偏頭痛、緊縮型頭痛、叢發性頭痛與其他三叉自律神經頭痛、及其他原發性頭痛；把頭痛分成主要兩大類，第一類是原發性頭痛，亦即沒有其他致病因的頭痛，包括偏頭痛、緊縮型頭痛、叢發性頭痛與其他三叉自律神經頭痛、及其他原發性頭痛；把頭痛分成主要兩大類，第一類是原發性頭痛，亦即沒有其他致病因的頭痛，包括偏頭痛、緊縮型頭痛、叢發性頭痛與其他三叉自律神經頭痛、及其他原發性頭痛；包括偏頭痛、緊縮型頭痛、叢發性頭痛與其他三叉自律神經頭痛、及其他原發性頭痛；包括偏頭痛、緊縮型頭痛、叢發性頭痛與其他三叉自律神經頭痛、及其他原發性頭痛；包括偏頭痛、緊縮型頭痛、叢發性頭痛與其他三叉自律神經頭痛、及其他原發性頭痛；包括偏頭痛、緊縮型頭痛、叢發性頭痛與其他三叉自律神經頭痛、及其他原發性頭痛；',
      isCollected: false,
    );
  }

  void addToCollection(BuildContext context) {
    CollectionType type = CollectionType.disease;
    if (typeName == '疾病') {
      type = CollectionType.disease;
    } else if (typeName == '症狀') {
      type = CollectionType.symptom;
    }
    Helper.instance.operateMemberCollection(
      value: name,
      type: type,
      action: ActionType.add,
      callback: GeneralCallback<GeneralResponse>(
          onError: (GeneralResponse e) => e.showToast(context),
          onSuccess: (GeneralResponse r) {
            context.read<CollectionData>().fetch();
            notifyListeners();
            Toast.show(
              context,
              '已添加至我的收藏',
              prefixIcon: IconAssets.collect,
              actionText: '查看項目',
              onActionClick: () {
                final CollectionData data = CollectionData.init();
                data.fetch();
                Navigator.of(context).push(
                  MaterialPageRoute<void>(
                    builder: (_) {
                      switch (typeName) {
                        case '疾病':
                          return ChangeNotifierProvider<CollectionData>.value(
                            value: data,
                            child: Consumer<CollectionData>(
                              builder: (_, CollectionData data, __) =>
                                  CollectionDiseasePage(
                                data: data.diseases,
                              ),
                            ),
                          );
                        case '症狀':
                        default:
                          return ChangeNotifierProvider<CollectionData>.value(
                            value: data,
                            child: Consumer<CollectionData>(
                              builder: (_, CollectionData data, __) =>
                                  CollectionSymptomPage(
                                data: data.symptoms,
                              ),
                            ),
                          );
                      }
                    },
                  ),
                );
              },
            );
          }),
    );
  }

  void removeFromCollection(BuildContext context) {
    CollectionType type = CollectionType.disease;
    if (typeName == '疾病') {
      type = CollectionType.disease;
    } else if (typeName == '症狀') {
      type = CollectionType.symptom;
    }
    Helper.instance.operateMemberCollection(
      type: type,
      action: ActionType.delete,
      value: name,
      callback: GeneralCallback<GeneralResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (GeneralResponse r) {
          context.read<CollectionData>().fetch();
          notifyListeners();
          Toast.show(
            context,
            '已取消收藏',
            prefixIcon: IconAssets.collect,
          );
        },
      ),
    );
  }

  void changeCollectState(BuildContext context) {
    if (isCollected) {
      removeFromCollection(context);
    } else {
      addToCollection(context);
    }
  }
}
