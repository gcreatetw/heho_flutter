import 'dart:async';

import 'package:flutter/widgets.dart';

const int _kDefaultSeconds = 60;

class CounterData extends ChangeNotifier {
  int seconds = _kDefaultSeconds;

  Timer? timer;

  bool get canReSend {
    return seconds == 0;
  }

  void start() {
    seconds = _kDefaultSeconds;
    notifyListeners();
    timer = Timer.periodic(
      const Duration(seconds: 1),
      (Timer timer) {
        seconds--;
        notifyListeners();
        if (seconds == 0) timer.cancel();
      },
    );
  }
}
