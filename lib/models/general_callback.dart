import 'general_response.dart';

export 'general_response.dart';

typedef GeneralResponseCallback = void Function(GeneralResponse e);

class GeneralCallback<T> {
  final GeneralResponseCallback onError;
  final void Function(T r) onSuccess;

  GeneralCallback({
    required this.onError,
    required this.onSuccess,
  });
}
