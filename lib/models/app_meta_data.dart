import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/config/constants.dart';
import 'package:hepo_app/models/api/external_link_response.dart';
import 'package:hepo_app/models/api/meta_data_response.dart';
import 'package:hepo_app/utils/preferences.dart';
import 'package:json_annotation/json_annotation.dart';

part 'app_meta_data.g.dart';

const String facebookUrlId = '1';
const String lineUrlId = '2';
const String youtubeUrlId = '3';
const String instagramUrlId = '4';
const String memberHopeStoreUrlId = '5';
const String hopeStoreUrlId = '6';
const String healthHelperUrlId = '7';
const String medicineSearchUrlId = '8';
const String humanBodyMapUrlId = '9';
const String privacyPolicyUrlId = '10';
const String servicePolicyUrlId = '11';
const String orderFormatUrlUrlId = '12';
const String followingUrlId = '13';
const String lineFriendUrlId = '14';

@JsonSerializable()
class AppMetaData extends ChangeNotifier {
  String? humanBodyMapUrl;
  String? medicineSearchUrl;
  String? healthHelperUrl;
  String? memberHopeStoreUrl;
  String? hopeStoreUrl;

  String? facebookUrl;
  String? lineUrl;
  String? youtubeUrl;
  String? instagramUrl;

  String? servicePolicyUrl;
  String? privacyPolicyUrl;

  String? orderFormatUrl;
  String? followingListUrl;

  String? lineFriendUrl;

  String? newsShareEndText;

  String? kidsUrl;

  AppMetaData({
    this.humanBodyMapUrl,
    this.medicineSearchUrl,
    this.healthHelperUrl,
    this.memberHopeStoreUrl,
    this.hopeStoreUrl,
    this.facebookUrl,
    this.lineUrl,
    this.youtubeUrl,
    this.instagramUrl,
    this.servicePolicyUrl,
    this.privacyPolicyUrl,
    this.orderFormatUrl,
    this.followingListUrl,
    this.lineFriendUrl,
  });

  Future<void> fetch() async {
    loadFromCache();
    Helper.instance.getExternalLink(
      callback: GeneralCallback<ExternalLinkResponse>(
        onError: (_) => notifyListeners(),
        onSuccess: (ExternalLinkResponse r) {
          final List<DataBean> list = r.data;
          facebookUrl = list
              .firstWhere((DataBean element) => element.id == facebookUrlId)
              .link;
          lineUrl = list
              .firstWhere((DataBean element) => element.id == lineUrlId)
              .link;
          youtubeUrl = list
              .firstWhere((DataBean element) => element.id == youtubeUrlId)
              .link;
          instagramUrl = list
              .firstWhere((DataBean element) => element.id == instagramUrlId)
              .link;
          memberHopeStoreUrl = list
              .firstWhere(
                  (DataBean element) => element.id == memberHopeStoreUrlId)
              .link;
          hopeStoreUrl = list
              .firstWhere((DataBean element) => element.id == hopeStoreUrlId)
              .link;
          healthHelperUrl = list
              .firstWhere((DataBean element) => element.id == healthHelperUrlId)
              .link;
          medicineSearchUrl = list
              .firstWhere(
                  (DataBean element) => element.id == medicineSearchUrlId)
              .link;
          humanBodyMapUrl = list
              .firstWhere((DataBean element) => element.id == humanBodyMapUrlId)
              .link;
          privacyPolicyUrl = list
              .firstWhere(
                  (DataBean element) => element.id == privacyPolicyUrlId)
              .link;
          servicePolicyUrl = list
              .firstWhere(
                  (DataBean element) => element.id == servicePolicyUrlId)
              .link;
          orderFormatUrl = list
              .firstWhere(
                  (DataBean element) => element.id == orderFormatUrlUrlId)
              .link;
          followingListUrl = list
              .firstWhere((DataBean element) => element.id == followingUrlId)
              .link;
          lineFriendUrl = list
              .firstWhere((DataBean element) => element.id == lineFriendUrlId)
              .link;
          notifyListeners();
          save();
        },
      ),
    );
    Helper.instance.getMetaConfig(
      callback: GeneralCallback<MetaDataResponse>(
        onError: (_) => notifyListeners(),
        onSuccess: (MetaDataResponse r) {
          newsShareEndText = r.shareTitle;
          notifyListeners();
          save();
        },
      ),
    );
  }

  void loadFromCache() {
    final AppMetaData data = AppMetaData.load();
    kidsUrl = data.kidsUrl ?? 'https://kids.heho.com.tw/';
    humanBodyMapUrl =
        data.humanBodyMapUrl ?? 'https://tools.heho.com.tw/bodymap/';
    medicineSearchUrl = data.medicineSearchUrl ??
        'https://tools.heho.com.tw/search-medicine.php';
    healthHelperUrl = data.healthHelperUrl ?? 'https://tools.heho.com.tw/';
    memberHopeStoreUrl = data.memberHopeStoreUrl ?? 'https://myhope.com.tw/';
    hopeStoreUrl = data.hopeStoreUrl ?? 'https://myhope.com.tw/';
    facebookUrl = data.facebookUrl ?? 'https://www.facebook.com/ilikeheho';
    lineUrl = data.lineUrl ?? 'https://page.line.me/lsx5958e';
    youtubeUrl = data.youtubeUrl ??
        'https://www.youtube.com/channel/UCFo2ocxRNZodXSRePresa_Q';
    instagramUrl = data.instagramUrl ?? 'https://www.instagram.com/ilikeheho/';
    servicePolicyUrl =
        data.servicePolicyUrl ?? 'https://heho.com.tw/app-privacy';
    privacyPolicyUrl = data.privacyPolicyUrl ?? 'https://heho.com.tw/app-privacy';
    orderFormatUrl = data.orderFormatUrl ??
        'https://myhope.com.tw/index.php?route=account/order/info&order_id=';
    followingListUrl = data.followingListUrl ??
        'https://myhope.com.tw/index.php?route=account/wishlist';
    lineFriendUrl =
        data.lineFriendUrl ?? 'https://page.line.me/lsx5958e?openQrModal=true';
    newsShareEndText = data.newsShareEndText ?? '';
  }

  factory AppMetaData.load() {
    final String rawJson = Preferences.getString(
      Constants.appMetaData,
      '{}',
    );
    return AppMetaData.fromRawJson(rawJson);
  }

  Future<void> save() async {
    await Preferences.setString(
      Constants.appMetaData,
      toRawJson(),
    );
  }

  factory AppMetaData.fromJson(Map<String, dynamic> json) =>
      _$AppMetaDataFromJson(json);

  Map<String, dynamic> toJson() => _$AppMetaDataToJson(this);

  factory AppMetaData.fromRawJson(String str) => AppMetaData.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
