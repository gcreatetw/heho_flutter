import 'package:flutter/cupertino.dart';

const String _allTypeText = '全部類別';
const String _allDepartmentText = '全部科別';

class FilterData extends ChangeNotifier {
  List<FilterItem> items;

  List<String> get values {
    return items
        .map<String>(
          (FilterItem e) => e.name,
        )
        .toList();
  }

  List<String> get activeValues {
    return items
        .where(
          (FilterItem element) =>
              element.isActive &&
              element.name != _allTypeText &&
              element.name != _allDepartmentText,
        )
        .map<String>(
          (FilterItem e) => e.name,
        )
        .toList();
  }

  FilterData({
    required this.items,
  });

  factory FilterData.initDepartment() {
    return FilterData(
      items: <FilterItem>[
        FilterItem(name: _allDepartmentText),
      ],
    );
  }

  factory FilterData.initHospitalType() {
    return FilterData(
      items: <FilterItem>[
        FilterItem(name: _allTypeText),
      ],
    );
  }

  void _loadDepartmentSample() {
    items.addAll(
      <FilterItem>[
        FilterItem(name: '耳鼻喉科'),
        FilterItem(name: '小兒科'),
        FilterItem(name: '婦產科'),
        FilterItem(name: '身心科'),
        FilterItem(name: '中醫'),
        FilterItem(name: '眼科'),
        FilterItem(name: '皮膚科'),
        FilterItem(name: '內科'),
        FilterItem(name: '家醫科'),
        FilterItem(name: '其他'),
      ],
    );
  }

  void _loadHospitalTypeSample() {
    items.addAll(
      <FilterItem>[
        FilterItem(name: '醫學中心'),
        FilterItem(name: '區域醫院'),
        FilterItem(name: '地區醫院'),
        FilterItem(name: '診所'),
      ],
    );
  }

  //ignore: avoid_positional_boolean_parameters
  void setActive(int index, bool isActive) {
    if (index == 0) {
      if (isActive) {
        for (int i = 0; i < items.length; i++) {
          items[i].isActive = true;
        }
      }
    } else {
      items[index].isActive = isActive;
      items[0].isActive = true;
      for (int i = 1; i < items.length; i++) {
        if (!items[i].isActive) {
          items[0].isActive = false;
        }
      }
    }
    notifyListeners();
  }
}

class FilterItem {
  String id;
  String name;
  bool isActive;

  FilterItem({
    this.id = '',
    required this.name,
    this.isActive = true,
  });
}
