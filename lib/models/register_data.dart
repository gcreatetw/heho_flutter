import 'package:flutter/cupertino.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/user_hobby_response.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/utils/utils.dart';

class RegisterData extends ChangeNotifier {
  ///Step 1

  int phoneAreaCodeIndex = 0;


  final TextEditingController cellPhoneNumber = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController passwordConfirm = TextEditingController();

  String? emailErrorText;
  String? passwordErrorText;
  String? passwordConfirmErrorText;
  String? regCode;
  String? cellPhoneNumberConverted;

  ///Step 2

  Gender gender = Gender.female;

  DateTime? birthday;

  final TextEditingController name = TextEditingController();

  bool confirmPrivacy = false;

  ///Step 3

  List<InterestedCategory> categoryList = <InterestedCategory>[];

  String hobby = '';

  bool _isSending = false;

  bool get step1Confirm {
    return (isPhoneNumberValidate || isEmailValidate) &&
        isPasswordValidate &&
        isPasswordConfirmValidate &&
        password.text == passwordConfirm.text &&
        !isSending;
  }

  bool get isPhoneNumberValidate {
    return cellPhoneNumber.text.isNotEmpty && cellPhoneNumber.text.length == 10;
  }

  bool get isEmailValidate {
    return email.text.isNotEmpty && Utils.isValidEmail(email.text);
  }

  bool get isPasswordValidate {
    return password.text.isNotEmpty && Utils.isValidPassword(password.text);
  }

  bool get isPasswordConfirmValidate {
    return passwordConfirm.text.isNotEmpty;
  }

  bool get step2Confirm {
    return name.text.isNotEmpty && confirmPrivacy;
  }

  bool get step3Confirm {
    int count = 0;
    for (final InterestedCategory value in categoryList) {
      if (value.isCheck) {
        count++;
      }
    }
    return count >= 3 && !isSending;
  }

  bool checkStep1() {
    if (email.text.isNotEmpty) {
      emailErrorText = isEmailValidate ? null : '電子信箱格式錯誤';
    }
    passwordErrorText = isPasswordValidate ? null : '請輸入 8-20 字元包含英文及數字';
    passwordConfirmErrorText =
        isPasswordConfirmValidate ? null : '密碼不相同，請再確認一次';
    notifyListeners();
    if (step1Confirm) {
      return true;
    } else {
      return false;
    }
  }

  bool checkStep2() {
    notifyListeners();
    return step2Confirm;
  }

  bool checkStep3() {
    notifyListeners();
    return step3Confirm;
  }

  bool get isSending {
    return _isSending;
  }

  set isSending(bool value) {
    _isSending = value;
    notifyListeners();
  }

  void clearEmailErrorText() {
    emailErrorText = null;
    notifyListeners();
  }

  void clearPasswordErrorText() {
    passwordErrorText = null;
    notifyListeners();
  }

  void clearPasswordConfirmErrorText() {
    passwordConfirmErrorText = null;
    notifyListeners();
  }

  void setBirthday(DateTime datetime) {
    birthday = datetime;
    notifyListeners();
  }

  void updateCategoryList(int index) {
    categoryList[index].isCheck = !categoryList[index].isCheck;
    hobby = '';
    for (int i = 0; i < categoryList.length; i++) {
      if (categoryList[i].isCheck) {
        hobby += ',${categoryList[i].id}';
      }
    }
    if (hobby.isNotEmpty) {
      hobby = hobby.substring(1, hobby.length);
    }
    notifyListeners();
  }


  Future<void> fetchHobby(BuildContext context) async {
    Helper.instance.getUserHobby(
      callback: GeneralCallback<UserHobbyResponse>(
        onError: (GeneralResponse e) {
          CenterToast.show(context, e.message);
        },
        onSuccess: (UserHobbyResponse r) {
          final List<UserHobbyDataBean> data = r.data;
          categoryList.clear();
          for (final UserHobbyDataBean element in data) {
            categoryList.add(
              InterestedCategory(
                  id: element.id,
                  title: element.name,
                  imagePath: hobbyTransfer(element.id)),
            );
          }
          notifyListeners();
        },
      ),
    );
  }

  String hobbyTransfer(String id) {
    switch (id) {
      case '2':
        return 'assets/images/register_category_2@2x.png';
      case '3':
        return 'assets/images/register_category_3@2x.png';
      case '4':
        return 'assets/images/register_category_4@2x.png';
      case '5':
        return 'assets/images/register_category_5@2x.png';
      case '6':
        return 'assets/images/register_category_6@2x.png';
      case '7':
        return 'assets/images/register_category_7@2x.png';
      case '8':
        return 'assets/images/register_category_8@2x.png';
      case '9':
        return 'assets/images/register_category_9@2x.png';
      case '10':
        return 'assets/images/register_category_10@2x.png';
      case '11':
        return 'assets/images/register_category_11@2x.png';
      default:
        return 'assets/images/register_category_8@2x.png';
    }
  }
}

class InterestedCategory {
  String id;
  bool isCheck;
  String title;
  String imagePath;

  InterestedCategory({
    required this.id,
    required this.title,
    this.isCheck = false,
    this.imagePath = 'assets/images/register_category_8@2x.png',
  });
}
