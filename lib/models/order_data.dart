import 'package:flutter/widgets.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/order_list_response.dart';
import 'package:hepo_app/models/general_state.dart';
import 'package:intl/intl.dart';

class OrderData extends ChangeNotifier {
  GeneralState state = GeneralState.loading;

  String? hint;

  List<Order> list;

  OrderData({
    required this.list,
  });

  factory OrderData.init() {
    return OrderData(
      list: <Order>[],
    );
  }

  Future<void> fetch() async {
    state = GeneralState.loading;
    hint = null;
    notifyListeners();
    await Future<void>.delayed(const Duration(milliseconds: 600));
    Helper.instance.getOrderList(
      callback: GeneralCallback<OrderListResponse>(
        onError: (GeneralResponse e) {
          hint = e.message;
          state = GeneralState.error;
          notifyListeners();
        },
        onSuccess: (OrderListResponse r) {
          for (final OrderDataBean item in r.data ?? <OrderDataBean>[]) {
            list.add(
              Order(
                id: item.order_id,
                serialNumber: '# ${item.order_id}',
                date: DateFormat('yyyy-MM-dd').parse(item.date_added),
                productCounts: item.quantity,
                stateMessage: item.order_status,
                total: item.total,
              ),
            );
          }
          state = GeneralState.finished;
          notifyListeners();
        },
      ),
    );
  }
}

class Order {
  String id;
  String serialNumber;
  DateTime date;
  String productCounts;
  String stateMessage;
  String total;

  Order({
    required this.id,
    required this.serialNumber,
    required this.date,
    required this.productCounts,
    required this.stateMessage,
    required this.total,
  });

  static List<Order> sampleList() {
    return <Order>[
      Order(
        id: '1',
        serialNumber: '# 1',
        date: DateTime(2021, 02, 26),
        productCounts: '2',
        stateMessage: '待出貨',
        total: '838',
      ),
      Order(
        id: '2',
        serialNumber: '# 12',
        date: DateTime(2021, 01, 28),
        productCounts: '2',
        stateMessage: '已完成',
        total: '752',
      ),
      Order(
        id: '3',
        serialNumber: '# 8',
        date: DateTime(2021, 01, 03),
        productCounts: '1',
        stateMessage: '已完成',
        total: '560',
      ),
    ];
  }
}
