import 'package:flutter/widgets.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/expert_data_response.dart';
import 'package:hepo_app/models/api/heho_advertisement_response.dart';
import 'package:hepo_app/models/expert_category_data.dart';

class ExpertData extends ChangeNotifier {
  int currentIndex = 0;

  List<Expert> items;

  String advertisementUrl = '';
  String advertisementLinkUrl = '';

  ExpertCategoryData categories = ExpertCategoryData.init();

  ExpertData({
    required this.items,
  });

  factory ExpertData.init() {
    return ExpertData(
      items: <Expert>[],
    );
  }

  void fetchAdvertisement(BuildContext context) {
    Helper.instance.getAdvertisement(
      callback: GeneralCallback<HehoAdvertisementResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (HehoAdvertisementResponse r) {
          advertisementUrl = r.data.img;
          advertisementLinkUrl = r.data.url;
          notifyListeners();
        },
      ),
    );
  }

  void _loadSample() {
    items.addAll(
      <Expert>[
        Expert(
          id: '1',
          name: '吳宜庭 Melody',
          title: 'Heho營養師',
          description: '兒童營養、肌少症、疾病營養、毒理食安',
          avatarUrl:
              'https://img.heho.com.tw/wp-content/uploads/2021/03/1615531502.5791.png',
        ),
        Expert(
          id: '2',
          name: '威廉氏後人 李毅評醫師',
          title: '婦產科',
          description: '不孕症、試管嬰兒',
          avatarUrl:
              'https://img.heho.com.tw/wp-content/uploads/2021/03/1615531502.5791.png',
        ),
        Expert(
          id: '3',
          name: '吳宜庭 Melody',
          title: 'Heho營養師',
          description: '兒童營養、肌少症、疾病營養、毒理食安',
          avatarUrl:
              'https://img.heho.com.tw/wp-content/uploads/2021/03/1615531502.5791.png',
        ),
      ],
    );
  }

  void updateIndex(int index) {
    currentIndex = index;
    notifyListeners();
  }

  Future<void> fetch() async {
    items.clear();
    categories.items.clear();
    Helper.instance.getExpertData(
      callback: GeneralCallback<ExpertDataResponse>(
        onError: (GeneralResponse e) {},
        onSuccess: (ExpertDataResponse r) {
          for (final ExpertBean element in r.data.expert) {
            items.add(
              Expert(
                id: element.wpExpertId,
                name: element.expertName,
                title: element.jobDesc,
                avatarUrl: element.imageUrl,
                description: element.expertise,
                btn: element.btn,
                btnUrl: element.btnUrl,
              ),
            );
          }
          categories.loadFromApi(r.data.category);
          notifyListeners();
          categories.notifyListeners();
        },
      ),
    );
  }
}

class Expert {
  String id;
  String name;
  String title;
  String description;
  String avatarUrl;
  String? btn;
  String? btnUrl;

  Expert({
    required this.id,
    required this.name,
    required this.title,
    required this.description,
    required this.avatarUrl,
    this.btn,
    this.btnUrl
  });
}
