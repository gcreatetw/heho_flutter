import 'dart:convert';

import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/config/constants.dart';
import 'package:hepo_app/models/api/phone_area_code_response.dart';
import 'package:hepo_app/utils/preferences.dart';

class PhoneAreaCode {
  String title = '';
  String code = '';

  PhoneAreaCode({
    required this.title,
    required this.code,
  });

  static List<PhoneAreaCode> generateList() {
    return <PhoneAreaCode>[
      PhoneAreaCode(
        title: '台灣(+886)',
        code: '+886',
      ),
      PhoneAreaCode(
        title: '中國(+86)',
        code: '+86',
      ),
      PhoneAreaCode(
        title: '泰國(+66)',
        code: '+66',
      ),
      PhoneAreaCode(
        title: '馬來西亞(+60)',
        code: '+60',
      ),
    ];
  }

  @override
  String toString() {
    return '{code: $code, title: $title}';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = code;
    data['name'] = title;
    return data;
  }

  PhoneAreaCode.fromJson(Map<String, dynamic> json) {
    code = json['id'].toString();
    title = json['name'].toString();
  }
}

class PhoneAreaCodeData {
  List<PhoneAreaCode> phoneCodeData = <PhoneAreaCode>[];

  PhoneAreaCodeData({
    required this.phoneCodeData,
  });

  factory PhoneAreaCodeData.init() {
    return PhoneAreaCodeData(
      phoneCodeData: <PhoneAreaCode>[],
    );
  }

  Future<void> fetch() async {
    Helper.instance.getPhoneAreaCode(
      callback: GeneralCallback<PhoneAreaCodeResponse>(
        onError: (GeneralResponse e) {},
        onSuccess: (PhoneAreaCodeResponse r) {
          phoneCodeData.clear();
          final List<PhoneAreaCodeDataBean> data = r.data;
          for (final PhoneAreaCodeDataBean element in data) {
            phoneCodeData.add(
              PhoneAreaCode(
                title: element.name,
                code: element.id,
              ),
            );
          }
          save();
        },
      ),
    );
  }

  Future<void> save() async {
    await Preferences.setString(
      Constants.phoneAreaCode,
      toRawJson(),
    );
  }

  factory PhoneAreaCodeData.load() {
    final String json = Preferences.getString(
      Constants.phoneAreaCode,
      '{}',
    );
    final PhoneAreaCodeData phoneAreaCodeData =
        PhoneAreaCodeData.fromJson(jsonDecode(json) as Map<String, dynamic>);
    final List<PhoneAreaCode> convertData = <PhoneAreaCode>[];
    for (final PhoneAreaCode element in phoneAreaCodeData.phoneCodeData) {
      convertData.add(
        PhoneAreaCode(
          title: '${element.title}(${element.code})',
          code: element.code,
        ),
      );
    }
    return PhoneAreaCodeData(phoneCodeData: convertData);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['data'] = phoneCodeData.map((PhoneAreaCode v) => v.toJson()).toList();
    return data;
  }

  PhoneAreaCodeData.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      phoneCodeData = <PhoneAreaCode>[];
      json['data'].forEach(
        (dynamic v) {
          phoneCodeData.add(PhoneAreaCode.fromJson(v as Map<String, dynamic>));
        },
      );
    }
  }

  String toRawJson() => jsonEncode(toJson());
}
