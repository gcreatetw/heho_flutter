import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hepo_app/api/helper.dart';

import 'api/weather_info_response.dart';
import 'general_callback.dart';

class WeatherData extends ChangeNotifier {
  String headImageUrl;
  String areaTitle;
  String iconUrl;
  String temperature;
  String ultravioletRays;
  String probabilityOfPrecipitation;
  String airQuality;
  String subTitle;
  String subMessage;
  int locationLoadingTime = 0;
  int weatherLoadingTime = 0;

  WeatherData({
    required this.headImageUrl,
    required this.areaTitle,
    required this.iconUrl,
    required this.temperature,
    required this.ultravioletRays,
    required this.probabilityOfPrecipitation,
    required this.airQuality,
    required this.subTitle,
    required this.subMessage,
  });

  factory WeatherData.init() {
    return WeatherData(
      temperature: '0',
      headImageUrl: '',
      areaTitle: '',
      iconUrl: '',
      ultravioletRays: '',
      probabilityOfPrecipitation: '',
      airQuality: '',
      subTitle: '',
      subMessage: '',
    );
  }

  void loadSample() {
    temperature = '25.5';
    headImageUrl =
        'https://img.heho.com.tw/wp-content/uploads/2021/07/1626143786.6601.png';
    areaTitle = '台北市 大安區';
    iconUrl =
        'https://img.heho.com.tw/wp-content/uploads/2021/08/1628558830.3858.png';
    ultravioletRays = '中量級 4.43';
    probabilityOfPrecipitation = '大安區 20%';
    airQuality = 'AQI= 1 品質= 良好';
    subTitle = '明日白天';
    subMessage = '大安區 明日最高氣溫: 30℃ 降雨機率: 20%';
  }

  void setLocationTime(int time){
    locationLoadingTime = time;
    notifyListeners();
  }

  void setWeatherTime(int time){
    weatherLoadingTime = time;
    notifyListeners();
  }

  void reset(){
    temperature = '0';
    headImageUrl =
    '';
    areaTitle = '';
    iconUrl =
    '';
    ultravioletRays = '';
    probabilityOfPrecipitation = '';
    airQuality = '';
    subTitle = '';
    subMessage = '';
    notifyListeners();
  }

  Future<void> fetch(
    BuildContext context, {
    double? latitude,
    double? longitude,
  }) async {
    reset();
    int tick = 0;
    final Timer time = Timer.periodic(const Duration(milliseconds: 1), (Timer timer) {
      tick ++;
    });
    await Helper.instance.getWeatherInfo(
      latitude: latitude,
      longitude: longitude,
      callback: GeneralCallback<WeatherInfoResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (WeatherInfoResponse r) {
          time.cancel();
          weatherLoadingTime = tick;

          final DataBean data = r.data;
          final TodayBean todayWeather = r.data.weather.today;
          final TomorrowBean tomorrowWeather = r.data.weather.tomorrow;
          temperature = data.weather.today.temp;
          headImageUrl = data.healthCalendar.url;
          areaTitle = '${todayWeather.locationName} '
              '${todayWeather.locationTownName}';

          ///2021/10/13 取得現在時間 並獲取對應圖片地址
          final int now = DateTime.now().hour;
          final List<String> wxIdDiff = <String>[
            '1',
            '2',
            '3',
            '8',
            '19',
            '21',
            '24',
            '25',
            '26'
          ];
          if (wxIdDiff.contains(todayWeather.wxId)) {
            if (now > 6 && now < 18) {
              iconUrl = 'assets/weather/${todayWeather.wxId}.png';
            } else {
              iconUrl = 'assets/weather/night/n_${todayWeather.wxId}.png';
            }
          } else {
            iconUrl = 'assets/weather/${todayWeather.wxId}.png';
          }
          ultravioletRays = '${todayWeather.uviDesc} ${todayWeather.uvi}';
          probabilityOfPrecipitation = '${todayWeather.locationTownName} '
              '${todayWeather.rainP}%';
          airQuality = 'AQI= ${todayWeather.aqi} 品質= ${todayWeather.aqiDesc}';
          subTitle = '明日${tomorrowWeather.wxName}';
          subMessage = '${tomorrowWeather.locationTownName} '
              '明日最高氣溫: ${tomorrowWeather.temp}℃ '
              '降雨機率: ${tomorrowWeather.rainP}%';
          notifyListeners();
        },
      ),
    );
    time.cancel();
  }
}
