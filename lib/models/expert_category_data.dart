import 'package:flutter/cupertino.dart';
import 'package:hepo_app/models/api/expert_data_response.dart';

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final StringBuffer buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(
      int.parse(buffer.toString(), radix: 16),
    );
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

class ExpertCategoryData extends ChangeNotifier {
  List<ExpertCategory> items;

  ExpertCategoryData({
    required this.items,
  });

  factory ExpertCategoryData.init() {
    return ExpertCategoryData(
      items: <ExpertCategory>[],
    );
  }

  void _loadSample() {
    items = <ExpertCategory>[
      ExpertCategory(
        id: '1',
        name: '醫生說',
        color: const Color(0xFF566F31),
      ),
      ExpertCategory(
        id: '2',
        name: '牙醫說',
        color: const Color(0xFF566F31),
      ),
      ExpertCategory(
        id: '3',
        name: '中醫師說',
        color: const Color(0xFFAAD68D),
      ),
      ExpertCategory(
        id: '4',
        name: '藥師說',
        color: const Color(0xFFAAD68D),
      ),
      ExpertCategory(
        id: '5',
        name: '營養師說',
        color: const Color(0xFFAAD68D),
      ),
      ExpertCategory(
        id: '6',
        name: '治療師說',
        color: const Color(0xFFAAD68D),
      ),
      ExpertCategory(
        id: '7',
        name: '護理師說',
        color: const Color(0xFF5B9794),
      ),
      ExpertCategory(
        id: '8',
        name: '心理師說',
        color: const Color(0xFF5B9794),
      ),
      ExpertCategory(
        id: '9',
        name: '護理師說',
        color: const Color(0xFF86C7C5),
      ),
      ExpertCategory(
        id: '1',
        name: '心理師說',
        color: const Color(0xFF86C7C5),
      ),
    ];
  }

  Future<void> fetch() async {
    await Future<void>.delayed(const Duration(milliseconds: 300));
    _loadSample();
    notifyListeners();
  }

  void loadFromApi(List<CategoryBean> categories) {
    for (final CategoryBean element in categories) {
      items.add(
        ExpertCategory(
          id: element.categoryId,
          name: element.categoryName,
          color: element.fontColor != null && element.fontColor!.isNotEmpty
              ? HexColor.fromHex(
                  element.fontColor!,
                )
              : const Color(0xFF566F31),
        ),
      );
    }
  }
}

class ExpertCategory {
  String id;
  String name;
  Color? color;

  ExpertCategory({
    required this.id,
    required this.name,
    this.color,
  });
}
