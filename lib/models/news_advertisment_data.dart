import 'package:flutter/material.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/heho_advertisement_response.dart';

class NewsAdvertisementData extends ChangeNotifier {
  String url;
  String img;

  NewsAdvertisementData({
    required this.url,
    required this.img,
  });

  factory NewsAdvertisementData.init() {
    return NewsAdvertisementData(
      url: '',
      img: '',
    );
  }

  Future<void> fetchAdvertisement() async {
    Helper.instance.getNewsContentAdvertisement(
      callback: GeneralCallback<HehoAdvertisementResponse>(
        onError: (GeneralResponse e) {},
        onSuccess: (HehoAdvertisementResponse r) {
          final HehoAdvertisementBean data = r.data;
          url = data.url;
          img = data.img;
          notifyListeners();
        },
      ),
    );
  }
}
