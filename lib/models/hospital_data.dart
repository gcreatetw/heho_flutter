import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/action_type.dart';
import 'package:hepo_app/models/api/collection_type.dart';
import 'package:hepo_app/models/api/hospital_content_response.dart';
import 'package:hepo_app/models/api/hospital_list_response.dart';
import 'package:hepo_app/models/collection_data.dart';
import 'package:hepo_app/models/general_state.dart';
import 'package:hepo_app/pages/collect/collection_hospital_page.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/toast.dart';
import 'package:provider/provider.dart';

import 'general_callback.dart';
import 'general_response.dart';

const int _kDefaultPage = 1;
const int _kDefaultLimit = 10;

enum HospitalType {
  clinic,
  medicalCenter,
  regionalHospital,
  localHospital,
}

extension HospitalTypeEX on HospitalType {
  String get text {
    switch (this) {
      case HospitalType.clinic:
        return '特約診所';
      case HospitalType.medicalCenter:
        return '醫院中心';
      case HospitalType.regionalHospital:
        return '區域醫院';
      case HospitalType.localHospital:
        return '地區醫院';
    }
  }
}

class HospitalData extends ChangeNotifier {
  List<Hospital> items;

  GeneralState state = GeneralState.loading;

  int page = _kDefaultPage;

  int limit = _kDefaultLimit;

  String? hint;

  bool canLoadMore = false;

  HospitalData({
    required this.items,
  });

  factory HospitalData.init() {
    return HospitalData(
      items: <Hospital>[],
    );
  }

  void _loadSample() {
    items.addAll(
      <Hospital>[
        Hospital(
          id: '1',
          name: '瑞光眼科診所',
          departments: <String>['眼科'],
          type: '特約診所',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
        Hospital(
          id: '1',
          name: '瑞光婦產科診所',
          departments: <String>['婦產科'],
          type: '特約診所',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
        Hospital(
          id: '1',
          name: '晨間婦產科診所',
          departments: <String>['婦產科'],
          type: '特約診所',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
        Hospital(
          id: '1',
          name: '養樂多中醫診所',
          departments: <String>['中醫'],
          type: '特約診所',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
        Hospital(
          id: '1',
          name: '瑞光醫院',
          departments: <String>['眼科', '婦產科'],
          type: '地區醫院',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
      ],
    );
  }

  Future<void> fetch({
    bool append = false,
    String? areaName,
    double? latitude,
    double? longitude,
    List<String>? types,
    List<String>? departments,
    String? keyword,
  }) async {
    if (!append) {
      items.clear();
      page = _kDefaultPage;
      state = GeneralState.loading;
      hint = null;
      notifyListeners();
    } else if (canLoadMore) {
      page++;
    } else {
      return;
    }
    if (kDebugMode) {
      debugPrint(
        'fetch home current page = $page data, canLoadMore = $canLoadMore',
      );
      debugPrint('type data = ${types?.toList().toString()}');
      debugPrint('departments data = ${departments?.toList().toString()}');
    }
    Helper.instance.getHospitalList(
      page: page,
      limit: limit,
      latitude: latitude,
      longitude: longitude,
      types: types,
      departments: departments,
      countries: <String>[if (areaName != null) areaName],
      keyword: keyword,
      callback: GeneralCallback<HospitalListResponse>(
        onError: (GeneralResponse e) {
          state = GeneralState.error;
          hint = e.message;
          notifyListeners();
        },
        onSuccess: (HospitalListResponse r) {
          for (final HospitalDataBean element
              in r.data ?? <HospitalDataBean>[]) {
            items.add(
              Hospital(
                id: element.id,
                name: element.name,
                distance: element.dist,
                address: element.address,
                phoneNumber: element.tel,
              ),
            );
          }
          canLoadMore = r.data?.length == limit;
          if (items.isEmpty) {
            state = GeneralState.error;
            hint = '查無結果';
          } else {
            state = GeneralState.finished;
          }
          notifyListeners();
        },
      ),
    );
  }
}

class Hospital extends ChangeNotifier {
  String id;
  String name;
  String? type;
  List<String>? departments;
  String distance;
  String address;
  String phoneNumber;
  String? description;
  String? link;

  List<TimeCode>? timeCodes;

  Hospital({
    required this.distance,
    required this.name,
    required this.id,
    required this.address,
    required this.phoneNumber,
    this.departments,
    this.type,
    this.description,
    this.timeCodes,
  });

  bool isCollected(BuildContext context) {
    final CollectionData data = context.read<CollectionData>();
    return data.isHospitalCollection(this);
  }

  Future<void> fetchDetail(BuildContext context) async {
    Helper.instance.getHospitalContent(
      id: id,
      callback: GeneralCallback<HospitalContentResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (HospitalContentResponse r) {
          final DataBean dataBean = r.data;
          description = dataBean.service;
          link = dataBean.reservationLink;
          type = dataBean.category;
          timeCodes = <TimeCode>[
            TimeCode('上午'),
            TimeCode('下午'),
            TimeCode('晚上'),
          ];
          for (final int weekday in <int>[
            DateTime.monday,
            DateTime.tuesday,
            DateTime.wednesday,
            DateTime.thursday,
            DateTime.friday,
            DateTime.saturday,
            DateTime.sunday,
          ]) {
            late WeekdayBean weekdayBean;
            switch (weekday) {
              case DateTime.sunday:
                weekdayBean = dataBean.open.sun;
                break;
              case DateTime.monday:
                weekdayBean = dataBean.open.mon;
                break;
              case DateTime.tuesday:
                weekdayBean = dataBean.open.tues;
                break;
              case DateTime.wednesday:
                weekdayBean = dataBean.open.wed;
                break;
              case DateTime.thursday:
                weekdayBean = dataBean.open.thurs;
                break;
              case DateTime.friday:
                weekdayBean = dataBean.open.fri;
                break;
              case DateTime.saturday:
                weekdayBean = dataBean.open.sat;
                break;
            }
            timeCodes![0].serviceAvailable[weekday] =
                weekdayBean.morning == 'Y';
            timeCodes![1].serviceAvailable[weekday] =
                weekdayBean.afternoon == 'Y';
            timeCodes![2].serviceAvailable[weekday] = weekdayBean.night == 'Y';
            notifyListeners();
          }
        },
      ),
    );
  }

  void _loadSample() {
    description = '門診診療、兒童預防保健、成人預防保健、婦女子宮頸抹片檢查、孕婦產檢、定量免疫法糞便潛血檢查';
    timeCodes = <TimeCode>[
      TimeCode('上午'),
      TimeCode('下午')..serviceAvailable[DateTime.saturday] = false,
      TimeCode('晚上')..serviceAvailable[DateTime.saturday] = false,
    ];
    link = 'https://www.hospital.fju.edu.tw/OPDSchedule';
  }

  void addToCollection(BuildContext context) {
    final GeneralCallback<GeneralResponse> callback =
        GeneralCallback<GeneralResponse>(
      onError: (GeneralResponse e) => e.showToast(context),
      onSuccess: (GeneralResponse r) {
        context.read<CollectionData>().fetch();
        notifyListeners();
        Toast.show(
          context,
          '已添加至我的收藏',
          prefixIcon: IconAssets.collect,
          actionText: '查看項目',
          onActionClick: () {
            final CollectionData data = CollectionData.init();
            data.fetch();
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (_) => ChangeNotifierProvider<CollectionData>.value(
                  value: data,
                  child: Consumer<CollectionData>(
                    builder: (_, CollectionData data, __) {
                      return CollectionHospitalPage(
                        data: data.hospitals,
                      );
                    },
                  ),
                ),
              ),
            );
          },
        );
      },
    );
    Helper.instance.operateMemberCollection(
      type: CollectionType.hospital,
      action: ActionType.add,
      value: id,
      callback: callback,
    );
  }

  void removeFromCollection(BuildContext context) {
    final GeneralCallback<GeneralResponse> callback =
        GeneralCallback<GeneralResponse>(
      onError: (GeneralResponse e) => e.showToast(context),
      onSuccess: (GeneralResponse r) {
        context.read<CollectionData>().fetch();
        notifyListeners();
        Toast.show(
          context,
          '已取消收藏',
          prefixIcon: IconAssets.collect,
        );
      },
    );

    Helper.instance.operateMemberCollection(
      type: CollectionType.hospital,
      action: ActionType.delete,
      value: id,
      callback: callback,
    );
  }

  void changeCollectState(BuildContext context) {
    if (isCollected(context)) {
      removeFromCollection(context);
    } else {
      addToCollection(context);
    }
  }
}

class TimeCode {
  String name;

  Map<int, bool> serviceAvailable = <int, bool>{
    DateTime.monday: true,
    DateTime.tuesday: true,
    DateTime.wednesday: true,
    DateTime.thursday: true,
    DateTime.friday: true,
    DateTime.saturday: true,
    DateTime.sunday: false,
  };

  TimeCode(this.name);
}
