import 'package:flutter/cupertino.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/recommend_text_response.dart';

class TextBannerData extends ChangeNotifier {
  List<TextBanner> items;

  TextBannerData({
    required this.items,
  });

  factory TextBannerData.init() {
    return TextBannerData(
      items: <TextBanner>[],
    );
  }

  void _loadSample() {
    items.addAll(
      <TextBanner>[
        TextBanner(message: '高血糖壞處多！5個降低血糖的天然方法'),
        TextBanner(message: '高血糖壞處多！6個降低血糖的天然方法'),
        TextBanner(message: '高血糖壞處多！7個降低血糖的天然方法'),
        TextBanner(message: '高血糖壞處多！8個降低血糖的天然方法'),
      ],
    );
  }

  Future<void> fetch() async {
    Helper.instance.getRecommendText(
      callback: GeneralCallback<RecommendTextResponse>(
        onError: (_) {},
        onSuccess: (RecommendTextResponse r) {
          items.clear();
          for (final RecommendTextBean item in r.data) {
            items.add(
              TextBanner(
                message: item.title,
                link: item.url,
              ),
            );
          }
          notifyListeners();
        },
      ),
    );
  }
}

class TextBanner {
  String message;
  String? link;

  TextBanner({
    required this.message,
    this.link,
  });
}
