import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/home_category_response.dart';
import 'package:hepo_app/models/api/media_health_categories_response.dart';
import 'package:hepo_app/models/api/symptom_category_response.dart';
import 'package:package_info_plus/package_info_plus.dart';

class CategoryData extends ChangeNotifier {
  List<Category> items;

  int currentIndex = 0;

  Category get current => items[currentIndex];

  bool get isHotType => current.name == '熱門';

  CategoryData({
    required this.items,
  });

  factory CategoryData.init() {
    return CategoryData(
      items: <Category>[],
    );
  }

  void loadSample() {
    items.addAll(
      <Category>[
        Category.sample1(
          name: '最新',
        ),
        Category.sample2(
          name: '熱門',
        ),
        Category.sample1(
          name: '健康百科',
        ),
        Category.sample1(
          name: '請問專家',
        ),
        Category.sample1(
          name: '醫學專區',
        ),
        Category.sample2(
          name: '圖解健康',
        ),
        Category.sample1(
          name: '癌症百科',
        ),
      ],
    );
  }

  void loadSymptomSample() {
    items = <Category>[
      Category.symptomSample(
        name: '耳鼻喉科',
      ),
      Category.symptomSample(
        name: '內外科',
      ),
      Category.symptomSample(
        name: '皮膚科',
      ),
      Category.symptomSample(
        name: '其他',
      ),
    ];
  }

  void _loadMediaHealthSample() {
    items = <Category>[
      Category.mediaHealth(name: '最新'),
      Category.mediaHealth(name: '熱門'),
      Category.mediaHealth(name: 'Heho 健康突擊隊'),
      Category.mediaHealth(name: 'Heho Yoga'),
      Category.mediaHealth(name: 'Heho 專家說'),
      Category.mediaHealth(name: '蔬菜營養師'),
      Category.mediaHealth(name: '營養 talk talk'),
      Category.mediaHealth(name: '健康小撇步'),
      Category.mediaHealth(name: '一天一健康'),
    ];
  }

  void setIndex(int index) {
    currentIndex = index;
    notifyListeners();
  }

  Future<void> fetchHome({
    required VoidCallback onSuccess,
  }) async {
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final String nowVersion = packageInfo.version;
    Helper.instance.getHomeCategories(
      version: nowVersion,
      callback: GeneralCallback<HomeCategoryResponse>(
        onError: (GeneralResponse e) {},
        onSuccess: (HomeCategoryResponse r) {
          int index = 0;
          for (final DataBean element in r.data) {
            final Category category = Category(
              id: element.paperid,
              name: element.name,
              iconStyle: element.iconStyle,
              iconCode: element.iconCode,
              items: <SubCategory>[],
              advType: element.advType,
              advValue: element.advValue,
            );
            if (element.defaultData != null &&
                element.defaultData!.isNotEmpty) {
              currentIndex = index;
            }
            for (final SubDataBean subElement
                in element.sub ?? <SubDataBean>[]) {
              final SubCategory subCategory = SubCategory(
                id: subElement.paperid,
                name: subElement.name,
                linkUrl: subElement.external,
                advType: subElement.advType,
                advValue: subElement.advValue,
              );
              category.items.add(subCategory);
            }
            items.add(category);
            index++;
          }
          onSuccess.call();
          notifyListeners();
        },
      ),
    );
  }

  Future<void> fetchSymptom(BuildContext context) async {
    Helper.instance.getSymptomCategory(
      callback: GeneralCallback<SymptomCategoryResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (SymptomCategoryResponse r) {
          final List<SymptomCategoryDataBean> data = r.data;
          for (final SymptomCategoryDataBean element in data) {
            final String mainCategory = element.main_category;
            final List<SubCategory> subCategory = <SubCategory>[];
            for (final String element2 in element.sub_category) {
              subCategory.add(SubCategory(id: '1', name: element2));
            }
            items
                .add(Category(id: '1', name: mainCategory, items: subCategory));
          }
          notifyListeners();
        },
      ),
    );
  }

  Future<void> fetchMediaHealth({
    required BuildContext context,
    required VoidCallback onSuccess,
  }) async {
    Helper.instance.getMediaHealthCategories(
      callback: GeneralCallback<MediaHealthCategoriesResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (MediaHealthCategoriesResponse r) {
          final List<MediaHealthCategoryBean> data = r.data;
          int index = 0;
          for (final MediaHealthCategoryBean element in data) {
            if (element.defaultData != null &&
                element.defaultData!.isNotEmpty) {
              currentIndex = index;
            }
            items.add(
              Category(
                id: element.paperid,
                name: element.name,
                items: <SubCategory>[],
              ),
            );
            index++;
          }
          notifyListeners();
          onSuccess.call();
        },
      ),
    );
  }
}

class Category {
  String id;
  String name;
  String? iconStyle;
  String? iconCode;
  List<SubCategory> items;
  String? advType;
  String? advValue;

  IconData? get icon {
    if (iconStyle == null || iconCode == null) {
      return null;
    } else {
      final int hexCode = int.parse(iconCode!, radix: 16);
      switch (iconStyle) {
        case 'regular':
          return IconDataRegular(hexCode);
        case 'solid':
          return IconDataSolid(hexCode);
        case 'brands':
          return IconDataBrands(hexCode);
      }
    }
  }

  Category({
    required this.id,
    required this.name,
    required this.items,
    this.iconStyle,
    this.iconCode,
    this.advType,
    this.advValue,
  });

  factory Category.sample1({required String name}) {
    return Category(
      id: '1',
      name: name,
      iconCode: 'f1ea',
      iconStyle: 'solid',
      items: <SubCategory>[
        SubCategory(id: '1', name: '科研新知'),
        SubCategory(id: '1', name: '用藥安全'),
        SubCategory(id: '1', name: '營養衛教'),
        SubCategory(id: '1', name: '中醫養生'),
        SubCategory(id: '1', name: '運動健身'),
        SubCategory(id: '1', name: '心理健康'),
        SubCategory(id: '1', name: '育兒親子'),
        SubCategory(id: '1', name: '罕見疾病'),
        SubCategory(id: '1', name: '醫學故事'),
        SubCategory(id: '1', name: '傳染疾病'),
      ],
    );
  }

  factory Category.sample2({required String name}) {
    return Category(
      id: '1',
      name: name,
      iconCode: 'f1ea',
      iconStyle: 'solid',
      items: <SubCategory>[
        SubCategory(id: '1', name: '科研新知'),
        SubCategory(id: '1', name: '用藥安全'),
        SubCategory(id: '1', name: '營養衛教'),
        SubCategory(id: '1', name: '罕見疾病'),
        SubCategory(id: '1', name: '醫學故事'),
        SubCategory(id: '1', name: '傳染疾病'),
      ],
    );
  }

  factory Category.symptomSample({required String name}) {
    return Category(
      id: '1',
      name: name,
      items: <SubCategory>[
        SubCategory(id: '1', name: '流鼻血'),
        SubCategory(id: '1', name: '鼻水'),
        SubCategory(id: '1', name: '鼻塞'),
        SubCategory(id: '1', name: '乾眼'),
        SubCategory(id: '1', name: '耳鳴'),
        SubCategory(id: '1', name: '口臭'),
        SubCategory(id: '1', name: '吞嚥困難'),
        SubCategory(id: '1', name: '頭痛'),
      ],
    );
  }

  factory Category.mediaHealth({required String name}) {
    return Category(
      id: '1',
      name: name,
      items: <SubCategory>[],
    );
  }
}

class SubCategory {
  String id;
  String name;
  String? linkUrl;
  String? advType;
  String? advValue;

  SubCategory({
    required this.id,
    required this.name,
    this.linkUrl,
    this.advType,
    this.advValue,
  });
}
