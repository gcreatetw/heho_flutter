import 'dart:math';

enum AdvertisementType {
  fullScreen,
  square,
  rectangle,
}

class AdvertisementData {
  AdvertisementType type;
  String imageUrl;
  String? linkUrl;

  bool get linkAvailable => linkUrl != null && linkUrl!.isNotEmpty;

  AdvertisementData({
    required this.type,
    required this.imageUrl,
    this.linkUrl,
  });

  factory AdvertisementData.sample() {
    final int index = Random().nextInt(500) % 3;
    switch (index) {
      case 1:
        return AdvertisementData.sample1();
      case 2:
        return AdvertisementData.sample2();
      case 3:
      default:
        return AdvertisementData.sample3();
    }
  }

  factory AdvertisementData.sample1() {
    return AdvertisementData(
      type: AdvertisementType.fullScreen,
      imageUrl:
          'https://img.heho.com.tw/wp-content/uploads/2021/08/1628472128.1606.png',
      linkUrl: 'https://heho.com.tw/',
    );
  }

  factory AdvertisementData.sample2() {
    return AdvertisementData(
      type: AdvertisementType.square,
      imageUrl:
          'https://img.heho.com.tw/wp-content/uploads/2021/08/1628472134.318.png',
      linkUrl: 'https://heho.com.tw/',
    );
  }

  factory AdvertisementData.sample3() {
    return AdvertisementData(
      type: AdvertisementType.rectangle,
      imageUrl:
          'https://img.heho.com.tw/wp-content/uploads/2021/08/1628472139.2975.png',
      linkUrl: 'https://heho.com.tw/',
    );
  }
}
