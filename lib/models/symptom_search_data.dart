import 'package:flutter/cupertino.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/get_hot_disease_response.dart';
import 'package:hepo_app/models/api/heho_advertisement_response.dart';
import 'package:hepo_app/models/api/recommend_article_response.dart';

import 'news_data.dart';

class SymptomSearchData extends ChangeNotifier {
  List<String> hotKeyWords;

  String advertisementUrl;
  String advertisementLinkUrl;

  List<News> recommendNews;

  SymptomSearchData({
    required this.hotKeyWords,
    required this.advertisementUrl,
    required this.advertisementLinkUrl,
    required this.recommendNews,
  });

  factory SymptomSearchData.init() {
    return SymptomSearchData(
      hotKeyWords: <String>[],
      advertisementUrl: '',
      advertisementLinkUrl: '',
      recommendNews: <News>[],
    );
  }

  void loadSample() {
    hotKeyWords = <String>[
      '腸胃炎',
      '糖尿病',
      '腸胃炎',
      '糖尿病',
      '腸胃炎',
      '糖尿病',
      '腸胃炎',
      '糖尿病',
      '腸胃炎',
      '糖尿病',
      '腸胃炎',
      '糖尿病',
    ];
    advertisementUrl =
        'https://img.heho.com.tw/wp-content/uploads/2021/06/1622713458.1349.png';
    advertisementLinkUrl =
        'https://myhope.com.tw/product/625/?utm_source=heho&utm_medium=endbanner&utm_campaign=hopextimewell&utm_content=0603mobile';
    recommendNews = News.sampleList(hasBig: false);
  }

  Future<void> fetch(BuildContext context) async {
    Helper.instance.getHotDisease(
      callback: GeneralCallback<GetHotDiseaseResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (GetHotDiseaseResponse r) {
          hotKeyWords = r.data;
          notifyListeners();
        },
      ),
    );
    Helper.instance.getSymptomAdvertisement(
      callback: GeneralCallback<HehoAdvertisementResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (HehoAdvertisementResponse r) {
          advertisementUrl = r.data.img;
          advertisementLinkUrl = r.data.url;
          notifyListeners();
        },
      ),
    );
    Helper.instance.getRecommendArticle(
      callback: GeneralCallback<RecommendArticleResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (RecommendArticleResponse r) {
          recommendNews.clear();
          final List<RecommendArticleDataBean> data = r.data;
          for (final RecommendArticleDataBean element in data) {
            recommendNews.add(
              News(
                id: element.postId,
                type: NewsType.normal,
                imageUrl: element.img,
                title: element.title,
                link: element.url,
                dateTime: DateTime.parse(element.date),
              ),
            );
          }
          notifyListeners();
        },
      ),
    );
  }
}
