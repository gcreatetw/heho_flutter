import 'package:flutter/cupertino.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:location/location.dart';

class UserLocation extends ChangeNotifier {
  Location location = Location();

  double? lat;
  double? lng;

  UserLocation({
    this.lat,
    this.lng,
  });

  Future<void> fetch() async {
    final PermissionStatus permission = await location.hasPermission();
    if (permission == PermissionStatus.granted ||
        permission == PermissionStatus.grantedLimited) {
      location.changeSettings(accuracy: LocationAccuracy.low);
      final LocationData position = await location.getLocation();
      lat = position.latitude;
      lng = position.longitude;
      notifyListeners();
    }
  }

  Future<bool> requestPermission() async {
    final bool serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      return false;
    }
    PermissionStatus permission = await location.hasPermission();
    if (permission == PermissionStatus.denied) {
      permission = await location.requestPermission();
    }
    if (permission == PermissionStatus.granted ||
        permission == PermissionStatus.grantedLimited) {
      return true;
    }
    return false;
  }

  double? getDistance(double lat, double lng) {
    if (this.lat != null && this.lng != null) {
      return Utils.measure(this.lat!, this.lng!, lat, lng);
    } else {
      return null;
    }
  }
}
