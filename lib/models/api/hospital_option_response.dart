import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'hospital_option_response.g.dart';

@JsonSerializable()
class HospitalOptionResponse {
  String message;
  String code;
  DataBean data;

  HospitalOptionResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory HospitalOptionResponse.fromJson(Map<String, dynamic> json) =>
      _$HospitalOptionResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HospitalOptionResponseToJson(this);

  factory HospitalOptionResponse.fromRawJson(String str) =>
      HospitalOptionResponse.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class DataBean {
  List<String> north;
  List<String> south;
  List<String> mid;
  List<String> others;
  List<String> category;
  Map<String, ServiceBean> service;

  DataBean({
    required this.north,
    required this.south,
    required this.mid,
    required this.others,
    required this.category,
    required this.service,
  });

  factory DataBean.fromJson(Map<String, dynamic> json) =>
      _$DataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$DataBeanToJson(this);
}

@JsonSerializable()
class ServiceBean {
  String name;
  List<String> item;

  ServiceBean({required this.name, required this.item});

  factory ServiceBean.fromJson(Map<String, dynamic> json) =>
      _$ServiceBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ServiceBeanToJson(this);

  factory ServiceBean.fromRawJson(String str) => ServiceBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
