// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hospital_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HospitalListResponse _$HospitalListResponseFromJson(
        Map<String, dynamic> json) =>
    HospitalListResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => HospitalDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$HospitalListResponseToJson(
        HospitalListResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

HospitalDataBean _$HospitalDataBeanFromJson(Map<String, dynamic> json) =>
    HospitalDataBean(
      id: json['id'] as String,
      name: json['name'] as String,
      address: json['address'] as String,
      tel: json['tel'] as String,
      dist: json['dist'] as String,
    );

Map<String, dynamic> _$HospitalDataBeanToJson(HospitalDataBean instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'address': instance.address,
      'tel': instance.tel,
      'dist': instance.dist,
    };
