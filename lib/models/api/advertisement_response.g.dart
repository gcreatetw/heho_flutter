// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'advertisement_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AdvertisementResponse _$AdvertisementResponseFromJson(
        Map<String, dynamic> json) =>
    AdvertisementResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => AdvertisementBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$AdvertisementResponseToJson(
        AdvertisementResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

AdvertisementBean _$AdvertisementBeanFromJson(Map<String, dynamic> json) =>
    AdvertisementBean(
      imageUrl: json['imageUrl'] as String,
      clickUrl: json['clickUrl'] as String,
      splashType: json['splashType'] as String,
    );

Map<String, dynamic> _$AdvertisementBeanToJson(AdvertisementBean instance) =>
    <String, dynamic>{
      'imageUrl': instance.imageUrl,
      'clickUrl': instance.clickUrl,
      'splashType': instance.splashType,
    };
