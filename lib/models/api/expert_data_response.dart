import 'package:json_annotation/json_annotation.dart';

part 'expert_data_response.g.dart';

@JsonSerializable()
class ExpertDataResponse {
  String message;
  String code;
  DataBean data;

  ExpertDataResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory ExpertDataResponse.fromJson(Map<String, dynamic> json) =>
      _$ExpertDataResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertDataResponseToJson(this);
}

@JsonSerializable()
class DataBean {
  List<ExpertBean> expert;
  List<CategoryBean> category;

  DataBean({
    required this.expert,
    required this.category,
  });

  factory DataBean.fromJson(Map<String, dynamic> json) =>
      _$DataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$DataBeanToJson(this);
}

@JsonSerializable()
class CategoryBean {
  String categoryId;
  String categoryName;
  String? icon;
  String? fontColor;

  CategoryBean({
    required this.categoryId,
    required this.categoryName,
    required this.icon,
    this.fontColor,
  });

  factory CategoryBean.fromJson(Map<String, dynamic> json) =>
      _$CategoryBeanFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryBeanToJson(this);
}

@JsonSerializable()
class ExpertBean {
  String wpExpertId;
  String expertName;
  String imageUrl;
  String jobDesc;
  String expertise;
  String note;
  String? btn;
  String? btnUrl;

  ExpertBean({
    required this.wpExpertId,
    required this.expertName,
    required this.imageUrl,
    required this.jobDesc,
    required this.expertise,
    required this.note,
    this.btn,
    this.btnUrl
  });

  factory ExpertBean.fromJson(Map<String, dynamic> json) =>
      _$ExpertBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertBeanToJson(this);
}
