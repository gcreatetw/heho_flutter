// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'advance_search_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AdvanceSearchResponse _$AdvanceSearchResponseFromJson(
        Map<String, dynamic> json) =>
    AdvanceSearchResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AdvanceSearchResponseToJson(
        AdvanceSearchResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      search: (json['search'] as List<dynamic>?)
          ?.map((e) => Search.fromJson(e as Map<String, dynamic>))
          .toList(),
      disease: json['disease'] == null
          ? null
          : Disease.fromJson(json['disease'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'search': instance.search,
      'disease': instance.disease,
    };

Search _$SearchFromJson(Map<String, dynamic> json) => Search(
      postId: json['postId'] as String,
      imgUrl: json['imgUrl'] as String,
      postDate: json['postDate'] as String,
      postTitle: json['postTitle'] as String,
      viewed: json['viewed'] as String,
      save: json['save'] as String,
      clickUrl: json['clickUrl'] as String,
    );

Map<String, dynamic> _$SearchToJson(Search instance) => <String, dynamic>{
      'postId': instance.postId,
      'imgUrl': instance.imgUrl,
      'postDate': instance.postDate,
      'postTitle': instance.postTitle,
      'viewed': instance.viewed,
      'save': instance.save,
      'clickUrl': instance.clickUrl,
    };

Disease _$DiseaseFromJson(Map<String, dynamic> json) => Disease(
      title: json['title'] as String,
      subTitle: json['subTitle'] as String,
      titleDesc: json['titleDesc'] as String,
      tag: json['tag'] as String,
      dataSource: json['dataSource'] as String?,
    );

Map<String, dynamic> _$DiseaseToJson(Disease instance) => <String, dynamic>{
      'title': instance.title,
      'subTitle': instance.subTitle,
      'titleDesc': instance.titleDesc,
      'tag': instance.tag,
      'dataSource': instance.dataSource,
    };
