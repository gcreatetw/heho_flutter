import 'package:json_annotation/json_annotation.dart';

part 'hospital_list_response.g.dart';

@JsonSerializable()
class HospitalListResponse {
  String message;
  String code;
  List<HospitalDataBean>? data;

  HospitalListResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory HospitalListResponse.fromJson(Map<String, dynamic> json) =>
      _$HospitalListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HospitalListResponseToJson(this);
}

@JsonSerializable()
class HospitalDataBean {
  String id;
  String name;
  String address;
  String tel;
  String dist;

  HospitalDataBean({
    required this.id,
    required this.name,
    required this.address,
    required this.tel,
    required this.dist,
  });

  factory HospitalDataBean.fromJson(Map<String, dynamic> json) =>
      _$HospitalDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$HospitalDataBeanToJson(this);
}
