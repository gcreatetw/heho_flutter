import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'member_collection_response.g.dart';

@JsonSerializable()
class MemberCollectionResponse {
  String message;
  String code;
  MemberCollectionDataBean data;

  MemberCollectionResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory MemberCollectionResponse.fromJson(Map<String, dynamic> json) =>
      _$MemberCollectionResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MemberCollectionResponseToJson(this);
}

@JsonSerializable()
class MemberCollectionDataBean {
  List<PostBean>? post;
  HospitalCollectionBean? hospital;
  List<String>? symptom;
  List<String>? disease;

  MemberCollectionDataBean({
    required this.post,
    required this.hospital,
    required this.symptom,
    required this.disease,
  });

  factory MemberCollectionDataBean.fromJson(Map<String, dynamic> json) =>
      _$MemberCollectionDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$MemberCollectionDataBeanToJson(this);

  factory MemberCollectionDataBean.fromRawJson(String str) =>
      MemberCollectionDataBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class HospitalCollectionBean {
  List<HospitalBean>? regional;
  List<HospitalBean>? local;
  List<HospitalBean>? clinic;
  List<HospitalBean>? medicalcenter;

  HospitalCollectionBean({
    required this.regional,
    required this.local,
    required this.clinic,
    required this.medicalcenter,
  });

  factory HospitalCollectionBean.fromJson(Map<String, dynamic> json) =>
      _$HospitalCollectionBeanFromJson(json);

  Map<String, dynamic> toJson() => _$HospitalCollectionBeanToJson(this);

  factory HospitalCollectionBean.fromRawJson(String str) =>
      HospitalCollectionBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class HospitalBean {
  String targetValue;
  String address;
  String tel;
  String name;

  HospitalBean({
    required this.targetValue,
    required this.address,
    required this.tel,
    required this.name,
  });

  factory HospitalBean.fromJson(Map<String, dynamic> json) =>
      _$HospitalBeanFromJson(json);

  Map<String, dynamic> toJson() => _$HospitalBeanToJson(this);

  factory HospitalBean.fromRawJson(String str) => HospitalBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class PostBean {
  String collectionName;
  String targetValue;
  String imgUrl;
  String postDate;
  String postTitle;
  String viewed;
  String clickUrl;

  PostBean({
    required this.collectionName,
    required this.targetValue,
    required this.imgUrl,
    required this.postDate,
    required this.postTitle,
    required this.viewed,
    required this.clickUrl,
  });

  factory PostBean.fromJson(Map<String, dynamic> json) =>
      _$PostBeanFromJson(json);

  Map<String, dynamic> toJson() => _$PostBeanToJson(this);
}
