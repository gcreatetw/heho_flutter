import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'hospital_content_response.g.dart';

@JsonSerializable()
class HospitalContentResponse {
  String message;
  String code;
  DataBean data;

  HospitalContentResponse(
      {required this.message, required this.code, required this.data});

  factory HospitalContentResponse.fromJson(Map<String, dynamic> json) =>
      _$HospitalContentResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HospitalContentResponseToJson(this);
}

@JsonSerializable()
class DataBean {
  String address;
  String tel;
  String name;
  String category;
  String service;
  String? reservationLink;
  OpenBean open;

  DataBean(
      {required this.address,
      required this.tel,
      required this.name,
      required this.category,
      required this.service,
      required this.reservationLink,
      required this.open});

  factory DataBean.fromJson(Map<String, dynamic> json) =>
      _$DataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$DataBeanToJson(this);
}

@JsonSerializable()
class OpenBean {
  WeekdayBean mon;
  WeekdayBean tues;
  WeekdayBean wed;
  WeekdayBean thurs;
  WeekdayBean fri;
  WeekdayBean sat;
  WeekdayBean sun;

  OpenBean(
      {required this.mon,
      required this.tues,
      required this.wed,
      required this.thurs,
      required this.fri,
      required this.sat,
      required this.sun});

  factory OpenBean.fromJson(Map<String, dynamic> json) =>
      _$OpenBeanFromJson(json);

  Map<String, dynamic> toJson() => _$OpenBeanToJson(this);
}

@JsonSerializable()
class WeekdayBean {
  String morning;
  String afternoon;
  String night;

  WeekdayBean({
    required this.morning,
    required this.afternoon,
    required this.night,
  });

  factory WeekdayBean.fromJson(Map<String, dynamic> json) =>
      _$WeekdayBeanFromJson(json);

  Map<String, dynamic> toJson() => _$WeekdayBeanToJson(this);

  factory WeekdayBean.fromRawJson(String str) => WeekdayBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
