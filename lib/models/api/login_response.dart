import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse {
  String message;
  String code;
  UserDataBean data;

  LoginResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}

@JsonSerializable()
class UserDataBean {
  String customer_id;
  String firstname;
  String lastname;
  String? email;
  String? telephone;
  String newsletter;
  String? birthday;
  String? session;
  String token;
  String? gender;
  int status;
  String? regDays;

  UserDataBean({
    required this.customer_id,
    required this.firstname,
    required this.lastname,
    required this.email,
    required this.telephone,
    required this.newsletter,
    required this.birthday,
    required this.session,
    required this.token,
    required this.gender,
    required this.status,
    required this.regDays,
  });

  factory UserDataBean.fromJson(Map<String, dynamic> json) =>
      _$UserDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$UserDataBeanToJson(this);

  factory UserDataBean.fromRawJson(String str) => UserDataBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
