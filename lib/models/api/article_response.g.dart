// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArticleResponse _$ArticleResponseFromJson(Map<String, dynamic> json) =>
    ArticleResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: json['data'] == null
          ? null
          : DataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ArticleResponseToJson(ArticleResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

DataBean _$DataBeanFromJson(Map<String, dynamic> json) => DataBean(
      search: (json['search'] as List<dynamic>)
          .map((e) => ArticleBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DataBeanToJson(DataBean instance) => <String, dynamic>{
      'search': instance.search,
    };

ArticleBean _$ArticleBeanFromJson(Map<String, dynamic> json) => ArticleBean(
      postId: json['postId'].toString() as String?,
      imgUrl: json['imgUrl'] as String,
      postDate: json['postDate'] as String,
      postTitle: json['postTitle'] as String,
      viewed: json['viewed'].toString() as String,
      save: json['save'].toString() as String?,
      clickUrl: json['clickUrl'] as String,
    );

Map<String, dynamic> _$ArticleBeanToJson(ArticleBean instance) =>
    <String, dynamic>{
      'postId': instance.postId,
      'imgUrl': instance.imgUrl,
      'postDate': instance.postDate,
      'postTitle': instance.postTitle,
      'viewed': instance.viewed,
      'save': instance.save,
      'clickUrl': instance.clickUrl,
    };
