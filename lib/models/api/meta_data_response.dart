import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'meta_data_response.g.dart';

@JsonSerializable()
class MetaDataResponse {
  MetaDataBean data;
  String message;
  String code;
  String shareTitle;

  MetaDataResponse({
    required this.data,
    required this.message,
    required this.code,
    required this.shareTitle,
  });

  factory MetaDataResponse.fromJson(Map<String, dynamic> json) =>
      _$MetaDataResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MetaDataResponseToJson(this);
}

@JsonSerializable()
class MetaDataBean {
  UpdateBean update;
  List<ApiBean> api;

  MetaDataBean({required this.update, required this.api});

  factory MetaDataBean.fromJson(Map<String, dynamic> json) =>
      _$MetaDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$MetaDataBeanToJson(this);

  factory MetaDataBean.fromRawJson(String str) => MetaDataBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class ApiBean {
  String id;
  String purpose;
  String url;
  String created_at;
  String updated_at;

  ApiBean({
    required this.id,
    required this.purpose,
    required this.url,
    required this.created_at,
    required this.updated_at,
  });

  factory ApiBean.fromJson(Map<String, dynamic> json) =>
      _$ApiBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ApiBeanToJson(this);

  factory ApiBean.fromRawJson(String str) => ApiBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class UpdateBean {
  AndroidBean android;
  IosBean ios;

  UpdateBean({
    required this.android,
    required this.ios,
  });

  factory UpdateBean.fromJson(Map<String, dynamic> json) =>
      _$UpdateBeanFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateBeanToJson(this);

  factory UpdateBean.fromRawJson(String str) => UpdateBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class IosBean {
  PushBean push;
  PromoteBean promote;

  IosBean({
    required this.push,
    required this.promote,
  });

  factory IosBean.fromJson(Map<String, dynamic> json) =>
      _$IosBeanFromJson(json);

  Map<String, dynamic> toJson() => _$IosBeanToJson(this);

  factory IosBean.fromRawJson(String str) => IosBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class AndroidBean {
  PushBean push;
  PromoteBean promote;

  AndroidBean({
    required this.push,
    required this.promote,
  });

  factory AndroidBean.fromJson(Map<String, dynamic> json) =>
      _$AndroidBeanFromJson(json);

  Map<String, dynamic> toJson() => _$AndroidBeanToJson(this);

  factory AndroidBean.fromRawJson(String str) => AndroidBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class PromoteBean {
  String mobileVersion;
  String appUrl;
  String message;

  PromoteBean({
    required this.mobileVersion,
    required this.appUrl,
    required this.message,
  });

  factory PromoteBean.fromJson(Map<String, dynamic> json) =>
      _$PromoteBeanFromJson(json);

  Map<String, dynamic> toJson() => _$PromoteBeanToJson(this);

  factory PromoteBean.fromRawJson(String str) => PromoteBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class PushBean {
  String mobileVersion;
  String appUrl;
  String message;

  PushBean({
    required this.mobileVersion,
    required this.appUrl,
    required this.message,
  });

  factory PushBean.fromJson(Map<String, dynamic> json) =>
      _$PushBeanFromJson(json);

  Map<String, dynamic> toJson() => _$PushBeanToJson(this);

  factory PushBean.fromRawJson(String str) => PushBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
