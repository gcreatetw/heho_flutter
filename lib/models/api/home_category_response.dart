import 'dart:convert';

import 'package:hepo_app/models/api/article_response.dart' show ArticleBean;
import 'package:json_annotation/json_annotation.dart';

part 'home_category_response.g.dart';

@JsonSerializable()
class HomeCategoryResponse {
  String message;
  String code;
  List<DataBean> data;

  HomeCategoryResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory HomeCategoryResponse.fromJson(Map<String, dynamic> json) =>
      _$HomeCategoryResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HomeCategoryResponseToJson(this);

  factory HomeCategoryResponse.fromRawJson(String str) =>
      HomeCategoryResponse.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class DataBean {
  String name;
  String paperid;
  String? external;
  String? iconStyle;
  String? iconCode;
  List<ArticleBean>? defaultData;
  List<SubDataBean>? sub;
  String? advType;
  String? advValue;

  DataBean({
    required this.name,
    required this.paperid,
    this.iconStyle,
    this.iconCode,
    this.external,
    this.defaultData,
  });

  factory DataBean.fromJson(Map<String, dynamic> json) =>
      _$DataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$DataBeanToJson(this);

  factory DataBean.fromRawJson(String str) => DataBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class SubDataBean {
  String name;
  String paperid;
  String? external;
  String? advType;
  String? advValue;

  SubDataBean({
    required this.name,
    required this.paperid,
    this.external,
  });

  factory SubDataBean.fromJson(Map<String, dynamic> json) =>
      _$SubDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$SubDataBeanToJson(this);

  factory SubDataBean.fromRawJson(String str) => SubDataBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
