// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recommend_article_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecommendArticleResponse _$RecommendArticleResponseFromJson(
        Map<String, dynamic> json) =>
    RecommendArticleResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>)
          .map((e) =>
              RecommendArticleDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RecommendArticleResponseToJson(
        RecommendArticleResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

RecommendArticleDataBean _$RecommendArticleDataBeanFromJson(
        Map<String, dynamic> json) =>
    RecommendArticleDataBean(
      postId: json['postId'] as String,
      url: json['url'] as String,
      img: json['img'] as String,
      title: json['title'] as String,
      save: json['save'] as String,
      date: json['date'] as String,
    );

Map<String, dynamic> _$RecommendArticleDataBeanToJson(
        RecommendArticleDataBean instance) =>
    <String, dynamic>{
      'postId': instance.postId,
      'url': instance.url,
      'img': instance.img,
      'title': instance.title,
      'save': instance.save,
      'date': instance.date,
    };
