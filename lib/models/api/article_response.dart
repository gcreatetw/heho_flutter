import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'article_response.g.dart';

@JsonSerializable()
class ArticleResponse {
  String message;
  String code;
  DataBean? data;

  ArticleResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory ArticleResponse.fromJson(Map<String, dynamic> json) =>
      _$ArticleResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ArticleResponseToJson(this);

  factory ArticleResponse.fromRawJson(String str) => ArticleResponse.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class DataBean {
  List<ArticleBean> search;

  DataBean({required this.search});

  factory DataBean.fromJson(Map<String, dynamic> json) =>
      _$DataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$DataBeanToJson(this);

  factory DataBean.fromRawJson(String str) => DataBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class ArticleBean {
  String? postId;
  String imgUrl;
  String postDate;
  String postTitle;
  String viewed;
  String? save;
  String clickUrl;

  ArticleBean({
    required this.postId,
    required this.imgUrl,
    required this.postDate,
    required this.postTitle,
    required this.viewed,
    required this.save,
    required this.clickUrl,
  });

  factory ArticleBean.fromJson(Map<String, dynamic> json) =>
      _$ArticleBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ArticleBeanToJson(this);

  factory ArticleBean.fromRawJson(String str) => ArticleBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
