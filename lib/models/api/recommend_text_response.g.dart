// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recommend_text_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecommendTextResponse _$RecommendTextResponseFromJson(
        Map<String, dynamic> json) =>
    RecommendTextResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>)
          .map((e) => RecommendTextBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RecommendTextResponseToJson(
        RecommendTextResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

RecommendTextBean _$RecommendTextBeanFromJson(Map<String, dynamic> json) =>
    RecommendTextBean(
      title: json['title'] as String,
      url: json['url'] as String?,
    );

Map<String, dynamic> _$RecommendTextBeanToJson(RecommendTextBean instance) =>
    <String, dynamic>{
      'title': instance.title,
      'url': instance.url,
    };
