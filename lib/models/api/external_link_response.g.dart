// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'external_link_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExternalLinkResponse _$ExternalLinkResponseFromJson(
        Map<String, dynamic> json) =>
    ExternalLinkResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>)
          .map((e) => DataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ExternalLinkResponseToJson(
        ExternalLinkResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

DataBean _$DataBeanFromJson(Map<String, dynamic> json) => DataBean(
      id: json['id'] as String,
      desc: json['desc'] as String,
      link: json['link'] as String,
    );

Map<String, dynamic> _$DataBeanToJson(DataBean instance) => <String, dynamic>{
      'id': instance.id,
      'desc': instance.desc,
      'link': instance.link,
    };
