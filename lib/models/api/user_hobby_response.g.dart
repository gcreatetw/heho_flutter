// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_hobby_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserHobbyResponse _$UserHobbyResponseFromJson(Map<String, dynamic> json) =>
    UserHobbyResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>)
          .map((e) => UserHobbyDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UserHobbyResponseToJson(UserHobbyResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

UserHobbyDataBean _$UserHobbyDataBeanFromJson(Map<String, dynamic> json) =>
    UserHobbyDataBean(
      id: json['id'] as String,
      name: json['name'] as String,
    );

Map<String, dynamic> _$UserHobbyDataBeanToJson(UserHobbyDataBean instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
