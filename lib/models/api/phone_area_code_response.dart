import 'package:json_annotation/json_annotation.dart';

part 'phone_area_code_response.g.dart';

@JsonSerializable()
class PhoneAreaCodeResponse {
  String message;
  String code;
  List<PhoneAreaCodeDataBean> data;
  String? disableMessage;

  PhoneAreaCodeResponse({
    required this.message,
    required this.code,
    required this.data,
    this.disableMessage,
  });

  factory PhoneAreaCodeResponse.fromJson(Map<String, dynamic> json) =>
      _$PhoneAreaCodeResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PhoneAreaCodeResponseToJson(this);
}

@JsonSerializable()
class PhoneAreaCodeDataBean {
  String id;
  String name;

  PhoneAreaCodeDataBean({
    required this.id,
    required this.name,
  });

  factory PhoneAreaCodeDataBean.fromJson(Map<String, dynamic> json) =>
      _$PhoneAreaCodeDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$PhoneAreaCodeDataBeanToJson(this);
}
