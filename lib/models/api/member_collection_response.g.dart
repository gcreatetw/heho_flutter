// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'member_collection_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberCollectionResponse _$MemberCollectionResponseFromJson(
        Map<String, dynamic> json) =>
    MemberCollectionResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: MemberCollectionDataBean.fromJson(
          json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MemberCollectionResponseToJson(
        MemberCollectionResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

MemberCollectionDataBean _$MemberCollectionDataBeanFromJson(
        Map<String, dynamic> json) =>
    MemberCollectionDataBean(
      post: (json['post'] as List<dynamic>?)
          ?.map((e) => PostBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      hospital: json['hospital'] == null
          ? null
          : HospitalCollectionBean.fromJson(
              json['hospital'] as Map<String, dynamic>),
      symptom:
          (json['symptom'] as List<dynamic>?)?.map((e) => e as String).toList(),
      disease:
          (json['disease'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$MemberCollectionDataBeanToJson(
        MemberCollectionDataBean instance) =>
    <String, dynamic>{
      'post': instance.post,
      'hospital': instance.hospital,
      'symptom': instance.symptom,
      'disease': instance.disease,
    };

HospitalCollectionBean _$HospitalCollectionBeanFromJson(
        Map<String, dynamic> json) =>
    HospitalCollectionBean(
      regional: (json['regional'] as List<dynamic>?)
          ?.map((e) => HospitalBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      local: (json['local'] as List<dynamic>?)
          ?.map((e) => HospitalBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      clinic: (json['clinic'] as List<dynamic>?)
          ?.map((e) => HospitalBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      medicalcenter: (json['medicalcenter'] as List<dynamic>?)
          ?.map((e) => HospitalBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$HospitalCollectionBeanToJson(
        HospitalCollectionBean instance) =>
    <String, dynamic>{
      'regional': instance.regional,
      'local': instance.local,
      'clinic': instance.clinic,
      'medicalcenter': instance.medicalcenter,
    };

HospitalBean _$HospitalBeanFromJson(Map<String, dynamic> json) => HospitalBean(
      targetValue: json['targetValue'] as String,
      address: json['address'] as String,
      tel: json['tel'] as String,
      name: json['name'] as String,
    );

Map<String, dynamic> _$HospitalBeanToJson(HospitalBean instance) =>
    <String, dynamic>{
      'targetValue': instance.targetValue,
      'address': instance.address,
      'tel': instance.tel,
      'name': instance.name,
    };

PostBean _$PostBeanFromJson(Map<String, dynamic> json) => PostBean(
      collectionName: json['collectionName'] as String,
      targetValue: json['targetValue'] as String,
      imgUrl: json['imgUrl'] as String,
      postDate: json['postDate'] as String,
      postTitle: json['postTitle'] as String,
      viewed: json['viewed'] as String,
      clickUrl: json['clickUrl'] as String,
    );

Map<String, dynamic> _$PostBeanToJson(PostBean instance) => <String, dynamic>{
      'collectionName': instance.collectionName,
      'targetValue': instance.targetValue,
      'imgUrl': instance.imgUrl,
      'postDate': instance.postDate,
      'postTitle': instance.postTitle,
      'viewed': instance.viewed,
      'clickUrl': instance.clickUrl,
    };
