import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'recommend_text_response.g.dart';

@JsonSerializable()
class RecommendTextResponse {
  String message;
  String code;
  List<RecommendTextBean> data;

  RecommendTextResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory RecommendTextResponse.fromJson(Map<String, dynamic> json) =>
      _$RecommendTextResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendTextResponseToJson(this);
}

@JsonSerializable()
class RecommendTextBean {
  String title;
  String? url;

  RecommendTextBean({
    required this.title,
    required this.url,
  });

  factory RecommendTextBean.fromJson(Map<String, dynamic> json) =>
      _$RecommendTextBeanFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendTextBeanToJson(this);

  factory RecommendTextBean.fromRawJson(String str) =>
      RecommendTextBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
