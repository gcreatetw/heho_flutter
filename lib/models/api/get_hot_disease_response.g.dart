// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_hot_disease_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetHotDiseaseResponse _$GetHotDiseaseResponseFromJson(
        Map<String, dynamic> json) =>
    GetHotDiseaseResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$GetHotDiseaseResponseToJson(
        GetHotDiseaseResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };
