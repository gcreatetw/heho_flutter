// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meta_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MetaDataResponse _$MetaDataResponseFromJson(Map<String, dynamic> json) =>
    MetaDataResponse(
      data: MetaDataBean.fromJson(json['data'] as Map<String, dynamic>),
      message: json['message'] as String,
      code: json['code'] as String,
      shareTitle: json['shareTitle'] as String,
    );

Map<String, dynamic> _$MetaDataResponseToJson(MetaDataResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'message': instance.message,
      'code': instance.code,
      'shareTitle': instance.shareTitle,
    };

MetaDataBean _$MetaDataBeanFromJson(Map<String, dynamic> json) => MetaDataBean(
      update: UpdateBean.fromJson(json['update'] as Map<String, dynamic>),
      api: (json['api'] as List<dynamic>)
          .map((e) => ApiBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MetaDataBeanToJson(MetaDataBean instance) =>
    <String, dynamic>{
      'update': instance.update,
      'api': instance.api,
    };

ApiBean _$ApiBeanFromJson(Map<String, dynamic> json) => ApiBean(
      id: json['id'] as String,
      purpose: json['purpose'] as String,
      url: json['url'] as String,
      created_at: json['created_at'] as String,
      updated_at: json['updated_at'] as String,
    );

Map<String, dynamic> _$ApiBeanToJson(ApiBean instance) => <String, dynamic>{
      'id': instance.id,
      'purpose': instance.purpose,
      'url': instance.url,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
    };

UpdateBean _$UpdateBeanFromJson(Map<String, dynamic> json) => UpdateBean(
      android: AndroidBean.fromJson(json['android'] as Map<String, dynamic>),
      ios: IosBean.fromJson(json['ios'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UpdateBeanToJson(UpdateBean instance) =>
    <String, dynamic>{
      'android': instance.android,
      'ios': instance.ios,
    };

IosBean _$IosBeanFromJson(Map<String, dynamic> json) => IosBean(
      push: PushBean.fromJson(json['push'] as Map<String, dynamic>),
      promote: PromoteBean.fromJson(json['promote'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$IosBeanToJson(IosBean instance) => <String, dynamic>{
      'push': instance.push,
      'promote': instance.promote,
    };

AndroidBean _$AndroidBeanFromJson(Map<String, dynamic> json) => AndroidBean(
      push: PushBean.fromJson(json['push'] as Map<String, dynamic>),
      promote: PromoteBean.fromJson(json['promote'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AndroidBeanToJson(AndroidBean instance) =>
    <String, dynamic>{
      'push': instance.push,
      'promote': instance.promote,
    };

PromoteBean _$PromoteBeanFromJson(Map<String, dynamic> json) => PromoteBean(
      mobileVersion: json['mobileVersion'] as String,
      appUrl: json['appUrl'] as String,
      message: json['message'] as String,
    );

Map<String, dynamic> _$PromoteBeanToJson(PromoteBean instance) =>
    <String, dynamic>{
      'mobileVersion': instance.mobileVersion,
      'appUrl': instance.appUrl,
      'message': instance.message,
    };

PushBean _$PushBeanFromJson(Map<String, dynamic> json) => PushBean(
      mobileVersion: json['mobileVersion'] as String,
      appUrl: json['appUrl'] as String,
      message: json['message'] as String,
    );

Map<String, dynamic> _$PushBeanToJson(PushBean instance) => <String, dynamic>{
      'mobileVersion': instance.mobileVersion,
      'appUrl': instance.appUrl,
      'message': instance.message,
    };
