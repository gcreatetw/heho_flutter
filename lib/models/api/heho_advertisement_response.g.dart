// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'heho_advertisement_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HehoAdvertisementResponse _$HehoAdvertisementResponseFromJson(
        Map<String, dynamic> json) =>
    HehoAdvertisementResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data:
          HehoAdvertisementBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$HehoAdvertisementResponseToJson(
        HehoAdvertisementResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

HehoAdvertisementBean _$HehoAdvertisementBeanFromJson(
        Map<String, dynamic> json) =>
    HehoAdvertisementBean(
      url: json['url'] as String,
      img: json['img'] as String,
    );

Map<String, dynamic> _$HehoAdvertisementBeanToJson(
        HehoAdvertisementBean instance) =>
    <String, dynamic>{
      'url': instance.url,
      'img': instance.img,
    };
