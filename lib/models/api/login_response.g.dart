// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) =>
    LoginResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: UserDataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

UserDataBean _$UserDataBeanFromJson(Map<String, dynamic> json) => UserDataBean(
      customer_id: json['customer_id'] as String,
      firstname: json['firstname'] as String,
      lastname: json['lastname'] as String,
      email: json['email'] as String?,
      telephone: json['telephone'] as String?,
      newsletter: json['newsletter'] as String,
      birthday: json['birthday'] as String?,
      session: json['session'] as String?,
      token: json['token'] as String,
      gender: json['gender'] as String?,
      status: json['status'] as int,
      regDays: json['regDays'] as String?,
    );

Map<String, dynamic> _$UserDataBeanToJson(UserDataBean instance) =>
    <String, dynamic>{
      'customer_id': instance.customer_id,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'email': instance.email,
      'telephone': instance.telephone,
      'newsletter': instance.newsletter,
      'birthday': instance.birthday,
      'session': instance.session,
      'token': instance.token,
      'gender': instance.gender,
      'status': instance.status,
      'regDays': instance.regDays,
    };
