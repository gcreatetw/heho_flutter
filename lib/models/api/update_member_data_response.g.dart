// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_member_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateMemberDataResponse _$UpdateMemberDataResponseFromJson(
        Map<String, dynamic> json) =>
    UpdateMemberDataResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: json['data'] == null
          ? null
          : Data.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UpdateMemberDataResponseToJson(
        UpdateMemberDataResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      customer_id: json['customer_id'] as String,
      firstname: json['firstname'] as String,
      lastname: json['lastname'] as String,
      email: json['email'] as String,
      email_old: json['email_old'] as String?,
      telephone: json['telephone'] as String?,
      newsletter: json['newsletter'] as String,
      birthday: json['birthday'] as String?,
      token: json['token'] as String,
      gender: json['gender'] as String?,
      regDays: json['regDays'] as String?,
      status: json['status'] as int,
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'customer_id': instance.customer_id,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'email': instance.email,
      'email_old': instance.email_old,
      'telephone': instance.telephone,
      'newsletter': instance.newsletter,
      'birthday': instance.birthday,
      'token': instance.token,
      'gender': instance.gender,
      'regDays': instance.regDays,
      'status': instance.status,
    };
