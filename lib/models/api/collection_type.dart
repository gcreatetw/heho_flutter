enum CollectionType {
  news,
  disease,
  symptom,
  hospital,
}

extension CollectionTypeEx on CollectionType {
  String get apiName {
    switch (this) {
      case CollectionType.news:
        return '文章';
      case CollectionType.disease:
        return '疾病';
      case CollectionType.symptom:
        return '症狀';
      case CollectionType.hospital:
        return '院所';
    }
  }
}
