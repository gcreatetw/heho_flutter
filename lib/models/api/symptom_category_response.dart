import 'package:json_annotation/json_annotation.dart';

part 'symptom_category_response.g.dart';

@JsonSerializable()
class SymptomCategoryResponse {
  String message;
  String code;
  List<SymptomCategoryDataBean> data;

  SymptomCategoryResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory SymptomCategoryResponse.fromJson(Map<String, dynamic> json) =>
      _$SymptomCategoryResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SymptomCategoryResponseToJson(this);
}

@JsonSerializable()
class SymptomCategoryDataBean {
  String main_category;
  List<String> sub_category;

  SymptomCategoryDataBean({
    required this.main_category,
    required this.sub_category,
  });

  factory SymptomCategoryDataBean.fromJson(Map<String, dynamic> json) =>
      _$SymptomCategoryDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$SymptomCategoryDataBeanToJson(this);
}
