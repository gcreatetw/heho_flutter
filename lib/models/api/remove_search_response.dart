import 'package:json_annotation/json_annotation.dart';

part 'remove_search_response.g.dart';

@JsonSerializable()
class RemoveSearchResponse {
  String message;
  String code;
  String data;

  RemoveSearchResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory RemoveSearchResponse.fromJson(Map<String, dynamic> json) =>
      _$RemoveSearchResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RemoveSearchResponseToJson(this);
}
