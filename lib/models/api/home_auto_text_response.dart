import 'package:json_annotation/json_annotation.dart';

part 'home_auto_text_response.g.dart';

@JsonSerializable()
class HomeAutoTextResponse {
  List<String>? data;
  String message;
  String code;

  HomeAutoTextResponse(
      {required this.data, required this.message, required this.code});

  factory HomeAutoTextResponse.fromJson(Map<String, dynamic> json) =>
      _$HomeAutoTextResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HomeAutoTextResponseToJson(this);
}
