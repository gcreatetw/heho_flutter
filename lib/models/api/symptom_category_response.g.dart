// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'symptom_category_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SymptomCategoryResponse _$SymptomCategoryResponseFromJson(
        Map<String, dynamic> json) =>
    SymptomCategoryResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>)
          .map((e) =>
              SymptomCategoryDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SymptomCategoryResponseToJson(
        SymptomCategoryResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

SymptomCategoryDataBean _$SymptomCategoryDataBeanFromJson(
        Map<String, dynamic> json) =>
    SymptomCategoryDataBean(
      main_category: json['main_category'] as String,
      sub_category: (json['sub_category'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$SymptomCategoryDataBeanToJson(
        SymptomCategoryDataBean instance) =>
    <String, dynamic>{
      'main_category': instance.main_category,
      'sub_category': instance.sub_category,
    };
