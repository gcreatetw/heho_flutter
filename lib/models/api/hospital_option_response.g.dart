// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hospital_option_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HospitalOptionResponse _$HospitalOptionResponseFromJson(
        Map<String, dynamic> json) =>
    HospitalOptionResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: DataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$HospitalOptionResponseToJson(
        HospitalOptionResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

DataBean _$DataBeanFromJson(Map<String, dynamic> json) => DataBean(
      north: (json['north'] as List<dynamic>).map((e) => e as String).toList(),
      south: (json['south'] as List<dynamic>).map((e) => e as String).toList(),
      mid: (json['mid'] as List<dynamic>).map((e) => e as String).toList(),
      others:
          (json['others'] as List<dynamic>).map((e) => e as String).toList(),
      category:
          (json['category'] as List<dynamic>).map((e) => e as String).toList(),
      service: (json['service'] as Map<String, dynamic>).map(
        (k, e) => MapEntry(k, ServiceBean.fromJson(e as Map<String, dynamic>)),
      ),
    );

Map<String, dynamic> _$DataBeanToJson(DataBean instance) => <String, dynamic>{
      'north': instance.north,
      'south': instance.south,
      'mid': instance.mid,
      'others': instance.others,
      'category': instance.category,
      'service': instance.service,
    };

ServiceBean _$ServiceBeanFromJson(Map<String, dynamic> json) => ServiceBean(
      name: json['name'] as String,
      item: (json['item'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$ServiceBeanToJson(ServiceBean instance) =>
    <String, dynamic>{
      'name': instance.name,
      'item': instance.item,
    };
