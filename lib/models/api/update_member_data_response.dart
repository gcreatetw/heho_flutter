import 'package:json_annotation/json_annotation.dart';

part 'update_member_data_response.g.dart';

@JsonSerializable()
class UpdateMemberDataResponse {
  String message;
  String code;
  Data? data;

  UpdateMemberDataResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory UpdateMemberDataResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdateMemberDataResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateMemberDataResponseToJson(this);
}

@JsonSerializable()
class Data {
  String customer_id;
  String firstname;
  String lastname;
  String email;
  String? email_old;
  String? telephone;
  String newsletter;
  String? birthday;
  String token;
  String? gender;
  String? regDays;
  int status;

  Data({
    required this.customer_id,
    required this.firstname,
    required this.lastname,
    required this.email,
    required this.email_old,
    required this.telephone,
    required this.newsletter,
    required this.birthday,
    required this.token,
    required this.gender,
    required this.regDays,
    required this.status,
  });

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}
