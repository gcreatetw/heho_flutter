// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'remove_search_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RemoveSearchResponse _$RemoveSearchResponseFromJson(
        Map<String, dynamic> json) =>
    RemoveSearchResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: json['data'] as String,
    );

Map<String, dynamic> _$RemoveSearchResponseToJson(
        RemoveSearchResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };
