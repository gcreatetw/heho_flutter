// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_auto_text_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeAutoTextResponse _$HomeAutoTextResponseFromJson(
        Map<String, dynamic> json) =>
    HomeAutoTextResponse(
      data: (json['data'] as List<dynamic>?)?.map((e) => e as String).toList(),
      message: json['message'] as String,
      code: json['code'] as String,
    );

Map<String, dynamic> _$HomeAutoTextResponseToJson(
        HomeAutoTextResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'message': instance.message,
      'code': instance.code,
    };
