// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expert_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpertDataResponse _$ExpertDataResponseFromJson(Map<String, dynamic> json) =>
    ExpertDataResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: DataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ExpertDataResponseToJson(ExpertDataResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

DataBean _$DataBeanFromJson(Map<String, dynamic> json) => DataBean(
      expert: (json['expert'] as List<dynamic>)
          .map((e) => ExpertBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      category: (json['category'] as List<dynamic>)
          .map((e) => CategoryBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DataBeanToJson(DataBean instance) => <String, dynamic>{
      'expert': instance.expert,
      'category': instance.category,
    };

CategoryBean _$CategoryBeanFromJson(Map<String, dynamic> json) => CategoryBean(
      categoryId: json['categoryId'] as String,
      categoryName: json['categoryName'] as String,
      icon: json['icon'] as String?,
      fontColor: json['fontColor'] as String?,
    );

Map<String, dynamic> _$CategoryBeanToJson(CategoryBean instance) =>
    <String, dynamic>{
      'categoryId': instance.categoryId,
      'categoryName': instance.categoryName,
      'icon': instance.icon,
      'fontColor': instance.fontColor,
    };

ExpertBean _$ExpertBeanFromJson(Map<String, dynamic> json) => ExpertBean(
      wpExpertId: json['wpExpertId'] as String,
      expertName: json['expertName'] as String,
      imageUrl: json['imageUrl'] as String,
      jobDesc: json['jobDesc'] as String,
      expertise: json['expertise'] as String,
      note: json['note'] as String,
      btn: json['btn'] as String?,
      btnUrl: json['btnUrl'] as String?,
    );

Map<String, dynamic> _$ExpertBeanToJson(ExpertBean instance) =>
    <String, dynamic>{
      'wpExpertId': instance.wpExpertId,
      'expertName': instance.expertName,
      'imageUrl': instance.imageUrl,
      'jobDesc': instance.jobDesc,
      'expertise': instance.expertise,
      'note': instance.note,
      'btn': instance.btn,
      'btnUrl': instance.btnUrl,
    };
