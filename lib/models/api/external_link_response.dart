import 'package:json_annotation/json_annotation.dart';

part 'external_link_response.g.dart';

@JsonSerializable()
class ExternalLinkResponse {
  String message;
  String code;
  List<DataBean> data;

  ExternalLinkResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory ExternalLinkResponse.fromJson(Map<String, dynamic> json) =>
      _$ExternalLinkResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ExternalLinkResponseToJson(this);
}

@JsonSerializable()
class DataBean {
  String id;
  String desc;
  String link;

  DataBean({
    required this.id,
    required this.desc,
    required this.link,
  });

  factory DataBean.fromJson(Map<String, dynamic> json) =>
      _$DataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$DataBeanToJson(this);
}
