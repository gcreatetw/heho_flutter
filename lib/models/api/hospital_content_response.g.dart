// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hospital_content_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HospitalContentResponse _$HospitalContentResponseFromJson(
        Map<String, dynamic> json) =>
    HospitalContentResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: DataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$HospitalContentResponseToJson(
        HospitalContentResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

DataBean _$DataBeanFromJson(Map<String, dynamic> json) => DataBean(
      address: json['address'] as String,
      tel: json['tel'] as String,
      name: json['name'] as String,
      category: json['category'] as String,
      service: json['service'] as String,
      reservationLink: json['reservationLink'] as String?,
      open: OpenBean.fromJson(json['open'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DataBeanToJson(DataBean instance) => <String, dynamic>{
      'address': instance.address,
      'tel': instance.tel,
      'name': instance.name,
      'category': instance.category,
      'service': instance.service,
      'reservationLink': instance.reservationLink,
      'open': instance.open,
    };

OpenBean _$OpenBeanFromJson(Map<String, dynamic> json) => OpenBean(
      mon: WeekdayBean.fromJson(json['mon'] as Map<String, dynamic>),
      tues: WeekdayBean.fromJson(json['tues'] as Map<String, dynamic>),
      wed: WeekdayBean.fromJson(json['wed'] as Map<String, dynamic>),
      thurs: WeekdayBean.fromJson(json['thurs'] as Map<String, dynamic>),
      fri: WeekdayBean.fromJson(json['fri'] as Map<String, dynamic>),
      sat: WeekdayBean.fromJson(json['sat'] as Map<String, dynamic>),
      sun: WeekdayBean.fromJson(json['sun'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$OpenBeanToJson(OpenBean instance) => <String, dynamic>{
      'mon': instance.mon,
      'tues': instance.tues,
      'wed': instance.wed,
      'thurs': instance.thurs,
      'fri': instance.fri,
      'sat': instance.sat,
      'sun': instance.sun,
    };

WeekdayBean _$WeekdayBeanFromJson(Map<String, dynamic> json) => WeekdayBean(
      morning: json['morning'] as String,
      afternoon: json['afternoon'] as String,
      night: json['night'] as String,
    );

Map<String, dynamic> _$WeekdayBeanToJson(WeekdayBean instance) =>
    <String, dynamic>{
      'morning': instance.morning,
      'afternoon': instance.afternoon,
      'night': instance.night,
    };
