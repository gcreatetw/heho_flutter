import 'package:json_annotation/json_annotation.dart';

part 'keys_response.g.dart';

@JsonSerializable()
class KeysResponse {
  String message;
  String code;
  String data;

  KeysResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory KeysResponse.fromJson(Map<String, dynamic> json) =>
      _$KeysResponseFromJson(json);

  Map<String, dynamic> toJson() => _$KeysResponseToJson(this);
}
