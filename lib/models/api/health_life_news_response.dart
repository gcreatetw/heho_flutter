import 'package:json_annotation/json_annotation.dart';

part 'health_life_news_response.g.dart';

@JsonSerializable()
class HealthLifeNewsResponse {
  String message;
  String code;
  HealthLifeNewsDataBean? data;

  HealthLifeNewsResponse({
    required this.message,
    required this.code,
    this.data,
  });

  factory HealthLifeNewsResponse.fromJson(Map<String, dynamic> json) =>
      _$HealthLifeNewsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HealthLifeNewsResponseToJson(this);
}

@JsonSerializable()
class HealthLifeNewsDataBean {
  List<HealthLifeSearch>? search;

  HealthLifeNewsDataBean({
    required this.search,
  });

  factory HealthLifeNewsDataBean.fromJson(Map<String, dynamic> json) =>
      _$HealthLifeNewsDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$HealthLifeNewsDataBeanToJson(this);
}

@JsonSerializable()
class HealthLifeSearch {
  String postId;
  String imgUrl;
  String postDate;
  String postTitle;
  String viewed;
  String save;
  String clickUrl;

  HealthLifeSearch({
    required this.postId,
    required this.imgUrl,
    required this.postDate,
    required this.postTitle,
    required this.viewed,
    required this.save,
    required this.clickUrl,
  });

  factory HealthLifeSearch.fromJson(Map<String, dynamic> json) =>
      _$HealthLifeSearchFromJson(json);

  Map<String, dynamic> toJson() => _$HealthLifeSearchToJson(this);
}
