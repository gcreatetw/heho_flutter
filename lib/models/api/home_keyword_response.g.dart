// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_keyword_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeKeywordResponse _$HomeKeywordResponseFromJson(Map<String, dynamic> json) =>
    HomeKeywordResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: DataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$HomeKeywordResponseToJson(
        HomeKeywordResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

DataBean _$DataBeanFromJson(Map<String, dynamic> json) => DataBean(
      topSearch: (json['topSearch'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      userSearch: (json['userSearch'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      recommand: (json['recommand'] as List<dynamic>)
          .map((e) => RecommandBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DataBeanToJson(DataBean instance) => <String, dynamic>{
      'topSearch': instance.topSearch,
      'userSearch': instance.userSearch,
      'recommand': instance.recommand,
    };

RecommandBean _$RecommandBeanFromJson(Map<String, dynamic> json) =>
    RecommandBean(
      postId: json['postId'] as String,
      url: json['url'] as String,
      save: json['save'] as String,
      img: json['img'] as String,
      title: json['title'] as String,
      date: json['date'] as String,
    );

Map<String, dynamic> _$RecommandBeanToJson(RecommandBean instance) =>
    <String, dynamic>{
      'postId': instance.postId,
      'url': instance.url,
      'save': instance.save,
      'img': instance.img,
      'title': instance.title,
      'date': instance.date,
    };
