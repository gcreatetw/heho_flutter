// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_area_code_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhoneAreaCodeResponse _$PhoneAreaCodeResponseFromJson(
        Map<String, dynamic> json) =>
    PhoneAreaCodeResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>)
          .map((e) => PhoneAreaCodeDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      disableMessage: json['disableMessage'] as String?,
    );

Map<String, dynamic> _$PhoneAreaCodeResponseToJson(
        PhoneAreaCodeResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
      'disableMessage': instance.disableMessage,
    };

PhoneAreaCodeDataBean _$PhoneAreaCodeDataBeanFromJson(
        Map<String, dynamic> json) =>
    PhoneAreaCodeDataBean(
      id: json['id'] as String,
      name: json['name'] as String,
    );

Map<String, dynamic> _$PhoneAreaCodeDataBeanToJson(
        PhoneAreaCodeDataBean instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
