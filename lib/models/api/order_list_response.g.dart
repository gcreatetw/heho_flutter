// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderListResponse _$OrderListResponseFromJson(Map<String, dynamic> json) =>
    OrderListResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => OrderDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$OrderListResponseToJson(OrderListResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

OrderDataBean _$OrderDataBeanFromJson(Map<String, dynamic> json) =>
    OrderDataBean(
      order_id: json['order_id'] as String,
      order_status: json['order_status'] as String,
      total: json['total'] as String,
      date_added: json['date_added'] as String,
      payment_method: json['payment_method'] as String,
      quantity: json['quantity'] as String,
    );

Map<String, dynamic> _$OrderDataBeanToJson(OrderDataBean instance) =>
    <String, dynamic>{
      'order_id': instance.order_id,
      'order_status': instance.order_status,
      'total': instance.total,
      'date_added': instance.date_added,
      'payment_method': instance.payment_method,
      'quantity': instance.quantity,
    };
