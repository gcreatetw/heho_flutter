import 'package:json_annotation/json_annotation.dart';

part 'heho_advertisement_response.g.dart';

@JsonSerializable()
class HehoAdvertisementResponse {
  String message;
  String code;
  HehoAdvertisementBean data;

  HehoAdvertisementResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory HehoAdvertisementResponse.fromJson(Map<String, dynamic> json) =>
      _$HehoAdvertisementResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HehoAdvertisementResponseToJson(this);
}

@JsonSerializable()
class HehoAdvertisementBean {
  String url;
  String img;

  HehoAdvertisementBean({required this.url, required this.img});

  factory HehoAdvertisementBean.fromJson(Map<String, dynamic> json) =>
      _$HehoAdvertisementBeanFromJson(json);

  Map<String, dynamic> toJson() => _$HehoAdvertisementBeanToJson(this);
}
