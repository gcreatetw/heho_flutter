import 'package:json_annotation/json_annotation.dart';

part 'recommend_article_response.g.dart';

@JsonSerializable()
class RecommendArticleResponse {
  String message;
  String code;
  List<RecommendArticleDataBean> data;

  RecommendArticleResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory RecommendArticleResponse.fromJson(Map<String, dynamic> json) =>
      _$RecommendArticleResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendArticleResponseToJson(this);
}

@JsonSerializable()
class RecommendArticleDataBean {
  String postId;
  String url;
  String img;
  String title;
  String save;
  String date;

  RecommendArticleDataBean({
    required this.postId,
    required this.url,
    required this.img,
    required this.title,
    required this.save,
    required this.date,
  });

  factory RecommendArticleDataBean.fromJson(Map<String, dynamic> json) =>
      _$RecommendArticleDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendArticleDataBeanToJson(this);
}
