// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_category_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeCategoryResponse _$HomeCategoryResponseFromJson(
        Map<String, dynamic> json) =>
    HomeCategoryResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>)
          .map((e) => DataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$HomeCategoryResponseToJson(
        HomeCategoryResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

DataBean _$DataBeanFromJson(Map<String, dynamic> json) => DataBean(
      name: json['name'] as String,
      paperid: json['paperid'] as String,
      iconStyle: json['iconStyle'] as String?,
      iconCode: json['iconCode'] as String?,
      external: json['external'] as String?,
      defaultData: (json['defaultData'] as List<dynamic>?)
          ?.map((e) => ArticleBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..sub = (json['sub'] as List<dynamic>?)
          ?.map((e) => SubDataBean.fromJson(e as Map<String, dynamic>))
          .toList()
      ..advType = json['advType'] as String?
      ..advValue = json['advValue'] as String?;

Map<String, dynamic> _$DataBeanToJson(DataBean instance) => <String, dynamic>{
      'name': instance.name,
      'paperid': instance.paperid,
      'external': instance.external,
      'iconStyle': instance.iconStyle,
      'iconCode': instance.iconCode,
      'defaultData': instance.defaultData,
      'sub': instance.sub,
      'advType': instance.advType,
      'advValue': instance.advValue,
    };

SubDataBean _$SubDataBeanFromJson(Map<String, dynamic> json) => SubDataBean(
      name: json['name'] as String,
      paperid: json['paperid'] as String,
      external: json['external'] as String?,
    )
      ..advType = json['advType'] as String?
      ..advValue = json['advValue'] as String?;

Map<String, dynamic> _$SubDataBeanToJson(SubDataBean instance) =>
    <String, dynamic>{
      'name': instance.name,
      'paperid': instance.paperid,
      'external': instance.external,
      'advType': instance.advType,
      'advValue': instance.advValue,
    };
