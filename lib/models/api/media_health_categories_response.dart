import 'dart:convert';

import 'package:hepo_app/models/api/article_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'media_health_categories_response.g.dart';

@JsonSerializable()
class MediaHealthCategoriesResponse {
  String message;
  String code;
  List<MediaHealthCategoryBean> data;

  MediaHealthCategoriesResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory MediaHealthCategoriesResponse.fromJson(Map<String, dynamic> json) =>
      _$MediaHealthCategoriesResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MediaHealthCategoriesResponseToJson(this);
}

@JsonSerializable()
class MediaHealthCategoryBean {
  String name;
  String paperid;
  String? external;
  String? iconStyle;
  String? iconCode;
  @JsonKey(name: 'default')
  String? isDefault;
  List<ArticleBean>? defaultData;

  MediaHealthCategoryBean({
    required this.name,
    required this.paperid,
    this.external,
    this.iconStyle,
    this.iconCode,
    this.isDefault,
    this.defaultData,
  });

  factory MediaHealthCategoryBean.fromJson(Map<String, dynamic> json) =>
      _$MediaHealthCategoryBeanFromJson(json);

  Map<String, dynamic> toJson() => _$MediaHealthCategoryBeanToJson(this);

  factory MediaHealthCategoryBean.fromRawJson(String str) =>
      MediaHealthCategoryBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
