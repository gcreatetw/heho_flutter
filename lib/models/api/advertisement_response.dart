import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'advertisement_response.g.dart';

@JsonSerializable()
class AdvertisementResponse {
  String message;
  String code;
  List<AdvertisementBean>? data;

  AdvertisementResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory AdvertisementResponse.fromJson(Map<String, dynamic> json) =>
      _$AdvertisementResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AdvertisementResponseToJson(this);

  factory AdvertisementResponse.fromRawJson(String str) =>
      AdvertisementResponse.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}

@JsonSerializable()
class AdvertisementBean {
  String imageUrl;
  String clickUrl;
  String splashType;

  AdvertisementBean({
    required this.imageUrl,
    required this.clickUrl,
    required this.splashType,
  });

  factory AdvertisementBean.fromJson(Map<String, dynamic> json) =>
      _$AdvertisementBeanFromJson(json);

  Map<String, dynamic> toJson() => _$AdvertisementBeanToJson(this);

  factory AdvertisementBean.fromRawJson(String str) =>
      AdvertisementBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
