enum ActionType {
  add,
  delete,
}

extension ActionTypeEx on ActionType {
  String get apiName {
    switch (this) {
      case ActionType.add:
        return 'add';
      case ActionType.delete:
        return 'del';
    }
  }
}
