import 'package:json_annotation/json_annotation.dart';

part 'home_keyword_response.g.dart';

@JsonSerializable()
class HomeKeywordResponse {
  String message;
  String code;
  DataBean data;

  HomeKeywordResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory HomeKeywordResponse.fromJson(Map<String, dynamic> json) =>
      _$HomeKeywordResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HomeKeywordResponseToJson(this);
}

@JsonSerializable()
class DataBean {
  List<String>? topSearch;
  List<String>? userSearch;
  List<RecommandBean> recommand;

  DataBean(
      {required this.topSearch,
      required this.userSearch,
      required this.recommand});

  factory DataBean.fromJson(Map<String, dynamic> json) =>
      _$DataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$DataBeanToJson(this);
}

@JsonSerializable()
class RecommandBean {
  String postId;
  String url;
  String save;
  String img;
  String title;
  String date;

  RecommandBean(
      {required this.postId,
      required this.url,
      required this.save,
      required this.img,
      required this.title,
      required this.date});

  factory RecommandBean.fromJson(Map<String, dynamic> json) =>
      _$RecommandBeanFromJson(json);

  Map<String, dynamic> toJson() => _$RecommandBeanToJson(this);
}
