// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'health_life_news_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HealthLifeNewsResponse _$HealthLifeNewsResponseFromJson(
        Map<String, dynamic> json) =>
    HealthLifeNewsResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: json['data'] == null
          ? null
          : HealthLifeNewsDataBean.fromJson(
              json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$HealthLifeNewsResponseToJson(
        HealthLifeNewsResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

HealthLifeNewsDataBean _$HealthLifeNewsDataBeanFromJson(
        Map<String, dynamic> json) =>
    HealthLifeNewsDataBean(
      search: (json['search'] as List<dynamic>?)
          ?.map((e) => HealthLifeSearch.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$HealthLifeNewsDataBeanToJson(
        HealthLifeNewsDataBean instance) =>
    <String, dynamic>{
      'search': instance.search,
    };

HealthLifeSearch _$HealthLifeSearchFromJson(Map<String, dynamic> json) =>
    HealthLifeSearch(
      postId: json['postId'] as String,
      imgUrl: json['imgUrl'] as String,
      postDate: json['postDate'] as String,
      postTitle: json['postTitle'] as String,
      viewed: json['viewed'] as String,
      save: json['save'] as String,
      clickUrl: json['clickUrl'] as String,
    );

Map<String, dynamic> _$HealthLifeSearchToJson(HealthLifeSearch instance) =>
    <String, dynamic>{
      'postId': instance.postId,
      'imgUrl': instance.imgUrl,
      'postDate': instance.postDate,
      'postTitle': instance.postTitle,
      'viewed': instance.viewed,
      'save': instance.save,
      'clickUrl': instance.clickUrl,
    };
