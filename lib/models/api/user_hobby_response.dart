import 'package:json_annotation/json_annotation.dart';

part 'user_hobby_response.g.dart';

@JsonSerializable()
class UserHobbyResponse {
  String message;
  String code;
  List<UserHobbyDataBean> data;

  UserHobbyResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory UserHobbyResponse.fromJson(Map<String, dynamic> json) =>
      _$UserHobbyResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserHobbyResponseToJson(this);
}

@JsonSerializable()
class UserHobbyDataBean {
  String id;
  String name;

  UserHobbyDataBean({
    required this.id,
    required this.name,
  });

  factory UserHobbyDataBean.fromJson(Map<String, dynamic> json) =>
      _$UserHobbyDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$UserHobbyDataBeanToJson(this);
}
