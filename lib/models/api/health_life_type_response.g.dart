// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'health_life_type_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HealthLifeTypeResponse _$HealthLifeTypeResponseFromJson(
        Map<String, dynamic> json) =>
    HealthLifeTypeResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>)
          .map(
              (e) => HealthLifeTypeDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$HealthLifeTypeResponseToJson(
        HealthLifeTypeResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

HealthLifeTypeDataBean _$HealthLifeTypeDataBeanFromJson(
        Map<String, dynamic> json) =>
    HealthLifeTypeDataBean(
      cat_id: json['cat_id'] as String,
      name: json['name'] as String,
      subCatName: (json['subCatName'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      isDefault: json['default'] as String?,
      defaultData: (json['defaultData'] as List<dynamic>?)
          ?.map((e) => DefaultData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$HealthLifeTypeDataBeanToJson(
        HealthLifeTypeDataBean instance) =>
    <String, dynamic>{
      'cat_id': instance.cat_id,
      'name': instance.name,
      'subCatName': instance.subCatName,
      'default': instance.isDefault,
      'defaultData': instance.defaultData,
    };

DefaultData _$DefaultDataFromJson(Map<String, dynamic> json) => DefaultData(
      postId: json['postId'] as String,
      imgUrl: json['imgUrl'] as String,
      postDate: json['postDate'] as String,
      postTitle: json['postTitle'] as String,
      viewed: json['viewed'] as String,
      save: json['save'] as String,
      clickUrl: json['clickUrl'] as String,
    );

Map<String, dynamic> _$DefaultDataToJson(DefaultData instance) =>
    <String, dynamic>{
      'postId': instance.postId,
      'imgUrl': instance.imgUrl,
      'postDate': instance.postDate,
      'postTitle': instance.postTitle,
      'viewed': instance.viewed,
      'save': instance.save,
      'clickUrl': instance.clickUrl,
    };
