import 'package:json_annotation/json_annotation.dart';

part 'health_life_type_response.g.dart';

@JsonSerializable()
class HealthLifeTypeResponse {
  String message;
  String code;
  List<HealthLifeTypeDataBean> data;

  HealthLifeTypeResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory HealthLifeTypeResponse.fromJson(Map<String, dynamic> json) =>
      _$HealthLifeTypeResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HealthLifeTypeResponseToJson(this);
}

@JsonSerializable()
class HealthLifeTypeDataBean {
  String cat_id;
  String name;
  List<String> subCatName;
  @JsonKey(name: 'default')
  String? isDefault;
  List<DefaultData>? defaultData;

  HealthLifeTypeDataBean({
    required this.cat_id,
    required this.name,
    required this.subCatName,
    this.isDefault,
    this.defaultData,
  });

  factory HealthLifeTypeDataBean.fromJson(Map<String, dynamic> json) =>
      _$HealthLifeTypeDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$HealthLifeTypeDataBeanToJson(this);
}

@JsonSerializable()
class DefaultData {
  String postId;
  String imgUrl;
  String postDate;
  String postTitle;
  String viewed;
  String save;
  String clickUrl;

  DefaultData({
    required this.postId,
    required this.imgUrl,
    required this.postDate,
    required this.postTitle,
    required this.viewed,
    required this.save,
    required this.clickUrl,
  });

  factory DefaultData.fromJson(Map<String, dynamic> json) =>
      _$DefaultDataFromJson(json);

  Map<String, dynamic> toJson() => _$DefaultDataToJson(this);
}
