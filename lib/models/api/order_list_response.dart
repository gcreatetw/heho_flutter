import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'order_list_response.g.dart';

@JsonSerializable()
class OrderListResponse {
  String message;
  String code;
  List<OrderDataBean>? data;

  OrderListResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory OrderListResponse.fromJson(Map<String, dynamic> json) =>
      _$OrderListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$OrderListResponseToJson(this);
}

@JsonSerializable()
class OrderDataBean {
  String order_id;
  String order_status;
  String total;
  String date_added;
  String payment_method;
  String quantity;

  OrderDataBean({
    required this.order_id,
    required this.order_status,
    required this.total,
    required this.date_added,
    required this.payment_method,
    required this.quantity,
  });

  factory OrderDataBean.fromJson(Map<String, dynamic> json) =>
      _$OrderDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$OrderDataBeanToJson(this);

  factory OrderDataBean.fromRawJson(String str) => OrderDataBean.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
