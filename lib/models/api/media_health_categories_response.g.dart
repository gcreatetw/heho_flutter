// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media_health_categories_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MediaHealthCategoriesResponse _$MediaHealthCategoriesResponseFromJson(
        Map<String, dynamic> json) =>
    MediaHealthCategoriesResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: (json['data'] as List<dynamic>)
          .map((e) =>
              MediaHealthCategoryBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MediaHealthCategoriesResponseToJson(
        MediaHealthCategoriesResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };

MediaHealthCategoryBean _$MediaHealthCategoryBeanFromJson(
        Map<String, dynamic> json) =>
    MediaHealthCategoryBean(
      name: json['name'] as String,
      paperid: json['paperid'] as String,
      external: json['external'] as String?,
      iconStyle: json['iconStyle'] as String?,
      iconCode: json['iconCode'] as String?,
      isDefault: json['default'] as String?,
      defaultData: (json['defaultData'] as List<dynamic>?)
          ?.map((e) => ArticleBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MediaHealthCategoryBeanToJson(
        MediaHealthCategoryBean instance) =>
    <String, dynamic>{
      'name': instance.name,
      'paperid': instance.paperid,
      'external': instance.external,
      'iconStyle': instance.iconStyle,
      'iconCode': instance.iconCode,
      'default': instance.isDefault,
      'defaultData': instance.defaultData,
    };
