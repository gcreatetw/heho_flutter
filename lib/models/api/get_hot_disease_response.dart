import 'package:json_annotation/json_annotation.dart';

part 'get_hot_disease_response.g.dart';

@JsonSerializable()
class GetHotDiseaseResponse {
  String message;
  String code;
  List<String> data;

  GetHotDiseaseResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory GetHotDiseaseResponse.fromJson(Map<String, dynamic> json) =>
      _$GetHotDiseaseResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetHotDiseaseResponseToJson(this);
}
