import 'package:json_annotation/json_annotation.dart';

part 'advance_search_response.g.dart';

@JsonSerializable()
class AdvanceSearchResponse {
  String message;
  String code;
  Data data;

  AdvanceSearchResponse({
    required this.message,
    required this.code,
    required this.data,
  });

  factory AdvanceSearchResponse.fromJson(Map<String, dynamic> json) =>
      _$AdvanceSearchResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AdvanceSearchResponseToJson(this);
}

@JsonSerializable()
class Data {
  List<Search>? search;
  Disease? disease;

  Data({
    required this.search,
    this.disease,
  });

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Search {
  String postId;
  String imgUrl;
  String postDate;
  String postTitle;
  String viewed;
  String save;
  String clickUrl;

  Search(
      {required this.postId,
      required this.imgUrl,
      required this.postDate,
      required this.postTitle,
      required this.viewed,
      required this.save,
      required this.clickUrl});

  factory Search.fromJson(Map<String, dynamic> json) => _$SearchFromJson(json);

  Map<String, dynamic> toJson() => _$SearchToJson(this);
}

@JsonSerializable()
class Disease {
  String title;
  String subTitle;
  String titleDesc;
  String tag;
  String? dataSource;

  Disease({
    required this.title,
    required this.subTitle,
    required this.titleDesc,
    required this.tag,
    this.dataSource,
  });

  factory Disease.fromJson(Map<String, dynamic> json) =>
      _$DiseaseFromJson(json);

  Map<String, dynamic> toJson() => _$DiseaseToJson(this);
}
