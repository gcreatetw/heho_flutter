import 'package:json_annotation/json_annotation.dart';

part 'weather_info_response.g.dart';

@JsonSerializable()
class WeatherInfoResponse {
  DataBean data;
  String message;
  String code;

  WeatherInfoResponse({
    required this.data,
    required this.message,
    required this.code,
  });

  factory WeatherInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$WeatherInfoResponseFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherInfoResponseToJson(this);
}

@JsonSerializable()
class DataBean {
  HealthCalendarBean healthCalendar;
  WeatherBean weather;

  DataBean({
    required this.healthCalendar,
    required this.weather,
  });

  factory DataBean.fromJson(Map<String, dynamic> json) =>
      _$DataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$DataBeanToJson(this);
}

@JsonSerializable()
class WeatherBean {
  TodayBean today;
  TomorrowBean tomorrow;

  WeatherBean({
    required this.today,
    required this.tomorrow,
  });

  factory WeatherBean.fromJson(Map<String, dynamic> json) =>
      _$WeatherBeanFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherBeanToJson(this);
}

@JsonSerializable()
class TomorrowBean {
  String locationName;
  String locationTownName;
  String temp;
  String rainP;
  String wxName;

  TomorrowBean(
      {required this.locationName,
      required this.locationTownName,
      required this.temp,
      required this.rainP,
      required this.wxName});

  factory TomorrowBean.fromJson(Map<String, dynamic> json) =>
      _$TomorrowBeanFromJson(json);

  Map<String, dynamic> toJson() => _$TomorrowBeanToJson(this);
}

@JsonSerializable()
class TodayBean {
  String locationName;
  String locationTownName;
  String temp;
  String rainP;
  String wxName;
  String wxId;
  String aqi;
  String aqiDesc;
  String uvi;
  String uviDesc;

  TodayBean({
    required this.locationName,
    required this.locationTownName,
    required this.temp,
    required this.rainP,
    required this.wxName,
    required this.wxId,
    required this.aqi,
    required this.aqiDesc,
    required this.uvi,
    required this.uviDesc,
  });

  factory TodayBean.fromJson(Map<String, dynamic> json) =>
      _$TodayBeanFromJson(json);

  Map<String, dynamic> toJson() => _$TodayBeanToJson(this);
}

@JsonSerializable()
class HealthCalendarBean {
  String url;

  HealthCalendarBean({
    required this.url,
  });

  factory HealthCalendarBean.fromJson(Map<String, dynamic> json) =>
      _$HealthCalendarBeanFromJson(json);

  Map<String, dynamic> toJson() => _$HealthCalendarBeanToJson(this);
}
