// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'keys_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

KeysResponse _$KeysResponseFromJson(Map<String, dynamic> json) => KeysResponse(
      message: json['message'] as String,
      code: json['code'] as String,
      data: json['data'] as String,
    );

Map<String, dynamic> _$KeysResponseToJson(KeysResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'data': instance.data,
    };
