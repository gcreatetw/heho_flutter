// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_info_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherInfoResponse _$WeatherInfoResponseFromJson(Map<String, dynamic> json) =>
    WeatherInfoResponse(
      data: DataBean.fromJson(json['data'] as Map<String, dynamic>),
      message: json['message'] as String,
      code: json['code'] as String,
    );

Map<String, dynamic> _$WeatherInfoResponseToJson(
        WeatherInfoResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'message': instance.message,
      'code': instance.code,
    };

DataBean _$DataBeanFromJson(Map<String, dynamic> json) => DataBean(
      healthCalendar: HealthCalendarBean.fromJson(
          json['healthCalendar'] as Map<String, dynamic>),
      weather: WeatherBean.fromJson(json['weather'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DataBeanToJson(DataBean instance) => <String, dynamic>{
      'healthCalendar': instance.healthCalendar,
      'weather': instance.weather,
    };

WeatherBean _$WeatherBeanFromJson(Map<String, dynamic> json) => WeatherBean(
      today: TodayBean.fromJson(json['today'] as Map<String, dynamic>),
      tomorrow: TomorrowBean.fromJson(json['tomorrow'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$WeatherBeanToJson(WeatherBean instance) =>
    <String, dynamic>{
      'today': instance.today,
      'tomorrow': instance.tomorrow,
    };

TomorrowBean _$TomorrowBeanFromJson(Map<String, dynamic> json) => TomorrowBean(
      locationName: json['locationName'] as String,
      locationTownName: json['locationTownName'] as String,
      temp: json['temp'] as String,
      rainP: json['rainP'] as String,
      wxName: json['wxName'] as String,
    );

Map<String, dynamic> _$TomorrowBeanToJson(TomorrowBean instance) =>
    <String, dynamic>{
      'locationName': instance.locationName,
      'locationTownName': instance.locationTownName,
      'temp': instance.temp,
      'rainP': instance.rainP,
      'wxName': instance.wxName,
    };

TodayBean _$TodayBeanFromJson(Map<String, dynamic> json) => TodayBean(
      locationName: json['locationName'] as String,
      locationTownName: json['locationTownName'] as String,
      temp: json['temp'] as String,
      rainP: json['rainP'] as String,
      wxName: json['wxName'] as String,
      wxId: json['wxId'] as String,
      aqi: json['aqi'] as String,
      aqiDesc: json['aqiDesc'] as String,
      uvi: json['uvi'] as String,
      uviDesc: json['uviDesc'] as String,
    );

Map<String, dynamic> _$TodayBeanToJson(TodayBean instance) => <String, dynamic>{
      'locationName': instance.locationName,
      'locationTownName': instance.locationTownName,
      'temp': instance.temp,
      'rainP': instance.rainP,
      'wxName': instance.wxName,
      'wxId': instance.wxId,
      'aqi': instance.aqi,
      'aqiDesc': instance.aqiDesc,
      'uvi': instance.uvi,
      'uviDesc': instance.uviDesc,
    };

HealthCalendarBean _$HealthCalendarBeanFromJson(Map<String, dynamic> json) =>
    HealthCalendarBean(
      url: json['url'] as String,
    );

Map<String, dynamic> _$HealthCalendarBeanToJson(HealthCalendarBean instance) =>
    <String, dynamic>{
      'url': instance.url,
    };
