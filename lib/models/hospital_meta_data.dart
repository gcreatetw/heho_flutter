import 'package:flutter/cupertino.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/hospital_option_response.dart';
import 'package:hepo_app/models/filter_data.dart';
import 'package:hepo_app/models/general_state.dart';

class HospitalMetaData extends ChangeNotifier {
  GeneralState state = GeneralState.loading;

  List<Area> areas;

  ///院所類型
  FilterData types;

  ///科別類型
  FilterData departments;

  HospitalMetaData({
    required this.areas,
    required this.types,
    required this.departments,
  });

  factory HospitalMetaData.init() {
    return HospitalMetaData(
      areas: <Area>[],
      types: FilterData.initHospitalType(),
      departments: FilterData.initDepartment(),
    );
  }

  factory HospitalMetaData.sample() {
    return HospitalMetaData(
      areas: <Area>[
        Area(
          name: '北部',
          items: <City>[
            City(id: 'element', name: '台北市'),
            City(id: 'element', name: '新北市'),
            City(id: 'element', name: '基隆市'),
            City(id: 'element', name: '桃園市'),
            City(id: 'element', name: '新竹縣/市'),
            City(id: 'element', name: '宜蘭縣'),
          ],
        ),
        Area(
          name: '中部',
          items: <City>[
            City(id: 'element', name: '苗栗縣'),
            City(id: 'element', name: '台中市'),
            City(id: 'element', name: '彰化縣'),
            City(id: 'element', name: '南投縣'),
            City(id: 'element', name: '雲林縣'),
          ],
        ),
        Area(
          name: '南部',
          items: <City>[
            City(id: 'element', name: '嘉義縣/市'),
            City(id: 'element', name: '台南市'),
            City(id: 'element', name: '高雄市'),
            City(id: 'element', name: '屏東縣'),
          ],
        ),
        Area(
          name: '東部及離島',
          items: <City>[
            City(id: 'element', name: '花蓮縣'),
            City(id: 'element', name: '台東縣'),
            City(id: 'element', name: '澎湖縣'),
            City(id: 'element', name: '金門縣'),
            City(id: 'element', name: '連江縣'),
          ],
        ),
      ],
      types: FilterData(
        items: <FilterItem>[],
      ),
      departments: FilterData(
        items: <FilterItem>[],
      ),
    );
  }

  void fetch() {
    Helper.instance.getHospitalOption(
      callback: GeneralCallback<HospitalOptionResponse>(
        onError: (GeneralResponse e) {
          state = GeneralState.error;
          notifyListeners();
        },
        onSuccess: (HospitalOptionResponse r) {
          final DataBean dataBean = r.data;
          areas.addAll(
            <Area>[
              Area(
                name: '北部',
                items: <City>[
                  for (String element in dataBean.north)
                    City(id: element, name: element),
                ],
              ),
              Area(
                name: '中部',
                items: <City>[
                  for (String element in dataBean.mid)
                    City(id: element, name: element),
                ],
              ),
              Area(
                name: '南部',
                items: <City>[
                  for (String element in dataBean.south)
                    City(id: element, name: element),
                ],
              ),
              Area(
                name: '東部及離島',
                items: <City>[
                  for (String element in dataBean.others)
                    City(id: element, name: element),
                ],
              ),
            ],
          );
          final List<FilterItem> _typeItems = <FilterItem>[];
          for (final String element in dataBean.category) {
            _typeItems.add(
              FilterItem(
                name: element,
              ),
            );
          }
          types.items.addAll(_typeItems);
          for (final String key in dataBean.service.keys) {
            if (dataBean.service[key] == null) continue;
            departments.items.add(
              FilterItem(
                name: dataBean.service[key]!.name,
              ),
            );
          }
          state = GeneralState.finished;
          notifyListeners();
        },
      ),
    );
  }
}

class City {
  String id = '';
  String name;

  City({
    required this.id,
    required this.name,
  });
}

class Area {
  String name;
  List<City> items;

  Area({
    required this.name,
    required this.items,
  });
}
