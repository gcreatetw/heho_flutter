import 'package:flutter/cupertino.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/member_collection_response.dart';
import 'package:hepo_app/models/general_state.dart';
import 'package:hepo_app/models/hospital_data.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:intl/intl.dart';

extension _HospitalBeanEx on List<HospitalBean> {
  List<Hospital> toModelList(String type) {
    return <Hospital>[
      for (HospitalBean element in this)
        Hospital(
          id: element.targetValue,
          address: element.address,
          distance: '',
          phoneNumber: element.tel,
          name: element.name,
          type: type,
        ),
    ];
  }
}

class CollectionData extends ChangeNotifier {
  GeneralState state = GeneralState.loading;
  String? hint;

  List<News> articles = <News>[];
  List<String> diseases = <String>[];
  List<String> symptoms = <String>[];
  List<Hospital> hospitals = <Hospital>[];

  CollectionData();

  factory CollectionData.init() {
    return CollectionData();
  }

  void _loadSample() {
    articles.addAll(News.sampleList(hasBig: false));
    diseases.addAll(
      <String>[
        '憂鬱症',
        '敗血症',
        '新冠肺炎',
        '經前症候群',
        '脊椎側彎',
      ],
    );
    symptoms.addAll(
      <String>[
        '肚子痛',
        '脹氣',
        '頭痛',
        '消化不涼',
        '頻尿',
      ],
    );
    hospitals.addAll(
      <Hospital>[
        Hospital(
          id: '1',
          name: '瑞光眼科診所',
          departments: <String>['眼科'],
          type: '特約診所',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
        Hospital(
          id: '1',
          name: '瑞光婦產科診所',
          departments: <String>['婦產科'],
          type: '特約診所',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
        Hospital(
          id: '1',
          name: '晨間婦產科診所',
          departments: <String>['婦產科'],
          type: '特約診所',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
        Hospital(
          id: '1',
          name: '養樂多中醫診所',
          departments: <String>['中醫'],
          type: '特約診所',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
        Hospital(
          id: '1',
          name: '瑞光醫院',
          departments: <String>['眼科', '婦產科'],
          type: '地區醫院',
          distance: '2.2',
          address: '臺北市中山區南京東路1段25號、25號2樓',
          phoneNumber: '02-2523694',
        ),
      ],
    );
  }

  Future<void> fetch() async {
    state = GeneralState.loading;
    hint = null;
    notifyListeners();
    Helper.instance.getMemberCollection(
      callback: GeneralCallback<MemberCollectionResponse>(
        onError: (GeneralResponse e) {
          hint = e.message;
          state = GeneralState.error;
          notifyListeners();
        },
        onSuccess: (MemberCollectionResponse r) {
          final MemberCollectionDataBean bean = r.data;
          articles.clear();
          for (final PostBean element in bean.post ?? <PostBean>[]) {
            articles.add(
              News(
                id: element.targetValue,
                title: element.postTitle,
                dateTime: DateFormat('yyyy-MM-dd').parse(element.postDate),
                link: element.clickUrl,
                type: NewsType.normal,
                imageUrl: element.imgUrl,
              ),
            );
          }
          diseases = bean.disease ?? <String>[];
          symptoms = bean.symptom ?? <String>[];
          hospitals.clear();
          if (bean.hospital != null) {
            final HospitalCollectionBean collectionBean = bean.hospital!;
            hospitals.addAll(
              collectionBean.clinic?.toModelList(HospitalType.clinic.text) ??
                  <Hospital>[],
            );
            hospitals.addAll(
              collectionBean.medicalcenter
                      ?.toModelList(HospitalType.medicalCenter.text) ??
                  <Hospital>[],
            );
            hospitals.addAll(
              collectionBean.regional
                      ?.toModelList(HospitalType.regionalHospital.text) ??
                  <Hospital>[],
            );
            hospitals.addAll(
              collectionBean.local
                      ?.toModelList(HospitalType.localHospital.text) ??
                  <Hospital>[],
            );
            state = GeneralState.finished;
            notifyListeners();
          }
        },
      ),
    );
  }

  bool isArticleCollection(News data) {
    final List<News> result = articles
        .where(
          (News element) => element.id == data.id,
        )
        .toList();
    return result.isNotEmpty;
  }

  bool isDiseaseCollection(String data) {
    return diseases.contains(data);
  }

  bool isSymptomCollection(String data) {
    return symptoms.contains(data);
  }

  bool isHospitalCollection(Hospital data) {
    final List<Hospital> result = hospitals
        .where(
          (Hospital element) => element.id == data.id,
        )
        .toList();
    return result.isNotEmpty;
  }

  void clear() {
    articles.clear();
    diseases.clear();
    symptoms.clear();
    hospitals.clear();
    notifyListeners();
  }
}
