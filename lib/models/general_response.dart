import 'dart:convert';

import 'package:flutter/material.dart' show BuildContext;
import 'package:hepo_app/utils/toast.dart';
import 'package:json_annotation/json_annotation.dart';

part 'general_response.g.dart';

@JsonSerializable()
class GeneralResponse {
  @JsonKey(name: 'code')
  String statusCode;
  @JsonKey(name: 'message')
  String message;

  GeneralResponse({
    required this.statusCode,
    required this.message,
  });

  factory GeneralResponse.success() => GeneralResponse(
        statusCode: '200',
        message: '成功',
      );

  void showToast(BuildContext context) => Toast.show(context, message);

  factory GeneralResponse.fromJson(Map<String, dynamic> json) =>
      _$GeneralResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GeneralResponseToJson(this);

  factory GeneralResponse.fromRawJson(String str) => GeneralResponse.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
