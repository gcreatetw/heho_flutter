import 'package:flutter/cupertino.dart';
import 'package:hepo_app/models/general_state.dart';
import 'package:hepo_app/resources/colors.dart';

@Deprecated('無使用類別')
class MediaHealthData extends ChangeNotifier {
  List<MediaHealth> list;

  String? keyword;

  GeneralState state = GeneralState.loading;

  bool get hasResult => list.isNotEmpty;

  MediaHealthData({
    required this.list,
    this.keyword,
  });

  factory MediaHealthData.init() {
    return MediaHealthData(
      list: <MediaHealth>[],
    );
  }

  void _loadHealthLifeSample1(bool hasBig) {
    list = MediaHealth.sampleList(hasBig: hasBig);
  }

  Future<void> fetch({String? id, String? keyword}) async {
    state = GeneralState.loading;
    notifyListeners();
    await Future<void>.delayed(const Duration(milliseconds: 600));
    if (id != null) {
      _loadHealthLifeSample1(true);
    } else if (keyword != null) {
      _loadHealthLifeSample1(false);
    }
    state = GeneralState.finished;
    notifyListeners();
  }
}

enum MediaHealthType {
  big,
  normal,
  advertisement,
}

enum MediaHealthTagType {
  hot,
  recommend,
  live,
}

extension MediaHealthTagTypeEx on MediaHealthTagType {
  String get text {
    switch (this) {
      case MediaHealthTagType.hot:
        return '熱門';
      case MediaHealthTagType.recommend:
        return '推薦';
      case MediaHealthTagType.live:
        return '直播';
    }
  }

  Color get color {
    switch (this) {
      case MediaHealthTagType.hot:
        return AppColors.orangeYellow;
      case MediaHealthTagType.recommend:
        return AppColors.darkOliveGreen;
      case MediaHealthTagType.live:
        return AppColors.darkRed;
    }
  }
}

class MediaHealth extends ChangeNotifier {
  MediaHealthType type;

  String imageUrl;
  String title;
  String link;
  DateTime dateTime;
  int views;
  bool isCollected;

  MediaHealthTagType? tagType;

  MediaHealth({
    required this.type,
    required this.imageUrl,
    required this.title,
    required this.link,
    required this.views,
    required this.dateTime,
    this.tagType,
    this.isCollected = false,
  });

  String get shareText => '$title\n$link';

  static List<MediaHealth> sampleList({bool hasBig = true}) {
    return <MediaHealth>[
      MediaHealth(
        type: hasBig ? MediaHealthType.big : MediaHealthType.normal,
        title: '你喝的是果汁還是化學添加劑？營養師示範製作「10% 葡萄汁」',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        views: 2314,
        link: 'https://www.youtube.com/channel/',
        tagType: MediaHealthTagType.hot,
        dateTime: DateTime.now(),
      ),
      MediaHealth(
        type: MediaHealthType.normal,
        title: '子宮肌瘤一定要手術嗎？婦產科醫師：從症狀、有無影響生活做判斷',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        views: 2314,
        link: 'https://www.youtube.com/channel/',
        tagType: MediaHealthTagType.recommend,
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      MediaHealth(
        type: MediaHealthType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        views: 2314,
        link: 'https://www.youtube.com/channel/',
        tagType: MediaHealthTagType.hot,
        dateTime: DateTime.now().subtract(const Duration(days: 2)),
      ),
      MediaHealth(
        type: MediaHealthType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        views: 2314,
        link: 'https://www.youtube.com/channel/',
        tagType: MediaHealthTagType.live,
        dateTime: DateTime.now().subtract(const Duration(days: 1)),
      ),
      MediaHealth(
        type: MediaHealthType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        views: 2314,
        link: 'https://www.youtube.com/channel/',
        dateTime: DateTime.now().subtract(const Duration(days: 2)),
      ),
      MediaHealth(
        type: MediaHealthType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        views: 2314,
        link: 'https://www.youtube.com/channel/',
        dateTime: DateTime.now().subtract(const Duration(days: 30)),
      ),
      MediaHealth(
        type: MediaHealthType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        views: 2314,
        link: 'https://www.youtube.com/channel/',
        dateTime: DateTime.now().subtract(const Duration(days: 60)),
      ),
      MediaHealth(
        type: MediaHealthType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        views: 2314,
        link: 'https://www.youtube.com/channel/',
        dateTime: DateTime.now().subtract(const Duration(days: 368)),
      ),
      MediaHealth(
        type: MediaHealthType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        views: 2314,
        link: 'https://www.youtube.com/channel/',
        dateTime: DateTime.now().subtract(const Duration(days: 3670)),
      ),
    ];
  }
}
