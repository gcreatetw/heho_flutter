import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/action_type.dart';
import 'package:hepo_app/models/api/advance_search_response.dart';
import 'package:hepo_app/models/api/article_response.dart';
import 'package:hepo_app/models/api/collection_type.dart';
import 'package:hepo_app/models/api/health_life_news_response.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/models/collection_data.dart';
import 'package:hepo_app/models/general_callback.dart';
import 'package:hepo_app/models/general_state.dart';
import 'package:hepo_app/pages/collect/collect_news_list_page.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/toast.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'api/home_keyword_response.dart';
import 'disease_data.dart';

const int _kDefaultPage = 1;
const int _kDefaultLimit = 10;
const int _kAdBetween = 8;

class NewsData extends ChangeNotifier {
  List<News> list;
  List<News> hot;
  List<News> recommend;

  List<String> tags;

  String? keyword;

  DiseaseData? disease;

  GeneralState state = GeneralState.loading;

  String? hint;

  ///2021/10/15 修改:recommend to list
  List<List<News>> get advancedResult => <List<News>>[
        hot,
        list,
      ];

  bool get hasResult => list.isNotEmpty || hot.isNotEmpty;

  int page = _kDefaultPage;

  int pageHot = _kDefaultPage;

  int pageNew = _kDefaultPage;

  int limit = _kDefaultLimit;

  bool canLoadMore = false;

  bool loading = false;

  final Random random = Random();

  NewsData({
    required this.list,
    required this.hot,
    required this.recommend,
    required this.tags,
    this.disease,
    this.keyword,
  });

  factory NewsData.init() {
    return NewsData(
      list: <News>[],
      hot: <News>[],
      recommend: <News>[],
      tags: <String>[],
    );
  }

  factory NewsData.sample() {
    return NewsData(
      list: News.sampleList(),
      hot: <News>[],
      recommend: <News>[],
      tags: <String>[],
    );
  }

  factory NewsData.advancedSample() {
    return NewsData(
      list: News.sampleList(hasBig: false),
      hot: News.sampleList(hasBig: false),
      recommend: <News>[],
      tags: <String>[],
    );
  }

  factory NewsData.diseaseSample({DiseaseData? diseaseData}) {
    return NewsData(
      disease: diseaseData ?? DiseaseData.sample1(),
      list: News.sampleList(hasBig: false),
      hot: News.sampleList(hasBig: false),
      recommend: News.sampleList(hasBig: false),
      tags: <String>[],
    );
  }

  factory NewsData.noResultSample() {
    return NewsData(
      list: <News>[],
      hot: <News>[],
      recommend: News.sampleList(hasBig: false),
      tags: <String>[],
    );
  }

  void _loadHealthLifeSample1() {
    list = News.sampleList2();
    tags = <String>[
      '#168斷食法',
      '#兒童飲食',
      '#營養師',
      '#睡前溫開水',
    ];
  }

  void _loadHealthLifeSample2() {
    list = News.sampleList2();
    tags = <String>[
      '#冬蟲夏草',
      '#新冠一號',
    ];
  }

  ///2021/10/15 獲取hot、new、recommend api資料
  Future<void> fetchAdvanceSearch(BuildContext context, String? keyword) async {
    state = GeneralState.loading;

    ///取得 熱門 搜尋結果
    await Helper.instance.getAdvanceSearch(
        query: keyword,
        log: '1',
        sort: 'hot',
        limit: limit,
        page: page,
        callback: GeneralCallback<AdvanceSearchResponse>(
            onError: (GeneralResponse e) {
          e.showToast(context);
          state = GeneralState.error;
        }, onSuccess: (AdvanceSearchResponse r) {
          final Data data = r.data;
          final List<News> newsList = <News>[];
          if (data.search != null) {
            for (int i = 0; i < data.search!.length; i++) {
              newsList.add(
                News(
                  id: data.search![i].postId,
                  type: NewsType.normal,
                  imageUrl: data.search![i].imgUrl,
                  title: data.search![i].postTitle,
                  link: data.search![i].clickUrl,
                  views: int.tryParse(data.search![i].viewed) ?? 0,
                  dateTime: DateTime.parse(data.search![i].postDate),
                ),
              );
            }
            canLoadMore = data.search!.length == limit;
          }
          hot = newsList;
          if (data.disease != null) {
            disease = DiseaseData(
              name: data.disease!.title,
              typeName: data.disease!.tag,
              subTitle: data.disease!.subTitle,
              introduction: data.disease!.titleDesc,
              isCollected: false,
            );
          } else {
            disease = null;
          }
          notifyListeners();
        }));

    ///取得 最新 搜尋結果
    await Helper.instance.getAdvanceSearch(
      query: keyword,
      log: '1',
      sort: 'new',
      limit: limit,
      page: page,
      callback: GeneralCallback<AdvanceSearchResponse>(
        onError: (GeneralResponse e) {
          e.showToast(context);
          state = GeneralState.error;
        },
        onSuccess: (AdvanceSearchResponse r) {
          final Data data = r.data;
          final List<News> newsList = <News>[];
          if (data.search != null) {
            for (int i = 0; i < data.search!.length; i++) {
              newsList.add(
                News(
                  id: data.search![i].postId,
                  type: NewsType.normal,
                  imageUrl: data.search![i].imgUrl,
                  title: data.search![i].postTitle,
                  link: data.search![i].clickUrl,
                  dateTime: DateTime.parse(data.search![i].postDate),
                ),
              );
            }
            canLoadMore = data.search!.length == limit;
          }
          list = newsList;
          notifyListeners();
        },
      ),
    );

    ///取得 推薦 搜尋結果
    if (hot.isEmpty || list.isEmpty) {
      await Helper.instance.getHomeKeyWordInfo(
        callback: GeneralCallback<HomeKeywordResponse>(
          onError: (GeneralResponse e) {
            e.showToast(context);
            state = GeneralState.error;
          },
          onSuccess: (HomeKeywordResponse r) {
            final List<News> recommendList = <News>[];
            for (int i = 0; i < r.data.recommand.length; i++) {
              recommendList.add(
                News(
                  id: r.data.recommand[i].postId,
                  type: NewsType.normal,
                  imageUrl: r.data.recommand[i].img,
                  title: r.data.recommand[i].title,
                  link: r.data.recommand[i].url,
                  dateTime: DateTime.parse(r.data.recommand[i].date),
                ),
              );
            }
            recommend = recommendList;
            notifyListeners();
          },
        ),
      );
    }
    state = GeneralState.finished;
    notifyListeners();
  }

  Future<void> fetch({
    bool append = false,
    String? id,
    String? keyword,
    int? currentIndex = 0,
    int? advStart,
    int? advBetween,
  }) async {
    if (!append) {
      list.clear();
      pageNew = _kDefaultPage;
      pageHot = _kDefaultPage;
      page = _kDefaultPage;
      state = GeneralState.loading;
      hint = null;
      notifyListeners();
    } else if (canLoadMore) {
      page++;
      loading = true;
      notifyListeners();
      canLoadMore = false;
    } else {
      return;
    }
    if (kDebugMode) {
      debugPrint(
          'fetch home current page = $page data, canLoadMore = $canLoadMore');
    }
    if (id != null) {
      Helper.instance.getHomeArticles(
        id: id,
        page: page,
        limit: limit,
        callback: GeneralCallback<ArticleResponse>(
          onError: (GeneralResponse e) {
            hint = e.message;
            state = GeneralState.error;
            notifyListeners();
          },
          onSuccess: (ArticleResponse r) {
            final List<ArticleBean> dataList =
                r.data?.search ?? <ArticleBean>[];
            for (int i = 0; i < dataList.length; i++) {
              NewsType newsType = NewsType.normal;


              if (advBetween != null &&
                  advStart != null &&
                  (list.length - advStart) % (advBetween + 1) == 0) {
                list.add(
                  News.advertisement(),
                );
              } else if (list.isNotEmpty &&
                  advBetween == null &&
                  advStart == null &&
                  (list.length + 1) % _kAdBetween == 0) {
                list.add(
                  News.advertisement(),
                );
              }

              if (list.isEmpty && page == 1) {
                newsType = NewsType.big;
              }
              final News data = News(
                id: dataList[i].postId!,
                type: newsType,
                title: dataList[i].postTitle,
                imageUrl: dataList[i].imgUrl,
                dateTime: DateFormat('yyyy-MM-dd HH:mm:ss')
                    .parse(dataList[i].postDate),
                views: int.tryParse(dataList[i].viewed) ?? 0,
                link: dataList[i].clickUrl,
                // isCollected: element.save == '1',
              );
              list.add(data);
            }
            canLoadMore = dataList.length == limit;
            state = GeneralState.finished;
            loading = false;
            notifyListeners();
          },
        ),
      );
    } else if (keyword != null) {
      String sort = 'hot';
      if (currentIndex == 0) {
        sort = 'hot';
        pageHot++;
        page = pageHot;
      } else {
        sort = 'new';
        pageNew++;
        page = pageNew;
      }
      Helper.instance.getAdvanceSearch(
        query: keyword,
        log: '1',
        sort: sort,
        page: page,
        limit: limit,
        callback: GeneralCallback<AdvanceSearchResponse>(
          onError: (GeneralResponse e) {
            hint = e.message;
            state = GeneralState.error;
            notifyListeners();
          },
          onSuccess: (AdvanceSearchResponse r) {
            final List<Search> data = r.data.search ?? <Search>[];
            for (final Search element in data) {
              if (currentIndex == 0) {
                hot.add(
                  News(
                    id: element.postId,
                    type: NewsType.normal,
                    imageUrl: element.imgUrl,
                    title: element.postTitle,
                    link: element.clickUrl,
                    views: int.tryParse(element.viewed) ?? 0,
                    dateTime: DateTime.parse(element.postDate),
                  ),
                );
              } else {
                list.add(
                  News(
                    id: element.postId,
                    type: NewsType.normal,
                    imageUrl: element.imgUrl,
                    title: element.postTitle,
                    link: element.clickUrl,
                    views: int.tryParse(element.viewed) ?? 0,
                    dateTime: DateTime.parse(element.postDate),
                  ),
                );
              }
            }
            canLoadMore = data.length == limit;
            state = GeneralState.finished;
            loading = false;
            notifyListeners();
          },
        ),
      );
    }
  }

  Future<void> fetchHealthLife({
    bool append = false,
    String? id,
    String? query,
    String? sort,
  }) async {
    if (!append) {
      list.clear();
      pageNew = _kDefaultPage;
      pageHot = _kDefaultPage;
      page = _kDefaultPage;
      state = GeneralState.loading;
      hint = null;
      notifyListeners();
    } else if (canLoadMore) {
      page++;
      loading = true;
      notifyListeners();
      canLoadMore = false;
    } else {
      return;
    }
    if (kDebugMode) {
      debugPrint(
          'fetch home current page = $page data, canLoadMore = $canLoadMore');
    }
    Helper.instance.getHealthLifeNews(
      page: page,
      limit: limit,
      query: query,
      sort: sort,
      id: id,
      callback: GeneralCallback<HealthLifeNewsResponse>(
        onError: (GeneralResponse e) {
          hint = e.message;
          state = GeneralState.error;
          notifyListeners();
        },
        onSuccess: (HealthLifeNewsResponse r) {
          final List<HealthLifeSearch> data =
              r.data?.search ?? <HealthLifeSearch>[];
          for (final HealthLifeSearch element in data) {
            if (list.isNotEmpty && (list.length + 1) % _kAdBetween == 0) {
              list.add(
                News.advertisement(),
              );
            }
            list.add(
              News(
                id: element.postId,
                type: NewsType.normal,
                imageUrl: element.imgUrl,
                title: element.postTitle,
                link: element.clickUrl,
                views: int.parse(element.viewed),
                dateTime: DateTime.parse(
                  element.postDate,
                ),
              ),
            );
          }
          canLoadMore = data.length == limit;
          state = GeneralState.finished;
          loading = false;
          notifyListeners();
        },
      ),
    );
  }
}

enum NewsType {
  big,
  normal,
  advertisement,
}

class News extends ChangeNotifier {
  NewsType type;

  String id;
  String imageUrl;
  String title;
  String link;
  int? views;
  DateTime dateTime;

  News({
    required this.id,
    required this.type,
    required this.imageUrl,
    required this.title,
    required this.link,
    required this.dateTime,
    this.views,
  });

  factory News.advertisement() {
    return News(
      id: '999',
      type: NewsType.advertisement,
      title: '',
      imageUrl: '',
      link: '',
      dateTime: DateTime.now(),
    );
  }

  bool isCollected(BuildContext context) {
    final CollectionData data = context.read<CollectionData>();
    return data.isArticleCollection(this);
  }

  ///文章標題
  /// 文章網址
  ///
  /// 自訂訊息（API給）
  String shareText(BuildContext context) => '$title\n'
      '$link\n\n'
      '${context.read<AppMetaData>().newsShareEndText}';

  static List<News> sampleList({bool hasBig = true}) {
    return <News>[
      News(
        id: '1',
        type: hasBig ? NewsType.big : NewsType.normal,
        title: '遠離阿茲海默症從年輕就開始預防！21種全球認證降低罹患風險方法',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now(),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '子宮肌瘤一定要手術嗎？婦產科醫師：從症狀、有無影響生活做判斷',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.advertisement,
        title: '',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/06/1623221347.4605.png',
        link:
            'https://myhope.com.tw/product/625/?utm_source=heho&utm_medium=endbanner&utm_campaign=hopextimewell&utm_content=0603mobile',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/07/1627700277.2142.jpg',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
    ];
  }

  static List<News> sampleList2() {
    return <News>[
      News(
        id: '1',
        type: NewsType.normal,
        title: '遠離阿茲海默症從年輕就開始預防！21種全球認證降低罹患風險方法',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now(),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '子宮肌瘤一定要手術嗎？婦產科醫師：從症狀、有無影響生活做判斷',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
      News(
        id: '1',
        type: NewsType.normal,
        title: '跑完步膝蓋好痛！3招提升「髖關節穩定度」改善跑者膝',
        imageUrl:
            'https://img.heho.com.tw/wp-content/uploads/2021/04/1618905104.0039.png',
        link: 'https://heho.com.tw/privacy',
        dateTime: DateTime.now().subtract(const Duration(hours: 2)),
      ),
    ];
  }

  void addToCollection(BuildContext context) {
    Helper.instance.operateMemberCollection(
      type: CollectionType.news,
      action: ActionType.add,
      value: id,
      callback: GeneralCallback<GeneralResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (GeneralResponse r) {
          context.read<CollectionData>().fetch();
          notifyListeners();
          Toast.show(
            context,
            '已添加至我的收藏',
            prefixIcon: IconAssets.collect,
            actionText: '查看項目',
            onActionClick: () {
              final CollectionData data = CollectionData.init();
              data.fetch();
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (_) => ChangeNotifierProvider<CollectionData>.value(
                    value: data,
                    child: Consumer<CollectionData>(
                      builder: (_, CollectionData data, __) {
                        return CollectNewsListPage(
                          data: data.articles,
                        );
                      },
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }

  void removeFromCollection(BuildContext context) {
    Helper.instance.operateMemberCollection(
      type: CollectionType.news,
      action: ActionType.delete,
      value: id,
      callback: GeneralCallback<GeneralResponse>(
        onError: (GeneralResponse e) => e.showToast(context),
        onSuccess: (GeneralResponse r) {
          context.read<CollectionData>().fetch();
          notifyListeners();
          Toast.show(
            context,
            '已取消收藏',
            prefixIcon: IconAssets.collect,
          );
        },
      ),
    );
  }

  void changeCollectState(BuildContext context) {
    if (isCollected(context)) {
      removeFromCollection(context);
    } else {
      addToCollection(context);
    }
  }
}
