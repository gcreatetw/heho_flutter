// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_meta_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppMetaData _$AppMetaDataFromJson(Map<String, dynamic> json) => AppMetaData(
      humanBodyMapUrl: json['humanBodyMapUrl'] as String?,
      medicineSearchUrl: json['medicineSearchUrl'] as String?,
      healthHelperUrl: json['healthHelperUrl'] as String?,
      memberHopeStoreUrl: json['memberHopeStoreUrl'] as String?,
      hopeStoreUrl: json['hopeStoreUrl'] as String?,
      facebookUrl: json['facebookUrl'] as String?,
      lineUrl: json['lineUrl'] as String?,
      youtubeUrl: json['youtubeUrl'] as String?,
      instagramUrl: json['instagramUrl'] as String?,
      servicePolicyUrl: json['servicePolicyUrl'] as String?,
      privacyPolicyUrl: json['privacyPolicyUrl'] as String?,
      orderFormatUrl: json['orderFormatUrl'] as String?,
      followingListUrl: json['followingListUrl'] as String?,
      lineFriendUrl: json['lineFriendUrl'] as String?,
    )..newsShareEndText = json['newsShareEndText'] as String?;

Map<String, dynamic> _$AppMetaDataToJson(AppMetaData instance) =>
    <String, dynamic>{
      'humanBodyMapUrl': instance.humanBodyMapUrl,
      'medicineSearchUrl': instance.medicineSearchUrl,
      'healthHelperUrl': instance.healthHelperUrl,
      'memberHopeStoreUrl': instance.memberHopeStoreUrl,
      'hopeStoreUrl': instance.hopeStoreUrl,
      'facebookUrl': instance.facebookUrl,
      'lineUrl': instance.lineUrl,
      'youtubeUrl': instance.youtubeUrl,
      'instagramUrl': instance.instagramUrl,
      'servicePolicyUrl': instance.servicePolicyUrl,
      'privacyPolicyUrl': instance.privacyPolicyUrl,
      'orderFormatUrl': instance.orderFormatUrl,
      'followingListUrl': instance.followingListUrl,
      'lineFriendUrl': instance.lineFriendUrl,
      'newsShareEndText': instance.newsShareEndText,
    };
