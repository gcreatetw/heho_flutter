import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/home_auto_text_response.dart';
import 'package:hepo_app/models/api/home_keyword_response.dart';
import 'package:hepo_app/models/api/remove_search_response.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';

import 'news_data.dart';

class KeywordData extends ChangeNotifier {
  String text;

  List<String> auto;
  List<String> hot;
  List<String> recent;

  List<News> recommend;

  KeywordData({
    required this.text,
    required this.hot,
    required this.recent,
    required this.auto,
    required this.recommend,
  });

  factory KeywordData.init() {
    return KeywordData(
      text: '',
      hot: <String>[],
      recent: <String>[],
      auto: <String>[],
      recommend: <News>[],
    );
  }

  Future<void> fetch(AdvanceSearchType type, BuildContext context) async {
    switch (type) {
      case AdvanceSearchType.home:
      case AdvanceSearchType.symptom:
      case AdvanceSearchType.askExpert:
      case AdvanceSearchType.healthLife:
      case AdvanceSearchType.mediaHealth:

        ///2021/10/13 獲取熱門搜尋、推薦閱讀api
        ///type:未設定normal以外其他類型
        Helper.instance.getHomeKeyWordInfo(
          callback: GeneralCallback<HomeKeywordResponse>(
            onError: (GeneralResponse e) => e.showToast(context),
            onSuccess: (HomeKeywordResponse r) {
              final DataBean dataBean = r.data;
              hot = dataBean.topSearch ?? <String>[];
              recent = dataBean.userSearch ?? <String>[];
              recommend.clear();
              for (int i = 0; i < dataBean.recommand.length; i++) {
                recommend.add(
                  News(
                    id: dataBean.recommand[i].postId,
                    type: NewsType.normal,
                    imageUrl: dataBean.recommand[i].img,
                    title: dataBean.recommand[i].title,
                    link: dataBean.recommand[i].url,
                    dateTime: DateTime.parse(dataBean.recommand[i].date),
                  ),
                );
              }
              notifyListeners();
            },
          ),
        );
        break;
      case AdvanceSearchType.hospital:
        // 查院所不顯示近期搜尋
        break;
    }
    if (text.isNotEmpty) {
      fetchAutoTextData(context, type);
    }
  }

  void _fetchFromSample() {
    hot = <String>[
      '頭痛',
      '咳嗽',
      '過敏',
      '便秘',
      '梅尼爾氏症',
      '發燒',
      '胃食道逆流',
      '高血壓',
      '呼吸困難',
      '脂肪肝',
      '腹痛',
      '糖尿病',
      '新冠肺炎',
    ];
    auto = <String>[];
    recommend = News.sampleList(hasBig: false);
  }

  void _fetchHospitalSample() {
    hot = <String>[
      '曾建元診所',
      '元成診所',
      '鳳山元新眼科診所',
      '開元骨外科診所',
    ];
    auto = <String>[];
    recommend = News.sampleList(hasBig: false);
  }

  void _fetchAutoSample() {
    auto.addAll(<String>[
      text,
      '$text 症狀',
      '$text 味覺',
      '$text 大腦',
      '$text 呼吸衰竭',
    ]);
  }

  void _fetchAutoHospitalSample() {
    auto.addAll(<String>[
      text,
      '$text 診所',
      '$text 醫院',
      '$text 醫療中心',
      '$text 復健中心',
    ]);
  }

  void fetchAutoTextData(BuildContext context, AdvanceSearchType type) {
    auto.clear();
    notifyListeners();
    switch (type) {
      case AdvanceSearchType.home:
      case AdvanceSearchType.symptom:
      case AdvanceSearchType.askExpert:
      case AdvanceSearchType.healthLife:
      case AdvanceSearchType.mediaHealth:
      case AdvanceSearchType.hospital:
        Helper.instance.getHomeAutoText(
          query: text,
          searchType: type.apiKeyName,
          callback: GeneralCallback<HomeAutoTextResponse>(
            onError: (GeneralResponse e) {},
            onSuccess: (HomeAutoTextResponse r) {
              auto.add(text);
              if (r.data != null) {
                for (int i = 0; i < r.data!.length; i++) {
                  auto.add(r.data![i]);
                }
              }
              notifyListeners();
            },
          ),
        );
        break;
    }
  }

  void setKeyword(AdvanceSearchType type, String text, BuildContext context) {
    this.text = text;
    fetchAutoTextData(context, type);
    notifyListeners();
  }

  void removeRecent(BuildContext context, String text) {
    final List<String> tempList = <String>[];
    for (final String element in recent) {
      tempList.add(element);
    }
    recent.remove(text);
    notifyListeners();

    ///刪除remote搜尋紀錄
    Helper.instance.getRemoveSearch(
      keyword: text,
      callback: GeneralCallback<RemoveSearchResponse>(
        onError: (GeneralResponse e) {
          recent = tempList;
          notifyListeners();
          e.showToast(context);
        },
        onSuccess: (RemoveSearchResponse r) {},
      ),
    );
  }
}
