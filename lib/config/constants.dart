class Constants {
  static const bool alpha = false;
  static const bool thirdPartyLoginInterrupt = false;

  static const bool showWelcomePage = true;
  static const String isLogin = 'is_login';
  static const String lastVisitTime = 'last_visit_time';
  static const String appMetaData = 'app_meta_data';
  static const String id = 'id';
  static const String deviceId = 'deviceId';
  static const String token = 'token';
  static const String session = 'session';
  static const String phoneAreaCode = 'phone_area_code';
}
