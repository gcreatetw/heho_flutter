import 'dart:io';

class AdConstants {
  static String get bannerUnitId => Platform.isAndroid
      ? 'ca-app-pub-5693807149055825/3797276291'
      : 'ca-app-pub-5693807149055825/3030989537';

  static String get nativeUnitId => Platform.isAndroid
      ? 'ca-app-pub-5693807149055825/2766576633'
      : 'ca-app-pub-5693807149055825/3321599072';
}
