import 'dart:io';

import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart' as facebook;
import 'package:flutter_line_sdk/flutter_line_sdk.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/config/constants.dart';
import 'package:hepo_app/models/api/login_response.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/models/general_callback.dart';
import 'package:hepo_app/models/general_response.dart';
import 'package:hepo_app/models/phone_area_code.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/register/forget_password_page.dart';
import 'package:hepo_app/pages/register/register_hone_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/utils/toast.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:hepo_app/widgets/outline_text_field.dart';
import 'package:hepo_app/widgets/yes_no_dialog.dart';
import 'package:provider/provider.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:url_launcher/url_launcher.dart';

import 'common/phone_area_code_picker.dart';
import 'common/web_view_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final EdgeInsets contentPadding =
      const EdgeInsets.symmetric(horizontal: 41.0);

  PhoneAreaCodeData phoneAreaCodeData = PhoneAreaCodeData.load();

  late List<PhoneAreaCode> phoneAreaCodeList;

  int phoneAreaCodeIndex = 0;

  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();

  final FocusNode _usernameFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  bool enablePasswordObscure = true;

  bool get canLogin {
    return _username.text.isNotEmpty && _password.text.isNotEmpty;
  }

  @override
  void initState() {
    phoneAreaCodeList = phoneAreaCodeData.phoneCodeData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Image.asset(
              ImageAssets.loginBackground2x,
              fit: BoxFit.cover,
              width: double.infinity,
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 32.0),
                  child: Image.asset(
                    ImageAssets.loginForeground2x,
                    height: 110.0,
                    width: 180.0,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).viewInsets.bottom / 2,
                ),
                constraints: const BoxConstraints(maxHeight: 482.0),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(34),
                  ),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Color(0x4d000000),
                      offset: Offset(-3, 3),
                      blurRadius: 16,
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const SizedBox(height: 16.0),
                    const Text(
                      '登入',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: AppColors.grey2F,
                        fontSize: 22,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Padding(
                      padding: contentPadding,
                      child: PhoneAreaCodePicker(
                        index: phoneAreaCodeIndex,
                        phoneAreaCodeList: phoneAreaCodeList,
                        onChanged: (int value) {
                          setState(() {
                            phoneAreaCodeIndex = value;
                          });
                        },
                      ),
                    ),
                    const SizedBox(height: 6.0),
                    Padding(
                      padding: contentPadding,
                      child: OutlineTextField(
                        controller: _username,
                        focusNode: _usernameFocusNode,
                        nextFocusNode: _passwordFocusNode,
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        autofillHints: const <String>[AutofillHints.username],
                        borderRadius: 26.0,
                        height: 46.0,
                        contentPadding:
                            const EdgeInsets.symmetric(horizontal: 4.0),
                        hintText: '手機號碼 / E-mail',
                        prefixSize: 20.0,
                        prefixIcon: Image.asset(
                          IconAssets.userGreen2x,
                        ),
                        onChanged: (_) => setState(() {}),
                      ),
                    ),
                    const SizedBox(height: 5.0),
                    Padding(
                      padding: contentPadding,
                      child: OutlineTextField(
                        controller: _password,
                        focusNode: _passwordFocusNode,
                        textInputAction: TextInputAction.send,
                        autofillHints: const <String>[AutofillHints.password],
                        borderRadius: 26.0,
                        height: 46.0,
                        contentPadding:
                            const EdgeInsets.symmetric(horizontal: 4.0),
                        hintText: '密 碼',
                        obscureText: enablePasswordObscure,
                        maxLines: 1,
                        prefixSize: 20.0,
                        prefixIcon: Image.asset(
                          IconAssets.lockGreen2x,
                        ),
                        suffixSize: 18.0,
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              enablePasswordObscure = !enablePasswordObscure;
                            });
                            // FocusScope.of(context).unfocus();
                          },
                          child: Image.asset(
                            enablePasswordObscure
                                ? IconAssets.eyeOff2x
                                : IconAssets.eyeOpen2x,
                          ),
                        ),
                        onChanged: (_) => setState(() {}),
                        onEditingComplete: canLogin
                            ? () {
                                _login(LoginType.normal);
                              }
                            : null,
                      ),
                    ),
                    Padding(
                      padding: contentPadding,
                      child: GestureDetector(
                        onTap: () {
                          YesNoDialog.show(
                            context,
                            '忘記密碼',
                            message: '需先進行身分驗證後取回密碼，請選擇以下任一種身分驗證',
                            width: 285.0,
                            buttonWidth: 110.0,
                            leftActionText: 'Email驗證',
                            rightActionText: '手機驗證',
                            onLeftActionClick: () {
                              Navigator.of(context).pop();
                              Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                  builder: (_) => const ForgetPasswordPage(
                                    mode: ForgetPasswordMode.email,
                                  ),
                                ),
                              );
                            },
                            onRightActionClick: () {
                              Navigator.of(context).pop();
                              Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                  builder: (_) => const ForgetPasswordPage(
                                    mode: ForgetPasswordMode.cellphoneNumber,
                                  ),
                                ),
                              );
                            },
                            padding: const EdgeInsets.symmetric(
                              horizontal: 23.0,
                              vertical: 14.0,
                            ),
                          );
                        },
                        child: ConstrainedBox(
                          constraints: const BoxConstraints(maxHeight: 20.0),
                          child: const AutoSizeText(
                            '忘記密碼？',
                            minFontSize: 9.0,
                            maxFontSize: 14.0,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              color: AppColors.mediumGrey,
                              fontFamily: 'SFProDisplay',
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 12.0),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal:
                            (MediaQuery.of(context).size.width - 122) / 2,
                      ),
                      child: GreenButton(
                        text: '登入',
                        fontSize: 17,
                        onTap: canLogin
                            ? () {
                                _login(LoginType.normal);
                              }
                            : null,
                      ),
                    ),
                    const Spacer(),
                    Padding(
                      padding: contentPadding,
                      child: Row(
                        children: <Widget>[
                          const Expanded(
                            child: Divider(
                              color: AppColors.greyC6,
                            ),
                          ),
                          const SizedBox(width: 24.0),
                          ConstrainedBox(
                            constraints: const BoxConstraints(maxHeight: 20.0),
                            child: const AutoSizeText(
                              '快速登入',
                              textAlign: TextAlign.center,
                              minFontSize: 9.0,
                              maxFontSize: 14.0,
                              style: TextStyle(
                                color: AppColors.mediumGrey,
                                fontFamily: 'SFProDisplay',
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          const SizedBox(width: 24.0),
                          const Expanded(
                            child: Divider(
                              color: AppColors.greyC6,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Spacer(),
                    Padding(
                      padding: contentPadding,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          for (LoginType value in <LoginType>[
                            LoginType.facebook,
                            LoginType.line,
                            LoginType.google,
                            if (!kIsWeb && Platform.isIOS) LoginType.apple,
                          ])
                            IconCircleButton(
                              value.icon,
                              backgroundColor: value.backgroundColor,
                              onTap: () {
                                _login(value);
                              },
                            ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 18.0),
                    Padding(
                      padding: contentPadding,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                style: const TextStyle(
                                  color: Color(0xff999999),
                                  fontSize: 12,
                                  fontFamily: 'SFProText',
                                ),
                                children: [
                                  const TextSpan(
                                      text: '當您使用本服務時，即表示您已閱讀、了解並同意接受 '),
                                  TextSpan(
                                      text: '會員隱私權條款',
                                      style: const TextStyle(
                                        color: AppColors.muddyGreen,
                                        fontFamily: 'SFProText',
                                      ),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          final AppMetaData data =
                                              context.read<AppMetaData>();
                                          Navigator.of(context).push(
                                            MaterialPageRoute<void>(
                                              builder: (BuildContext context) =>
                                                  WebViewPage(
                                                title: '會員隱私權條款',
                                                url: data.privacyPolicyUrl!,
                                              ),
                                            ),
                                          );
                                          // launch(data.privacyPolicyUrl!);
                                        }),
                                  const TextSpan(text: ' 之所有內容及其後之修改變更。'),
                                ]),
                          )),
                        ],
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute<void>(
                            builder: (_) => const RegisterHomePage(),
                          ),
                        );
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 24.0),
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(34),
                          ),
                          gradient: LinearGradient(
                            colors: <Color>[
                              AppColors.green300,
                              AppColors.green600,
                            ],
                          ),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Color(0x36000000),
                              offset: Offset(0, 3),
                              blurRadius: 30,
                            ),
                          ],
                        ),
                        child: ConstrainedBox(
                          constraints: const BoxConstraints(maxHeight: 30.0),
                          child: const AutoSizeText(
                            '註冊',
                            maxFontSize: 22.0,
                            minFontSize: 14.0,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: EdgeInsets.only(
                left: 12.0,
                top: MediaQuery.of(context).padding.top + 12.0,
              ),
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Image.asset(
                  IconAssets.close2x,
                  width: 24.0,
                  height: 24.0,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void onSuccess(LoginResponse r) {
    final UserData data = context.read<UserData>();
    data.isLogin = true;
    // data.fetchUserData();
    data.loadFromApi(r.data);
    TextInput.finishAutofillContext();
    Navigator.of(context).pop(true);
  }

  void onError(GeneralResponse e) {
    //210 表示無法取得電子信箱 跳出註冊讓使用者使用一般註冊
    if (e.statusCode == '210') {
      Navigator.of(context).push(
        MaterialPageRoute<void>(
          builder: (_) => const RegisterHomePage(),
        ),
      );
    }
    e.showToast(context);
  }

  Future<void> _login(LoginType type) async {
    String username = _username.text;
    if (Utils.isValidCellPhoneNumber(username)) {
      username = Utils.contactPhoneNumber(
        areaCode: phoneAreaCodeList[phoneAreaCodeIndex].code,
        number: username,
      );
    }
    switch (type) {
      case LoginType.normal:
        Helper.instance.normalLogin(
          username: username,
          password: _password.text,
          callback: GeneralCallback<LoginResponse>(
            onError: onError,
            onSuccess: onSuccess,
          ),
        );
        break;
      case LoginType.facebook:
        await facebook.FacebookAuth.instance.logOut();
        final facebook.LoginResult result =
            await facebook.FacebookAuth.instance.login();
        if (kDebugMode) {
          debugPrint(result.status.toString());
          debugPrint(result.message);
        }
        if (result.status == facebook.LoginStatus.success) {
          final facebook.AccessToken accessToken = result.accessToken!;
          if (kDebugMode) {
            debugPrint('Facebook accessToken = ${accessToken.toJson()}');
          }
          if (Constants.thirdPartyLoginInterrupt) {
            await _showDebugDialog(
              userId: accessToken.userId,
              accessToken: accessToken.token,
              expireTime: accessToken.expires.toString(),
              rawData: accessToken.toJson().toString(),
            );
          }
          Helper.instance.facebookLogin(
            accessToken: accessToken.token,
            callback: GeneralCallback<LoginResponse>(
              onError: onError,
              onSuccess: onSuccess,
            ),
          );
        }
        break;
      case LoginType.google:
        final GoogleSignIn _googleSignIn = GoogleSignIn(
          scopes: <String>['email'],
        );
        try {
          await _googleSignIn.signOut();
          final GoogleSignInAccount? googleSignInAccount =
              await _googleSignIn.signIn();
          if (googleSignInAccount != null) {
            final GoogleSignInAuthentication authentication =
                await googleSignInAccount.authentication;
            if (kDebugMode) {
              debugPrint('Google accessToken = ${authentication.accessToken}');
              debugPrint('Google idToken = ${authentication.idToken}');
            }
            if (Constants.thirdPartyLoginInterrupt) {
              await _showDebugDialog(
                userId: googleSignInAccount.id,
                name: googleSignInAccount.displayName,
                email: googleSignInAccount.email,
                idToken: authentication.idToken ?? '',
                accessToken: authentication.accessToken ?? '',
                rawData: ' id = ${googleSignInAccount.id} '
                    ' displayName = ${googleSignInAccount.displayName} '
                    ' email = ${googleSignInAccount.email} '
                    ' photoUrl = ${googleSignInAccount.photoUrl} '
                    ' idToken = ${authentication.idToken ?? ''} '
                    ' accessToken = ${authentication.accessToken ?? ''} ',
              );
            }
            if (authentication.accessToken != null &&
                authentication.idToken != null) {
              Helper.instance.googleLogin(
                accessToken: authentication.accessToken!,
                idToken: authentication.idToken!,
                callback: GeneralCallback<LoginResponse>(
                  onError: onError,
                  onSuccess: onSuccess,
                ),
              );
            }
          }
        } catch (error) {
          Toast.show(context, '登入失敗');
          rethrow;
        }
        break;
      case LoginType.line:
        try {
          final LoginResult result = await LineSDK.instance.login(
            scopes: <String>['profile', 'openid', 'email'],
          );
          final UserProfile? _userProfile = result.userProfile;
          if (kDebugMode) {
            debugPrint('${_userProfile!.data.toString()} ');
            debugPrint('${result.accessToken.data}');
          }
          if (Constants.thirdPartyLoginInterrupt) {
            await _showDebugDialog(
              userId: result.userProfile?.userId,
              name: result.userProfile?.displayName,
              expireTime: DateTime.now()
                  .add(
                    Duration(
                      seconds: result.accessToken.data['expires_in'] as int,
                    ),
                  )
                  .toString(),
              accessToken: result.accessToken.data['access_token'] as String,
              idToken: result.accessToken.data['id_token'] as String,
              rawData: '${_userProfile!.data.toString()} '
                  '${result.accessToken.data}',
            );
          }
          if (_userProfile != null) {
            ///Obtain displayName, pictureUrl, userId
          }

          Helper.instance.lineLogin(
            accessToken: result.accessToken.data['access_token'] as String,
            idToken: result.accessToken.data['id_token'] as String,
            callback: GeneralCallback<LoginResponse>(
              onError: onError,
              onSuccess: onSuccess,
            ),
          );
        } on PlatformException {
          Toast.show(context, '登入失敗');
        }
        break;
      case LoginType.apple:
        try {
          final AuthorizationCredentialAppleID credential =
              await SignInWithApple.getAppleIDCredential(
            scopes: <AppleIDAuthorizationScopes>[
              AppleIDAuthorizationScopes.email,
              AppleIDAuthorizationScopes.fullName,
            ],
          );
          if (credential.userIdentifier != null) {
            if (kDebugMode) {
              debugPrint('givenName ${credential.givenName} '
                  'familyName ${credential.familyName} '
                  'state ${credential.state} '
                  'email ${credential.email} '
                  'userIdentifier = ${credential.userIdentifier}'
                  'identityToken = ${credential.identityToken}'
                  'authorizationCode = ${credential.authorizationCode}');
            }
          }
          if (Constants.thirdPartyLoginInterrupt) {
            await _showDebugDialog(
              userId: credential.userIdentifier,
              name: '${credential.givenName} ${credential.familyName}',
              email: credential.email,
              idToken: credential.identityToken,
              accessToken: credential.authorizationCode,
              rawData: 'givenName ${credential.givenName} '
                  'familyName ${credential.familyName} '
                  'state ${credential.state} '
                  'email ${credential.email} '
                  'userIdentifier = ${credential.userIdentifier}'
                  'identityToken = ${credential.identityToken}'
                  'authorizationCode = ${credential.authorizationCode}',
            );
          }
          if (credential.identityToken != null) {
            Helper.instance.appleLogin(
              accessToken: credential.authorizationCode,
              idToken: credential.identityToken!,
              givenName: credential.givenName,
              familyName: credential.familyName,
              callback: GeneralCallback<LoginResponse>(
                onError: onError,
                onSuccess: onSuccess,
              ),
            );
          }
        } on SignInWithAppleAuthorizationException catch (e) {
          if (e.code != AuthorizationErrorCode.canceled) {
            rethrow;
          }
        }
    }
  }

  Future<void> _showDebugDialog({
    String? accessToken,
    String? idToken,
    String? expireTime,
    String? userId,
    String? name,
    String? email,
    String? rawData,
  }) async {
    return showDialog(
      context: context,
      builder: (_) => YesNoDialog(
        title: 'Oath Data',
        hideLeftButton: true,
        width: MediaQuery.of(context).size.width * 0.9,
        contentWidget: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InkWell(
              onTap: () async {
                await FlutterClipboard.copy(userId ?? '');
                CenterToast.show(context, '已複製 User Id');
              },
              child: Text(
                'User Id = ${userId ?? '無法獲得'}',
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            const Divider(),
            InkWell(
              onTap: () async {
                await FlutterClipboard.copy(email ?? '');
                CenterToast.show(context, '已複製 Email');
              },
              child: Text(
                'Email = ${email ?? '無法獲得'}',
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            const Divider(),
            Text(
              'Name = ${name ?? '無法獲得'}',
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
            const Divider(),
            InkWell(
              onTap: () async {
                await FlutterClipboard.copy(accessToken ?? '');
                CenterToast.show(context, '已複製 Access Token');
              },
              child: Text(
                'Access Token = ${accessToken ?? '無此參數'}',
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            const Divider(),
            InkWell(
              onTap: () async {
                await FlutterClipboard.copy(idToken ?? '');
                CenterToast.show(context, '已複製 Id Token');
              },
              child: Text(
                'ID Token = ${idToken ?? '無此參數'}',
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            const Divider(),
            Text(
              'Expire Time = $expireTime',
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
            const Divider(),
            InkWell(
              onTap: () async {
                await FlutterClipboard.copy(rawData ?? '');
                CenterToast.show(context, '已複製 Raw Data');
              },
              child: Text(
                'Raw Data = $rawData',
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class IconCircleButton extends StatelessWidget {
  final String assetName;
  final Color backgroundColor;
  final VoidCallback? onTap;

  const IconCircleButton(
    this.assetName, {
    Key? key,
    required this.backgroundColor,
    this.onTap,
  }) : super(key: key);

  double get padding {
    switch (assetName) {
      case IconAssets.facebookWhite2x:
        return 12.0;
      case IconAssets.apple2x:
        return 12.0;
      default:
        return 12.0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      shadowColor: Colors.transparent,
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(32.0),
        child: Container(
          height: 52,
          width: 52,
          margin: const EdgeInsets.all(3.0),
          padding: EdgeInsets.all(padding),
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: const BorderRadius.all(
              Radius.circular(36),
            ),
            boxShadow: const <BoxShadow>[
              BoxShadow(
                color: Colors.black12,
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
          child: SizedBox(
            width: 30.0,
            height: 30.0,
            child: Image.asset(
              assetName,
            ),
          ),
        ),
      ),
    );
  }
}
