import 'package:flutter/material.dart';
import 'package:hepo_app/models/collection_data.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:hepo_app/pages/collect/collect_news_list_page.dart';
import 'package:hepo_app/pages/collect/collection_disease_page.dart';
import 'package:hepo_app/pages/collect/collection_hospital_page.dart';
import 'package:hepo_app/pages/collect/collection_symptom_page.dart';
import 'package:hepo_app/pages/home/news_list_title.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:provider/provider.dart';

class MyCollectionPage extends StatefulWidget {
  const MyCollectionPage({
    Key? key,
  }) : super(key: key);

  @override
  _MyCollectionPageState createState() => _MyCollectionPageState();
}

class _MyCollectionPageState extends State<MyCollectionPage> {
  @override
  void initState() {
    Future<void>.microtask(
      () {
        context.read<CollectionData>().fetch();
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '我的收藏',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: Consumer<CollectionData>(
        builder: (BuildContext context, CollectionData data, Widget? child) {
          return ListView(
            padding: const EdgeInsets.symmetric(
              vertical: 20.0,
            ),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                ),
                child: Row(
                  children: <Widget>[
                    const Expanded(
                      child: Text(
                        '收藏文章',
                        style: TextStyle(
                          color: AppColors.charcoalGrey,
                          fontSize: 18,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (_) => CollectNewsListPage(
                              data: data.articles,
                            ),
                          ),
                        );
                      },
                      child: Row(
                        children: <Widget>[
                          const Text(
                            '查看全部',
                            style: TextStyle(
                              color: AppColors.grey79,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          const SizedBox(width: 8.0),
                          Image.asset(
                            IconAssets.arrowRightGrey2x,
                            height: 10.0,
                            width: 12.0,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 16.0),
              ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: data.articles.length > 3 ? 3 : data.articles.length,
                itemBuilder: (_, int index) {
                  final News news = data.articles[index];
                  return NewsListTitle(
                    data: news,
                  );
                },
                separatorBuilder: (_, int index) {
                  return const Divider(
                    color: AppColors.greyD3,
                  );
                },
              ),
              const SizedBox(height: 30.0),
              const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 16.0,
                ),
                child: Text(
                  '收藏分類',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              const SizedBox(height: 16.0),
              CollectionCategoryCard(
                icon: IconAssets.collectIconDisease2x,
                title: '疾病',
                color: AppColors.lightOlive,
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                      builder: (_) => CollectionDiseasePage(
                        data: data.diseases,
                      ),
                    ),
                  );
                },
              ),
              CollectionCategoryCard(
                icon: IconAssets.collectIconSymptom2x,
                title: '症狀',
                color: AppColors.fadedOrange,
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                      builder: (_) => CollectionSymptomPage(
                        data: data.symptoms,
                      ),
                    ),
                  );
                },
              ),
              CollectionCategoryCard(
                icon: IconAssets.collectIconHospital2x,
                title: '院所',
                color: AppColors.blueGrey,
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                      builder: (_) => CollectionHospitalPage(
                        data: data.hospitals,
                      ),
                    ),
                  );
                },
              ),
            ],
          );
        },
      ),
    );
  }
}

class CollectionCategoryCard extends StatelessWidget {
  final String title;
  final String icon;
  final Color color;
  final VoidCallback? onTap;

  const CollectionCategoryCard({
    Key? key,
    required this.title,
    required this.icon,
    required this.color,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16.0,
        vertical: 5.0,
      ),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: AppColors.greyF5,
            borderRadius: BorderRadius.circular(6),
            boxShadow: const <BoxShadow>[
              BoxShadow(
                color: Color(0x52c1c1c1),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
          child: IntrinsicHeight(
            child: Row(
              children: <Widget>[
                Container(
                  width: 11,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: color,
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(6),
                      bottomLeft: Radius.circular(6),
                    ),
                  ),
                ),
                const SizedBox(width: 20.0),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0),
                  child: Image.asset(
                    icon,
                    height: 18.0,
                    width: 18.0,
                  ),
                ),
                const SizedBox(width: 8.0),
                Expanded(
                  child: Text(
                    title,
                    style: const TextStyle(
                      color: AppColors.grey33,
                      fontSize: 16,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                Image.asset(
                  IconAssets.arrowRightBlack2x,
                  height: 18.0,
                  width: 18.0,
                ),
                const SizedBox(width: 16.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
