import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:hepo_app/pages/home/news_list_title.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/round_text_field.dart';

enum NewsListMode {
  normal,
  advanced,
}

class CollectNewsListPage extends StatefulWidget {
  final List<News> data;

  const CollectNewsListPage({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  _CollectNewsListPageState createState() => _CollectNewsListPageState();
}

class _CollectNewsListPageState extends State<CollectNewsListPage>
    with SingleTickerProviderStateMixin {
  final TextEditingController _search = TextEditingController();

  List<News> get data => widget.data;

  bool isShowKeyword = false;

  @override
  void initState() {
    Future<void>.microtask(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: isShowKeyword
            ? RoundTextField(
                controller: _search,
                hintText: '搜尋（收藏文章）',
                onEditingComplete: () {},
              )
            : const Text(
                '收藏文章',
                style: TextStyle(
                  color: AppColors.grey2F,
                ),
              ),
        actions: <Widget>[
          if (!isShowKeyword)
            InkWell(
              onTap: () {
                setState(() => isShowKeyword = true);
              },
              borderRadius: const BorderRadius.all(
                Radius.circular(24.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Image.asset(
                  IconAssets.search2x,
                  width: 24.0,
                  height: 24.0,
                ),
              ),
            )
        ],
      ),
      body: _content(context),
    );
  }

  Widget _content(BuildContext context) {
    return ValueListenableBuilder<TextEditingValue>(
      valueListenable: _search,
      builder: (_, TextEditingValue? value, __) {
        return ListView.separated(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          itemCount: data.length,
          itemBuilder: (_, int index) {
            final News news = data[index];
            final List<String> keywords = value?.text.split(' ') ?? <String>[];
            bool containKeyword = true;
            for (final String keyword in keywords) {
              if (!news.title.contains(keyword)) containKeyword = false;
            }
            return Visibility(
              visible: keywords.isEmpty || containKeyword,
              child: NewsListTitle(
                keywords: keywords,
                data: news,
              ),
            );
          },
          separatorBuilder: (_, int index) {
            final News news = data[index];
            final List<String> keywords = value?.text.split(' ') ?? <String>[];
            bool containKeyword = true;
            for (final String keyword in keywords) {
              if (!news.title.contains(keyword)) containKeyword = false;
            }
            return Visibility(
              visible: keywords.isEmpty || containKeyword,
              child: const Divider(
                color: AppColors.greyD3,
              ),
            );
          },
        );
      },
    );
  }
}
