import 'package:flutter/material.dart';
import 'package:hepo_app/models/hospital_data.dart';
import 'package:hepo_app/pages/home/hospital_content_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:provider/provider.dart';

class CollectionHospitalPage extends StatefulWidget {
  final List<Hospital> data;

  const CollectionHospitalPage({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  _CollectionHospitalPageState createState() => _CollectionHospitalPageState();
}

class _CollectionHospitalPageState extends State<CollectionHospitalPage>
    with TickerProviderStateMixin {
  late TabController tabController;

  List<Hospital> get data => widget.data;

  @override
  void initState() {
    tabController = TabController(
      length: 4,
      vsync: this,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '收藏院所',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          const SizedBox(height: 14.0),
          SizedBox(
            height: 32.0,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              child: TabBar(
                controller: tabController,
                indicatorSize: TabBarIndicatorSize.label,
                labelPadding: EdgeInsets.zero,
                padding: EdgeInsets.zero,
                labelColor: AppColors.grey2F,
                indicatorColor: AppColors.blueGrey,
                unselectedLabelColor: AppColors.black50,
                labelStyle: const TextStyle(
                  fontSize: 16,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w700,
                ),
                unselectedLabelStyle: const TextStyle(
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w700,
                ),
                tabs: <Widget>[
                  for (HospitalType value in HospitalType.values)
                    Tab(
                      text: value.text,
                    ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 11.0),
          const Divider(
            height: 0.0,
            color: AppColors.grey79,
          ),
          Expanded(
            child: TabBarView(
              controller: tabController,
              children: <Widget>[
                for (HospitalType value in HospitalType.values)
                  Builder(
                    builder: (BuildContext context) {
                      final List<Hospital> list = data
                          .where(
                            (Hospital element) => element.type == value.text,
                          )
                          .toList();
                      return ListView.separated(
                        itemCount: list.length,
                        itemBuilder: (_, int index) {
                          final Hospital hospital = list[index];
                          return _CollectionHospital(
                            hospital: hospital,
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute<void>(
                                  builder: (_) =>
                                      ChangeNotifierProvider<Hospital>.value(
                                    value: data[index],
                                    child: const HospitalContentPage(),
                                  ),
                                ),
                              );
                            },
                          );
                        },
                        separatorBuilder: (_, int index) {
                          return const Divider(
                            height: 0.0,
                            color: AppColors.grey79,
                          );
                        },
                      );
                    },
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _CollectionHospital extends StatelessWidget {
  final Hospital hospital;
  final VoidCallback? onTap;

  const _CollectionHospital({
    Key? key,
    required this.hospital,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 18.0,
          vertical: 8.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              hospital.name,
              style: const TextStyle(
                color: AppColors.grey33,
                fontSize: 16,
                fontFamily: 'HelveticaNeue',
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 5.0),
            Text(
              hospital.address,
              style: const TextStyle(
                fontSize: 14.0,
                color: AppColors.grey79,
                fontFamily: 'HelveticaNeue',
              ),
            ),
            const SizedBox(height: 5.0),
            Text(
              hospital.phoneNumber,
              style: const TextStyle(
                fontSize: 14.0,
                color: AppColors.grey79,
                fontFamily: 'HelveticaNeue',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
