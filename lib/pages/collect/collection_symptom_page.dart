import 'package:flutter/material.dart';
import 'package:hepo_app/pages/home/news_list_page.dart';
import 'package:hepo_app/resources/colors.dart';

class CollectionSymptomPage extends StatefulWidget {
  final List<String> data;

  const CollectionSymptomPage({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  _CollectionSymptomPageState createState() => _CollectionSymptomPageState();
}

class _CollectionSymptomPageState extends State<CollectionSymptomPage> {
  List<String> get data => widget.data;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '收藏症狀',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: ListView.separated(
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        itemCount: data.length,
        itemBuilder: (_, int index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute<void>(
                  builder: (_) => NewsListPage(
                    mode: NewsListMode.advanced,
                    keyword: data[index],
                  ),
                ),
              );
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 38.0,
                vertical: 8.0,
              ),
              child: Text(
                data[index],
                style: const TextStyle(
                  color: AppColors.grey79,
                  fontSize: 16,
                  fontFamily: 'HelveticaNeue',
                ),
              ),
            ),
          );
        },
        separatorBuilder: (_, int index) {
          return const Divider(
            height: 0.0,
            color: AppColors.grey79,
          );
        },
      ),
    );
  }
}
