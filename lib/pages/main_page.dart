import 'dart:collection';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/advertisement_data.dart';
import 'package:hepo_app/models/general_callback.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/ask/ask_expert_screen.dart';
import 'package:hepo_app/pages/home/advertisement_dialog.dart';
import 'package:hepo_app/pages/home/home_screen.dart';
import 'package:hepo_app/pages/human_map/human_map_screen.dart';
import 'package:hepo_app/pages/media/media_health_screen.dart';
import 'package:hepo_app/pages/member/member_screen.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/firebase_remote_config_utils.dart';
import 'package:hepo_app/widgets/yes_no_dialog.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MainPage extends StatefulWidget {
  static const String routeName = '/home';

  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  late TabController controller;

  int currentIndex = 0;

  final ListQueue<int> _navigationQueue = ListQueue<int>();

  @override
  void initState() {
    context.read<UserData>().fetchMemberData(context);
    controller = TabController(length: 5, vsync: this);
    controller.addListener(() {
      FocusScope.of(context).unfocus();
    });
    Future<void>.microtask(
      () async {
        _checkVersion();
        if (kReleaseMode) {
          Helper.instance.getHomeAdvertisement(
            callback: GeneralCallback<AdvertisementData?>(
              onError: (_) {},
              onSuccess: (AdvertisementData? advertisement) {
                if (advertisement != null) {
                  AdvertisementDialog.show(
                    context,
                    data: advertisement,
                  );
                }
              },
            ),
          );
        }
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async {
        return _onWillPop();
      },
      child: Scaffold(
        body: Container(
          color: AppColors.greyF5,
          child: IndexedStack(
            index: currentIndex,
            children: <Widget>[
              HomeScreen(
                onFeatureClick: (FeatureCategory category) {
                  switch (category) {
                    // case FeatureCategory.bodyMap:
                    //   _onItemChange(1);
                    //   break;
                    // case FeatureCategory.askExpert:
                    //   _onItemChange(3);
                    //   break;
                    default:
                      break;
                  }
                },
              ),
              const HumanMapScreen(),
              const MediaHealthScreen(),
              const AskExpertScreen(),
              const MemberScreen(),
            ],
          ),
        ),
        bottomNavigationBar: Material(
          elevation: 6.0,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(12.0),
              topRight: Radius.circular(12.0),
            ),
          ),
          color: AppColors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                width: width,
                height: 4.0,
                child: Stack(
                  children: <Widget>[
                    AnimatedPositioned(
                      left: width / 5 * currentIndex + width / 10 - 15.0,
                      duration: const Duration(milliseconds: 300),
                      child: Container(
                        width: 30.0,
                        height: 4.0,
                        decoration: const BoxDecoration(
                          color: AppColors.muddyGreen,
                          borderRadius: BorderRadius.all(
                            Radius.circular(20.0),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 2.0),
              BottomNavigationBar(
                onTap: _onItemChange,
                backgroundColor: Colors.transparent,
                currentIndex: currentIndex,
                unselectedItemColor: AppColors.black,
                selectedItemColor: AppColors.muddyGreen,
                type: BottomNavigationBarType.fixed,
                elevation: 0.0,
                unselectedFontSize: 11.0,
                selectedLabelStyle: const TextStyle(
                  fontSize: 11.0,
                  fontWeight: FontWeight.w600,
                ),
                unselectedLabelStyle: const TextStyle(
                  fontSize: 11.0,
                  fontWeight: FontWeight.w600,
                ),
                items: <BottomNavigationBarItem>[
                  for (_NavigatorItem item in _NavigatorItem.values)
                    BottomNavigationBarItem(
                      label: item.title,
                      icon: Padding(
                        padding: const EdgeInsets.only(bottom: 4.0),
                        child: Image.asset(
                          currentIndex == item.index
                              ? item.iconSelected
                              : item.icon,
                          width: 24.0,
                          height: 24.0,
                        ),
                      ),
                    ),
                ],
              ),
              const SizedBox(height: 5.0),
            ],
          ),
        ),
      ),
    );
  }

  void _onItemChange(int index) {
    if (index != currentIndex) {
      setState(() {
        currentIndex = index;
        controller.animateTo(currentIndex);
      });
      _navigationQueue.addLast(index);
    }
  }

  bool _onWillPop() {
    if (_navigationQueue.isEmpty) return true;
    setState(() {
      _navigationQueue.removeLast();
      final int position = _navigationQueue.isEmpty ? 0 : _navigationQueue.last;
      currentIndex = position;
      controller.animateTo(currentIndex);
    });
    return false;
  }

  Future<void> _checkVersion() async {
    await FirebaseRemoteConfigUtils.instance.fetchAndActivate();
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final String nowVersion = packageInfo.version;
    final String newVersion =
        FirebaseRemoteConfigUtils.instance.config!.getString('app_version');
    final List<String> nowVersions = nowVersion.split('.');
    final List<String> newVersions = newVersion.split('.');
    if (nowVersion.isNotEmpty && newVersion.isNotEmpty) {
      if (int.parse(newVersions[0]) > int.parse(nowVersions[0])) {
        _showUpdateDialog(packageInfo);
      } else {
        if (int.parse(newVersions[1]) > int.parse(nowVersions[1])) {
          _showUpdateDialog(packageInfo);
        } else {
          if (int.parse(newVersions[2]) > int.parse(nowVersions[2])) {
            _showUpdateDialog(packageInfo);
          }
        }
      }
    }
  }

  void _showUpdateDialog(PackageInfo packageInfo) {
    String url = 'market://details?id=${packageInfo.packageName}';
    const String iOSAppId = '1587324631';
    if (Platform.isAndroid) {
      url = 'market://details?id=${packageInfo.packageName}';
    } else if (Platform.isIOS || Platform.isMacOS) {
      url = 'itms-apps://itunes.apple.com/tw/app/apple-store/$iOSAppId?mt=8';
    }
    YesNoDialog.show(
      context,
      '需要更新',
      message: '若要使用此App，您必須更新至最新版本',
      onRightActionClick: () {
        launch(url);
      },
      rightActionText: '更新',
    );
  }
}

enum _NavigatorItem {
  home,
  bodyMap,
  video,
  askExpert,
  member,
}

extension _NavigatorItemEx on _NavigatorItem {
  String get title {
    switch (this) {
      case _NavigatorItem.home:
        return '首頁';
      case _NavigatorItem.bodyMap:
        return '人體地圖';
      case _NavigatorItem.video:
        return '影音健康';
      case _NavigatorItem.askExpert:
        return '請問專家';
      case _NavigatorItem.member:
        return '會員';
    }
  }

  String get icon {
    switch (this) {
      case _NavigatorItem.home:
        return IconAssets.home3x;
      case _NavigatorItem.bodyMap:
        return IconAssets.bodymap3x;
      case _NavigatorItem.video:
        return IconAssets.video3x;
      case _NavigatorItem.askExpert:
        return IconAssets.askExpert3x;
      case _NavigatorItem.member:
        return IconAssets.member3x;
    }
  }

  String get iconSelected {
    switch (this) {
      case _NavigatorItem.home:
        return IconAssets.homeSelected3x;
      case _NavigatorItem.bodyMap:
        return IconAssets.bodymapSelected3x;
      case _NavigatorItem.video:
        return IconAssets.videoSelected3x;
      case _NavigatorItem.askExpert:
        return IconAssets.askExpertSelected3x;
      case _NavigatorItem.member:
        return IconAssets.memberSelected3x;
    }
  }
}
