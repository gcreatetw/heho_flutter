import 'package:easy_rich_text/easy_rich_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hepo_app/models/health_life_data.dart';
import 'package:hepo_app/models/hospital_meta_data.dart';
import 'package:hepo_app/models/keyword_data.dart';
import 'package:hepo_app/pages/home/health_life_news_list_page.dart';
import 'package:hepo_app/pages/home/hospital_list_page.dart';
import 'package:hepo_app/pages/home/news_list_page.dart';
import 'package:hepo_app/pages/home/news_list_title.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/widgets/clip_text.dart';
import 'package:hepo_app/widgets/round_text_field.dart';
import 'package:hepo_app/widgets/search_text.dart';
import 'package:provider/provider.dart';

enum AdvanceSearchType {
  home,
  symptom,
  hospital,
  healthLife,
  mediaHealth,
  askExpert,
}

extension AdvanceSearchTypeEx on AdvanceSearchType {
  String get apiKeyName {
    switch (this) {
      case AdvanceSearchType.home:
      case AdvanceSearchType.symptom:
      case AdvanceSearchType.mediaHealth:
      case AdvanceSearchType.askExpert:
      case AdvanceSearchType.healthLife:
        return 'search';
      case AdvanceSearchType.hospital:
        return 'hospital';
    }
  }
}

class AdvanceSearchPage extends StatefulWidget {
  final AdvanceSearchType type;
  final String? keyword;
  final HealthLifeTypeData? healthLifeTypeData;

  const AdvanceSearchPage({
    Key? key,
    required this.type,
    this.keyword,
    this.healthLifeTypeData,
  }) : super(key: key);

  @override
  _AdvanceSearchPageState createState() => _AdvanceSearchPageState();
}

class _AdvanceSearchPageState extends State<AdvanceSearchPage> {
  final TextEditingController _search = TextEditingController();
  final FocusNode _searchFocusNode = FocusNode();

  bool focusSearch = false;

  KeywordData keywordData = KeywordData.init();

  @override
  void initState() {
    Future<void>.microtask(() {
      KeyboardVisibilityController().onChange.listen((bool visible) {
        if (!visible) {
          _searchFocusNode.unfocus();
        }
      });
      _searchFocusNode.addListener(() {
        if (_searchFocusNode.hasFocus) {
          setState(() => focusSearch = true);
        }else{

        }
      });
      keywordData.text = widget.keyword ?? '';
      _search.text = widget.keyword ?? '';
      keywordData.fetch(widget.type, context);
    });
    super.initState();
  }

  String get hintText {
    switch (widget.type) {
      case AdvanceSearchType.home:
      case AdvanceSearchType.symptom:
        return '搜尋（疾病/症狀/科別文章）';
      case AdvanceSearchType.hospital:
        return '搜尋（醫事機構名稱）';
      case AdvanceSearchType.healthLife:
        return '搜尋（飲食/睡眠/運動/中醫文章）';
      case AdvanceSearchType.mediaHealth:
        return '搜尋（影音）';
      case AdvanceSearchType.askExpert:
        return '搜尋';
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<KeywordData>.value(
      value: keywordData,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: RoundTextField(
            controller: _search,
            focusNode: _searchFocusNode,
            hintText: hintText,
            onChanged: (String text) {
              keywordData.setKeyword(widget.type, text, context);
            },
            onEditingComplete: () {
              _searchFocusNode.unfocus();
              onKeywordTap(_search.text);
            },
            onClearClick: (){
              keywordData.setKeyword(widget.type, '', context);
            },
          ),
        ),
        body: Builder(
          builder: (BuildContext context) {
            final KeywordData data = context.read<KeywordData>();
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const SizedBox(
                    height: 8.0,
                  ),
                  if (context.watch<KeywordData>().text.isNotEmpty) ...<Widget>[
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 18.0),
                      child: Text(
                        '自動搜尋',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    for (String text in data.auto) ...<Widget>[
                      GestureDetector(
                        onTap: () => onKeywordTap(text),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 18.0,
                            vertical: 8.0,
                          ),
                          child: EasyRichText(
                            text,
                            patternList: <EasyRichTextPattern>[
                              EasyRichTextPattern(
                                targetString: data.text,
                                style: const TextStyle(
                                  color: AppColors.muddyGreen,
                                  fontSize: 16,
                                  fontFamily: 'HelveticaNeue',
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                            defaultStyle: const TextStyle(
                              color: AppColors.grey70,
                              fontSize: 16,
                              fontFamily: 'HelveticaNeue',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ),
                      const Divider(
                        color: AppColors.charcoalGrey36,
                        height: 0.0,
                      ),
                    ],
                  ] else ...<Widget>[
                    if (data.hot.isNotEmpty &&
                        widget.type != AdvanceSearchType.hospital) ...<Widget>[
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Text(
                          '熱門關鍵字',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      const SizedBox(height: 3.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Wrap(
                          children: <Widget>[
                            for (String text in data.hot)
                              ClipText(
                                text,
                                onTap: () => onKeywordTap(text),
                              ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 10.0),
                    ],
                    if (data.recent.isNotEmpty) ...<Widget>[
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Text(
                          '最近搜尋',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      const SizedBox(height: 6.0),
                      for (String text in data.recent)
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: SearchText(
                            text,
                            onTap: () => onKeywordTap(text),
                            onCancelClick: () {
                              keywordData.removeRecent(context, text);
                            },
                          ),
                        ),
                    ],
                    if (data.recommend.isNotEmpty) ...<Widget>[
                      const Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: 8.0,
                          horizontal: 16.0,
                        ),
                        child: Text(
                          '推薦閱讀',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: 'HelveticaNeue',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.zero,
                        itemCount: data.recommend.length,
                        itemBuilder: (_, int index) {
                          return NewsListTitle(
                            data: data.recommend[index],
                          );
                        },
                        separatorBuilder: (_, int index) {
                          return const Divider(
                            color: AppColors.greyD3,
                          );
                        },
                      ),
                    ],
                    const SizedBox(height: 6.0),
                  ],
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Future<void> onKeywordTap(String text) async {
    switch (widget.type) {
      case AdvanceSearchType.home:
      case AdvanceSearchType.symptom:
      case AdvanceSearchType.askExpert:
      case AdvanceSearchType.mediaHealth:
        await Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (_) => NewsListPage(
              mode: NewsListMode.advanced,
              keyword: text,
            ),
          ),
        );
        keywordData.fetch(widget.type, context);
        break;
      case AdvanceSearchType.hospital:
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (_) => ChangeNotifierProvider<HospitalMetaData>.value(
              value: context.read<HospitalMetaData>(),
              child: HospitalListPage(
                keyword: text,
              ),
            ),
          ),
        );
        break;
      case AdvanceSearchType.healthLife:
        keywordData.recent.add(text);
        await Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (_) => HealthLifeNewListPage(
              healthLifeTypeData: widget.healthLifeTypeData,
              keyword: text,
            ),
          ),
        );
        keywordData.fetch(widget.type, context);
        break;
    }
  }
}
