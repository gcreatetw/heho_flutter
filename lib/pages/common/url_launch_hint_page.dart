import 'package:flutter/material.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/svg_hint_content.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlLaunchHintPage extends StatefulWidget {
  final String url;

  const UrlLaunchHintPage({
    Key? key,
    required this.url,
  }) : super(key: key);

  @override
  _UrlLaunchHintPageState createState() => _UrlLaunchHintPageState();
}

class _UrlLaunchHintPageState extends State<UrlLaunchHintPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '貼心提醒',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: Center(
        child: SvgHintContent(
          ImageAssets.goToLink,
          actionText: '立即前往',
          onActionClick: () {
            launch(widget.url);
          },
          suffixes: <Widget>[
            Text(
              widget.url,
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: AppColors.grey70,
                fontFamily: 'HelveticaNeue',
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
