import 'package:flutter/material.dart';
import 'package:hepo_app/models/category_data.dart';
import 'package:hepo_app/resources/colors.dart';

class CategoryListView extends StatefulWidget {
  final CategoryData categoryData;
  final Function(SubCategory subCategory) onTap;

  const CategoryListView({
    Key? key,
    required this.categoryData,
    required this.onTap,
  }) : super(key: key);

  @override
  _CategoryListViewState createState() => _CategoryListViewState();
}

class _CategoryListViewState extends State<CategoryListView> {
  @override
  Widget build(BuildContext context) {
    final CategoryData data = widget.categoryData;
    final List<SubCategory> subItems = data.current.items;
    return Row(
      children: <Widget>[
        Container(
          width: 170.0,
          color: AppColors.muddyGreen,
          child: ListView.builder(
            itemCount: data.items.length,
            itemBuilder: (_, int index) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    data.currentIndex = index;
                  });
                },
                child: Container(
                  margin: const EdgeInsets.only(
                    top: 8.0,
                    bottom: 8.0,
                    left: 2.0,
                  ),
                  padding: const EdgeInsets.symmetric(
                    vertical: 9.0,
                    horizontal: 18.0,
                  ),
                  decoration: data.currentIndex == index
                      ? const BoxDecoration(
                          color: AppColors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(22.0),
                            bottomLeft: Radius.circular(22.0),
                          ),
                        )
                      : null,
                  child: Row(
                    children: <Widget>[
                      if (data.items[index].icon != null)
                        Icon(
                          data.items[index].icon,
                          size: 18.0,
                          color: data.currentIndex == index
                              ? AppColors.black
                              : Colors.white,
                        )
                      else
                        const SizedBox(
                          width: 18.0,
                        ),
                      const SizedBox(width: 16),
                      Text(
                        data.items[index].name,
                        style: TextStyle(
                          color: data.currentIndex == index
                              ? AppColors.black
                              : Colors.white,
                          fontSize: 18,
                          fontFamily: 'HelveticaNeue',
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
        Expanded(
          child: ListView.builder(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            itemCount: subItems.length,
            itemBuilder: (_, int index) {
              return GestureDetector(
                onTap: () {
                  widget.onTap(subItems[index]);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 18.0,
                    vertical: 9.0,
                  ),
                  child: Text(
                    subItems[index].name,
                    style: const TextStyle(
                      color: AppColors.black,
                      fontSize: 18,
                      fontFamily: 'HelveticaNeue',
                    ),
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
