import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/phone_area_code.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/widgets/custom_item_picker.dart';

class PhoneAreaCodePicker extends StatelessWidget {
  final int index;
  final List<PhoneAreaCode> phoneAreaCodeList;
  final ValueChanged<int>? onChanged;
  final Color? textColor;
  final double? textSize;

  const PhoneAreaCodePicker({
    Key? key,
    required this.index,
    required this.phoneAreaCodeList,
    this.onChanged,
    this.textColor,
    this.textSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showCupertinoModalPopup(
          context: context,
          barrierColor: Colors.transparent,
          builder: (BuildContext context) {
            return CustomItemPicker(
              initialIndex: index,
              onConfirmClick: (int value) {
                onChanged?.call(value);
              },
              itemCount: phoneAreaCodeList.length,
              itemBuilder: (int index) {
                return phoneAreaCodeList[index].title;
              },
            );
          },
        );
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IntrinsicWidth(
            stepHeight: 1.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      phoneAreaCodeList[index].title,
                      style: TextStyle(
                        color: textColor ?? AppColors.grey79,
                        fontSize: textSize ?? 10,
                        fontFamily: '.AppleSystemUIFont',
                      ),
                    ),
                    const Icon(
                      Icons.arrow_drop_down,
                      color: AppColors.muddyGreen,
                      size: 16.0,
                    ),
                  ],
                ),
                const SizedBox(height: 5.0),
                Container(
                  color: AppColors.greyD0,
                  height: 1.0,
                ),
              ],
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
