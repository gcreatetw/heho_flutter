import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/svg_hint_content.dart';
import 'package:provider/provider.dart';

class OrderContentPage extends StatefulWidget {
  final String id;

  const OrderContentPage({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  _OrderContentPageState createState() => _OrderContentPageState();
}

class _OrderContentPageState extends State<OrderContentPage> {
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
    crossPlatform: InAppWebViewOptions(
      useShouldOverrideUrlLoading: true,
      mediaPlaybackRequiresUserGesture: false,
    ),
    android: AndroidInAppWebViewOptions(
      useHybridComposition: true,
    ),
    ios: IOSInAppWebViewOptions(
      allowsInlineMediaPlayback: true,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '訂單資訊',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: Consumer<AppMetaData>(
        builder: (BuildContext context, AppMetaData data, Widget? child) {
          final String postData = Helper.instance.generateMap(
            ignoreSession: false,
            data: <String, String>{
              Helper.targetUrl: '${data.orderFormatUrl!}${widget.id}',
            },
          ).postData;
          if (kDebugMode) {
            debugPrint(
                'order content Url = ${data.orderFormatUrl!}${widget.id}');
            debugPrint('postData = $postData');
          }
          return (!kIsWeb && (Platform.isAndroid || Platform.isIOS))
              ? InAppWebView(
                  initialOptions: options,
                  initialUrlRequest: URLRequest(
                    url: Uri.parse(Helper.webviewJumpUrl),
                    method: 'POST',
                    headers: <String, String>{
                      'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    body: Uint8List.fromList(
                      utf8.encode(postData),
                    ),
                  ),
                  onLoadStart: (InAppWebViewController c, Uri? u) async {
                    if (kDebugMode) {
                      debugPrint('onLoadStart');
                      debugPrint('u = ${u.toString()}');
                    }
                  },
                  onLoadStop: (InAppWebViewController c, Uri? u) async {
                    if (kDebugMode) {
                      debugPrint('onLoadStop');
                      debugPrint('u = ${u.toString()}');
                    }
                  },
                )
              : const Center(
                  child: SvgHintContent(
                    ImageAssets.cryCancelLoveNoText,
                    message: '此平台未開放',
                  ),
                );
        },
      ),
    );
  }
}
