import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/models/order_data.dart';
import 'package:hepo_app/pages/member/order_content_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/general_state_content.dart';
import 'package:hepo_app/widgets/svg_hint_content.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class OrderListPage extends StatefulWidget {
  const OrderListPage({Key? key}) : super(key: key);

  @override
  _OrderListPageState createState() => _OrderListPageState();
}

class _OrderListPageState extends State<OrderListPage> {
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');

  OrderData data = OrderData.init();

  @override
  void initState() {
    Future<void>.microtask(() {
      data.fetch();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '我的訂單',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: ChangeNotifierProvider<OrderData>.value(
        value: data,
        child: Consumer<OrderData>(
          builder: (BuildContext context, OrderData data, Widget? child) {
            return GeneralStateContent(
              state: data.state,
              child: data.list.isNotEmpty
                  ? ListView.builder(
                      padding: const EdgeInsets.symmetric(vertical: 20.0),
                      itemCount: data.list.length,
                      itemBuilder: (_, int index) {
                        final Order order = data.list[index];
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16.0,
                            vertical: 8.0,
                          ),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                  builder: (_) =>
                                      OrderContentPage(id: order.id),
                                ),
                              );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: const <BoxShadow>[
                                  BoxShadow(
                                    color: Color(0x33c1c1c1),
                                    offset: Offset(3, 3),
                                    blurRadius: 10,
                                  ),
                                ],
                              ),
                              padding: const EdgeInsets.symmetric(
                                vertical: 6.0,
                                horizontal: 9.0,
                              ),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          '訂單編號：${order.serialNumber}',
                                          style: const TextStyle(
                                            color: AppColors.grey33,
                                            fontFamily: 'HelveticaNeue',
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        const SizedBox(height: 3),
                                        Text(
                                          '訂單日期：'
                                          '${dateFormat.format(order.date)}',
                                          style: const TextStyle(
                                            color: AppColors.grey33,
                                            fontFamily: 'HelveticaNeue',
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        const SizedBox(height: 3),
                                        Text(
                                          '商品數量：${order.productCounts}',
                                          style: const TextStyle(
                                            color: AppColors.grey33,
                                            fontFamily: 'HelveticaNeue',
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        const SizedBox(height: 3),
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              '狀態：${order.stateMessage}',
                                              style: const TextStyle(
                                                color: AppColors.grey33,
                                                fontFamily: 'HelveticaNeue',
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                            const Spacer(),
                                            Text(
                                              '合計：${order.total}',
                                              style: const TextStyle(
                                                color: AppColors.grey33,
                                                fontFamily: 'HelveticaNeue',
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Image.asset(
                                    IconAssets.arrowRightGrey2x,
                                    color: AppColors.black,
                                    width: 14.0,
                                    height: 14.0,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    )
                  : Center(
                      child: Center(
                        child: SvgHintContent(
                          ImageAssets.noOrder,
                          message: '希望商城 健康知識型商城\n為您的健康把關',
                          messageTextColor: AppColors.grey33,
                          actionText: '立即前往',
                          suffixes: <Widget>[
                            Text(
                              context.read<AppMetaData>().hopeStoreUrl!,
                              style: const TextStyle(
                                color: AppColors.grey70,
                                fontFamily: 'HelveticaNeue',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                          onActionClick: () {
                            final AppMetaData data =
                                context.read<AppMetaData>();
                            launch(data.hopeStoreUrl!);
                          },
                        ),
                      ),
                    ),
            );
          },
        ),
      ),
    );
  }
}
