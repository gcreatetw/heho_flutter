import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/general_callback.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/toast.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:hepo_app/widgets/outline_text_field.dart';
import 'package:hepo_app/widgets/svg_hint_content.dart';

enum CommentLevel {
  veryLow,
  low,
  medium,
  height,
  veryHeight,
}

extension CommentLevelEx on CommentLevel {
  String get icon {
    switch (this) {
      case CommentLevel.veryLow:
        return IconAssets.commentsVeryLow2x;
      case CommentLevel.low:
        return IconAssets.commentsLow2x;
      case CommentLevel.medium:
        return IconAssets.commentsMedium2x;
      case CommentLevel.height:
        return IconAssets.commentsHeight2x;
      case CommentLevel.veryHeight:
      default:
        return IconAssets.commentsVeryHeight2x;
    }
  }
}

class CommentPage extends StatefulWidget {
  const CommentPage({
    Key? key,
  }) : super(key: key);

  @override
  _CommentPageState createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  bool isSuccess = false;

  CommentLevel? level;

  final TextEditingController _email = TextEditingController();
  final TextEditingController _comment = TextEditingController();

  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _commentFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '意見與回饋',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        body: isSuccess
            ? _successContent()
            : Stack(
                children: <Widget>[
                  Image.asset(
                    ImageAssets.commentsBackground2x,
                    fit: BoxFit.cover,
                    width: double.infinity,
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(31),
                        topRight: Radius.circular(31),
                      ),
                    ),
                    margin: const EdgeInsets.only(top: 100.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        const SizedBox(height: 26.0),
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20.0),
                          child: Text(
                            '本次體驗的體驗評價為',
                            style: TextStyle(
                              color: AppColors.grey2F,
                              fontSize: 16,
                              fontFamily: 'Montserrat',
                            ),
                          ),
                        ),
                        const SizedBox(height: 5.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            for (CommentLevel value in CommentLevel.values)
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    level = value;
                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(13.0),
                                  child: Image.asset(
                                    value.icon,
                                    height: 32.0,
                                    width: 32.0,
                                    color: level == value
                                        ? AppColors.muddyGreen
                                        : null,
                                  ),
                                ),
                              )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: OutlineTextField(
                            controller: _email,
                            focusNode: _emailFocusNode,
                            nextFocusNode: _commentFocusNode,
                            textInputAction: TextInputAction.next,
                            height: 50.0,
                            borderRadius: 28.0,
                            hintText: '請輸入您的E-mail',
                          ),
                        ),
                        const SizedBox(height: 23.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: OutlineTextField(
                            controller: _comment,
                            focusNode: _commentFocusNode,
                            borderRadius: 24.0,
                            hintText: '告訴我們您的想法',
                            height:
                                MediaQuery.of(context).size.height - 285 - 220,
                            contentPadding: const EdgeInsets.symmetric(
                              vertical: 13.0,
                              horizontal: 14.0,
                            ),
                            maxLines: (MediaQuery.of(context).size.height -
                                    285 -
                                    230) ~/
                                24,
                            onEditingComplete: () {
                              _commentFocusNode.unfocus();
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: SizedBox(
                            height: 50.0,
                            child: GreenButton(
                              text: '送出',
                              radius: 26.0,
                              onTap: _sendComments,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  Widget _successContent() {
    return const Center(
      child: SvgHintContent(
        ImageAssets.submited,
        message: '感謝您的意見 ！\n再麻煩留意您的E-mail',
      ),
    );
  }

  void _sendComments() {
    if (level == null) {
      Toast.show(context, '請選擇你的評價');
    } else if (_email.text.isEmpty || _comment.text.isEmpty) {
      Toast.show(context, '尚有欄位未填寫');
    } else if (!Utils.isValidEmail(_email.text)) {
      Toast.show(context, '電子信箱格式錯誤');
    } else {
      Helper.instance.sendComments(
        level: level!,
        email: _email.text,
        comments: _comment.text,
        callback: GeneralCallback<GeneralResponse>(
          onError: (GeneralResponse e) => e.showToast(context),
          onSuccess: (GeneralResponse r) {
            r.showToast(context);
            setState(() {
              isSuccess = true;
            });
          },
        ),
      );
    }
  }
}
