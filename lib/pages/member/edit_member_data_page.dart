import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/member/confirm_email_page.dart';
import 'package:hepo_app/pages/member/edit_birthday_page.dart';
import 'package:hepo_app/pages/member/edit_password_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:hepo_app/widgets/gender_picker.dart';
import 'package:hepo_app/widgets/outline_text_field.dart';
import 'package:hepo_app/widgets/yes_no_dialog.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'edit_cellphone_number_page.dart';
import 'edit_name_page.dart';

enum MemberDataType {
  name,
  gender,
  birthday,
  cellPhoneNumber,
  email,
  password,
}

class EditMemberDataPage extends StatefulWidget {
  const EditMemberDataPage({Key? key}) : super(key: key);

  @override
  _EditMemberDataPageState createState() => _EditMemberDataPageState();
}

class _EditMemberDataPageState extends State<EditMemberDataPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final UserData data = context.read<UserData>();
        final bool hasChanged = data.hasChanged;
        if (hasChanged) {
          YesNoDialog.show(
            context,
            '尚未儲存',
            message: '您的資料尚未儲存\n確定要離開嗎？',
            onRightActionClick: () {
              data.revertData();
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
          );
        }
        return !hasChanged;
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '編輯會員資料',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        body: Consumer<UserData>(
          builder: (BuildContext context, UserData data, _) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const SizedBox(height: 9),
                MemberDataListTile(
                  title: '姓名',
                  content: data.newName,
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute<bool>(
                        builder: (_) => EditNamePage(
                          context: context,
                          oldData: data.newName,
                        ),
                      ),
                    );
                  },
                ),
                _divider(),
                MemberDataListTile(
                  title: '性別',
                  content: data.newGender.text,
                  onTap: () {
                    showCupertinoModalPopup(
                      context: context,
                      barrierColor: Colors.transparent,
                      builder: (BuildContext _context) {
                        return GenderPicker(
                          initialValue: data.newGender,
                          onConfirmClick: (Gender value) {
                            data.setGender(value);
                            context.read<UserData>().updateData(
                                  context: context,
                                  updateType: UpdateType.gender,
                                );
                          },
                        );
                      },
                    );
                  },
                ),
                _divider(),
                MemberDataListTile(
                  title: '生日',
                  content: data.newBirthday == null
                      ? '未設定'
                      : DateFormat('yyyy/MM/dd').format(data.newBirthday!),
                  onTap: data.birthday != null
                      ? null
                      : () {
                          Navigator.of(context).push(
                            MaterialPageRoute<bool>(
                              builder: (_) => EditBirthdayPage(
                                context: context,
                              ),
                            ),
                          );
                        },
                ),
                _divider(),
                MemberDataListTile(
                  title: '手機',
                  content: data.newCellphoneNumber,
                  onTap: () async {
                    final bool? success = await Navigator.of(context).push(
                      MaterialPageRoute<bool>(
                        builder: (_) => EditCellphoneNumberPage(
                          oldData: data.newCellphoneNumber,
                        ),
                      ),
                    );
                    if (success ?? false) {
                      CenterToast.show(context, '手機號碼已更新');
                    }
                  },
                ),
                _divider(),
                MemberDataListTile(
                  title: '信箱',
                  content: data.newEmail.isEmpty ? '未填寫' : data.newEmail,
                  onTap: data.newEmail.isNotEmpty || data.status == 1
                      ? null
                      : () async {
                          final TextEditingController controller =
                              TextEditingController(text: data.newEmail);
                          showDialog(
                            context: context,
                            barrierColor: Colors.transparent,
                            builder: (_) => EmailEditDialog(
                              controller: controller,
                            ),
                          );
                        },
                  type: MemberDataType.email,
                  confirm: data.status == 1,
                ),
                _divider(),
                MemberDataListTile(
                  title: '修改密碼',
                  content: '*********',
                  onTap: () async {
                    Navigator.of(context).push(
                      MaterialPageRoute<bool>(
                        builder: (_) => EditPasswordPage(
                          context: context,
                        ),
                      ),
                    );
                  },
                ),
                _divider(),
              ],
            );
          },
        ),
      ),
    );
  }

  Divider _divider() {
    return const Divider(
      color: AppColors.greyC3,
      height: 0.0,
    );
  }
}

class MemberDataListTile extends StatelessWidget {
  final String title;
  final String content;
  final VoidCallback? onTap;
  final MemberDataType? type;
  final bool? confirm;

  const MemberDataListTile({
    Key? key,
    required this.title,
    required this.content,
    this.onTap,
    this.type,
    this.confirm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16.0,
          vertical: 14.0,
        ),
        child: Row(
          children: <Widget>[
            Text(
              title,
              style: const TextStyle(
                color: AppColors.grey2F,
                fontSize: 16,
                fontFamily: 'Montserrat',
              ),
            ),
            const SizedBox(width: 5.4),
            if (type == MemberDataType.email) ...<Widget>[
              Container(
                padding: const EdgeInsets.fromLTRB(4, 1, 4, 1),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  color: confirm! ? AppColors.muddyGreen : AppColors.terracota,
                ),
                child: Text(
                  confirm! ? '已驗證' : '尚未驗證',
                  style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Montserrat',
                      fontStyle: FontStyle.normal,
                      fontSize: 12.0),
                ),
              )
            ],
            Expanded(
              child: Text(
                content,
                textAlign: TextAlign.end,
                style: const TextStyle(
                  color: AppColors.grey2F,
                  fontSize: 16,
                  fontFamily: 'Montserrat',
                ),
              ),
            ),
            if (onTap != null) ...<Widget>[
              const SizedBox(width: 17.0),
              Image.asset(
                IconAssets.edit2x,
                width: 17.0,
                height: 17.0,
              )
            ],
            const SizedBox(width: 12.0),
          ],
        ),
      ),
    );
  }
}

class EmailEditDialog extends StatefulWidget {
  final TextEditingController controller;

  const EmailEditDialog({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  _EmailEditDialogState createState() => _EmailEditDialogState();
}

class _EmailEditDialogState extends State<EmailEditDialog> {
  bool correct = false;

  @override
  Widget build(BuildContext context) {
    bool correct = Utils.isValidEmail(widget.controller.text);
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).padding.bottom,
      ),
      child: YesNoDialog(
        title: '請輸入電子郵件',
        width: 285.0,
        buttonWidth: 110.0,
        contentWidget: OutlineTextField(
          controller: widget.controller,
          enabledBorderColor: Colors.transparent,
          suffixIcon: Image.asset(
            correct ? IconAssets.correct2x : IconAssets.incorrect2x,
          ),
          suffixSize: 10.0,
          onChanged: (String text) {
            setState(() {
              correct = Utils.isValidEmail(text);
            });
          },
          contentPadding: const EdgeInsets.symmetric(horizontal: 10.0),
        ),
        rightActionText: '發送驗證碼',
        onRightActionClick: () {
          if (correct) {
            Helper.instance.sendForgetPassword(
              account: widget.controller.text,
              callback: GeneralCallback<String>(
                onSuccess: (String data) async {
                  Navigator.of(context).pop();
                  final bool? success = await Navigator.of(context).push(
                    MaterialPageRoute<bool>(
                      builder: (_) => ConfirmEmailPage(
                        emailAddress: widget.controller.text,
                      ),
                    ),
                  );
                  if (success ?? false) {
                    CenterToast.show(context, '驗證成功');
                  }
                },
                onError: (GeneralResponse response) {
                  CenterToast.show(context, response.message);
                },
              ),
            );
          }
        },
      ),
    );
  }
}
