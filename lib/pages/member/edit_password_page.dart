import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:hepo_app/widgets/outline_text_field.dart';
import 'package:provider/provider.dart';

class EditPasswordPage extends StatefulWidget {
  final BuildContext context;

  const EditPasswordPage({
    Key? key,
    required this.context,
  }) : super(key: key);

  @override
  _EditPasswordPageState createState() => _EditPasswordPageState();
}

class _EditPasswordPageState extends State<EditPasswordPage> {
  final TextEditingController _newPassword = TextEditingController();
  final TextEditingController _newPasswordSecond = TextEditingController();

  final FocusNode _newPasswordFocusNode = FocusNode();
  final FocusNode _newPasswordSecondFocusNode = FocusNode();

  bool? isFirstCorrect;
  String firstErrorHint = '';
  bool? isSecondCorrect;
  String secondErrorHint = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '修改密碼',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        backgroundColor: AppColors.greyF5,
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 26.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              const SizedBox(height: 20.0),
              OutlineTextField(
                height: 50.0,
                controller: _newPassword,
                focusNode: _newPasswordFocusNode,
                nextFocusNode: _newPasswordSecondFocusNode,
                textInputAction: TextInputAction.next,
                enabledBorderColor: Colors.transparent,
                obscureText: true,
                hintText: '新密碼（8-20 字元包含英文及數字）',
                suffixIcon: isFirstCorrect == null
                    ? null
                    : Image.asset(
                        isFirstCorrect!
                            ? IconAssets.correct2x
                            : IconAssets.incorrect2x,
                      ),
                onChanged: (String text) {
                  setState(() {
                    isFirstCorrect = Utils.isValidPassword(text);
                    firstErrorHint = '';
                  });
                },
              ),
              Text(
                firstErrorHint,
                textAlign: TextAlign.end,
                style: const TextStyle(
                  color: AppColors.terracota,
                  fontSize: 10,
                  fontFamily: 'SFProText',
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(height: 14.0),
              OutlineTextField(
                height: 50.0,
                controller: _newPasswordSecond,
                focusNode: _newPasswordSecondFocusNode,
                enabledBorderColor: Colors.transparent,
                obscureText: true,
                hintText: '再次確認密碼',
                suffixIcon: isSecondCorrect == null
                    ? null
                    : Image.asset(
                        isSecondCorrect!
                            ? IconAssets.correct2x
                            : IconAssets.incorrect2x,
                      ),
                onChanged: (String text) {
                  setState(() {
                    isSecondCorrect =
                        _newPassword.text == _newPasswordSecond.text;
                    secondErrorHint = '';
                  });
                },
                onEditingComplete: () {
                  _newPasswordSecondFocusNode.unfocus();
                  _confirm();
                },
              ),
              Text(
                secondErrorHint,
                textAlign: TextAlign.end,
                style: const TextStyle(
                  color: AppColors.terracota,
                  fontSize: 10,
                  fontFamily: 'SFProText',
                  fontWeight: FontWeight.w600,
                ),
              ),
              const Spacer(),
              GreenButton(
                text: '重設密碼',
                radius: 26.0,
                padding: const EdgeInsets.symmetric(
                  vertical: 13.0,
                ),
                onTap: _confirm,
              ),
              const SizedBox(height: 35.0),
            ],
          ),
        ),
      ),
    );
  }

  void _confirm() {
    if ((isFirstCorrect ?? false) && (isSecondCorrect ?? false)) {
      context.read<UserData>().newPassword = _newPassword.text;
      context.read<UserData>().updateData(
            context: widget.context,
            updateType: UpdateType.password,
          );
      Navigator.of(context).pop(true);
    }
    setState(() {
      if (!(isFirstCorrect ?? false)) {
        firstErrorHint = '請輸入 8-20 字元包含英文及數字';
      }
      if (!(isSecondCorrect ?? false)) {
        secondErrorHint = '密碼不相同，請再確認一次';
      }
    });
  }
}
