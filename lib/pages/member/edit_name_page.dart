import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:hepo_app/widgets/outline_text_field.dart';
import 'package:provider/provider.dart';

class EditNamePage extends StatefulWidget {
  final String oldData;
  final BuildContext context;

  const EditNamePage({
    Key? key,
    required this.oldData,
    required this.context,
  }) : super(key: key);

  @override
  _EditNamePageState createState() => _EditNamePageState();
}

class _EditNamePageState extends State<EditNamePage> {
  final TextEditingController _controller = TextEditingController();

  final FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    _controller.text = widget.oldData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '姓名',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        backgroundColor: AppColors.greyF5,
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 26.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              const SizedBox(height: 20.0),
              OutlineTextField(
                controller: _controller,
                focusNode: _focusNode,
                textColor: AppColors.grey79,
                enabledBorderColor: Colors.transparent,
                maxLength: 20,
                onEditingComplete: () {
                  _focusNode.unfocus();
                  _confirm();
                },
              ),
              const Spacer(),
              GreenButton(
                text: '儲存',
                radius: 26.0,
                padding: const EdgeInsets.symmetric(
                  vertical: 13.0,
                ),
                onTap: _confirm,
              ),
              const SizedBox(height: 35.0),
            ],
          ),
        ),
      ),
    );
  }

  void _confirm() {
    context.read<UserData>().setName(_controller.text);
    context.read<UserData>().updateData(
          context: widget.context,
          updateType: UpdateType.name,
        );
    Navigator.of(context).pop(true);
  }
}
