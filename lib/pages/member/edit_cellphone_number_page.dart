import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/phone_area_code.dart';
import 'package:hepo_app/pages/common/phone_area_code_picker.dart';
import 'package:hepo_app/pages/register/phone_validate_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:hepo_app/widgets/outline_text_field.dart';

class EditCellphoneNumberPage extends StatefulWidget {
  final String oldData;

  const EditCellphoneNumberPage({
    Key? key,
    required this.oldData,
  }) : super(key: key);

  @override
  _EditCellphoneNumberPageState createState() =>
      _EditCellphoneNumberPageState();
}

class _EditCellphoneNumberPageState extends State<EditCellphoneNumberPage> {
  final TextEditingController _cellphoneNumber = TextEditingController();

  final FocusNode _cellphoneNumberFocusNode = FocusNode();

  PhoneAreaCodeData phoneAreaCodeData = PhoneAreaCodeData.load();

  late List<PhoneAreaCode> phoneAreaCodeList;

  int phoneAreaCodeIndex = 0;

  @override
  void initState() {
    phoneAreaCodeList = phoneAreaCodeData.phoneCodeData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '手機',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        backgroundColor: AppColors.greyF5,
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 26.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              const SizedBox(height: 14.0),
              const Text(
                '目前手機號碼',
                style: TextStyle(
                  color: AppColors.grey2F,
                  fontFamily: 'Montserrat',
                ),
              ),
              const SizedBox(height: 4.0),
              OutlineTextField(
                enable: false,
                controller: TextEditingController(text: widget.oldData),
              ),
              const SizedBox(height: 9.0),
              const Text(
                '新手機號碼',
                style: TextStyle(
                  color: AppColors.grey2F,
                  fontFamily: 'Montserrat',
                ),
              ),
              const SizedBox(height: 4.0),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 80.0,
                    height: 42.0,
                    child: PhoneAreaCodePicker(
                      index: phoneAreaCodeIndex,
                      phoneAreaCodeList: phoneAreaCodeList,
                      textColor: AppColors.black,
                      onChanged: (int value) {
                        setState(() {
                          phoneAreaCodeIndex = value;
                        });
                      },
                    ),
                  ),
                  Expanded(
                    child: OutlineTextField(
                      controller: _cellphoneNumber,
                      keyboardType: TextInputType.phone,
                      focusNode: _cellphoneNumberFocusNode,
                    ),
                  ),
                ],
              ),
              const Spacer(),
              GreenButton(
                text: '驗證手機號碼',
                radius: 26.0,
                padding: const EdgeInsets.symmetric(
                  vertical: 13.0,
                ),
                onTap: _openValidatePage,
              ),
              const SizedBox(height: 35.0),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _openValidatePage() async {
    _cellphoneNumberFocusNode.unfocus();
    if (Utils.isValidCellPhoneNumber(_cellphoneNumber.text)) {
      final String number = Utils.contactPhoneNumber(
        areaCode: phoneAreaCodeList[phoneAreaCodeIndex].code,
        number: _cellphoneNumber.text,
      );
      Helper.instance.sendForgetPassword(
        account: number,
        callback: GeneralCallback<String>(
          onSuccess: (String data) async {
            Navigator.of(context).pop();
            Navigator.of(context).push(
              MaterialPageRoute<bool>(
                builder: (_) => PhoneValidatePage(
                  validateType: ValidateType.update,
                  cellPhoneNumber: number,
                ),
              ),
            );
          },
          onError: (GeneralResponse response) {
            CenterToast.show(context, response.message);
          },
        ),
      );
    } else {
      CenterToast.show(context, '電話格式錯誤');
    }
  }
}
