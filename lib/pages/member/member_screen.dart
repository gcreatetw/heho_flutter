import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/models/text_banner_data.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/collect/my_collection_page.dart';
import 'package:hepo_app/pages/member/following_list_page.dart';
import 'package:hepo_app/pages/member/order_list_page.dart';
import 'package:hepo_app/pages/member/setting_page.dart';
import 'package:hepo_app/pages/welcome_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/toast.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../login_page.dart';

class MemberScreen extends StatefulWidget {
  const MemberScreen({
    Key? key,
  }) : super(key: key);

  @override
  _MemberScreenState createState() => _MemberScreenState();
}

class _MemberScreenState extends State<MemberScreen>
    with SingleTickerProviderStateMixin {
  final double barHeight = 52;

  final BoxDecoration greyGradiant = const BoxDecoration(
    gradient: LinearGradient(
      colors: <Color>[AppColors.greyF5, AppColors.greyE5],
      begin: FractionalOffset(0.0, 0.0),
      end: FractionalOffset(1.0, 0.0),
      stops: <double>[0.0, 1.0],
      // tileMode: TileMode.clamp,
    ),
  );

  final ScrollController _scrollController = ScrollController();

  double opacity = 0.0;

  TextBannerData bannerData = TextBannerData.init();

  @override
  void initState() {
    Future<void>.microtask(() {
      bannerData.fetch();
      _scrollController.addListener(_scrollListener);
      context.read<UserData>().fetchAdvertisement(context);
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    final double _currentPosition = _scrollController.offset;
    const double maxOffset = 60;

    if (maxOffset > _scrollController.offset && _currentPosition > 1) {
      setState(() {
        opacity = _currentPosition / maxOffset;
      });
    } else if (_currentPosition > maxOffset && opacity != 1) {
      setState(() {
        opacity = 1;
      });
    } else if (_currentPosition <= 0 && opacity != 0) {
      setState(() {
        opacity = 0;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<TextBannerData>.value(value: bannerData),
      ],
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: AppColors.forestGreen.withOpacity(
            opacity,
          ),
          centerTitle: true,
          title: Opacity(
            opacity: opacity,
            child: Text(
              context.watch<UserData>().isLogin ? '會員' : '訪客',
            ),
          ),
          leading: context.watch<UserData>().isLogin
              ? null
              : Opacity(
                  opacity: opacity,
                  child: InkWell(
                    onTap: opacity == 0 ? null : _openLoginPage,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Image.asset(
                        IconAssets.person2x,
                        width: 20.0,
                        height: 20.0,
                      ),
                    ),
                  ),
                ),
          elevation: 0.0,
          actions: <Widget>[
            InkWell(
              onTap: _openWelcomePage,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Image.asset(
                  IconAssets.calendar2x,
                  width: 20.0,
                  height: 20.0,
                ),
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          controller: _scrollController,
          child: Container(
            color: AppColors.greyEF,
            child: Column(
              children: <Widget>[
                Stack(
                  alignment: Alignment.bottomLeft,
                  children: <Widget>[
                    Image.asset(
                      ImageAssets.memberScreenBackground2x,
                      width: double.infinity,
                      height: 260,
                      fit: BoxFit.cover,
                    ),
                    Container(
                      height: 36.0,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(24.0),
                          topRight: Radius.circular(24.0),
                        ),
                        color: AppColors.greyEF,
                      ),
                    ),
                    Consumer<UserData>(
                      builder: (BuildContext context, UserData data, _) {
                        return GestureDetector(
                          onTap: data.isLogin ? null : _openLoginPage,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 16.0,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 30.0,
                                  ),
                                  child: Text(
                                    data.isLogin ? data.name : '訪客模式',
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 24,
                                      fontFamily: '.AppleSystemUIFont',
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 30.0,
                                  ),
                                  child: Text(
                                    data.isLogin
                                        ? '陪伴天數 ${data.days} 天'
                                        : '註冊/登入',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: data.isLogin ? 12 : 16,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 15.0),
                                MemberTitleCard(
                                  title: '為你推薦',
                                  icon: IconAssets.information2x,
                                  radius: 8.0,
                                  titleFontSize: 16,
                                  content: Consumer<TextBannerData>(
                                    builder: (BuildContext context,
                                        TextBannerData data, _) {
                                      return data.items.isEmpty
                                          ? Container()
                                          : TextBannerView(
                                              items: data.items,
                                            );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
                const SizedBox(height: 15.0),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16.0,
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: MemberTitleCard(
                          title: '會員中心',
                          icon: IconAssets.memberBlack2x,
                          content: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              TitleIconButton(
                                title: '設定',
                                icon: IconAssets.setting2x,
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute<void>(
                                      builder: (_) => const SettingPage(),
                                    ),
                                  );
                                },
                              ),
                              TitleIconButton(
                                title: '我的收藏',
                                icon: IconAssets.collectGreen2x,
                                onTap: () {
                                  if (context.read<UserData>().isLogin) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                        builder: (_) =>
                                            const MyCollectionPage(),
                                      ),
                                    );
                                  } else {
                                    Toast.show(context, '尚未登入 請先登入才能享受完整功能');
                                    _openLoginPage();
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(width: 8.0),
                      Expanded(
                        child: MemberTitleCard(
                          title: '希望商城',
                          icon: IconAssets.light2x,
                          trailing: GestureDetector(
                            onTap: () {
                              final AppMetaData data =
                                  context.read<AppMetaData>();
                              if (data.memberHopeStoreUrl != null) {
                                launch(data.memberHopeStoreUrl!);
                              }
                            },
                            child: Row(
                              children: <Widget>[
                                Image.asset(
                                  IconAssets.shoppingBag2x,
                                  width: 12.0,
                                  height: 12.0,
                                ),
                                const SizedBox(width: 2.0),
                                if (MediaQuery.of(context).size.width >= 400)
                                  const Text(
                                    '去逛逛',
                                    style: TextStyle(
                                      color: AppColors.darkGreyBlue,
                                      fontSize: 12.0,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                              ],
                            ),
                          ),
                          content: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              TitleIconButton(
                                title: '我的訂單',
                                icon: IconAssets.order2x,
                                onTap: () {
                                  if (context.read<UserData>().isLogin) {
                                    Navigator.of(context).push(
                                      MaterialPageRoute<void>(
                                        builder: (_) => const OrderListPage(),
                                      ),
                                    );
                                  } else {
                                    Toast.show(context, '尚未登入 請先登入才能享受完整功能');
                                    _openLoginPage();
                                  }
                                },
                              ),
                              TitleIconButton(
                                title: '追蹤清單',
                                icon: IconAssets.favorite2x,
                                onTap: () {
                                  if (context.read<UserData>().isLogin) {
                                    Navigator.of(context).push(
                                      MaterialPageRoute<void>(
                                        builder: (_) =>
                                            const FollowingListPage(),
                                      ),
                                    );
                                  } else {
                                    Toast.show(context, '尚未登入 請先登入才能享受完整功能');
                                    _openLoginPage();
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 15.0),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16.0,
                  ),
                  child: Consumer<UserData>(
                    builder: (BuildContext context, UserData data, _) {
                      return Container(
                        width: double.infinity,
                        padding: const EdgeInsets.all(4.0),
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Color(0x29c1c1c1),
                              offset: Offset(0, 3),
                              blurRadius: 6,
                            ),
                          ],
                        ),
                        child: data.advertisementUrl.isNotEmpty
                            ? ClipRRect(
                                child: GestureDetector(
                                  onTap: () {
                                    launch(data.advertisementLinkUrl);
                                  },
                                  child: Image.network(
                                    data.advertisementUrl,
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                              )
                            : null,
                      );
                    },
                  ),
                ),
                const SizedBox(height: 15.0),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16.0,
                  ),
                  child: MemberTitleCard(
                    title: '關注 Heho',
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        TitleIconButton(
                          title: 'Facebook',
                          icon: IconAssets.facebookGreen2x,
                          onTap: () {
                            final AppMetaData data =
                                context.read<AppMetaData>();
                            launch(data.facebookUrl!);
                          },
                        ),
                        TitleIconButton(
                          title: 'LINE',
                          icon: IconAssets.lineGreen2x,
                          onTap: () {
                            final AppMetaData data =
                                context.read<AppMetaData>();
                            launch(data.lineUrl!);
                          },
                        ),
                        TitleIconButton(
                          title: 'YouTube',
                          icon: IconAssets.youtube2x,
                          onTap: () {
                            final AppMetaData data =
                                context.read<AppMetaData>();
                            launch(data.youtubeUrl!);
                          },
                        ),
                        TitleIconButton(
                          title: 'Instagram',
                          icon: IconAssets.instagram2x,
                          onTap: () {
                            final AppMetaData data =
                                context.read<AppMetaData>();
                            launch(data.instagramUrl!);
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 300),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _openWelcomePage() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (_) => WelcomePage(
          onTap: (BuildContext context) => Navigator.of(context).pop(),
        ),
      ),
    );
  }

  void _openLoginPage() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (_) => const LoginPage(),
      ),
    );
  }
}

class MemberTitleCard extends StatelessWidget {
  final String title;
  final Widget? trailing;
  final Widget content;
  final String? icon;
  final double? titleFontSize;
  final double? radius;

  const MemberTitleCard({
    Key? key,
    required this.title,
    required this.content,
    this.trailing,
    this.icon,
    this.radius,
    this.titleFontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.greyFA,
        borderRadius: BorderRadius.all(
          Radius.circular(radius ?? 12.0),
        ),
        boxShadow: const <BoxShadow>[
          BoxShadow(
            color: Color(0x34c1c1c1),
            offset: Offset(0, 3),
            blurRadius: 10,
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15.0,
              vertical: 8.0,
            ),
            child: Row(
              children: <Widget>[
                if (icon != null) ...<Widget>[
                  Image.asset(
                    icon!,
                    width: 16.0,
                    height: 16.0,
                  ),
                  const SizedBox(width: 5.0),
                ],
                Text(
                  title,
                  style: TextStyle(
                    color: AppColors.grey2F,
                    fontSize: titleFontSize ?? 15.0,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const Spacer(),
                if (trailing != null) trailing!,
              ],
            ),
          ),
          Container(
            height: 0,
            decoration: BoxDecoration(
              border: Border.all(
                color: AppColors.greyEF,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 8.0,
              vertical: 16.0,
            ),
            child: content,
          )
        ],
      ),
    );
  }
}

class TextBannerView extends StatefulWidget {
  final List<TextBanner> items;

  const TextBannerView({
    Key? key,
    required this.items,
  }) : super(key: key);

  @override
  _TextBannerViewState createState() => _TextBannerViewState();
}

class _TextBannerViewState extends State<TextBannerView> {
  PageController? pageController;

  Timer? _timer;

  int _currentNewsIndex = 0;

  @override
  void initState() {
    setTimer();
    pageController = PageController(
      initialPage: _currentNewsIndex,
    );
    super.initState();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 20.0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: PageView.builder(
          controller: pageController,
          scrollDirection: Axis.vertical,
          itemCount: widget.items.length,
          itemBuilder: (_, int index) {
            return GestureDetector(
              onTap: widget.items[index].link != null
                  ? () {
                      launch(widget.items[index].link!);
                    }
                  : null,
              child: Text(
                widget.items[index].message,
                style: const TextStyle(
                  color: AppColors.grey2F,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w600,
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  void setTimer() {
    _timer = Timer.periodic(
      const Duration(milliseconds: 3000),
      (Timer timer) {
        if (pageController!.hasClients) {
          setState(() {
            _currentNewsIndex++;
            if (_currentNewsIndex >= widget.items.length) {
              _currentNewsIndex = 0;
            }
            pageController?.animateToPage(
              _currentNewsIndex,
              duration: const Duration(milliseconds: 500),
              curve: Curves.easeOutQuint,
            );
          });
        }
      },
    );
  }
}

class TitleIconButton extends StatelessWidget {
  final String title;
  final String icon;
  final VoidCallback? onTap;

  const TitleIconButton({
    Key? key,
    required this.title,
    required this.icon,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Column(
          children: <Widget>[
            Container(
              height: 42.0,
              width: 45.0,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: const <BoxShadow>[
                  BoxShadow(
                    color: AppColors.black16,
                    offset: Offset(0, 3),
                    blurRadius: 6,
                  ),
                ],
              ),
              child: Image.asset(
                icon,
                height: 24.0,
                width: 24.0,
              ),
            ),
            const SizedBox(height: 7.5),
            Text(
              title,
              maxLines: 1,
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width > 380 ? 14.0 : 11.0,
                color: AppColors.grey2F,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
