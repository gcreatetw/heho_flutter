import 'package:app_settings/app_settings.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/config/constants.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/models/collection_data.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/models/weather_data.dart';
import 'package:hepo_app/pages/common/web_view_page.dart';
import 'package:hepo_app/pages/member/comments_page.dart';
import 'package:hepo_app/pages/member/edit_member_data_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/app_tracking_transparency_utils.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/utils/firebase_message_utils.dart';
import 'package:hepo_app/utils/preferences.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:hepo_app/widgets/yes_no_dialog.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '設定',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Image.asset(
            ImageAssets.memberScreenBackground2x,
            width: double.infinity,
            height: 260,
            fit: BoxFit.cover,
          ),
          Container(
            margin: const EdgeInsets.only(top: 21.0),
            height: double.maxFinite,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(24.0),
                topRight: Radius.circular(24.0),
              ),
              color: AppColors.greyEF,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const SizedBox(height: 9),
                  const Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20.0,
                      vertical: 8.0,
                    ),
                    child: Text(
                      '會員相關',
                      style: TextStyle(
                        color: AppColors.muddyGreen,
                        fontSize: 18,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  if (context.read<UserData>().isLogin)
                    SettingTextItem(
                      title: '編輯會員資料',
                      onTap: () {
                        context.read<UserData>().fetchMemberData(context);
                        Navigator.of(context).push(
                          MaterialPageRoute<void>(
                            builder: (_) => const EditMemberDataPage(),
                          ),
                        );
                      },
                    ),
                  _divider(),
                  SettingTextItem(
                    title: '消息通知',
                    onTap: () {
                      AppSettings.openNotificationSettings();
                    },
                  ),
                  _divider(),
                  SettingTextItem(
                    title: '意見與回饋',
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (_) => const CommentPage(),
                        ),
                      );
                    },
                  ),
                  _divider(),
                  SettingTextItem(
                    title: '會員服務條款',
                    onTap: () {
                      final AppMetaData data = context.read<AppMetaData>();
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (BuildContext context) => WebViewPage(
                            title: '會員服務條款',
                            url: data.servicePolicyUrl!,
                          ),
                        ),
                      );
                      // launch(data.servicePolicyUrl!);
                    },
                  ),
                  _divider(),
                  SettingTextItem(
                    title: '隱私權政策',
                    onTap: () {
                      final AppMetaData data = context.read<AppMetaData>();
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (BuildContext context) => WebViewPage(
                            title: '隱私權政策',
                            url: data.privacyPolicyUrl!,
                          ),
                        ),
                      );
                      // launch(data.privacyPolicyUrl!);
                    },
                  ),
                  _divider(),
                  FutureProvider<PackageInfo>(
                    initialData: PackageInfo(
                      appName: 'Heho',
                      buildNumber: '',
                      version: '',
                      packageName: '',
                    ),
                    create: (_) => PackageInfo.fromPlatform(),
                    builder: (BuildContext context, _) {
                      final PackageInfo packageInfo =
                          context.watch<PackageInfo>();
                      return SettingTextItem(
                        title: '版本資訊',
                        subTitle: packageInfo.version,
                        onTap: () {},
                      );
                    },
                  ),
                  _divider(),
                  SettingTextItem(
                    title: '申請刪除帳號',
                    onTap: () {
                      final AppMetaData data = context.read<AppMetaData>();
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (BuildContext context) => const WebViewPage(
                            title: '申請刪除帳號',
                            url: 'https://heho.com.tw/account-deletion',
                          ),
                        ),
                      );
                      // launch(data.privacyPolicyUrl!);
                    },
                  ),
                  if (Constants.alpha) ...<Widget>[
                    _divider(),
                    SettingTextItem(
                      title: 'User ID',
                      subTitle: Preferences.getStringSecurity(
                        Constants.id,
                        '尚未產生',
                      ),
                      onTap: () async {
                        await FlutterClipboard.copy(
                          Preferences.getStringSecurity(Constants.id, '尚未產生') ??
                              '',
                        );
                        CenterToast.show(context, '已複製 User ID');
                      },
                    ),
                    _divider(),
                    SettingTextItem(
                      title: 'Device ID',
                      subTitle: Preferences.getStringSecurity(
                        Constants.deviceId,
                        '尚未產生',
                      ),
                      onTap: () async {
                        await FlutterClipboard.copy(
                          Preferences.getStringSecurity(
                                  Constants.deviceId, '尚未產生') ??
                              '',
                        );
                        CenterToast.show(context, '已複製 Device ID');
                      },
                    ),
                    _divider(),
                    SettingTextItem(
                      title: 'API Token',
                      subTitle: Preferences.getStringSecurity(
                        Constants.token,
                        '尚未產生',
                      ),
                      onTap: () async {
                        await FlutterClipboard.copy(
                          Preferences.getStringSecurity(
                                  Constants.token, '尚未產生') ??
                              '',
                        );
                        CenterToast.show(context, '已複製 API Token');
                      },
                    ),
                    _divider(),
                    FutureProvider<String?>(
                      initialData: null,
                      create: (_) => FirebaseMessagingUtils.instance.getToken(),
                      builder: (BuildContext context, _) {
                        return SettingTextItem(
                          title: 'FCM token',
                          subTitle: context.watch<String?>() ?? '',
                          onTap: () async {
                            await FlutterClipboard.copy(
                              context.read<String?>() ?? '',
                            );
                            CenterToast.show(context, '已複製 API Token');
                          },
                        );
                      },
                    ),
                    _divider(),
                    FutureProvider<String?>(
                      initialData: null,
                      create: (_) => AppTrackingUtils.instance.uuid,
                      builder: (BuildContext context, _) {
                        return SettingTextItem(
                          title: 'IDFA (iOS 限定)',
                          subTitle: context.watch<String?>() ?? '',
                          onTap: () async {
                            await FlutterClipboard.copy(
                              context.read<String?>() ?? '',
                            );
                            CenterToast.show(context, '已複製 IDFA');
                          },
                        );
                      },
                    ),
                    _divider(),
                    SettingTextItem(
                      title: '位置加載時間(milliseconds)',
                      subTitle: Provider.of<WeatherData>(context)
                          .locationLoadingTime
                          .toString(),
                    ),
                    _divider(),
                    SettingTextItem(
                      title: '天氣加載時間(milliseconds)',
                      subTitle: Provider.of<WeatherData>(context)
                          .weatherLoadingTime
                          .toString(),
                    ),
                    const SizedBox(height: 100),
                  ]
                ],
              ),
            ),
          ),
          if (context.watch<UserData>().isLogin)
            Positioned(
              bottom: 35.0,
              left: 20.0,
              right: 20.0,
              child: GreenButton(
                text: '登出',
                padding: const EdgeInsets.symmetric(
                  vertical: 13.0,
                ),
                radius: 26.0,
                onTap: () {
                  YesNoDialog.show(
                    context,
                    '確定要登出？',
                    onRightActionClick: () {
                      Navigator.of(context).pop();
                      context.read<UserData>().logout();
                      context.read<CollectionData>().clear();
                      Helper.instance.logout();
                    },
                  );
                },
              ),
            ),
        ],
      ),
    );
  }

  Divider _divider() {
    return const Divider(
      color: AppColors.greyC3,
      height: 0.0,
    );
  }
}

class SettingTextItem extends StatelessWidget {
  final String title;
  final String? subTitle;
  final VoidCallback? onTap;

  const SettingTextItem({
    Key? key,
    required this.title,
    this.subTitle,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 24.0,
          vertical: 6.0,
        ),
        child: Row(
          children: <Widget>[
            Text(
              title,
              style: const TextStyle(
                color: AppColors.grey2F,
                fontSize: 16,
                fontFamily: 'Montserrat',
              ),
            ),
            if (subTitle != null)
              Expanded(
                child: Text(
                  subTitle!,
                  textAlign: TextAlign.end,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    color: AppColors.grey2F,
                    fontSize: 16,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
