import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/widgets/custom_datetime_picker.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EditBirthdayPage extends StatefulWidget {
  final BuildContext context;

  const EditBirthdayPage({
    Key? key,
    required this.context,
  }) : super(key: key);

  @override
  _EditBirthdayPageState createState() => _EditBirthdayPageState();
}

class _EditBirthdayPageState extends State<EditBirthdayPage> {
  DateTime? newBirthday;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '生日',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      backgroundColor: AppColors.greyF5,
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 26.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const SizedBox(height: 20.0),
            InkWell(
              onTap: () {
                showCupertinoModalPopup(
                  context: context,
                  barrierColor: Colors.transparent,
                  builder: (BuildContext context) {
                    return CustomDatePicker(
                      initialDateTime: DateTime.now().subtract(
                        Duration(
                          days: (365.25 * 30).ceil(),
                        ),
                      ),
                      maximumDate: DateTime.now(),
                      onConfirmClick: (DateTime datetime) {
                        setState(() {
                          newBirthday = datetime;
                        });
                      },
                    );
                  },
                );
              },
              child: Container(
                height: 41.0,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Row(
                  children: <Widget>[
                    const SizedBox(
                      width: 14.0,
                    ),
                    Expanded(
                      child: Text(
                        newBirthday == null
                            ? '未設定'
                            : DateFormat('yyyy-MM-dd').format(newBirthday!),
                        style: const TextStyle(
                          color: AppColors.grey2F,
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Montserrat',
                          fontStyle: FontStyle.normal,
                          fontSize: 16,
                        ),
                      ),
                    ),
                    const Icon(
                      Icons.arrow_drop_down,
                      color: AppColors.mediumGrey,
                    ),
                    const SizedBox(
                      width: 16.0,
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 5.0,
            ),
            const Text('請正確填寫，儲存後將無法更改',
                style: TextStyle(
                    color: AppColors.muddyGreen,
                    fontWeight: FontWeight.w700,
                    fontFamily: 'Montserrat',
                    fontStyle: FontStyle.normal,
                    fontSize: 11.0),
                textAlign: TextAlign.end),
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(
          left: 20.0,
          right: 20.0,
          bottom: 35.0,
          top: 8.0,
        ),
        child: SizedBox(
          height: 50.0,
          child: GreenButton(
            text: '儲存',
            radius: 36.0,
            onTap: () {
              if (newBirthday != null) {
                context.read<UserData>().setBirthday(newBirthday!);
                context.read<UserData>().updateData(
                      context: widget.context,
                      updateType: UpdateType.birthday,
                    );
              }
              Navigator.of(context).pop(true);
            },
          ),
        ),
      ),
    );
  }
}
