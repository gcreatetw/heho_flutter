import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/svg_hint_content.dart';
import 'package:provider/provider.dart';

class HumanMapScreen extends StatefulWidget {
  const HumanMapScreen({Key? key}) : super(key: key);

  @override
  _HumanMapScreenState createState() => _HumanMapScreenState();
}

class _HumanMapScreenState extends State<HumanMapScreen> {
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
    crossPlatform: InAppWebViewOptions(
      useShouldOverrideUrlLoading: true,
      mediaPlaybackRequiresUserGesture: false,
    ),
    android: AndroidInAppWebViewOptions(
      useHybridComposition: true,
    ),
    ios: IOSInAppWebViewOptions(
      allowsInlineMediaPlayback: true,
    ),
  );

  late InAppWebViewController _webViewController;

  bool canGoBack = false;

  bool canGoForward = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          leading: IconButton(
              onPressed: canGoBack
                  ? () async {
                      if (await _webViewController.canGoBack()) {
                        _webViewController.goBack();
                      }
                    }
                  : null,
              icon: const Icon(Icons.arrow_back_ios)),
          title: Row(
            children: const <Widget>[
              // IconButton(
              //     onPressed: canGoForward
              //         ? () async {
              //             if (await _webViewController.canGoForward()) {
              //               _webViewController.goForward();
              //             }
              //           }
              //         : null,
              //     icon: const Icon(Icons.arrow_forward_ios)),
              Text(
                '人體地圖',
                style: TextStyle(
                  color: AppColors.grey2F,
                ),
              ),
            ],
          )),
      body: Consumer<AppMetaData>(
        builder: (BuildContext context, AppMetaData data, Widget? child) {
          if (data.humanBodyMapUrl == null) {
            return Container();
          }
          return (!kIsWeb && (Platform.isAndroid || Platform.isIOS))
              ? InAppWebView(
                  initialOptions: options,
                  initialUrlRequest: URLRequest(
                    url: Uri.parse(data.humanBodyMapUrl!),
                  ),
                  onWebViewCreated: (InAppWebViewController controller) {
                    _webViewController = controller;
                  },
                  onUpdateVisitedHistory: (InAppWebViewController controller,
                      Uri? uri, bool? bo) async {
                    final bool back = await _webViewController.canGoBack();
                    final bool forward =
                        await _webViewController.canGoForward();
                    setState(() {
                      canGoBack = back;
                      canGoForward = forward;
                    });
                  },
                )
              : const Center(
                  child: SvgHintContent(
                    ImageAssets.cryCancelLoveNoText,
                    message: '此平台未開放',
                  ),
                );
        },
      ),
    );
  }
}
