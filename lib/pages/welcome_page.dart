import 'dart:async';
import 'dart:math' as math;

import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/user_location.dart';
import 'package:hepo_app/models/weather_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

import 'main_page.dart';

class WelcomePage extends StatefulWidget {
  final void Function(BuildContext)? onTap;

  const WelcomePage({
    Key? key,
    this.onTap,
  }) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {


  @override
  void initState() {
    Future<void>.microtask(() async {
      final WeatherData data = Provider.of<WeatherData>(context,listen: false);
      data.reset();
      int tick = 0;
      final Timer time = Timer.periodic(const Duration(milliseconds: 1), (Timer timer) {
        tick ++;
      });
      final UserLocation location = context.read<UserLocation>();
      final bool canFetch = await location.requestPermission();
      if (canFetch) {
        await location.fetch();
      }
      time.cancel();
      data.setLocationTime(tick);
      data.fetch(
        context,
        latitude: location.lat,
        longitude: location.lng,
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[AppColors.green200, AppColors.green700],
            begin: Alignment(-1.0, -1.0),
            end: Alignment(1.0, 1.0),
            stops: <double>[0.0, 1.0],
            transform: GradientRotation(math.pi / 4),
            // tileMode: TileMode.clamp,
          ),
        ),
        child: ChangeNotifierProvider<WeatherData>.value(
            value: Provider.of<WeatherData>(context),
            builder: (BuildContext context, _) {
              final WeatherData data = context.watch<WeatherData>();
              const double imageDesignHeight = 316;
              final bool isLongHeight =
                  MediaQuery.of(context).size.height * 0.38 > 316;
              final double greenHeight = isLongHeight
                  ? MediaQuery.of(context).size.height - imageDesignHeight + 32
                  : (450.0 / MediaQuery.of(context).size.height) *
                      MediaQuery.of(context).size.height;
              final double imageSize = MediaQuery.of(context).size.height -
                  greenHeight +
                  (isLongHeight ? 60 : 20);
              return Stack(
                alignment: Alignment.topCenter,
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      height: greenHeight,
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                        color: AppColors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40.0),
                          topRight: Radius.circular(40.0),
                        ),
                      ),
                    ),
                  ),
                  SingleChildScrollView(
                    child: Container(
                      constraints: const BoxConstraints(
                        maxWidth: 680,
                      ),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: MediaQuery.of(context).padding.top + 32,
                          ),
                          // if (isLongHeight)
                          //   Container(
                          //     padding: const EdgeInsets.symmetric(
                          //       horizontal: 30.0,
                          //     ),
                          //     height: imageDesignHeight,
                          //     child: data.headImageUrl.isNotEmpty
                          //         ? Image.network(
                          //             data.headImageUrl,
                          //             alignment: Alignment.bottomCenter,
                          //           )
                          //         : Shimmer.fromColors(
                          //             baseColor: AppColors.shimmerBaseColor,
                          //             highlightColor:
                          //                 AppColors.shimmerHighlightColor,
                          //             child: Container(
                          //               color: Colors.white,
                          //             ),
                          //           ),
                          //   )
                          // else

                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 30.0,
                            ),
                            child: data.headImageUrl.isNotEmpty
                                ? Image.network(
                                    data.headImageUrl,
                                    width: imageSize,
                                    height: imageSize,
                                    alignment: Alignment.bottomCenter,
                                  )
                                : Shimmer.fromColors(
                                    baseColor: AppColors.shimmerBaseColor,
                                    highlightColor:
                                        AppColors.shimmerHighlightColor,
                                    child: Container(
                                      width: imageSize,
                                      height: imageSize,
                                      color: Colors.white,
                                    ),
                                  ),
                          ),

                          const SizedBox(height: 16.0),
                          Container(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 16.0,
                            ),
                            margin:
                                const EdgeInsets.symmetric(horizontal: 26.0),
                            decoration: const BoxDecoration(
                              color: AppColors.greyF5,
                              borderRadius: BorderRadius.all(
                                Radius.circular(15.0),
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                const SizedBox(height: 15.0),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    const SizedBox(width: 30.0),
                                    Expanded(
                                      child: data.iconUrl.isNotEmpty
                                          ? Image.asset(
                                              data.iconUrl,
                                              width: 91,
                                              height: 91,
                                            )
                                          : Shimmer.fromColors(
                                              baseColor:
                                                  AppColors.shimmerBaseColor,
                                              highlightColor: AppColors
                                                  .shimmerHighlightColor,
                                              child: Container(
                                                color: Colors.white30,
                                                width: 80,
                                                height: 80,
                                              ),
                                            ),
                                    ),
                                    const SizedBox(width: 30.0),
                                    Expanded(
                                      flex: 2,
                                      child: data.areaTitle.isNotEmpty
                                          ? Text.rich(
                                              TextSpan(
                                                text: data.areaTitle,
                                                style: const TextStyle(
                                                  color: AppColors.grey33,
                                                  fontSize: 10,
                                                  fontFamily: 'HelveticaNeue',
                                                  fontWeight: FontWeight.w700,
                                                ),
                                                children: <TextSpan>[
                                                  const TextSpan(
                                                    text: '\n平均溫度\n',
                                                    style: TextStyle(
                                                      color: AppColors.grey33,
                                                      fontSize: 20,
                                                      fontFamily:
                                                          'HelveticaNeue',
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                  ),
                                                  TextSpan(
                                                    text: data.temperature,
                                                    style: const TextStyle(
                                                      color: AppColors.grey33,
                                                      fontSize: 30,
                                                      fontFamily:
                                                          'HelveticaNeue',
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: '°C',
                                                    style: TextStyle(
                                                      color: AppColors.black,
                                                      fontSize: 24,
                                                      fontFamily:
                                                          'HelveticaNeue',
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          : Column(
                                              children: <Widget>[
                                                SizedBox(
                                                  height: 14.0,
                                                  child: Shimmer.fromColors(
                                                    baseColor: AppColors
                                                        .shimmerBaseColor,
                                                    highlightColor: AppColors
                                                        .shimmerHighlightColor,
                                                    child: Container(
                                                      color: Colors.white30,
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(height: 8),
                                                SizedBox(
                                                  height: 20.0,
                                                  child: Shimmer.fromColors(
                                                    baseColor: AppColors
                                                        .shimmerBaseColor,
                                                    highlightColor: AppColors
                                                        .shimmerHighlightColor,
                                                    child: Container(
                                                      color: Colors.white30,
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(height: 8),
                                                SizedBox(
                                                  height: 30.0,
                                                  child: Shimmer.fromColors(
                                                    baseColor: AppColors
                                                        .shimmerBaseColor,
                                                    highlightColor: AppColors
                                                        .shimmerHighlightColor,
                                                    child: Container(
                                                      color: Colors.white30,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 6.0),
                                const Divider(
                                  color: AppColors.greyDD,
                                  height: 0.0,
                                  indent: 0.0,
                                ),
                                const SizedBox(height: 15.0),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 6.0,
                                  ),
                                  child: IconInfoText(
                                    icon: IconAssets.welcomeCalendar2x,
                                    title: '紫外線',
                                    message: data.ultravioletRays,
                                  ),
                                ),
                                const SizedBox(height: 10.0),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 6.0,
                                  ),
                                  child: IconInfoText(
                                    icon: IconAssets.welcomeRain2x,
                                    title: '降雨機率',
                                    message: data.probabilityOfPrecipitation,
                                  ),
                                ),
                                const SizedBox(height: 10.0),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 6.0,
                                  ),
                                  child: IconInfoText(
                                    icon: IconAssets.welcomeAirQuality2x,
                                    title: '空氣品質',
                                    shimmerWidth: 180.0,
                                    message: data.airQuality,
                                  ),
                                ),
                                const SizedBox(height: 18.0),
                                Container(
                                  margin: const EdgeInsets.symmetric(
                                    horizontal: 6.0,
                                  ),
                                  decoration: BoxDecoration(
                                    color: const Color(0xc1ffffff),
                                    borderRadius: BorderRadius.circular(15),
                                    boxShadow: const <BoxShadow>[
                                      BoxShadow(
                                        color: Color(0x29cccccc),
                                        offset: Offset(0, 3),
                                        blurRadius: 6,
                                      ),
                                    ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0,
                                      vertical: 4.0,
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Image.asset(
                                              IconAssets.welcomeCalendar2x,
                                              width: 18.0,
                                              height: 18.0,
                                            ),
                                            const SizedBox(width: 8.0),
                                            if (data.subTitle.isNotEmpty)
                                              AutoSizeText(
                                                data.subTitle,
                                                minFontSize: 9.0,
                                                maxFontSize: 14.0,
                                                maxLines: 1,
                                                style: const TextStyle(
                                                  color: AppColors.grey33,
                                                  fontFamily: 'HelveticaNeue',
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              )
                                            else
                                              Expanded(
                                                child: SizedBox(
                                                  width: 150.0,
                                                  height: 17.0,
                                                  child: Shimmer.fromColors(
                                                    baseColor: AppColors
                                                        .shimmerBaseColor,
                                                    highlightColor: AppColors
                                                        .shimmerHighlightColor,
                                                    child: Container(
                                                      color: Colors.white30,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                          ],
                                        ),
                                        if (data.subMessage.isNotEmpty)
                                          AutoSizeText(
                                            data.subMessage,
                                            minFontSize: 8.0,
                                            maxFontSize: 13.0,
                                            style: const TextStyle(
                                              color: AppColors.grey33,
                                              fontFamily: 'HelveticaNeue',
                                              fontWeight: FontWeight.w700,
                                            ),
                                          )
                                        else
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                              vertical: 2.0,
                                            ),
                                            child: SizedBox(
                                              height: 17.0,
                                              child: Shimmer.fromColors(
                                                baseColor:
                                                    AppColors.shimmerBaseColor,
                                                highlightColor: AppColors
                                                    .shimmerHighlightColor,
                                                child: Container(
                                                  color: Colors.white30,
                                                ),
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 15.0),
                              ],
                            ),
                          ),
                          const SizedBox(height: 85.0),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      padding: const EdgeInsets.only(bottom: 16),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        gradient: LinearGradient(
                          colors: <Color>[
                            Colors.white.withOpacity(0.0),
                            Colors.white
                          ],
                          begin: const Alignment(0.0, -1.0),
                          end: const Alignment(0.0, 0.0),
                        ),
                      ),
                      child: FractionallySizedBox(
                        widthFactor: 0.65,
                        child: GreenButton(
                          radius: 26.0,
                          padding: const EdgeInsets.symmetric(vertical: 14.0),
                          text: '繼續瀏覽',
                          onTap: widget.onTap == null
                              ? () {
                                  Navigator.of(context).popAndPushNamed(
                                    MainPage.routeName,
                                  );
                                }
                              : () => widget.onTap!.call(context),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }
}

class IconInfoText extends StatelessWidget {
  final String icon;
  final String title;
  final String? message;
  final double? shimmerWidth;

  const IconInfoText({
    Key? key,
    required this.icon,
    required this.title,
    this.message,
    this.shimmerWidth,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Image.asset(
          icon,
          width: 18.0,
          height: 18.0,
        ),
        const SizedBox(width: 8.0),
        AutoSizeText(
          title,
          minFontSize: 9.0,
          maxFontSize: 14.0,
          maxLines: 1,
          style: const TextStyle(
            color: AppColors.grey33,
            fontFamily: 'HelveticaNeue',
            fontWeight: FontWeight.w700,
          ),
        ),
        Expanded(
          child: message != null && message!.isNotEmpty
              ? Text(
                  message ?? '',
                  textAlign: TextAlign.end,
                  style: const TextStyle(
                    color: AppColors.grey33,
                    fontFamily: 'HelveticaNeue',
                    fontWeight: FontWeight.w700,
                  ),
                )
              : Align(
                  alignment: Alignment.centerRight,
                  child: SizedBox(
                    width: shimmerWidth ?? 150.0,
                    height: 17.0,
                    child: Shimmer.fromColors(
                      baseColor: AppColors.shimmerBaseColor,
                      highlightColor: AppColors.shimmerHighlightColor,
                      child: Container(
                        color: Colors.white30,
                      ),
                    ),
                  ),
                ),
        ),
      ],
    );
  }
}
