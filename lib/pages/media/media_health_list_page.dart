import 'package:flutter/material.dart';
import 'package:hepo_app/models/media_health_data.dart';
import 'package:hepo_app/pages/media/media_health_list_title.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:provider/provider.dart';

class MediaHealthListPage extends StatefulWidget {
  final String? keyword;

  const MediaHealthListPage({
    Key? key,
    this.keyword,
  }) : super(key: key);

  @override
  _MediaHealthListPageState createState() => _MediaHealthListPageState();
}

class _MediaHealthListPageState extends State<MediaHealthListPage> {
  MediaHealthData data = MediaHealthData.init();

  @override
  void initState() {
    Future<void>.microtask(() {
      data.fetch(
        keyword: widget.keyword,
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MediaHealthData>.value(
      value: data,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: Text(
            widget.keyword ?? '',
            style: const TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: Builder(
                builder: (BuildContext context) {
                  final MediaHealthData data = context.watch<MediaHealthData>();
                  return ListView.separated(
                    padding: const EdgeInsets.symmetric(
                      vertical: 16.0,
                    ),
                    itemBuilder: (_, int index) {
                      return MediaHealthListTitle(
                        data: data.list[index],
                      );
                    },
                    separatorBuilder: (_, int index) {
                      return const Divider(
                        color: AppColors.greyD3,
                      );
                    },
                    itemCount: data.list.length,
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
