import 'package:flutter/material.dart';
import 'package:hepo_app/models/category_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/widgets/clip_text.dart';
import 'package:provider/provider.dart';

class MediaHealthCategoryPage extends StatefulWidget {
  const MediaHealthCategoryPage({Key? key}) : super(key: key);

  @override
  _MediaHealthCategoryPageState createState() =>
      _MediaHealthCategoryPageState();
}

class _MediaHealthCategoryPageState extends State<MediaHealthCategoryPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '所有影音',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: Container(
        color: AppColors.greyF5,
        alignment: Alignment.topLeft,
        padding: const EdgeInsets.all(16.0),
        child: Consumer<CategoryData>(
          builder: (BuildContext context, CategoryData data, _) {
            return Wrap(
              children: <Widget>[
                ...test(
                  itemCount: data.items.length,
                  builder: (BuildContext context, int index) {
                    return ClipText(
                      data.items[index].name,
                      textSize: 16.0,
                      isActive: data.currentIndex == index,
                      onTap: () {
                        data.setIndex(index);
                      },
                    );
                  },
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  List<Widget> test({
    required int itemCount,
    required Widget Function(BuildContext context, int index) builder,
  }) {
    return <Widget>[
      for (int i = 0; i < itemCount; i++) builder(context, i),
    ];
  }
}

class WrapBuilder extends StatelessWidget {
  final int itemCount;
  final Widget Function(BuildContext context, int index) builder;

  const WrapBuilder({
    Key? key,
    required this.itemCount,
    required this.builder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: <Widget>[
        for (int i = 0; i < itemCount; i++) builder(context, i),
      ],
    );
  }
}
