import 'package:flutter/material.dart';
import 'package:hepo_app/models/media_health_data.dart';
import 'package:hepo_app/pages/common/url_launch_hint_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

class MediaHealthListTitle extends StatelessWidget {
  final MediaHealth data;

  const MediaHealthListTitle({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Widget textBlock = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          data.title,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(
            color: AppColors.charcoalGrey,
            fontSize: 16,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w600,
          ),
        ),
        if (data.type != MediaHealthType.big) const SizedBox(height: 6.0),
        Text(
          '觀看次數：${data.views}人．'
          '${timeago.format(data.dateTime, locale: 'zh-TW')}',
          style: const TextStyle(
            color: AppColors.black50,
            fontSize: 10,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w600,
          ),
          textAlign: TextAlign.end,
        ),
      ],
    );
    final Widget image = Stack(
      children: <Widget>[
        if (data.type == MediaHealthType.big)
          SizedBox.expand(
            child: Image.network(
              data.imageUrl,
              fit: BoxFit.fitHeight,
            ),
          )
        else
          Image.network(
            data.imageUrl,
            fit: BoxFit.fitHeight,
          ),
        if (data.tagType != null)
          Positioned(
            top: data.type == MediaHealthType.big ? 12.0 : 5.0,
            child: Container(
              decoration: BoxDecoration(
                color: data.tagType?.color,
              ),
              padding: const EdgeInsets.symmetric(
                vertical: 2.0,
                horizontal: 12.0,
              ),
              child: Text(
                data.tagType?.text ?? '',
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 11,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          )
      ],
    );
    switch (data.type) {
      case MediaHealthType.big:
      case MediaHealthType.normal:
        return GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (_) => UrlLaunchHintPage(url: data.link),
              ),
            );
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16.0,
            ),
            child: data.type == MediaHealthType.big
                ? SizedBox(
                    height: 240,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: image,
                        ),
                        const SizedBox(height: 5.0),
                        textBlock,
                      ],
                    ),
                  )
                : Row(
                    children: <Widget>[
                      Expanded(
                        child: image,
                      ),
                      const SizedBox(width: 8.0),
                      Expanded(
                        flex: 2,
                        child: textBlock,
                      ),
                    ],
                  ),
          ),
        );
      case MediaHealthType.advertisement:
        return GestureDetector(
          onTap: () {
            launch(data.link);
          },
          child: Image.network(
            data.imageUrl,
            fit: BoxFit.fitWidth,
            height: 100,
          ),
        );
    }
  }
}
