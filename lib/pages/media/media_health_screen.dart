import 'package:flutter/material.dart';
import 'package:hepo_app/models/category_data.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';
import 'package:hepo_app/pages/home/news_list_title.dart';
import 'package:hepo_app/pages/media/media_health_category_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/fake_text_field.dart';
import 'package:hepo_app/widgets/general_state_content.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class MediaHealthScreen extends StatefulWidget {
  const MediaHealthScreen({Key? key}) : super(key: key);

  @override
  _MediaHealthScreenState createState() => _MediaHealthScreenState();
}

class _MediaHealthScreenState extends State<MediaHealthScreen>
    with TickerProviderStateMixin {
  CategoryData categoryData = CategoryData.init();

  NewsData data = NewsData.init();

  final RefreshController _refreshController = RefreshController();

  Future<void> _onRefresh() async {
    // if failed,use refreshFailed()
    fetchData();

    _refreshController.refreshCompleted();
  }

  @override
  void initState() {
    Future<void>.microtask(() {
      categoryData.fetchMediaHealth(
        context: context,
        onSuccess: () {
          fetchData();
        },
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<CategoryData>.value(value: categoryData),
        ChangeNotifierProvider<NewsData>.value(value: data),
      ],
      child: Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  AppColors.green300,
                  AppColors.green600,
                ],
              ),
            ),
          ),
          title: FakeTextField(
            height: 32.0,
            title: '搜尋（影音）',
            textColor: AppColors.greyC6,
            borderColor: Colors.transparent,
            padding: const EdgeInsets.symmetric(
              horizontal: 16.0,
              vertical: 12.0,
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (_) => const AdvanceSearchPage(
                    type: AdvanceSearchType.mediaHealth,
                  ),
                ),
              );
            },
          ),
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 8.0,
                top: 16.0,
                bottom: 16.0,
              ),
              child: Consumer<CategoryData>(
                builder: (BuildContext context, CategoryData data, _) {
                  final TabController controller = TabController(
                    initialIndex: categoryData.currentIndex,
                    length: data.items.length,
                    vsync: this,
                  );
                  controller.addListener(() {
                    if (!controller.indexIsChanging) {
                      data.currentIndex = controller.index;
                      fetchData();
                    }
                  });
                  return SizedBox(
                    height: 32.0,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: TabBar(
                            controller: controller,
                            tabs: <Tab>[
                              for (Category item in data.items)
                                Tab(
                                  text: item.name,
                                ),
                            ],
                            labelStyle: TextStyle(
                              //TODO 字體大小更好的解決方法
                              fontSize: MediaQuery.of(context).size.height > 680
                                  ? 14.0
                                  : 11.0,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                            ),
                            indicatorSize: TabBarIndicatorSize.label,
                            indicatorWeight: 2.5,
                            isScrollable: true,
                            labelColor: AppColors.black,
                            indicatorColor: AppColors.muddyGreen,
                            unselectedLabelColor: AppColors.grey2f50,
                            labelPadding: const EdgeInsets.symmetric(
                              horizontal: 8.0,
                            ),
                          ),
                        ),
                        const SizedBox(width: 11.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: GestureDetector(
                            onTap: () async {
                              await Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                  builder: (_) => ChangeNotifierProvider<
                                      CategoryData>.value(
                                    value: categoryData,
                                    child: const MediaHealthCategoryPage(),
                                  ),
                                ),
                              );
                              controller.animateTo(categoryData.currentIndex);
                              fetchData();
                            },
                            child: Image.asset(
                              IconAssets.homeCategoryList2x,
                              width: 18.0,
                              height: 18.0,
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
            Expanded(
              child: Builder(
                builder: (BuildContext context) {
                  final NewsData data = context.watch<NewsData>();
                  return NotificationListener<ScrollNotification>(
                    onNotification: (ScrollNotification scrollInfo) {
                      if (scrollInfo.metrics.pixels ==
                          scrollInfo.metrics.maxScrollExtent) {
                        _loadMore();
                      }
                      return true;
                    },
                    child: GeneralStateContent(
                      state: data.state,
                      child: SmartRefresher(
                        controller: _refreshController,
                        onRefresh: _onRefresh,
                        header: const ClassicHeader(
                          idleText: '下拉更新',
                          releaseText: '放開更新',
                        ),
                        child: ListView.separated(
                          shrinkWrap: true,
                          padding: const EdgeInsets.symmetric(
                            vertical: 8.0,
                          ),
                          itemBuilder: (_, int index) {
                            if (index == data.list.length) {
                              return Container(
                                alignment: Alignment.center,
                                padding: const EdgeInsets.only(bottom: 10.0),
                                child: Text(
                                  data.loading
                                      ? '載入中'
                                      : data.canLoadMore
                                          ? ''
                                          : '已經沒有更多文章了',
                                  style: const TextStyle(
                                    color: AppColors.charcoalGrey,
                                    fontSize: 16,
                                    fontFamily: 'HelveticaNeue',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              );
                            } else {
                              return NewsListTitle(
                                data: data.list[index],
                                useViews: false,
                              );
                            }
                          },
                          separatorBuilder: (_, int index) {
                            return const Divider(
                              color: AppColors.greyD3,
                            );
                          },
                          itemCount: data.list.length + 1,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void fetchData() {
    data.fetch(
      id: categoryData.current.id,
    );
  }

  void _loadMore() {
    data.fetch(
      append: true,
      id: categoryData.current.id,
    );
  }
}
