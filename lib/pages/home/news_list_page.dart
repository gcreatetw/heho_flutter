import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/collection_data.dart';
import 'package:hepo_app/models/disease_data.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';
import 'package:hepo_app/pages/login_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/toast.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:hepo_app/widgets/fake_text_field.dart';
import 'package:hepo_app/widgets/general_state_content.dart';
import 'package:provider/provider.dart';

import 'news_content_page.dart';
import 'news_list_title.dart';

enum NewsListMode {
  normal,
  advanced,
}

class NewsListPage extends StatefulWidget {
  final NewsListMode mode;
  final String? keyword;
  final String? title;
  final String? id;
  final String? advType;
  final String? advValue;

  const NewsListPage({
    Key? key,
    required this.mode,
    this.keyword,
    this.title,
    this.id,
    this.advType,
    this.advValue,
  }) : super(key: key);

  @override
  _NewsListPageState createState() => _NewsListPageState();
}

class _NewsListPageState extends State<NewsListPage>
    with SingleTickerProviderStateMixin {
  late TabController? controller;

  ScrollController scrollController = ScrollController();

  late NewsData data = NewsData.init();

  bool get isShowKeyword =>
      (data.keyword?.isNotEmpty ?? false) && data.disease == null;

  int currentIndex = 0;

  double scrollOffset = 0;

  @override
  void initState() {
    controller = TabController(length: 2, vsync: this);
    controller?.addListener(() {
      setState(() {
        currentIndex = controller?.index ?? 0;
      });
      scrollController.jumpTo(0);
    });
    _fetchData();
    Future<void>.microtask(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: widget.title != null
            ? Text(
                widget.title ?? '',
                style: const TextStyle(
                  color: AppColors.grey2F,
                ),
              )
            : FakeTextField(
                height: 32.0,
                title: isShowKeyword ? data.keyword ?? '' : '搜尋（疾病/症狀/科別文章）',
                textColor:
                    isShowKeyword ? AppColors.charcoalGrey : AppColors.greyC6,
                onTap: _onSearchAreaTap,
              ),
        actions: <Widget>[
          if (widget.title != null)
            InkWell(
              onTap: _onSearchAreaTap,
              borderRadius: const BorderRadius.all(
                Radius.circular(24.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Image.asset(
                  IconAssets.search2x,
                  width: 24.0,
                  height: 24.0,
                ),
              ),
            )
        ],
      ),
      body: ChangeNotifierProvider<NewsData>.value(
        value: data,
        child: Consumer<NewsData>(
          builder: (_, NewsData data, __) {
            return GeneralStateContent(
              state: data.state,
              child: _content(context, data),
            );
          },
        ),
      ),
    );
  }

  Widget _diseaseArea(BuildContext context) {
    final String introduction = data.disease?.introduction ?? '';
    const TextStyle introductionStyle = TextStyle(
      color: AppColors.grey70,
      fontSize: 16,
      fontFamily: 'HelveticaNeue',
    );
    final double textHeight = Utils.textHeight(
      introduction,
      introductionStyle,
      MediaQuery.of(context).size.width - 32,
    );
    final bool isOverMaxHeight = textHeight > 69;
    return SliverOverlapAbsorber(
      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
      sliver: SliverAppBar(
        pinned: true,
        floating: true,
        expandedHeight: 200 + (isOverMaxHeight ? 30 : 0),
        toolbarHeight: 0.0,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          background: ChangeNotifierProvider<DiseaseData>.value(
            value: data.disease!,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 5.0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(height: 16.0),
                  Row(
                    children: <Widget>[
                      Text(
                        data.disease?.name ?? '',
                        style: const TextStyle(
                          color: AppColors.charcoalGrey,
                          fontSize: 24,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(width: 18.0),
                      Container(
                        decoration: const BoxDecoration(
                          color: AppColors.beige,
                        ),
                        padding: const EdgeInsets.symmetric(
                          horizontal: 8.0,
                          vertical: 2.0,
                        ),
                        child: Text(
                          data.disease?.typeName ?? '',
                          style: const TextStyle(
                            color: AppColors.charcoalGrey,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      const Spacer(),
                      Consumer<DiseaseData>(
                          builder: (_, DiseaseData diseaseData, __) {
                        return Consumer<CollectionData>(
                            builder: (_, CollectionData collectionData, __) {
                          if (data.disease!.typeName == '症狀') {
                            diseaseData.isCollected = collectionData
                                .isSymptomCollection(data.disease!.name);
                          } else if (data.disease!.typeName == '疾病') {
                            diseaseData.isCollected = collectionData
                                .isDiseaseCollection(data.disease!.name);
                          }
                          return ImageButton(
                            diseaseData.isCollected
                                ? IconAssets.newsNavCollectSelected2x
                                : IconAssets.newsNavCollectUnselected2x,
                            onTap: () {
                              if (context.read<UserData>().isLogin) {
                                diseaseData.changeCollectState(context);
                              } else {
                                Toast.show(context, '尚未登入 請先登入才能享受完整功能');
                                Navigator.of(context).push(
                                  MaterialPageRoute<void>(
                                    builder: (_) => const LoginPage(),
                                  ),
                                );
                              }
                            },
                          );
                        });
                      }),
                    ],
                  ),
                  const SizedBox(height: 10.0),
                  Text(
                    data.disease!.subTitle,
                    style: const TextStyle(
                      color: AppColors.greyishBrown,
                      fontSize: 16,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  Text(
                    introduction,
                    style: introductionStyle,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  ),
                  if (isOverMaxHeight)
                    Expanded(
                      child: Center(
                        child: GestureDetector(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (_) => AlertDialog(
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(12.0),
                                  ),
                                ),
                                titlePadding: const EdgeInsets.only(
                                  top: 20.0,
                                  left: 11.0,
                                  right: 11.0,
                                ),
                                title: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        data.disease!.subTitle,
                                        style: const TextStyle(
                                          color: AppColors.muddyGreen,
                                          fontSize: 16,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () => Navigator.of(context).pop(),
                                      child: Image.asset(
                                        IconAssets.cancel2x,
                                        width: 18.0,
                                        height: 18.0,
                                      ),
                                    )
                                  ],
                                ),
                                contentPadding: const EdgeInsets.symmetric(
                                  vertical: 16.0,
                                  horizontal: 2.0,
                                ),
                                content: RawScrollbar(
                                  thickness: 5.0,
                                  thumbColor: AppColors.muddyGreen,
                                  radius: const Radius.circular(20),
                                  child: SingleChildScrollView(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 10.0,
                                      ),
                                      child: Text(
                                        data.disease?.introduction ?? '',
                                        style: const TextStyle(
                                          color: AppColors.grey70,
                                          fontSize: 15,
                                          fontFamily: 'HelveticaNeue',
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                              vertical: 2.0,
                              horizontal: 8.0,
                            ),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                  color: Color(0x14000000),
                                  offset: Offset(0, 3),
                                  blurRadius: 6,
                                ),
                              ],
                            ),
                            child: const Text(
                              '看更多',
                              style: TextStyle(
                                color: AppColors.greyishBrown,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _content(BuildContext context, NewsData data) {
    final List<News> currentList = data.advancedResult[currentIndex];
    final List<News> recommend = data.recommend;
    final bool hasResult = data.hasResult;
    final Widget listview = Container(
      color: AppColors.white,
      child: ListView.separated(
        controller: scrollController,
        padding: EdgeInsets.only(
          top: widget.mode == NewsListMode.normal ? 26.0 : 8.0,
        ),
        itemCount: hasResult
            ? currentList.length + recommend.length + (hasResult ? 0 : 1) + 1
            : currentList.length + recommend.length + (hasResult ? 0 : 1),
        itemBuilder: (_, int index) {
          if (index < currentList.length) {
            return NewsListTitle(
              data: currentList[index],

              ///2021/10/18 切割關鍵字' '、'-'
              keywords: widget.keyword?.split(RegExp('[ -]')) ?? <String>[],
              useViews: currentIndex == 0,
              advType: widget.advType,
            );
          } else if (index == currentList.length && hasResult) {
            return Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Text(
                data.loading
                    ? '載入中'
                    : data.canLoadMore
                        ? ''
                        : '已經沒有更多文章了',
                style: const TextStyle(
                  color: AppColors.charcoalGrey,
                  fontSize: 16,
                  fontFamily: 'HelveticaNeue',
                  fontWeight: FontWeight.w600,
                ),
              ),
            );
          } else if (index == currentList.length && !hasResult) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 14.0,
                vertical: 14.0,
              ),
              child: Text.rich(
                TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: '關於「${data.keyword}」的搜尋結果：找不到\n',
                      style: const TextStyle(
                        color: AppColors.grey70,
                        fontSize: 18,
                        fontFamily: 'HelveticaNeue',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    const TextSpan(
                      text: '抱歉，沒有這個關鍵字的搜尋結果。\n請使用其他不同的關鍵字再試一次。',
                      style: TextStyle(
                        color: AppColors.grey70,
                        fontSize: 15,
                        fontFamily: 'HelveticaNeue',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
                style: const TextStyle(
                  color: AppColors.grey70,
                  fontSize: 18,
                  fontFamily: 'HelveticaNeue',
                  fontWeight: FontWeight.w700,
                ),
              ),
            );
          } else {
            final int recommendIndex =
                index - currentList.length - (hasResult ? 0 : 1);
            return NewsListTitle(
              data: recommend[recommendIndex],
            );
          }
        },
        separatorBuilder: (_, int index) {
          if (index == currentList.length) {
            return const Padding(
              padding: EdgeInsets.symmetric(
                vertical: 8.0,
                horizontal: 14.0,
              ),
              child: Text(
                '推薦閱讀',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: 'HelveticaNeue',
                  fontWeight: FontWeight.w700,
                ),
              ),
            );
          } else {
            return const Divider(
              color: AppColors.greyD3,
            );
          }
        },
      ),
    );

    switch (widget.mode) {
      case NewsListMode.normal:
        return NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification scrollInfo) {
            if (scrollInfo.metrics.pixels ==
                scrollInfo.metrics.maxScrollExtent) {
              _loadMore();
            }
            return true;
          },
          child: listview,
        );
      case NewsListMode.advanced:
        return NotificationListener<ScrollNotification>(
            onNotification: (ScrollNotification scrollInfo) {
              setState(() {
                scrollOffset = scrollInfo.metrics.pixels;
              });
              return true;
            },
            child: NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  if (data.disease != null) _diseaseArea(context),
                  if (data.hasResult)
                    SliverPersistentHeader(
                      delegate: _SliverAppBarDelegate(
                        TabBar(
                          controller: controller,
                          tabs: const <Tab>[
                            Tab(text: '熱門'),
                            Tab(text: '最新'),
                          ],
                          labelStyle: const TextStyle(
                            fontSize: 14.0,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600,
                          ),
                          indicatorSize: TabBarIndicatorSize.label,
                          indicatorWeight: 2.5,
                          isScrollable: innerBoxIsScrolled,
                          labelColor: AppColors.black,
                          indicatorColor: AppColors.muddyGreen,
                          unselectedLabelColor: AppColors.grey2f50,
                          labelPadding: const EdgeInsets.symmetric(
                            horizontal: 8.0,
                          ),
                        ),
                      ),
                      pinned: true,
                    )
                ];
              },
              body: NotificationListener<ScrollNotification>(
                onNotification: (ScrollNotification scrollInfo) {
                  if (scrollInfo.metrics.pixels ==
                      scrollInfo.metrics.maxScrollExtent) {
                    _loadMore();
                  }
                  return true;
                },
                child: Column(
                  children: <Widget>[
                    SizedBox(
                        height: scrollOffset - 198.5 > 0
                            ? scrollOffset - 198.5
                            : 0),
                    Expanded(child: listview)
                  ],
                ),
              ),
            ));
    }
  }

  void _fetchData() {
    switch (widget.mode) {
      case NewsListMode.normal:
        currentIndex = 1;
        data.keyword = widget.keyword;
        data.fetch(
            keyword: widget.keyword,
            id: widget.id,
            advBetween: widget.advValue != null
                ? int.parse(widget.advValue!.split(',')[1])
                : null,
            advStart: widget.advValue != null
                ? int.parse(widget.advValue!.split(',')[0])
                : null);
        break;
      case NewsListMode.advanced:

        ///2021/10/15 調用api
        if (widget.keyword != null) {
          data.keyword = widget.keyword;
          data.fetchAdvanceSearch(context, widget.keyword);
        }
        break;
    }
  }

  void _loadMore() {
    switch (widget.mode) {
      case NewsListMode.normal:
        data.fetch(
            append: true,
            keyword: widget.keyword,
            id: widget.id,
            advBetween: widget.advValue != null
                ? int.parse(widget.advValue!.split(',')[1])
                : null,
            advStart: widget.advValue != null
                ? int.parse(widget.advValue!.split(',')[0])
                : null);
        break;
      case NewsListMode.advanced:
        data.fetch(
          append: true,
          currentIndex: currentIndex,
          keyword: widget.keyword,
        );
        break;
    }
  }

  void _onSearchAreaTap() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (_) => AdvanceSearchPage(
          type: AdvanceSearchType.home,
          keyword: data.keyword,
        ),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar tabBar;

  _SliverAppBarDelegate(
    this.tabBar,
  );

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24.0),
          topRight: Radius.circular(24.0),
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: AppColors.black16,
            offset: Offset(0, -22),
            blurRadius: 22,
            spreadRadius: -22,
          ),
        ],
      ),
      child: Container(
        padding: const EdgeInsets.only(
          left: 20.0,
          right: 18.0,
          top: 16.0,
          bottom: 8.0,
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              child: tabBar.controller == null ? Container() : tabBar,
            ),
            const SizedBox(width: 11.0),
          ],
        ),
      ),
    );
  }

  @override
  double get maxExtent => tabBar.preferredSize.height;

  @override
  double get minExtent => tabBar.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
