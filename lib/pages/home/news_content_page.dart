import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/models/collection_data.dart';
import 'package:hepo_app/models/news_advertisment_data.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/login_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/toast.dart';
import 'package:hepo_app/widgets/svg_hint_content.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

enum _NewsContentMode {
  normal,
  changeTextLevel,
}

enum TextLevel {
  small,
  normal,
  big,
  veryBig,
}

extension TextLevelEx on TextLevel {
  String get text {
    switch (this) {
      case TextLevel.small:
        return '小';
      case TextLevel.normal:
        return '預設';
      case TextLevel.big:
        return '大';
      case TextLevel.veryBig:
        return '特大';
    }
  }

  int get fontSize {
    switch (this) {
      case TextLevel.small:
        return 16;
      case TextLevel.normal:
        return 24;
      case TextLevel.big:
        return 28;
      case TextLevel.veryBig:
        return 72;
    }
  }
}

class NewsContentPage extends StatefulWidget {
  const NewsContentPage({
    Key? key,
  }) : super(key: key);

  @override
  _NewsContentPageState createState() => _NewsContentPageState();
}

class _NewsContentPageState extends State<NewsContentPage> {
  _NewsContentMode mode = _NewsContentMode.normal;

  TextLevel textLevel = TextLevel.normal;

  InAppWebViewController? webViewController;

  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
    crossPlatform: InAppWebViewOptions(
      useShouldOverrideUrlLoading: true,
      mediaPlaybackRequiresUserGesture: false,
      minimumFontSize: TextLevel.veryBig.fontSize,
    ),
    android: AndroidInAppWebViewOptions(
      useHybridComposition: true,
    ),
    ios: IOSInAppWebViewOptions(
      allowsInlineMediaPlayback: true,
    ),
  );

  late NewsAdvertisementData data = NewsAdvertisementData.init();

  @override
  void initState() {
    data.fetchAdvertisement();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final News data = context.read<News>();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '最新',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: (!kIsWeb && (Platform.isAndroid || Platform.isIOS))
          ? InAppWebView(
              initialUrlRequest: URLRequest(
                url: Uri.parse(data.link),
              ),
              initialOptions: options,
              onWebViewCreated: (InAppWebViewController controller) {
                webViewController = controller;
                updateFontSize();
              },
            )
          : const Center(
              child: SvgHintContent(
                ImageAssets.cryCancelLoveNoText,
                message: '此平台未開放',
              ),
            ),
      bottomNavigationBar: Container(
        height: 80,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12.0),
            topRight: Radius.circular(12.0),
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Color(0x24000000),
              offset: Offset(3, -3),
              blurRadius: 12,
            ),
          ],
        ),
        child: bottomContent(data),
      ),
    );
  }

  Widget bottomContent(News data) {
    switch (mode) {
      case _NewsContentMode.normal:
        return Consumer<News>(builder: (_, News news, __) {
          return Row(
            children: <Widget>[
              ChangeNotifierProvider<NewsAdvertisementData>.value(
                value: this.data,
                child: Consumer<NewsAdvertisementData>(
                  builder: (_, NewsAdvertisementData data, __) {
                    return Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: data.img.isEmpty
                            ? Container()
                            : InkWell(
                                child: Image.network(
                                  data.img,
                                  alignment: Alignment.centerLeft,
                                ),
                                onTap: () {
                                  launch(data.url);
                                },
                              ),
                      ),
                    );
                  },
                ),
              ),
              ImageButton(
                IconAssets.newsNavLine2x,
                onTap: () {
                  final AppMetaData data = context.read<AppMetaData>();
                  if (data.lineFriendUrl != null) {
                    launch(data.lineFriendUrl!);
                  }
                },
              ),
              Consumer<CollectionData>(
                builder: (_, CollectionData collectionData, __) {
                  return ImageButton(
                    collectionData.isArticleCollection(data)
                        ? IconAssets.newsNavCollectSelected2x
                        : IconAssets.newsNavCollectUnselected2x,
                    onTap: () {
                      if (context.read<UserData>().isLogin) {
                        news.changeCollectState(context);
                      } else {
                        Toast.show(context, '尚未登入 請先登入才能享受完整功能');
                        Navigator.of(context).push(
                          MaterialPageRoute<void>(
                            builder: (_) => const LoginPage(),
                          ),
                        );
                      }
                    },
                  );
                },
              ),
              ImageButton(
                IconAssets.newsNavIconShare2x,
                onTap: () {
                  Share.share(news.shareText(context));
                },
              ),
              const SizedBox(width: 16.0),
              // ImageButton(
              //   IconAssets.newsNavWordLevel2x,
              //   padding: const EdgeInsets.symmetric(
              //     vertical: 10.0,
              //     horizontal: 16.0,
              //   ),
              //   onTap: () {
              //     setState(() {
              //       mode = _NewsContentMode.changeTextLevel;
              //     });
              //   },
              // ),
            ],
          );
        });
      case _NewsContentMode.changeTextLevel:
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  activeTrackColor: AppColors.muddyGreen,
                  inactiveTrackColor: AppColors.grey79,
                  thumbColor: AppColors.muddyGreen,
                  trackShape: const RectangularSliderTrackShape(),
                  trackHeight: 4.0,
                  thumbShape: const RoundSliderThumbShape(),
                  overlayShape:
                      const RoundSliderOverlayShape(overlayRadius: 0.0),
                  tickMarkShape:
                      const RoundSliderTickMarkShape(tickMarkRadius: 6.0),
                  inactiveTickMarkColor: AppColors.grey79,
                  activeTickMarkColor: AppColors.muddyGreen,
                ),
                child: Slider(
                  value: textLevel.index.toDouble(),
                  max: TextLevel.values.length.toDouble() - 1,
                  divisions: 3,
                  onChangeEnd: (double value) {
                    setState(() {
                      mode = _NewsContentMode.normal;
                    });
                    updateFontSize();
                  },
                  onChanged: (double value) {
                    setState(() {
                      textLevel = TextLevel.values[value.toInt()];
                    });
                  },
                ),
              ),
              const SizedBox(height: 4.0),
              Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    for (TextLevel item in TextLevel.values) Text(item.text),
                  ],
                ),
              ),
            ],
          ),
        );
    }
  }

  //未使用
  void updateFontSize() {
    options.crossPlatform.minimumFontSize = textLevel.fontSize;
    webViewController?.setOptions(options: options);
    webViewController?.reload();
  }
}

class ImageButton extends StatelessWidget {
  final String image;
  final VoidCallback? onTap;
  final EdgeInsets? padding;
  final double? size;

  const ImageButton(
    this.image, {
    Key? key,
    this.onTap,
    this.padding,
    this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: padding ?? const EdgeInsets.all(10.0),
        child: Image.asset(
          image,
          width: size ?? 27.0,
          height: size ?? 27.0,
        ),
      ),
    );
  }
}
