import 'package:flutter/material.dart';
import 'package:hepo_app/models/category_data.dart';
import 'package:hepo_app/pages/common/category_list_view.dart';
import 'package:hepo_app/pages/common/url_launch_hint_page.dart';
import 'package:hepo_app/pages/home/news_list_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:url_launcher/url_launcher.dart';

class NewCategoryPage extends StatefulWidget {
  final CategoryData categoryData;

  const NewCategoryPage({
    Key? key,
    required this.categoryData,
  }) : super(key: key);

  @override
  _NewCategoryPageState createState() => _NewCategoryPageState();
}

class _NewCategoryPageState extends State<NewCategoryPage> {
  @override
  Widget build(BuildContext context) {
    final CategoryData data = widget.categoryData;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '所有分類',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: CategoryListView(
        categoryData: data,
        onTap: (SubCategory subCategory) {
          if (subCategory.linkUrl != null) {
            if (subCategory.linkUrl!.contains('heho.com.tw')) {
              launch(subCategory.linkUrl!);
            } else {
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (_) => UrlLaunchHintPage(
                    url: subCategory.linkUrl!,
                  ),
                ),
              );
            }
          } else {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (_) => NewsListPage(
                  mode: NewsListMode.normal,
                  id: subCategory.id,
                  advType: subCategory.advType,
                  advValue: subCategory.advValue,
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
