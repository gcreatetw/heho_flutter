import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/symptom_search_data.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';
import 'package:hepo_app/pages/home/hospital_search_page.dart';
import 'package:hepo_app/pages/home/symptom_category_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/clip_text.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'news_list_page.dart';
import 'news_list_title.dart';

class SymptomSearchPage extends StatefulWidget {
  static const String routeName = '/symptom';

  const SymptomSearchPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<SymptomSearchPage> {
  SymptomSearchData data = SymptomSearchData.init();

  @override
  void initState() {
    Future<void>.microtask(() {
      data.fetch(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SymptomSearchData>.value(
      value: data,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '症狀查詢',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        backgroundColor: AppColors.white,
        body: SingleChildScrollView(
          child: Consumer<SymptomSearchData>(
            builder: (_, SymptomSearchData data, __) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _header(),
                  const SizedBox(height: 8.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 21.0),
                    child: Row(
                      children: <Widget>[
                        const Expanded(
                          child: Text(
                            '常見熱門症狀',
                            style: TextStyle(
                              color: AppColors.charcoalGrey,
                              fontSize: 18,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute<void>(
                                builder: (_) => const SymptomCategoryPage(),
                              ),
                            );
                          },
                          child: Row(
                            children: <Widget>[
                              const Text(
                                '症狀列表',
                                style: TextStyle(
                                  color: AppColors.grey79,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              const SizedBox(width: 8.0),
                              Image.asset(
                                IconAssets.arrowRightGrey2x,
                                height: 10.0,
                                width: 12.0,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Wrap(
                      children: <Widget>[
                        for (String text in data.hotKeyWords)
                          ClipText(
                            text,
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                  builder: (_) => NewsListPage(
                                    mode: NewsListMode.advanced,
                                    keyword: text,
                                  ),
                                ),
                              );
                            },
                          ),
                      ],
                    ),
                  ),
                  if (data.advertisementUrl.isNotEmpty)
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: GestureDetector(
                          onTap: () => launch(data.advertisementLinkUrl),
                          child: Image.network(data.advertisementUrl),
                        ),
                      ),
                    ),
                  if (data.recommendNews.isNotEmpty) ...<Widget>[
                    const Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 16.0,
                      ),
                      child: Text(
                        '推薦文章',
                        style: TextStyle(
                          color: AppColors.charcoalGrey,
                          fontSize: 18,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.zero,
                      itemCount: data.recommendNews.length,
                      itemBuilder: (_, int index) {
                        return NewsListTitle(
                          data: data.recommendNews[index],
                        );
                      },
                      separatorBuilder: (_, int index) {
                        return const Divider(
                          color: AppColors.greyD3,
                        );
                      },
                    ),
                  ],
                  const SizedBox(height: 6.0),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _header() {
    return SizedBox(
      height: 158.0,
      child: Stack(
        children: <Widget>[
          Image.asset(
            ImageAssets.symptomSearchBackground2x,
            fit: BoxFit.cover,
            width: double.infinity,
          ),
          Positioned(
            right: 1.0,
            top: 30.0,
            child: GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(
                  context,
                  HospitalSearchPage.routeName,
                );
              },
              child: Container(
                padding: const EdgeInsets.only(
                  left: 14.0,
                  top: 3.0,
                  bottom: 3.0,
                  right: 6.0,
                ),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    bottomLeft: Radius.circular(25.0),
                  ),
                ),
                child: const Text.rich(
                  TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: 'GO\n',
                        style: TextStyle(
                          color: AppColors.muddyGreen,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      TextSpan(
                        text: '找院所',
                        style: TextStyle(
                          color: AppColors.muddyGreen,
                          fontSize: 9,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: AppColors.muddyGreen,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 48.0,
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(31),
                  topRight: Radius.circular(31),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (_) => const AdvanceSearchPage(
                            type: AdvanceSearchType.symptom,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16.0,
                        vertical: 6.0,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(28),
                        boxShadow: const <BoxShadow>[
                          BoxShadow(
                            color: Color(0x14000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 11.0,
                              right: 13.0,
                            ),
                            child: Image.asset(
                              IconAssets.search,
                              color: AppColors.grey7F,
                            ),
                          ),
                          const AutoSizeText(
                            '搜尋（疾病/症狀/科別文章）',
                            maxFontSize: 14.0,
                            style: TextStyle(
                              color: AppColors.grey33,
                              fontSize: 14.0,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
