import 'package:flutter/material.dart';
import 'package:hepo_app/models/health_life_data.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';
import 'package:hepo_app/pages/home/health_life_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/widgets/fake_text_field.dart';
import 'package:hepo_app/widgets/general_state_content.dart';
import 'package:provider/provider.dart';

import 'news_list_title.dart';

class HealthLifeNewListPage extends StatefulWidget {
  final HealthLifeTypeData? healthLifeTypeData;
  final String? keyword;

  const HealthLifeNewListPage({
    Key? key,
    this.healthLifeTypeData,
    this.keyword,
  }) : super(key: key);

  @override
  _HealthLifeNewListPageState createState() => _HealthLifeNewListPageState();
}

class _HealthLifeNewListPageState extends State<HealthLifeNewListPage> {
  NewsData data = NewsData.init();

  @override
  void initState() {
    Future<void>.microtask(() {
      data.fetchHealthLife(
        id: widget.healthLifeTypeData == null
            ? null
            : widget.healthLifeTypeData!.id,
        query: widget.keyword,
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final HealthLifeTypeData? healthLifeTypeData = widget.healthLifeTypeData;
    return ChangeNotifierProvider<NewsData>.value(
      value: data,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: Text(
            widget.keyword ?? '',
            style: const TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            if (healthLifeTypeData != null)
              Container(
                height: 70.0,
                color: healthLifeTypeData.type.color,
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 5.0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FakeTextField(
                      height: 32.0,
                      radius: 28.0,
                      title: '搜尋（${healthLifeTypeData.name}文章）',
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute<void>(
                            builder: (_) => AdvanceSearchPage(
                              type: AdvanceSearchType.healthLife,
                              healthLifeTypeData: healthLifeTypeData,
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            Expanded(
              child: Builder(
                builder: (BuildContext context) {
                  final NewsData data = context.watch<NewsData>();
                  return GeneralStateContent(
                    state: data.state,
                    child: NotificationListener<ScrollNotification>(
                      onNotification: (ScrollNotification scrollInfo) {
                        if (scrollInfo.metrics.pixels ==
                            scrollInfo.metrics.maxScrollExtent) {
                          _loadMore(
                              id: healthLifeTypeData?.id,
                              query: healthLifeTypeData == null
                                  ? null
                                  : widget.keyword);
                        }
                        return true;
                      },
                      child: ListView.separated(
                        padding: const EdgeInsets.symmetric(
                          vertical: 16.0,
                        ),
                        itemBuilder: (_, int index) {
                          if (index == data.list.length) {
                            return Container(
                              alignment: Alignment.center,
                              padding: const EdgeInsets.only(bottom: 10.0),
                              child: Text(
                                data.loading
                                    ? '載入中'
                                    : data.canLoadMore
                                        ? ''
                                        : '已經沒有更多文章了',
                                style: const TextStyle(
                                  color: AppColors.charcoalGrey,
                                  fontSize: 16,
                                  fontFamily: 'HelveticaNeue',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            );
                          } else {
                            return NewsListTitle(
                              data: data.list[index],
                            );
                          }
                        },
                        separatorBuilder: (_, int index) {
                          return const Divider(
                            color: AppColors.greyD3,
                          );
                        },
                        itemCount: data.list.length + 1,
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  void _loadMore({String? id, String? query}) {
    data.fetchHealthLife(
      append: true,
      id: id,
      query: query,
    );
  }
}
