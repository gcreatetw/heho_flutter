import 'package:flutter/material.dart';
import 'package:hepo_app/models/advertisement_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:url_launcher/url_launcher.dart';

class AdvertisementDialog extends StatelessWidget {
  final AdvertisementData data;

  const AdvertisementDialog({
    Key? key,
    required this.data,
  }) : super(key: key);

  double get padding {
    switch (data.type) {
      case AdvertisementType.fullScreen:
        return 12.0;
      case AdvertisementType.square:
        return 12.0;
      case AdvertisementType.rectangle:
        return 28.0;
    }
  }

  static void show(
    BuildContext context, {
    required AdvertisementData data,
  }) {
    showDialog(
      context: context,
      barrierColor: AppColors.black87,
      barrierDismissible: false,
      builder: (_) => WillPopScope(
        onWillPop: () async => false,
        child: AdvertisementDialog(
          data: data,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Padding image = Padding(
      padding: const EdgeInsets.symmetric(vertical: 12.0),
      child: GestureDetector(
        onTap: data.linkUrl != null && data.linkUrl!.isNotEmpty
            ? () => launch(data.linkUrl!)
            : null,
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(
              data.type == AdvertisementType.rectangle ? 0.0 : 20.0,
            ),
            bottomLeft: Radius.circular(
              data.type == AdvertisementType.rectangle ? 0.0 : 20.0,
            ),
            topLeft: Radius.circular(
              data.type == AdvertisementType.rectangle ? 40.0 : 20.0,
            ),
            bottomRight: Radius.circular(
              data.type == AdvertisementType.rectangle ? 40.0 : 20.0,
            ),
          ),
          child: Image.network(
            data.imageUrl,
          ),
        ),
      ),
    );
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: EdgeInsets.all(padding),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if (data.type != AdvertisementType.fullScreen) ...<Widget>[
              Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    width: 28.0,
                    height: 28.0,
                    padding: const EdgeInsets.all(8.0),
                    decoration: const BoxDecoration(
                      color: AppColors.paleLilac,
                      borderRadius: BorderRadius.all(
                        Radius.circular(24.0),
                      ),
                    ),
                    child: Image.asset(IconAssets.cancel2x),
                  ),
                ),
              ),
              const SizedBox(height: 8.0),
            ],
            if (data.type == AdvertisementType.fullScreen)
              Expanded(child: image)
            else
              image,
            if (data.type == AdvertisementType.fullScreen) ...<Widget>[
              const SizedBox(height: 10.0),
              if (data.linkAvailable)
                FractionallySizedBox(
                  widthFactor: 0.65,
                  child: GreenButton(
                    text: '立即前往',
                    padding: const EdgeInsets.symmetric(vertical: 14.0),
                    radius: 26.0,
                    onTap:
                        data.linkAvailable ? () => launch(data.linkUrl!) : null,
                  ),
                ),
              const SizedBox(height: 32.0),
              GestureDetector(
                onTap: () => Navigator.of(context).pop(),
                child: const Text(
                  '關閉',
                  style: TextStyle(
                    color: AppColors.greyC6,
                    fontSize: 17,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              const SizedBox(
                height: 32.0,
                width: double.infinity,
              ),
            ],
          ],
        ),
      ),
    );
  }
}
