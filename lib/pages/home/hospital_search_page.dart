import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/hospital_meta_data.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';
import 'package:hepo_app/pages/home/hospital_list_page.dart';
import 'package:hepo_app/pages/home/symptom_search_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:provider/provider.dart';

class HospitalSearchPage extends StatefulWidget {
  static const String routeName = '/hospital';

  const HospitalSearchPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<HospitalSearchPage> {
  @override
  void initState() {
    Future<void>.microtask(() {
      context.read<HospitalMetaData>().fetch();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '查院所',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      backgroundColor: AppColors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _header(),
            Container(
              color: AppColors.white,
              child: Consumer<HospitalMetaData>(
                builder: (_, HospitalMetaData data, __) {
                  return ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: data.areas.length,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                      vertical: 8.0,
                    ),
                    itemBuilder: (_, int index) {
                      final Area area = data.areas[index];
                      final List<City> items = area.items;
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            area.name,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontFamily: 'Helvetica',
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          const SizedBox(height: 5.0),
                          Wrap(
                            spacing: 16.0,
                            children: <Widget>[
                              for (City city in items)
                                _CityClipText(
                                  city.name,
                                  onTap: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute<void>(
                                        builder: (_) => ChangeNotifierProvider<
                                            HospitalMetaData>.value(
                                          value: data,
                                          child: HospitalListPage(
                                            title: city.name,
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                            ],
                          ),
                        ],
                      );
                    },
                    separatorBuilder: (_, __) {
                      return const Divider(
                        color: AppColors.greyC6,
                      );
                    },
                  );
                },
              ),
            ),
            const SizedBox(height: 6.0),
          ],
        ),
      ),
    );
  }

  Widget _header() {
    return SizedBox(
      height: 158.0,
      child: Stack(
        children: <Widget>[
          Image.asset(
            ImageAssets.hospitalSearchBackground2x,
            fit: BoxFit.cover,
            width: double.infinity,
          ),
          Positioned(
            right: 1.0,
            top: 30.0,
            child: GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(
                  context,
                  SymptomSearchPage.routeName,
                );
              },
              child: Container(
                padding: const EdgeInsets.only(
                  left: 14.0,
                  top: 3.0,
                  bottom: 3.0,
                  right: 6.0,
                ),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    bottomLeft: Radius.circular(25.0),
                  ),
                ),
                child: const Text.rich(
                  TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: 'GO\n',
                        style: TextStyle(
                          color: AppColors.muddyGreen,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      TextSpan(
                        text: '找症狀',
                        style: TextStyle(
                          color: AppColors.muddyGreen,
                          fontSize: 9,
                          fontFamily: 'Arial',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: AppColors.muddyGreen,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 48.0,
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(31),
                  topRight: Radius.circular(31),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (_) =>
                              ChangeNotifierProvider<HospitalMetaData>.value(
                            value: context.read<HospitalMetaData>(),
                            child: const AdvanceSearchPage(
                              type: AdvanceSearchType.hospital,
                            ),
                          ),
                        ),
                      );
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16.0,
                        vertical: 6.0,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(28),
                        boxShadow: const <BoxShadow>[
                          BoxShadow(
                            color: Color(0x14000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 11.0,
                              right: 13.0,
                            ),
                            child: Image.asset(
                              IconAssets.search,
                              color: AppColors.grey7F,
                            ),
                          ),
                          const AutoSizeText(
                            '搜尋（醫事機構名稱）',
                            maxFontSize: 14.0,
                            style: TextStyle(
                              color: AppColors.grey33,
                              fontSize: 14.0,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _CityClipText extends StatelessWidget {
  final String text;
  final VoidCallback? onTap;

  const _CityClipText(
    this.text, {
    Key? key,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 42,
        width: 80,
        margin: const EdgeInsets.symmetric(
          vertical: 5.0,
        ),
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
        ),
        decoration: BoxDecoration(
          color: AppColors.greyF5,
          borderRadius: BorderRadius.circular(6),
        ),
        alignment: Alignment.center,
        child: Text(
          text,
          style: const TextStyle(
            color: AppColors.grey33,
            fontSize: 16,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
