import 'package:easy_rich_text/easy_rich_text.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/keyword_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/widgets/clip_text.dart';
import 'package:hepo_app/widgets/overlay_container.dart';
import 'package:hepo_app/widgets/search_text.dart';
import 'package:provider/provider.dart';

import 'news_list_title.dart';

class SearchHintPanel extends StatefulWidget {
  final bool show;
  final void Function(String) onKeywordTap;

  const SearchHintPanel({
    Key? key,
    required this.show,
    required this.onKeywordTap,
  }) : super(key: key);

  @override
  _SearchHintPanelState createState() => _SearchHintPanelState();
}

class _SearchHintPanelState extends State<SearchHintPanel> {
  @override
  Widget build(BuildContext context) {
    final KeywordData data = context.read<KeywordData>();
    return OverlayAnimationContainer(
      show: widget.show,
      key: widget.key,
      materialColor: AppColors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          const SizedBox(
            height: 8.0,
          ),
          if (context.watch<KeywordData>().text.isNotEmpty) ...<Widget>[
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 18.0),
              child: Text(
                '自動搜尋',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            for (String text in data.auto) ...<Widget>[
              GestureDetector(
                onTap: () => widget.onKeywordTap(text),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 18.0,
                    vertical: 8.0,
                  ),
                  child: EasyRichText(
                    text,
                    patternList: <EasyRichTextPattern>[
                      EasyRichTextPattern(
                        targetString: data.text,
                        style: const TextStyle(
                          color: AppColors.muddyGreen,
                          fontSize: 16,
                          fontFamily: 'HelveticaNeue',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                    defaultStyle: const TextStyle(
                      color: AppColors.grey70,
                      fontSize: 16,
                      fontFamily: 'HelveticaNeue',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              const Divider(
                color: AppColors.charcoalGrey36,
                height: 0.0,
              ),
            ],
          ] else ...<Widget>[
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                '熱門關鍵字',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            const SizedBox(height: 3.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Wrap(
                children: <Widget>[
                  for (String text in data.hot)
                    ClipText(
                      text,
                      onTap: () => widget.onKeywordTap(text),
                    ),
                ],
              ),
            ),
            const SizedBox(height: 10.0),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                '最近搜尋',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            const SizedBox(height: 6.0),
            for (String text in data.recent)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: SearchText(
                  text,
                  onTap: () => widget.onKeywordTap(text),
                ),
              ),
            if (data.recommend.isNotEmpty) ...<Widget>[
              const Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 8.0,
                  horizontal: 16.0,
                ),
                child: Text(
                  '推薦閱讀',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: 'HelveticaNeue',
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                padding: EdgeInsets.zero,
                itemCount: data.recommend.length,
                itemBuilder: (_, int index) {
                  return NewsListTitle(
                    data: data.recommend[index],
                  );
                },
                separatorBuilder: (_, int index) {
                  return const Divider(
                    color: AppColors.greyD3,
                  );
                },
              ),
            ],
            const SizedBox(height: 6.0),
          ],
        ],
      ),
    );
  }
}
