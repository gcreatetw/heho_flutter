import 'package:flutter/material.dart';
import 'package:hepo_app/models/collection_data.dart';
import 'package:hepo_app/models/hospital_data.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/common/url_launch_hint_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/toast.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../login_page.dart';
import 'news_content_page.dart';

class HospitalContentPage extends StatefulWidget {
  const HospitalContentPage({Key? key}) : super(key: key);

  @override
  _HospitalContentPageState createState() => _HospitalContentPageState();
}

class _HospitalContentPageState extends State<HospitalContentPage> {
  final List<int> weekDayList = <int>[
    DateTime.monday,
    DateTime.tuesday,
    DateTime.wednesday,
    DateTime.thursday,
    DateTime.friday,
    DateTime.saturday,
    DateTime.sunday,
  ];

  final Map<int, String> weekDayText = <int, String>{
    DateTime.monday: '週一',
    DateTime.tuesday: '週二',
    DateTime.wednesday: '週三',
    DateTime.thursday: '週四',
    DateTime.friday: '週五',
    DateTime.saturday: '週六',
    DateTime.sunday: '週日',
  };

  @override
  void initState() {
    Future<void>.microtask(
      () {
        context.read<Hospital>().fetchDetail(context);
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<Hospital>(
      builder: (_, Hospital data, __) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.white,
            iconTheme: const IconThemeData(
              color: AppColors.grey2F,
            ),
            title: const Text(
              '院內詳情',
              style: TextStyle(
                color: AppColors.grey2F,
              ),
            ),
            actions: <Widget>[
              Consumer<CollectionData>(
                builder: (_, CollectionData collectionData, __) {
                  return ImageButton(
                    collectionData.isHospitalCollection(data)
                        ? IconAssets.newsNavCollectSelected2x
                        : IconAssets.newsNavCollectUnselected2x,
                    size: 17.0,
                    padding: const EdgeInsets.symmetric(horizontal: 17.0),
                    onTap: () {
                      if (context.read<UserData>().isLogin) {
                        data.changeCollectState(context);
                      } else {
                        Toast.show(context, '尚未登入 請先登入才能享受完整功能');
                        Navigator.of(context).push(
                          MaterialPageRoute<void>(
                            builder: (_) => const LoginPage(),
                          ),
                        );
                      }
                    },
                  );
                },
              ),
            ],
          ),
          body: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      data.name,
                      style: const TextStyle(
                        color: AppColors.grey33,
                        fontSize: 24,
                        fontFamily: 'Helvetica',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    Text(
                      data.type ?? '',
                      style: const TextStyle(
                        color: AppColors.grey33,
                        fontFamily: 'Helvetica',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            '地址： ${data.address}',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: const TextStyle(
                              color: AppColors.grey33,
                              fontSize: 16,
                              fontFamily: 'HelveticaNeue',
                            ),
                          ),
                        ),
                        const SizedBox(width: 4.0),
                        InkWell(
                          onTap: () {
                            launch(
                              Uri.encodeFull(
                                'https://www.google.com/maps/search/?api=1&query='
                                '${data.address.substring(0, 3)}+${data.name}',
                              ),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(11.0),
                            child: Image.asset(
                              IconAssets.googleMap2x,
                              width: 18.0,
                              height: 18.0,
                            ),
                          ),
                        )
                      ],
                    ),
                    Text(
                      '電話： ${data.phoneNumber}',
                      style: const TextStyle(
                        color: AppColors.grey33,
                        fontSize: 16,
                        fontFamily: 'HelveticaNeue',
                      ),
                    ),
                    const SizedBox(height: 5),
                    const Text(
                      '服務項目：',
                      style: TextStyle(
                        color: AppColors.grey33,
                        fontSize: 16,
                        fontFamily: 'HelveticaNeue',
                      ),
                    ),
                    const SizedBox(height: 5),
                    Text(
                      data.description ?? '',
                      style: const TextStyle(
                        color: AppColors.grey33,
                        fontSize: 16,
                        fontFamily: 'HelveticaNeue',
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: const <BoxShadow>[
                      BoxShadow(
                        color: Color(0x1a000000),
                        offset: Offset(0, 3),
                        blurRadius: 6,
                      ),
                    ],
                  ),
                  child: Table(
                    border: const TableBorder(
                      verticalInside: BorderSide(
                        color: AppColors.greyE5,
                      ),
                    ),
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    columnWidths: const <int, TableColumnWidth>{
                      0: IntrinsicColumnWidth(),
                    },
                    children: <TableRow>[
                      TableRow(
                        decoration: const BoxDecoration(
                          color: AppColors.muddyGreen,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12.0),
                            topRight: Radius.circular(12.0),
                          ),
                        ),
                        children: <Widget>[
                          const Padding(
                            padding: EdgeInsets.all(11.0),
                            child: Text(
                              '服務時間',
                              maxLines: 1,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13,
                                fontFamily: 'Montserrat',
                              ),
                            ),
                          ),
                          for (String title in weekDayText.values)
                            Text(
                              title,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 13,
                                fontFamily: 'Montserrat',
                              ),
                            ),
                        ],
                      ),
                      for (int i = 0; i < (data.timeCodes?.length ?? 0); i++)
                        TableRow(
                          decoration: BoxDecoration(
                            color: i % 2 == 1
                                ? AppColors.greyF5
                                : Colors.transparent,
                          ),
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Text(
                                data.timeCodes![i].name,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontFamily: 'Montserrat',
                                ),
                              ),
                            ),
                            for (bool available
                                in data.timeCodes![i].serviceAvailable.values)
                              if (available)
                                Center(
                                  child: Container(
                                    height: 16.0,
                                    width: 16.0,
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(12.0),
                                      ),
                                      color: AppColors.muddyGreen,
                                    ),
                                  ),
                                )
                              else
                                Container(),
                          ],
                        ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 30.0),
              if (data.link?.isNotEmpty != null)
                SizedBox(
                  width: 140.0,
                  child: GreenButton(
                    text: '網路掛號',
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (_) => UrlLaunchHintPage(
                            url: data.link!,
                          ),
                        ),
                      );
                    },
                  ),
                ),
            ],
          ),
        );
      },
    );
  }
}
