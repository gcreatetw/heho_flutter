import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/health_life_data.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';
import 'package:hepo_app/pages/home/health_life_news_list_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/clip_text.dart';
import 'package:hepo_app/widgets/general_state_content.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import 'news_list_title.dart';

enum HealthLifeType {
  food,
  sleep,
  sport,
  chineseMedicine,
}

extension HealthLifeTypeEx on HealthLifeType {
  String get text {
    switch (this) {
      case HealthLifeType.food:
        return '食物';
      case HealthLifeType.sleep:
        return '睡眠';
      case HealthLifeType.sport:
        return '運動';
      case HealthLifeType.chineseMedicine:
        return '中醫';
    }
  }

  String get backgroundImage {
    switch (this) {
      case HealthLifeType.food:
        return ImageAssets.foodBackground2x;
      case HealthLifeType.sleep:
        return ImageAssets.sleepBackground2x;
      case HealthLifeType.sport:
        return ImageAssets.sportBackground2x;
      case HealthLifeType.chineseMedicine:
        return ImageAssets.chineseMedicineBackground2x;
    }
  }

  Color get color {
    switch (this) {
      case HealthLifeType.food:
        return const Color(0xFF566F31);
      case HealthLifeType.sleep:
        return const Color(0xFFAAD68D);
      case HealthLifeType.sport:
        return const Color(0xFF5B9794);
      case HealthLifeType.chineseMedicine:
        return const Color(0xFF86C7C5);
    }
  }
}

class HealthLifePage extends StatefulWidget {
  static const String routeName = '/health';

  const HealthLifePage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<HealthLifePage>
    with TickerProviderStateMixin {
  late TabController _tabController;

  NewsData data = NewsData.init();

  HealthLifeData healthLifeData = HealthLifeData.init();

  @override
  void initState() {
    Future<void>.microtask(() async {
      await healthLifeData.fetch(
        context,
        () {
          _fetchNewsData(id: healthLifeData.current.id, sort: 'hot');
        },
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<NewsData>.value(value: data),
        ChangeNotifierProvider<HealthLifeData>.value(value: healthLifeData)
      ],
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '健康生活',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        backgroundColor: AppColors.white,
        body: Consumer<HealthLifeData>(
          builder: (BuildContext context, HealthLifeData data, _) {
            _tabController = TabController(
              vsync: this,
              length: data.list.length,
              initialIndex: data.currentIndex,
            );
            _tabController.addListener(() {
              if (!_tabController.indexIsChanging) {
                data.currentIndex = _tabController.index;
                onTabChanged();
              }
            });
            return NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {
                  _loadMore(
                      id: data.list[_tabController.index].id, sort: 'hot');
                }
                return true;
              },
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    _header(),
                    const SizedBox(height: 8.0),
                    GridView.builder(
                      shrinkWrap: true,
                      padding: const EdgeInsets.symmetric(
                        horizontal: 8.0,
                      ),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio:
                            (MediaQuery.of(context).size.width / 2) / 70,
                      ),
                      itemCount: data.list.length,
                      itemBuilder: (_, int index) {
                        return HealthLifeCategoryCard(
                          data: data.list[index],
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute<void>(
                                builder: (_) => HealthLifeNewListPage(
                                  keyword: data.list[index].name,
                                  healthLifeTypeData: data.list[index],
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                    const SizedBox(height: 20.0),
                    SizedBox(
                      height: 32.0,
                      child: TabBar(
                        controller: _tabController,
                        tabs: <Tab>[
                          for (HealthLifeTypeData item in data.list)
                            Tab(
                              text: item.name,
                            ),
                        ],
                        labelStyle: TextStyle(
                          //TODO 字體大小更好的解決方法
                          fontSize: MediaQuery.of(context).size.height > 680
                              ? 14.0
                              : 11.0,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w600,
                        ),
                        indicatorSize: TabBarIndicatorSize.label,
                        indicatorWeight: 2.5,
                        labelColor: AppColors.black,
                        indicatorColor: AppColors.muddyGreen,
                        unselectedLabelColor: AppColors.grey2f50,
                        labelPadding: const EdgeInsets.symmetric(
                          horizontal: 8.0,
                        ),
                      ),
                    ),
                    const SizedBox(height: 16.0),
                    Builder(
                      builder: (BuildContext context) {
                        final NewsData newsData = context.watch<NewsData>();
                        return GeneralStateContent(
                          state: newsData.state,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0,
                                ),
                                child: Row(
                                  children: <Widget>[
                                    for (String tag in data.list.isNotEmpty
                                        ? data.list[_tabController.index]
                                            .subCatName
                                        : <String>[])
                                      ClipText(
                                        tag,
                                        onTap: () {
                                          Navigator.of(context).push(
                                            MaterialPageRoute<void>(
                                              builder: (_) =>
                                                  HealthLifeNewListPage(
                                                healthLifeTypeData: data
                                                    .list[_tabController.index],
                                                keyword: tag,
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 16.0),
                              ListView.separated(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (_, int index) {
                                  if (index == newsData.list.length) {
                                    return Container(
                                      alignment: Alignment.center,
                                      padding:
                                          const EdgeInsets.only(bottom: 10.0),
                                      child: Text(
                                        newsData.loading
                                            ? '載入中'
                                            : newsData.canLoadMore
                                                ? ''
                                                : '已經沒有更多文章了',
                                        style: const TextStyle(
                                          color: AppColors.charcoalGrey,
                                          fontSize: 16,
                                          fontFamily: 'HelveticaNeue',
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    );
                                  } else {
                                    return NewsListTitle(
                                      data: newsData.list[index],
                                      useViews: true,
                                    );
                                  }
                                },
                                separatorBuilder: (_, int index) {
                                  return const Divider(
                                    color: AppColors.greyD3,
                                  );
                                },
                                itemCount: newsData.list.length + 1,
                              ),
                            ],
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _header() {
    return SizedBox(
      height: 158.0,
      child: Stack(
        children: <Widget>[
          Image.asset(
            ImageAssets.healthLifeBackground2x,
            fit: BoxFit.cover,
            width: double.infinity,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 48.0,
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(31),
                  topRight: Radius.circular(31),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (_) => const AdvanceSearchPage(
                            type: AdvanceSearchType.healthLife,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16.0,
                        vertical: 6.0,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(28),
                        boxShadow: const <BoxShadow>[
                          BoxShadow(
                            color: Color(0x14000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 11.0,
                              right: 13.0,
                            ),
                            child: Image.asset(
                              IconAssets.search,
                              color: AppColors.grey7F,
                            ),
                          ),
                          const AutoSizeText(
                            '搜尋（飲食/睡眠/運動/中醫文章）',
                            maxFontSize: 14.0,
                            style: TextStyle(
                              color: AppColors.grey33,
                              fontSize: 14.0,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void onTabChanged() {
    _fetchNewsData(
      id: healthLifeData.list[_tabController.index].id,
      sort: 'hot',
    );
  }

  void _fetchNewsData({required String id, required String sort}) {
    data.fetchHealthLife(
      id: id,
      sort: sort,
    );
  }

  void _loadMore({required String id, required String sort}) {
    data.fetchHealthLife(
      append: true,
      id: id,
      sort: sort,
    );
  }
}

class HealthLifeCategoryCard extends StatelessWidget {
  final HealthLifeTypeData data;
  final VoidCallback? onTap;

  const HealthLifeCategoryCard({
    Key? key,
    required this.data,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 8.0,
        vertical: 5.0,
      ),
      child: GestureDetector(
        onTap: onTap,
        child: Material(
          elevation: 3.0,
          color: const Color(0xfff5f5f5),
          shadowColor: const Color(0x52C1C1C1),
          borderRadius: const BorderRadius.all(
            Radius.circular(12.0),
          ),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(
                Radius.circular(12.0),
              ),
              image: DecorationImage(
                image: AssetImage(data.type.backgroundImage),
                fit: BoxFit.cover,
              ),
            ),
            padding: const EdgeInsets.symmetric(
              horizontal: 16.0,
            ),
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 3.0,
                        width: 35.0,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(6.0),
                          ),
                          color: data.type.color,
                        ),
                      ),
                      const SizedBox(height: 2.0),
                      Text(
                        data.name,
                        style: const TextStyle(
                          color: AppColors.charcoalGrey,
                          fontSize: 18,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w600,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
