import 'package:flutter/material.dart';
import 'package:hepo_app/models/filter_data.dart';
import 'package:hepo_app/models/hospital_data.dart';
import 'package:hepo_app/models/hospital_meta_data.dart';
import 'package:hepo_app/models/user_location.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';
import 'package:hepo_app/pages/home/hospital_content_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/dummy_area.dart';
import 'package:hepo_app/widgets/fake_text_field.dart';
import 'package:hepo_app/widgets/general_state_content.dart';
import 'package:hepo_app/widgets/overlay_container.dart';
import 'package:provider/provider.dart';

class HospitalListPage extends StatefulWidget {
  final String? title;
  final String? keyword;

  const HospitalListPage({
    Key? key,
    this.title,
    this.keyword,
  }) : super(key: key);

  @override
  _HospitalListPageState createState() => _HospitalListPageState();
}

class _HospitalListPageState extends State<HospitalListPage> {
  HospitalMetaData get metaData => context.read<HospitalMetaData>();

  FilterData get department => metaData.departments;

  FilterData get hospitalType => metaData.types;

  bool isDepartmentFilterOpen = false;
  bool isHospitalTypeFilterOpen = false;

  bool get isFilterOpen => isDepartmentFilterOpen || isHospitalTypeFilterOpen;

  set isFilterOpen(bool value) {
    isDepartmentFilterOpen = false;
    isHospitalTypeFilterOpen = false;
  }

  HospitalData data = HospitalData.init();

  @override
  void initState() {
    Future<void>.microtask(() async {
      final UserLocation location = context.read<UserLocation>();
      final bool canFetch = await location.requestPermission();
      if (canFetch) {
        await location.fetch();
      }
      _fetchData();
    });
    super.initState();
  }

  final BoxDecoration filterDecoration = const BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.only(
      bottomLeft: Radius.circular(8.0),
      bottomRight: Radius.circular(8.0),
    ),
    boxShadow: <BoxShadow>[
      BoxShadow(
        color: Color(0x1a000000),
        offset: Offset(0, 3),
        blurRadius: 6,
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<HospitalData>.value(
      value: data,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: Text(
            widget.title ?? '搜尋結果',
            style: const TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            Container(
              height: 70.0,
              color: AppColors.muddyGreen,
              padding: const EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 5.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FakeTextField(
                    height: 32.0,
                    radius: 28.0,
                    title: '搜尋（醫事機構名稱）',
                    onTap: () {
                      setState(() {
                        isFilterOpen = false;
                      });
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (_) =>
                              ChangeNotifierProvider<HospitalMetaData>.value(
                            value: context.read<HospitalMetaData>(),
                            child: const AdvanceSearchPage(
                              type: AdvanceSearchType.hospital,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            Container(
              height: 36.0,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                color: Colors.white,
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Color(0x1a000000),
                    offset: Offset(0, 3),
                    blurRadius: 6,
                  ),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FilterTitle(
                    isActive: !department.items[0].isActive,
                    isOpen: isDepartmentFilterOpen,
                    icon: IconAssets.hospitalDepartment2x,
                    title: '診療科別',
                    onTap: () {
                      setState(() {
                        isDepartmentFilterOpen = !isDepartmentFilterOpen;
                        isHospitalTypeFilterOpen = false;
                      });
                    },
                  ),
                  Container(
                    height: 26.056640625,
                    width: 1.0,
                    color: AppColors.greyCE,
                  ),
                  FilterTitle(
                    isActive: !hospitalType.items[0].isActive,
                    isOpen: isHospitalTypeFilterOpen,
                    icon: IconAssets.hospitalType2x,
                    title: '特約類別',
                    onTap: () {
                      setState(() {
                        isHospitalTypeFilterOpen = !isHospitalTypeFilterOpen;
                        isDepartmentFilterOpen = false;
                      });
                    },
                  ),
                ],
              ),
            ),
            Expanded(
              child: DummyArea(
                ignoring: isFilterOpen,
                onIgnoreTap: () {
                  setState(() {
                    isFilterOpen = false;
                  });
                },
                child: Column(
                  children: <Widget>[
                    OverlayAnimationContainer(
                      show: isDepartmentFilterOpen,
                      decoration: filterDecoration,
                      child: ChangeNotifierProvider<FilterData>.value(
                        value: department,
                        builder: (BuildContext context, _) {
                          final FilterData data = context.watch<FilterData>();
                          return FilterListView(
                            data: data,
                            onTap: (int index, bool isActive) {
                              setState(() {
                                data.setActive(index, isActive);
                              });
                              _fetchData();
                            },
                          );
                        },
                      ),
                    ),
                    OverlayAnimationContainer(
                      show: isHospitalTypeFilterOpen,
                      decoration: filterDecoration,
                      child: ChangeNotifierProvider<FilterData>.value(
                        value: hospitalType,
                        builder: (BuildContext context, _) {
                          final FilterData data = context.watch<FilterData>();
                          return FilterListView(
                            data: data,
                            onTap: (int index, bool isActive) {
                              setState(() {
                                data.setActive(index, isActive);
                              });
                              _fetchData();
                            },
                          );
                        },
                      ),
                    ),
                    Expanded(
                      child: Consumer<HospitalData>(
                        builder: (BuildContext context, HospitalData data, _) {
                          return GeneralStateContent(
                            state: data.state,
                            hint: data.hint,
                            child: NotificationListener<ScrollNotification>(
                              onNotification: (ScrollNotification scrollInfo) {
                                if (scrollInfo.metrics.pixels ==
                                    scrollInfo.metrics.maxScrollExtent) {
                                  _fetchData(append: true);
                                }
                                return true;
                              },
                              child: ListView.separated(
                                shrinkWrap: true,
                                padding: const EdgeInsets.symmetric(
                                  vertical: 16.0,
                                ),
                                itemBuilder: (_, int index) {
                                  final Hospital hospital = data.items[index];
                                  return Visibility(
                                    visible: isVisible(hospital),
                                    child: HospitalListTitle(
                                      data: hospital,
                                    ),
                                  );
                                },
                                separatorBuilder: (_, int index) {
                                  final Hospital hospital = data.items[index];
                                  return Visibility(
                                    visible: isVisible(hospital),
                                    child: const Divider(
                                      color: AppColors.greyD3,
                                    ),
                                  );
                                },
                                itemCount: data.items.length,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool isVisible(Hospital hospital) {
    // 已確認從 API 取得 但還是先保留先前的本地過濾條件程式碼
    // final bool isContainDepartment =
    //     hospital.departments.contains(department.current?.name) ||
    //         department.index == 0;
    // final bool isContainHospitalType =
    //   hospital.type == hospitalType.current?.name || hospitalType.index == 0;
    // return (department.index == 0 && hospitalType.index == 0) ||
    //     (isContainDepartment && isContainHospitalType);
    return true;
  }

  void _fetchData({
    bool append = false,
  }) {
    final UserLocation location = context.read<UserLocation>();
    data.fetch(
      append: append,
      latitude: location.lat,
      longitude: location.lng,
      areaName: widget.title ?? '',
      types: hospitalType.activeValues,
      departments: department.activeValues,
      keyword: widget.keyword,
    );
  }
}

class HospitalListTitle extends StatelessWidget {
  final Hospital data;

  const HospitalListTitle({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute<dynamic>(
            builder: (_) => ChangeNotifierProvider<Hospital>.value(
              value: data,
              child: const HospitalContentPage(),
            ),
          ),
        );
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16.0,
          vertical: 4.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              data.name,
              style: const TextStyle(
                color: AppColors.grey33,
                fontSize: 16,
                fontFamily: 'HelveticaNeue',
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 2.0),
            Text(
              data.address,
              style: const TextStyle(
                color: AppColors.grey70,
                fontSize: 14,
                fontFamily: 'HelveticaNeue',
              ),
            ),
            const SizedBox(height: 2.0),
            Text(
              data.phoneNumber,
              style: const TextStyle(
                color: AppColors.grey70,
                fontSize: 14,
                fontFamily: 'HelveticaNeue',
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Text(
                '距離 ${data.distance} km',
                style: const TextStyle(
                  color: AppColors.muddyGreen,
                  fontSize: 12,
                  fontFamily: 'HelveticaNeue',
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FilterTitle extends StatelessWidget {
  final String icon;
  final String title;
  final bool isActive;
  final bool isOpen;
  final VoidCallback? onTap;

  const FilterTitle({
    Key? key,
    required this.icon,
    required this.title,
    required this.isActive,
    required this.isOpen,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              icon,
              color:
                  isActive || isOpen ? AppColors.muddyGreen : AppColors.grey79,
              width: 12.0,
              height: 12.0,
            ),
            const SizedBox(width: 4.0),
            Text(
              title,
              style: TextStyle(
                color: isActive || isOpen
                    ? AppColors.muddyGreen
                    : AppColors.grey79,
                fontSize: 15,
                fontFamily: 'HelveticaNeue',
                fontWeight: isActive || isOpen ? FontWeight.w700 : null,
              ),
            ),
            Icon(
              isOpen ? Icons.arrow_drop_up : Icons.arrow_drop_down,
              color: isActive ? AppColors.muddyGreen : AppColors.grey79,
              size: 18.0,
            ),
          ],
        ),
      ),
    );
  }
}

class FilterListView extends StatelessWidget {
  final FilterData data;
  final void Function(int, bool) onTap;

  const FilterListView({
    Key? key,
    required this.data,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: EdgeInsets.zero,
      itemBuilder: (_, int index) {
        final FilterItem item = data.items[index];
        return InkWell(
          onTap: () {
            onTap(index, !item.isActive);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 7.0,
            ),
            child: Row(
              children: <Widget>[
                const SizedBox(width: 32.0),
                Text(
                  item.name,
                  style: TextStyle(
                    color:
                        item.isActive ? AppColors.muddyGreen : AppColors.grey70,
                    fontFamily: 'HelveticaNeue',
                    fontWeight: item.isActive ? FontWeight.w700 : null,
                  ),
                ),
                const Spacer(),
                if (item.isActive)
                  Image.asset(
                    IconAssets.check2x,
                    width: 14.0,
                    height: 14.0,
                  ),
                const SizedBox(width: 22.0),
              ],
            ),
          ),
        );
      },
      separatorBuilder: (_, int index) {
        return const Divider(
          color: AppColors.greyD3,
          height: 0.0,
        );
      },
      itemCount: data.items.length,
    );
  }
}
