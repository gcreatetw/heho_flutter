import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/models/category_data.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';
import 'package:hepo_app/pages/common/url_launch_hint_page.dart';
import 'package:hepo_app/pages/common/web_view_page.dart';
import 'package:hepo_app/pages/home/health_life_page.dart';
import 'package:hepo_app/pages/home/hospital_search_page.dart';
import 'package:hepo_app/pages/home/news_category_page.dart';
import 'package:hepo_app/pages/home/symptom_search_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/fake_text_field.dart';
import 'package:hepo_app/widgets/general_state_content.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import 'news_list_title.dart';

enum FeatureCategory {
  symptomSearch,
  healthLife,
  healthHelp,
  kids,
  // askExpert,
  healthN,
  hospitalSearch,
  // bodyMap,
  medicineSearch,
  store,
}

extension FeatureCategoryEx on FeatureCategory {
  String get text {
    switch (this) {
      case FeatureCategory.symptomSearch:
        return '症狀查詢';
      // case FeatureCategory.bodyMap:
      //   return '人體地圖';
      case FeatureCategory.medicineSearch:
        return '藥品百科';
      // case FeatureCategory.askExpert:
      //   return '請問專家';
      case FeatureCategory.healthN:
        return '營養N次方';
      case FeatureCategory.healthHelp:
        return '健康小幫手';
      case FeatureCategory.hospitalSearch:
        return '查院所';
      case FeatureCategory.healthLife:
        return '健康生活';
      case FeatureCategory.store:
        return '希望商城';
      case FeatureCategory.kids:
        return '親子育兒';
    }
  }

  String get icon {
    switch (this) {
      case FeatureCategory.symptomSearch:
        return IconAssets.categorySymptomSearch2x;
      // case FeatureCategory.bodyMap:
      //   return IconAssets.categoryBodyMap2x;
      case FeatureCategory.medicineSearch:
        return IconAssets.categoryMedicineSearch2x;
      // case FeatureCategory.askExpert:
      //   return IconAssets.categoryAskExpert2x;
      case FeatureCategory.healthN:
        return IconAssets.categoryNPower2x;
      case FeatureCategory.healthHelp:
        return IconAssets.categoryHealthTools2x;
      case FeatureCategory.hospitalSearch:
        return IconAssets.categoryHospitalSearch2x;
      case FeatureCategory.healthLife:
        return IconAssets.categoryHealthLife2x;
      case FeatureCategory.store:
        return IconAssets.categoryHopeStore2x;
      case FeatureCategory.kids:
        return IconAssets.categoryKids2x;
    }
  }
}

class HomeScreen extends StatefulWidget {
  final void Function(FeatureCategory) onFeatureClick;

  const HomeScreen({
    Key? key,
    required this.onFeatureClick,
  }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  final double barHeight = 52;

  final BoxDecoration greyGradiant = const BoxDecoration(
    gradient: LinearGradient(
      colors: <Color>[AppColors.greyF5, AppColors.greyE5],
      begin: FractionalOffset(0.0, 0.0),
      end: FractionalOffset(1.0, 0.0),
      stops: <double>[0.0, 1.0],
      // tileMode: TileMode.clamp,
    ),
  );

  CategoryData categoryData = CategoryData.init();

  NewsData data = NewsData.init();

  final RefreshController _refreshController = RefreshController();

  Future<void> _onRefresh() async {
    // if failed,use refreshFailed()

    fetchNewsData();

    _refreshController.refreshCompleted();
  }

  @override
  void initState() {
    Future<void>.microtask(() {
      categoryData.fetchHome(
        onSuccess: () {
          fetchNewsData();
        },
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: <Color>[
                AppColors.green300,
                AppColors.green600,
              ],
            ),
          ),
        ),
        title: FakeTextField(
          height: 32.0,
          title: '搜尋（疾病/症狀/科別文章）',
          textColor: AppColors.greyC6,
          borderColor: Colors.transparent,
          padding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 12.0,
          ),
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (_) => const AdvanceSearchPage(
                  type: AdvanceSearchType.home,
                ),
              ),
            );
          },
        ),
      ),
      body: MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<CategoryData>.value(value: categoryData),
          ChangeNotifierProvider<NewsData>.value(value: data),
        ],
        child: Container(
          decoration: greyGradiant,
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  pinned: true,
                  floating: true,
                  expandedHeight: 96,
                  toolbarHeight: 0.0,
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent,
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    background: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 5.0,
                      ),
                      child: BannerSlider(
                        onFeatureClick: onFeatureClick,
                      ),
                    ),
                  ),
                ),
                Consumer<CategoryData>(
                  builder: (BuildContext context, CategoryData data, _) {
                    final TabController controller = TabController(
                      initialIndex: categoryData.currentIndex,
                      length: data.items.length,
                      vsync: this,
                    );
                    controller.addListener(() {
                      if (!controller.indexIsChanging) {
                        data.currentIndex = controller.index;
                        fetchNewsData();
                      }
                    });
                    return SliverPersistentHeader(
                      delegate: _SliverAppBarDelegate(
                        TabBar(
                          controller: controller,
                          tabs: <Tab>[
                            for (Category item in data.items)
                              Tab(
                                text: item.name,
                              ),
                          ],
                          labelStyle: TextStyle(
                            //TODO 字體大小更好的解決方法
                            fontSize: MediaQuery.of(context).size.height > 680
                                ? 14.0
                                : 11.0,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600,
                          ),
                          indicatorSize: TabBarIndicatorSize.label,
                          indicatorWeight: 2.5,
                          isScrollable: true,
                          labelColor: AppColors.black,
                          indicatorColor: AppColors.muddyGreen,
                          unselectedLabelColor: AppColors.grey2f50,
                          labelPadding: const EdgeInsets.symmetric(
                            horizontal: 8.0,
                          ),
                        ),
                        onCategoryTap: data.items.isNotEmpty
                            ? () {
                                Navigator.of(context).push(
                                  MaterialPageRoute<void>(
                                    builder: (_) =>
                                        NewCategoryPage(categoryData: data),
                                  ),
                                );
                              }
                            : null,
                      ),
                      pinned: true,
                    );
                  },
                )
              ];
            },
            body: Container(
              color: AppColors.white,
              child: Column(
                children: <Widget>[
                  const SizedBox(height: 16.0),
                  Expanded(
                    child: Builder(
                      builder: (BuildContext context) {
                        final NewsData data = context.watch<NewsData>();
                        return NotificationListener<ScrollNotification>(
                          onNotification: (ScrollNotification scrollInfo) {
                            if (scrollInfo.metrics.pixels ==
                                scrollInfo.metrics.maxScrollExtent) {
                              _loadMore();
                            }
                            return true;
                          },
                          child: GeneralStateContent(
                            state: data.state,
                            hint: data.hint,
                            child: SmartRefresher(
                              controller: _refreshController,
                              onRefresh: _onRefresh,
                              header: const ClassicHeader(
                                idleText: '下拉更新',
                                releaseText: '放開更新',
                              ),
                              child: ListView.separated(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (_, int index) {
                                  if (index == data.list.length) {
                                    return Container(
                                      alignment: Alignment.center,
                                      padding:
                                          const EdgeInsets.only(bottom: 10.0),
                                      child: Text(
                                        data.loading
                                            ? '載入中'
                                            : data.canLoadMore
                                                ? ''
                                                : '已經沒有更多文章了',
                                        style: const TextStyle(
                                          color: AppColors.charcoalGrey,
                                          fontSize: 16,
                                          fontFamily: 'HelveticaNeue',
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    );
                                  } else {
                                    return NewsListTitle(
                                      data: data.list[index],
                                      useViews: categoryData.isHotType,
                                      advType: categoryData
                                          .items[categoryData.currentIndex]
                                          .advType,
                                    );
                                  }
                                },
                                separatorBuilder: (_, int index) {
                                  return const Divider(
                                    color: AppColors.greyD3,
                                  );
                                },
                                itemCount: data.list.length + 1,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void fetchNewsData() {
    data.fetch(
        id: categoryData.current.id,
        advBetween: categoryData.current.advValue != null
            ? int.parse(categoryData.current.advValue!.split(',')[1])
            : null,
        advStart: categoryData.current.advValue != null
            ? int.parse(categoryData.current.advValue!.split(',')[0])
            : null);
  }

  void _loadMore() {
    data.fetch(
        append: true,
        id: categoryData.current.id,
        advBetween: categoryData.current.advValue != null
            ? int.parse(categoryData.current.advValue!.split(',')[1])
            : null,
        advStart: categoryData.current.advValue != null
            ? int.parse(categoryData.current.advValue!.split(',')[0])
            : null);
  }

  void onFeatureClick(FeatureCategory category) {
    final AppMetaData metaData = context.read<AppMetaData>();
    switch (category) {
      case FeatureCategory.symptomSearch:
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (_) => const SymptomSearchPage(),
          ),
        );
        break;
      case FeatureCategory.kids:
        // widget.onFeatureClick(category);
        launch(metaData.kidsUrl!);
        break;
      case FeatureCategory.medicineSearch:
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (_) => WebViewPage(
              title: '藥品百科',
              url: metaData.medicineSearchUrl!,
            ),
          ),
        );
        break;
      // case FeatureCategory.askExpert:
      //   widget.onFeatureClick(category);
      //   break;
      case FeatureCategory.healthN:
        launch('https://npower.heho.com.tw/');
        break;
      case FeatureCategory.healthHelp:
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (_) => WebViewPage(
              title: '健康小幫手',
              url: metaData.healthHelperUrl!,
            ),
          ),
        );
        break;
      case FeatureCategory.hospitalSearch:
        Navigator.of(context).pushNamed(HospitalSearchPage.routeName);
        break;
      case FeatureCategory.healthLife:
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (_) => const HealthLifePage(),
          ),
        );
        break;
      case FeatureCategory.store:
        if (metaData.hopeStoreUrl!.contains('heho.com.tw')) {
          launch(metaData.hopeStoreUrl!);
        } else {
          Navigator.of(context).push(
            MaterialPageRoute<void>(
              builder: (_) => UrlLaunchHintPage(
                url: metaData.hopeStoreUrl!,
              ),
            ),
          );
        }
        break;
    }
  }
}

class FeatureCategoryCard extends StatelessWidget {
  final FeatureCategory category;
  final void Function(FeatureCategory) onTap;

  const FeatureCategoryCard({
    Key? key,
    required this.category,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: const <BoxShadow>[
          BoxShadow(
            color: Color(0x0f000000),
            offset: Offset(0, 2),
            blurRadius: 8,
          ),
        ],
      ),
      margin: const EdgeInsets.symmetric(
        horizontal: 9.0,
        vertical: 5.0,
      ),
      child: Card(
        elevation: 0.0,
        margin: EdgeInsets.zero,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        child: InkWell(
          onTap: () => onTap(category),
          borderRadius: const BorderRadius.all(
            Radius.circular(10.0),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 4.0,
              horizontal: 2.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  category.icon,
                  width: 34.0,
                  height: 34.0,
                ),
                const SizedBox(height: 3.0),
                AutoSizeText(
                  category.text,
                  minFontSize: 9.0,
                  maxFontSize: 12.0,
                  maxLines: 1,
                  style: const TextStyle(
                    color: AppColors.grey33,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar tabBar;
  final VoidCallback? onCategoryTap;

  _SliverAppBarDelegate(
    this.tabBar, {
    this.onCategoryTap,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Material(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24.0),
          topRight: Radius.circular(24.0),
        ),
      ),
      color: AppColors.white,
      child: Container(
        padding: const EdgeInsets.only(
          left: 20.0,
          right: 8.0,
          top: 16.0,
          bottom: 8.0,
        ),
        child: Builder(
          builder: (BuildContext context) {
            return Row(
              children: <Widget>[
                Expanded(
                  child: tabBar.controller?.length == 0 ? Container() : tabBar,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 11.0),
                  child: GestureDetector(
                    onTap: onCategoryTap,
                    child: Image.asset(
                      IconAssets.homeCategoryList2x,
                      width: 18.0,
                      height: 18.0,
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }

  @override
  double get maxExtent => 48.0;

  @override
  double get minExtent => 48.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}

class BannerSlider extends StatefulWidget {
  final void Function(FeatureCategory) onFeatureClick;

  const BannerSlider({
    Key? key,
    required this.onFeatureClick,
  }) : super(key: key);

  @override
  _BannerSliderState createState() => _BannerSliderState();
}

class _BannerSliderState extends State<BannerSlider> {
  int pageIndex = 0;

  PageController pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Row(
            children: <Widget>[
              GestureDetector(
                onTap: pageIndex == 1
                    ? () {
                        setState(() {
                          pageIndex = 0;
                          pageController.animateToPage(pageIndex,
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.ease);
                        });
                      }
                    : null,
                child: pageIndex == 1
                    ? Container(
                        width: 16,
                        height: double.maxFinite,
                        margin: const EdgeInsets.symmetric(vertical: 5),
                        decoration: const BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                bottomRight: Radius.circular(10))),
                        child: const Icon(
                          Icons.arrow_back_ios_outlined,
                          color: AppColors.muddyGreen,
                          size: 12,
                        ),
                      )
                    : const SizedBox(width: 16),
              ),
              Expanded(
                child: PageView(
                  controller: pageController,
                  onPageChanged: (int index) {
                    setState(() {
                      pageIndex = index;
                    });
                  },
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        for (FeatureCategory item
                            in FeatureCategory.values.sublist(0, 4))
                          Expanded(
                            child: FeatureCategoryCard(
                              category: item,
                              onTap: widget.onFeatureClick,
                            ),
                          ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        for (FeatureCategory item
                            in FeatureCategory.values.sublist(4, 8))
                          Expanded(
                            child: FeatureCategoryCard(
                              category: item,
                              onTap: widget.onFeatureClick,
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: pageIndex == 0
                    ? () {
                        setState(() {
                          pageIndex = 1;
                          pageController.animateToPage(pageIndex,
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.ease);
                        });
                      }
                    : null,
                child: pageIndex == 0
                    ? Container(
                        width: 16,
                        height: double.maxFinite,
                        margin: const EdgeInsets.symmetric(vertical: 5),
                        decoration: const BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10))),
                        child: const Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: AppColors.muddyGreen,
                          size: 12,
                        ),
                      )
                    : const SizedBox(width: 16),
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 6,
              width: pageIndex == 0 ? 20 : 6,
              decoration: BoxDecoration(
                color:
                    pageIndex == 0 ? AppColors.muddyGreen : AppColors.black40,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            const SizedBox(width: 4),
            Container(
              height: 6,
              width: pageIndex == 1 ? 20 : 6,
              decoration: BoxDecoration(
                color:
                    pageIndex == 1 ? AppColors.muddyGreen : AppColors.black40,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
