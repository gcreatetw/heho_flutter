import 'dart:io';

import 'package:easy_rich_text/easy_rich_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:hepo_app/config/ad_constants.dart';
import 'package:hepo_app/models/news_data.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'news_content_page.dart';

class NewsListTitle extends StatefulWidget {
  final News data;
  final List<String>? keywords;
  final bool? useViews;
  final String? advType;

  const NewsListTitle({
    Key? key,
    required this.data,
    this.keywords,
    this.useViews,
    this.advType,
  }) : super(key: key);

  @override
  State<NewsListTitle> createState() => _NewsListTitleState();
}

class _NewsListTitleState extends State<NewsListTitle> {
  BannerAd? bannerAd;

  late NativeAd _ad;

  bool _isAdLoaded = false;

  bool isNative = false;

  @override
  void initState() {
    Future<void>.microtask(() async {
      // if (widget.data.type == NewsType.advertisement) {
      //   bannerAd = BannerAd(
      //     adUnitId: AdConstants.bannerUnitId,
      //     size: AnchoredAdaptiveBannerAdSize(
      //       MediaQuery.of(context).orientation,
      //       width: MediaQuery.of(context).size.width.toInt(),
      //       height: 90,
      //     ),
      //     request: const AdRequest(),
      //     listener: const BannerAdListener(),
      //   )..load();
      //   setState(() {});
      // }
      if (widget.data.type == NewsType.advertisement) {
        switch (widget.advType) {
          case 'clickforce':
            break;
          case 'ox':
            break;
          case 'google':
          default:
            _ad = NativeAd(
              adUnitId: AdConstants.nativeUnitId,
              factoryId: 'listTile',
              request: const AdRequest(),
              listener: NativeAdListener(
                onAdLoaded: (_) {
                  setState(() {
                    _isAdLoaded = true;
                  });
                },
                onAdFailedToLoad: (ad, error) {
                  // Releases an ad resource when it fails to load
                  ad.dispose();

                  print(
                      'Ad load failed (code=${error.code} message=${error.message})');
                },
              ),
            );

            _ad.load();
        }
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    bannerAd?.dispose();
    if (widget.data.type == NewsType.advertisement) {
      if (isNative) {
        _ad.dispose();
      }
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Widget textBlock = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        EasyRichText(
          widget.data.title,
          patternList: <EasyRichTextPattern>[
            for (String keyword in widget.keywords ?? <String>[])
              EasyRichTextPattern(
                targetString: keyword,
                style: const TextStyle(
                  color: AppColors.muddyGreen,
                  fontSize: 16,
                  fontFamily: 'HelveticaNeue',
                  fontWeight: FontWeight.w600,
                ),
              ),
          ],
          maxLines: 2,
          defaultStyle: const TextStyle(
            color: AppColors.charcoalGrey,
            fontSize: 16,
            fontFamily: 'HelveticaNeue',
            fontWeight: FontWeight.w600,
          ),
        ),
        Container(),
        Text(
          (widget.useViews ?? false)
              ? '瀏覽量：${NumberFormat().format(widget.data.views ?? 0)}'
              : DateFormat('yyyy/MM/dd HH:mm').format(widget.data.dateTime),
          style: const TextStyle(
            color: AppColors.grey8B,
            fontSize: 12,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w600,
          ),
          textAlign: TextAlign.end,
        ),
      ],
    );
    switch (widget.data.type) {
      case NewsType.big:
      case NewsType.normal:
        return InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (_) => ChangeNotifierProvider<News>.value(
                  value: widget.data,
                  child: const NewsContentPage(),
                ),
              ),
            );
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16.0,
            ),
            child: widget.data.type == NewsType.big
                ? Column(
                    children: <Widget>[
                      Container(
                        constraints: const BoxConstraints(maxHeight: 240.0),
                        child: Image.network(
                          widget.data.imageUrl,
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                      const SizedBox(height: 5.0),
                      textBlock,
                    ],
                  )
                : IntrinsicHeight(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Image.network(
                            widget.data.imageUrl,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        const SizedBox(width: 8.0),
                        Expanded(
                          flex: 2,
                          child: textBlock,
                        ),
                      ],
                    ),
                  ),
          ),
        );
      case NewsType.advertisement:
        switch(widget.advType){
          case 'clickforce':
            return Container(
              width: double.maxFinite,
              alignment: Alignment.center,
              height: 250,
              child: Platform.isAndroid ?  const AndroidView(
                viewType: 'ClickForce300250',
                creationParams: {},
                creationParamsCodec: StandardMessageCodec(),
              ) : const UiKitView(viewType: 'ClickForce300250',
                creationParams: {},
                creationParamsCodec: StandardMessageCodec(),),
            );
          case 'ox':
            return Container();
          case 'google':
          default:
            return _isAdLoaded
                ? SizedBox(
              height: (MediaQuery.of(context).size.width - 40 )/3/120*63,
              child: AdWidget(
                ad: _ad,
              ),
            )
                : SizedBox(height: (MediaQuery.of(context).size.width - 40 )/3/120*63,);
        }
      //   _ad == null
      //     ? bannerAd != null ? SizedBox(
      //   width: bannerAd!.size.width.toDouble(),
      //   height: bannerAd!.size.height.toDouble(),
      //   child: AdWidget(
      //     ad: bannerAd!,
      //   ),
      // ): Container()
      //     : _isAdLoaded ? SizedBox(
      //   height: 72,
      //   child: AdWidget(
      //     ad: _ad!,
      //   ),
      // ) :bannerAd != null ? SizedBox(
      //   width: bannerAd!.size.width.toDouble(),
      //   height: bannerAd!.size.height.toDouble(),
      //   child: AdWidget(
      //     ad: bannerAd!,
      //   ),
      // ): Container();
    }
  }
}
