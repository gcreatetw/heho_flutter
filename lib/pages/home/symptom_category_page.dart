import 'package:flutter/material.dart';
import 'package:hepo_app/models/category_data.dart';
import 'package:hepo_app/pages/common/category_list_view.dart';
import 'package:hepo_app/pages/home/news_list_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:provider/provider.dart';

class SymptomCategoryPage extends StatefulWidget {
  const SymptomCategoryPage({
    Key? key,
  }) : super(key: key);

  @override
  _SymptomCategoryPageState createState() => _SymptomCategoryPageState();
}

class _SymptomCategoryPageState extends State<SymptomCategoryPage> {
  CategoryData data = CategoryData.init();

  @override
  void initState() {
    Future<void>.microtask(() {
      data.fetchSymptom(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '症狀分類',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      body: ChangeNotifierProvider<CategoryData>.value(
        value: data,
        builder: (BuildContext context, _) {
          final CategoryData data = context.watch<CategoryData>();
          if (data.items.isEmpty) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          } else {
            return CategoryListView(
              categoryData: data,
              onTap: (SubCategory subCategory) {
                Navigator.of(context).push(
                  MaterialPageRoute<void>(
                    builder: (_) => NewsListPage(
                      mode: NewsListMode.advanced,
                      keyword: subCategory.name,
                    ),
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}
