import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:flutter/material.dart';
import 'package:hepo_app/models/expert_category_data.dart';
import 'package:hepo_app/models/expert_data.dart';
import 'package:hepo_app/pages/ask/expert_card.dart';
import 'package:hepo_app/pages/ask/expert_conent_page.dart';
import 'package:hepo_app/pages/ask/expert_list_page.dart';
import 'package:hepo_app/pages/ask/expert_news_list_page.dart';
import 'package:hepo_app/pages/common/advace_search_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

class AskExpertScreen extends StatefulWidget {
  const AskExpertScreen({Key? key}) : super(key: key);

  @override
  _AskExpertScreenState createState() => _AskExpertScreenState();
}

class _AskExpertScreenState extends State<AskExpertScreen> {
  PageController? controller;

  ExpertData data = ExpertData.init();

  final RefreshController _refreshController = RefreshController();

  Future<void> _onRefresh() async {
    // if failed,use refreshFailed()
    data.advertisementUrl = '';
    data.currentIndex = 0;
    data.fetch();
    data.fetchAdvertisement(context);

    setState(() {});

    _refreshController.refreshCompleted();
  }

  @override
  void initState() {
    Future<void>.microtask(
      () {
        data.fetch();
        data.fetchAdvertisement(context);
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<ExpertCategoryData>.value(
            value: data.categories,
          ),
          ChangeNotifierProvider<ExpertData>.value(value: data),
        ],
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.white,
            iconTheme: const IconThemeData(
              color: AppColors.grey2F,
            ),
            title: const Text(
              '請問專家',
              style: TextStyle(
                color: AppColors.grey2F,
              ),
            ),
          ),
          backgroundColor: AppColors.white,
          body: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            header: const MaterialClassicHeader(
              color: AppColors.muddyGreen,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  _header(),
                  const SizedBox(height: 8.0),
                  Consumer<ExpertData>(
                    builder: (BuildContext context, ExpertData data, _) {
                      if (data.advertisementUrl.isNotEmpty) {
                        return Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: ClipRRect(
                            child: GestureDetector(
                              onTap: () => launch(data.advertisementLinkUrl),
                              child: Image.network(data.advertisementUrl),
                            ),
                          ),
                        );
                      }
                      return Container();
                    },
                  ),
                  const SizedBox(height: 8.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 21.0),
                    child: Row(
                      children: <Widget>[
                        const Expanded(
                          child: Text(
                            '專家查詢',
                            style: TextStyle(
                              color: AppColors.charcoalGrey,
                              fontSize: 18,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute<void>(
                                builder: (_) => ExpertListPage(
                                  data: data.items,
                                ),
                              ),
                            );
                          },
                          child: Row(
                            children: <Widget>[
                              const Text(
                                '專家列表',
                                style: TextStyle(
                                  color: AppColors.grey79,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              const SizedBox(width: 8.0),
                              Image.asset(
                                IconAssets.arrowRightGrey2x,
                                height: 10.0,
                                width: 12.0,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  SizedBox(
                    height: 112,
                    child: Consumer<ExpertData>(
                      builder: (BuildContext context, ExpertData data, _) {
                        final double width = MediaQuery.of(context).size.width;
                        return PageView.builder(
                          controller: PageController(
                            viewportFraction: (width - 30) / width,
                          ),
                          onPageChanged: (int index) {
                            data.updateIndex(index);
                          },
                          itemCount:
                              data.items.length < 5 ? data.items.length : 5,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(
                                right: 16.0,
                              ),
                              child: Transform.translate(
                                offset: Offset(
                                    data.currentIndex != index ? -20 : 0, 0),
                                child: ExpertCard(
                                  data: data.items[index],
                                  decorationPadding: width - 24 - (16 + 30),
                                  outsideCard: true,
                                  onTap: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute<void>(
                                        builder: (_) => ExpertContentPage(
                                          expert: data.items[index],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  Consumer<ExpertData>(
                      builder: (BuildContext context, ExpertData data, _) {
                    final int count =
                        data.items.length < 5 ? data.items.length : 5;
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        for (int i = 0; i < count; i++)
                          Container(
                            height: 10,
                            width: 10,
                            margin: const EdgeInsets.symmetric(horizontal: 1.5),
                            decoration: BoxDecoration(
                              color: data.currentIndex == i
                                  ? AppColors.grey79
                                  : null,
                              borderRadius: const BorderRadius.all(
                                Radius.circular(12.0),
                              ),
                              border: Border.all(
                                color: data.currentIndex == i
                                    ? Colors.transparent
                                    : AppColors.greyC6,
                                width: 1.5,
                              ),
                            ),
                          ),
                      ],
                    );
                  }),
                  const SizedBox(height: 8.0),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 21.0),
                    child: Text(
                      '請問專家',
                      style: TextStyle(
                        color: AppColors.charcoalGrey,
                        fontSize: 18,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  Consumer<ExpertCategoryData>(
                    builder:
                        (BuildContext context, ExpertCategoryData data, _) {
                      return GridView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio:
                              (MediaQuery.of(context).size.width / 2) / 75,
                        ),
                        padding: const EdgeInsets.symmetric(
                          horizontal: 12.0,
                        ),
                        itemCount: data.items.length,
                        itemBuilder: (_, int index) {
                          return ExpertCategoryCard(
                            data: data.items[index],
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                  builder: (_) => ExpertNewsListPage(
                                    category: data.items[index],
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      );
                    },
                  ),
                  const SizedBox(height: 12.0),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _header() {
    return SizedBox(
      height: 158.0,
      child: Stack(
        children: <Widget>[
          Image.asset(
            ImageAssets.askExpertBackground2x,
            fit: BoxFit.cover,
            width: double.infinity,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 48.0,
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(31),
                  topRight: Radius.circular(31),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute<void>(
                          builder: (_) => const AdvanceSearchPage(
                            type: AdvanceSearchType.askExpert,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16.0,
                        vertical: 6.0,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(28),
                        boxShadow: const <BoxShadow>[
                          BoxShadow(
                            color: Color(0x14000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 11.0,
                              right: 13.0,
                            ),
                            child: Image.asset(
                              IconAssets.search,
                              color: AppColors.grey7F,
                            ),
                          ),
                          const AutoSizeText(
                            '搜尋',
                            maxFontSize: 14.0,
                            style: TextStyle(
                              color: AppColors.grey33,
                              fontSize: 14.0,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ExpertCategoryCard extends StatelessWidget {
  final ExpertCategory data;
  final VoidCallback? onTap;

  const ExpertCategoryCard({
    Key? key,
    required this.data,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: AppColors.greyF5,
            borderRadius: BorderRadius.circular(16),
            boxShadow: const <BoxShadow>[
              BoxShadow(
                color: Color(0x52c1c1c1),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
          child: Row(
            children: <Widget>[
              Container(
                width: 8,
                decoration: BoxDecoration(
                  color: data.color,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(16),
                    bottomLeft: Radius.circular(16),
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              const SizedBox(width: 10.0),
              Expanded(
                child: Text(
                  data.name,
                  style: const TextStyle(
                    color: AppColors.charcoalGrey,
                    fontSize: 16,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              const SizedBox(width: 16.0),
            ],
          ),
        ),
      ),
    );
  }
}
