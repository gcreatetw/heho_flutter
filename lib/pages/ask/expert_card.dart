import 'package:flutter/material.dart';
import 'package:hepo_app/models/expert_data.dart';
import 'package:hepo_app/resources/colors.dart';

class ExpertCard extends StatelessWidget {
  final Expert data;
  final VoidCallback? onTap;
  final double decorationPadding;
  final bool outsideCard;

  const ExpertCard({
    Key? key,
    required this.data,
    this.onTap,
    required this.decorationPadding,
    this.outsideCard = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: AppColors.greyF5,
            borderRadius: BorderRadius.circular(16),
            boxShadow: const <BoxShadow>[
              BoxShadow(
                color: Color(0x52c1c1c1),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
          child: Stack(
            children: <Widget>[
              Positioned.fill(
                right: decorationPadding,
                child: Container(
                  width: 8,
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                    color: AppColors.muddyGreen,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      bottomLeft: Radius.circular(16),
                    ),
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  const SizedBox(width: 18.0),
                  CircleAvatar(
                    backgroundImage: NetworkImage(data.avatarUrl),
                    backgroundColor: Colors.white,
                    maxRadius: 30.0,
                  ),
                  const SizedBox(width: 10.0),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                data.title,
                                style: const TextStyle(
                                  color: AppColors.charcoalGrey,
                                  fontSize: 16,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              const SizedBox(width: 8.0),
                              Expanded(
                                child: Text(
                                  data.name,
                                  overflow: TextOverflow.ellipsis,
                                  style: const TextStyle(
                                    color: AppColors.black,
                                    fontSize: 12,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 4.0),
                          Text(
                            data.description,
                            overflow:
                                outsideCard ? TextOverflow.ellipsis : null,
                            style: const TextStyle(
                              color: AppColors.mediumGrey,
                              fontSize: 12,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
