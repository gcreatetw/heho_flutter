import 'package:flutter/material.dart';
import 'package:hepo_app/models/expert_data.dart';
import 'package:hepo_app/models/expert_news_data.dart';
import 'package:hepo_app/pages/ask/expert_card.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:provider/provider.dart';

import 'expert_conent_page.dart';

class ExpertListPage extends StatefulWidget {
  final List<Expert> data;

  const ExpertListPage({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  _ExpertListPageState createState() => _ExpertListPageState();
}

class _ExpertListPageState extends State<ExpertListPage> {
  ExpertNewsData data = ExpertNewsData.init();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ExpertNewsData>.value(
      value: data,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '專家列表',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: Builder(
                builder: (BuildContext context) {
                  return ListView.builder(
                    padding: const EdgeInsets.symmetric(
                      vertical: 16.0,
                      horizontal: 8.0,
                    ),
                    itemBuilder: (_, int index) {
                      return ExpertCard(
                        data: widget.data[index],
                        decorationPadding:
                            MediaQuery.of(context).size.width - 40,
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute<void>(
                              builder: (_) => ExpertContentPage(
                                expert: widget.data[index],
                              ),
                            ),
                          );
                        },
                      );
                    },
                    itemCount: widget.data.length,
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
