import 'package:flutter/material.dart';
import 'package:hepo_app/models/expert_data.dart';
import 'package:hepo_app/models/expert_news_data.dart';
import 'package:hepo_app/pages/common/url_launch_hint_page.dart';
import 'package:hepo_app/pages/home/news_list_title.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/widgets/general_state_content.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class ExpertContentPage extends StatefulWidget {
  final Expert expert;

  const ExpertContentPage({
    Key? key,
    required this.expert,
  }) : super(key: key);

  @override
  _ExpertContentPageState createState() => _ExpertContentPageState();
}

class _ExpertContentPageState extends State<ExpertContentPage> {
  ExpertNewsData data = ExpertNewsData.init();

  @override
  void initState() {
    Future<void>.microtask(
      () {
        data.fetch(
          id: widget.expert.id,
        );
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<ExpertNewsData>.value(value: data),
      ],
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '專家查詢',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        backgroundColor: AppColors.white,
        body: Consumer<ExpertNewsData>(
          builder: (BuildContext context, ExpertNewsData data, _) {
            return NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {
                  _loadMore();
                }
                return true;
              },
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    _header(),
                    const SizedBox(height: 8.0),
                    Center(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 6.0,
                          horizontal: 10.0,
                        ),
                        decoration: BoxDecoration(
                          color: AppColors.greyF5,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Text.rich(
                          TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: '${widget.expert.title} ',
                                style: const TextStyle(
                                  color: AppColors.charcoalGrey,
                                  fontSize: 16,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              TextSpan(
                                text: widget.expert.name,
                                style: const TextStyle(
                                  color: AppColors.charcoalGrey,
                                  fontSize: 12,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                          style: const TextStyle(
                            color: AppColors.charcoalGrey,
                            fontSize: 16,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Text(
                        widget.expert.description,
                        style: const TextStyle(
                          color: AppColors.grey33,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    const SizedBox(height: 21.0),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 21.0),
                      child: Text(
                        '文章列表',
                        style: TextStyle(
                          color: AppColors.charcoalGrey,
                          fontSize: 18,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    GeneralStateContent(
                      state: data.state,
                      child: ListView.separated(
                        shrinkWrap: true,
                        padding: const EdgeInsets.symmetric(
                          vertical: 16.0,
                        ),
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (_, int index) {
                          if (index == data.list.length) {
                            return Container(
                              alignment: Alignment.center,
                              padding: const EdgeInsets.only(bottom: 10.0),
                              child: Text(
                                data.loading
                                    ? '載入中'
                                    : data.canLoadMore
                                        ? ''
                                        : '已經沒有更多文章了',
                                style: const TextStyle(
                                  color: AppColors.charcoalGrey,
                                  fontSize: 16,
                                  fontFamily: 'HelveticaNeue',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            );
                          } else {
                            return NewsListTitle(
                              data: data.list[index],
                            );
                          }
                        },
                        separatorBuilder: (_, int index) {
                          return const Divider(
                            color: AppColors.greyD3,
                          );
                        },
                        itemCount: data.list.length + 1,
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _header() {
    return SizedBox(
      height: 148.0,
      child: Stack(children: <Widget>[
        Image.asset(
          ImageAssets.expertPersonalBackground2x,
          fit: BoxFit.cover,
          width: double.infinity,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 64.0,
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
            ),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(31),
                topRight: Radius.circular(31),
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(60),
              ),
            ),
            padding: const EdgeInsets.all(4.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage(widget.expert.avatarUrl),
              backgroundColor: Colors.white,
              maxRadius: 50.0,
            ),
          ),
        ),
        if (widget.expert.btn != null)
          Positioned(
            right: 1.0,
            top: 30.0,
            child: GestureDetector(
              onTap: () {
                if (widget.expert.btnUrl != null) {
                  if (widget.expert.btnUrl!.contains('heho.com.tw')) {
                    launch(widget.expert.btnUrl!);
                  } else {
                    Navigator.of(context).push(
                      MaterialPageRoute<void>(
                        builder: (_) => UrlLaunchHintPage(
                          url: widget.expert.btnUrl!,
                        ),
                      ),
                    );
                  }
                }
              },
              child: Container(
                padding: const EdgeInsets.only(
                  left: 14.0,
                  top: 3.0,
                  bottom: 3.0,
                  right: 6.0,
                ),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    bottomLeft: Radius.circular(25.0),
                  ),
                ),
                child: Text(
                  widget.expert.btn!,
                  style: const TextStyle(
                    color: AppColors.muddyGreen,
                    fontFamily: 'Arial',
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          ),
      ]),
    );
  }

  void _loadMore() {
    data.fetch(
      append: true,
      id: widget.expert.id,
    );
  }
}
