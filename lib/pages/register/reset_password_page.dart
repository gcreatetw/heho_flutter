import 'package:flutter/material.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:hepo_app/widgets/outline_text_field.dart';

class ResetPasswordPage extends StatefulWidget {
  final String code;
  final String account;

  const ResetPasswordPage({
    Key? key,
    required this.account, required this.code,
  }) : super(key: key);

  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  final TextEditingController _newPassword = TextEditingController();
  final TextEditingController _newPasswordConfirm = TextEditingController();
  final FocusNode _newPasswordFocusNode = FocusNode();
  final FocusNode _newPasswordConfirmFocusNode = FocusNode();

  String firstErrorHint = '';
  String secondErrorHint = '';

  bool isSending = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        iconTheme: const IconThemeData(
          color: AppColors.grey2F,
        ),
        title: const Text(
          '重設密碼',
          style: TextStyle(
            color: AppColors.grey2F,
          ),
        ),
      ),
      backgroundColor: AppColors.greyF5,
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 26.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const SizedBox(height: 20.0),
            const Text(
              '新密碼',
              style: TextStyle(
                color: AppColors.grey2F,
                fontSize: 14.0,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(height: 10.0),
            OutlineTextField(
              height: 50.0,
              borderRadius: 26,
              controller: _newPassword,
              focusNode: _newPasswordFocusNode,
              nextFocusNode: _newPasswordConfirmFocusNode,
              enabledBorderColor: Colors.transparent,
              keyboardType: TextInputType.visiblePassword,
              autofillHints: const <String>[AutofillHints.newPassword],
              textInputAction: TextInputAction.next,
              obscureText: true,
              hintText: '新密碼（8-20 字元包含英文及數字）',
              suffixIcon: ValueListenableBuilder<TextEditingValue>(
                valueListenable: _newPassword,
                builder: (_, TextEditingValue value, __) {
                  if (value.text.isEmpty) {
                    return Image.asset(
                      IconAssets.checkNormal2x,
                    );
                  } else {
                    return Image.asset(
                      Utils.isValidPassword(value.text)
                          ? IconAssets.correct2x
                          : IconAssets.incorrect2x,
                    );
                  }
                },
              ),
              onChanged: (String text) {
                setState(() {
                  firstErrorHint = '';
                });
              },
              errorText: firstErrorHint,
            ),
            const SizedBox(height: 14.0),
            const Text(
              '確認新密碼',
              style: TextStyle(
                color: AppColors.grey2F,
                fontSize: 14.0,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(height: 10.0),
            OutlineTextField(
              height: 50.0,
              borderRadius: 26,
              controller: _newPasswordConfirm,
              focusNode: _newPasswordConfirmFocusNode,
              enabledBorderColor: Colors.transparent,
              keyboardType: TextInputType.visiblePassword,
              textInputAction: TextInputAction.send,
              obscureText: true,
              hintText: '再次確認密碼',
              onEditingComplete: _confirm,
              suffixIcon: ValueListenableBuilder<TextEditingValue>(
                valueListenable: _newPasswordConfirm,
                builder: (_, TextEditingValue value, __) {
                  if (value.text.isEmpty) {
                    return Image.asset(
                      IconAssets.checkNormal2x,
                    );
                  } else {
                    return Image.asset(
                      _newPassword.text == _newPasswordConfirm.text
                          ? IconAssets.correct2x
                          : IconAssets.incorrect2x,
                    );
                  }
                },
              ),
              onChanged: (String text) {
                setState(() {
                  secondErrorHint = '';
                });
              },
              errorText: secondErrorHint,
            ),
            const Spacer(),
            GreenButton(
              text: '重設密碼',
              radius: 26.0,
              padding: const EdgeInsets.symmetric(
                vertical: 13.0,
              ),
              onTap: isSending ? null : _confirm,
            ),
            const SizedBox(height: 30.0),
          ],
        ),
      ),
    );
  }

  void _confirm() {
    final bool isFirstCorrect = Utils.isValidPassword(_newPassword.text);
    final bool isSecondCorrect = _newPassword.text == _newPasswordConfirm.text;
    if (isFirstCorrect && isSecondCorrect) {
      setState(() {
        isSending = true;
      });
      Helper.instance.resetPassword(
        code: widget.code,
        account: widget.account,
        password: _newPassword.text,
        callback: GeneralCallback<GeneralResponse>(
          onSuccess: (GeneralResponse data) {
            Navigator.of(context).pop();
            CenterToast.show(context, '密碼變更成功');
          },
          onError: (GeneralResponse response) {
            setState(() {
              isSending = false;
            });
            CenterToast.show(context, response.message);
          },
        ),
      );
    } else {
      setState(() {
        if (!isFirstCorrect) {
          firstErrorHint = '請輸入8-20位英數字';
        }
        if (!isSecondCorrect) {
          secondErrorHint = '密碼不相同，請再確認一次';
        }
      });
    }
    setState(() {
      if (!isFirstCorrect) {
        firstErrorHint = '請輸入 8-20 字元包含英文及數字';
      }
      if (!isSecondCorrect) {
        secondErrorHint = '密碼不相同，請再確認一次';
      }
    });
  }
}
