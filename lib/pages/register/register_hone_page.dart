import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_segment/flutter_advanced_segment.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/forget_password_response.dart';
import 'package:hepo_app/models/api/login_response.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/models/phone_area_code.dart';
import 'package:hepo_app/models/register_data.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/common/phone_area_code_picker.dart';
import 'package:hepo_app/pages/common/web_view_page.dart';
import 'package:hepo_app/pages/register/phone_validate_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:hepo_app/widgets/custom_datetime_picker.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:hepo_app/widgets/outline_text_field.dart';
import 'package:hepo_app/widgets/yes_no_dialog.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class RegisterHomePage extends StatefulWidget {
  //現行無使用 原以為第一次第三方登入註冊的Email 要繼續填寫使用者資料
  final UserData? userData;

  const RegisterHomePage({
    Key? key,
    this.userData,
  }) : super(key: key);

  @override
  _RegisterHomePageState createState() => _RegisterHomePageState();
}

class _RegisterHomePageState extends State<RegisterHomePage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  bool get step2Active => tabController.index >= 1;

  bool get step3Active => tabController.index >= 2;

  RegisterData registerData = RegisterData();

  PhoneAreaCodeData phoneAreaCodeData = PhoneAreaCodeData.load();

  @override
  void initState() {
    registerData.fetchHobby(context);
    tabController = TabController(
      length: 3,
      vsync: this,
    );
    tabController.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<RegisterData>.value(
      value: registerData,
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top + 8,
          ),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(34),
            ),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Color(0x4d000000),
                offset: Offset(-3, 3),
                blurRadius: 16,
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 30.0),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      if (tabController.index > 0) {
                        setState(() {
                          tabController.index--;
                        });
                      } else {
                        Navigator.of(context).pop();
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Image.asset(
                        IconAssets.backGreen2x,
                        width: 20.0,
                        height: 20.0,
                      ),
                    ),
                  ),
                ),
              ),
              const Text(
                '會員註冊',
                style: TextStyle(
                  color: AppColors.grey2F,
                  fontSize: 28,
                  fontFamily: 'SFProDisplay',
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 34.0),
              Padding(
                padding: const EdgeInsets.only(left: 49.0, right: 27.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    NumberTitle(
                      number: 1,
                      title: '註冊',
                      active: true,
                      nextActive: step2Active,
                    ),
                    ExpandedLine(
                      flex: 4,
                      active: step2Active,
                    ),
                    NumberTitle(
                      number: 2,
                      title: '填寫會員資料',
                      active: step2Active,
                      nextActive: step3Active,
                    ),
                    ExpandedLine(
                      flex: 3,
                      active: step3Active,
                    ),
                    NumberTitle(
                      number: 3,
                      title: '選擇興趣內容',
                      active: step3Active,
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20.0),
              Expanded(
                child: TabBarView(
                  physics: const NeverScrollableScrollPhysics(),
                  controller: tabController,
                  children: const <Widget>[
                    BasicDataScreen(),
                    MemDataScreen(),
                    PickInterestedScreen(),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          height: 108.0,
          color: Colors.white,
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 8.0),
          child: Column(
            children: <Widget>[
              Consumer<RegisterData>(
                builder: (_, RegisterData data, __) {
                  bool canGoNext = false;
                  switch (tabController.index) {
                    case 0:
                      canGoNext = data.step1Confirm;
                      break;
                    case 1:
                      canGoNext = data.step2Confirm;
                      break;
                    case 2:
                      canGoNext = data.step3Confirm;
                      break;
                  }
                  return GreenButton(
                    text: tabController.index == tabController.length - 1
                        ? '完成'
                        : '下一步',
                    radius: 30.0,
                    padding: const EdgeInsets.symmetric(vertical: 13.0),
                    onTap: canGoNext ? () => _goToNext(data) : null,
                  );
                },
              ),
              const SizedBox(height: 16.0),
              GestureDetector(
                onTap: () {
                  final AppMetaData data = context.read<AppMetaData>();
                  Navigator.of(context).push(
                    MaterialPageRoute<void>(
                      builder: (BuildContext context) => WebViewPage(
                        title: '隱私權政策與服務條款',
                        url: data.privacyPolicyUrl!,
                      ),
                    ),
                  );
                  // launch(data.privacyPolicyUrl!);
                },
                child: const Text(
                  '隱私權政策與服務條款',
                  style: TextStyle(
                    color: AppColors.greyC6,
                    fontFamily: 'SFProText',
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _goToNext(RegisterData data) async {
    bool canGoNext = false;
    switch (tabController.index) {
      case 0:
        canGoNext = data.checkStep1();
        String code = '';
        if (data.cellPhoneNumber.text.isNotEmpty) {
          data.cellPhoneNumberConverted = Utils.contactPhoneNumber(
            areaCode:
                phoneAreaCodeData.phoneCodeData[data.phoneAreaCodeIndex].code,
            number: data.cellPhoneNumber.text,
          );
          canGoNext = false;
          if (!data.isSending) {
            data.isSending = true;
            await Helper.instance.sendValidationCode(
              cellPhoneNumber: data.cellPhoneNumberConverted!,
              callback: GeneralCallback<ForgetPasswordResponse>(
                onError: (GeneralResponse e) {
                  CenterToast.show(context, e.message);
                  canGoNext = false;
                  data.isSending = true;
                },
                onSuccess: (ForgetPasswordResponse r) async {
                  code = r.data;
                  data.regCode = code;
                  canGoNext = true;
                  data.isSending = false;
                },
              ),
            );
          }
          if (canGoNext) {
            final bool? success = await Navigator.of(context).push(
              MaterialPageRoute<bool>(
                builder: (_) => PhoneValidatePage(
                  validateType: ValidateType.register,
                  cellPhoneNumber: data.cellPhoneNumberConverted!,
                  code: code,
                ),
              ),
            );
            canGoNext = success ?? false;
          }
        }
        break;
      case 1:
        canGoNext = data.checkStep2();
        break;
      case 2:
        canGoNext = data.checkStep3();
        break;
    }
    if (canGoNext) {
      if (tabController.index < tabController.length - 1) {
        setState(() {
          tabController.index++;
        });
      } else {
        if (kDebugMode) {
          debugPrint('regcode: ${data.regCode}\n'
              'telephone: ${data.cellPhoneNumberConverted}'
              '\nusername: ${data.name.text}'
              '\nbirthday: ${data.birthday?.toString()}'
              '\nemail: ${data.email.text}'
              '\npassword: ${data.password.text}'
              '\ngender: ${data.gender.num}'
              '\nhobby: ${data.hobby}');
        }
        data.isSending = true;
        Helper.instance.finishRegister(
          regCode: data.regCode,
          telephone: data.cellPhoneNumberConverted,
          userName: data.name.text,
          birthday: data.birthday,
          email: data.email.text,
          password: data.password.text,
          gender: data.gender.num,
          hobby: data.hobby,
          callback: GeneralCallback<LoginResponse>(
            onError: (GeneralResponse e) {
              data.isSending = false;
              CenterToast.show(context, e.message);
            },
            onSuccess: (LoginResponse r) {
              Navigator.of(context).pop();
              if (data.email.text.isNotEmpty) {
                YesNoDialog.show(
                  context,
                  '驗證信已發送',
                  message: '已完成資料填寫，請至註冊 Email 啟用認證信函，即可完成信箱驗證。',
                  rightActionText: '我瞭解了',
                  hideLeftButton: true,
                  barrierColor: AppColors.black45,
                );
              } else {
                CenterToast.show(context, '已完成註冊');
              }
            },
          ),
        );
      }
    }
  }
}

class BasicDataScreen extends StatefulWidget {
  const BasicDataScreen({Key? key}) : super(key: key);

  @override
  _BasicDataScreenState createState() => _BasicDataScreenState();
}

class _BasicDataScreenState extends State<BasicDataScreen> {
  PhoneAreaCodeData phoneAreaCodeData = PhoneAreaCodeData.load();

  final FocusNode cellPhoneNumberFocusNode = FocusNode();
  final FocusNode emailFocusNode = FocusNode();
  final FocusNode passwordFocusNode = FocusNode();
  final FocusNode passwordConfirmFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 38.0),
      child: KeyboardDismissOnTap(
        child: Consumer<RegisterData>(
          builder: (_, RegisterData data, __) {
            return _content(data);
          },
        ),
      ),
    );
  }

  Widget _content(RegisterData data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: const <Widget>[
            Text(
              '手機號碼',
              style: TextStyle(
                fontSize: 14.0,
                color: AppColors.grey2F,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(width: 6.0),
            Text(
              '海外用戶請以 Email 註冊',
              style: TextStyle(
                color: AppColors.muddyGreen,
                fontWeight: FontWeight.w700,
                fontFamily: 'Montserrat',
                fontSize: 11.0,
              ),
            ),
          ],
        ),
        const SizedBox(height: 8.0),
        Row(
          children: <Widget>[
            Container(
              constraints: const BoxConstraints(maxWidth: 80.0),
              child: PhoneAreaCodePicker(
                index: data.phoneAreaCodeIndex,
                phoneAreaCodeList: phoneAreaCodeData.phoneCodeData,
              ),
            ),
            Expanded(
              child: OutlineTextField(
                controller: data.cellPhoneNumber,
                focusNode: cellPhoneNumberFocusNode,
                nextFocusNode: emailFocusNode,
                keyboardType: TextInputType.phone,
                textInputAction: TextInputAction.next,
                autofillHints: const <String>[AutofillHints.telephoneNumber],
                borderRadius: 26,
                enabledBorderColor: AppColors.greyC6,
              ),
            )
          ],
        ),
        const SizedBox(height: 8.0),
        const Text(
          'Email 地址',
          style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 8.0),
        OutlineTextField(
          controller: data.email,
          focusNode: emailFocusNode,
          nextFocusNode: passwordFocusNode,
          borderRadius: 26,
          enabledBorderColor: AppColors.greyC6,
          keyboardType: TextInputType.emailAddress,
          textInputAction: TextInputAction.next,
          autofillHints: const <String>[AutofillHints.email],
          hintText: '請輸入您的信箱',
          onChanged: (_) => data.clearEmailErrorText(),
          errorText: data.emailErrorText,
          suffixIcon: ValueListenableBuilder<TextEditingValue>(
            valueListenable: data.email,
            builder: (_, TextEditingValue value, __) {
              if (value.text.isEmpty) {
                return Image.asset(
                  IconAssets.checkNormal2x,
                );
              } else {
                return Image.asset(
                  Utils.isValidEmail(value.text)
                      ? IconAssets.correct2x
                      : IconAssets.incorrect2x,
                );
              }
            },
          ),
        ),
        const SizedBox(height: 8.0),
        const Text(
          '請輸入密碼',
          style: TextStyle(
            fontSize: 14.0,
            color: AppColors.grey2F,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 8.0),
        OutlineTextField(
          controller: data.password,
          focusNode: passwordFocusNode,
          nextFocusNode: passwordConfirmFocusNode,
          obscureText: true,
          borderRadius: 26,
          enabledBorderColor: AppColors.greyC6,
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.next,
          autofillHints: const <String>[AutofillHints.password],
          hintText: '請輸入 8-20 字元包含英文及數字',
          onChanged: (_) => data.clearPasswordErrorText(),
          errorText: data.passwordErrorText,
          suffixIcon: ValueListenableBuilder<TextEditingValue>(
            valueListenable: data.password,
            builder: (_, TextEditingValue value, __) {
              if (value.text.isEmpty) {
                return Image.asset(
                  IconAssets.checkNormal2x,
                );
              } else {
                return Image.asset(
                  Utils.isValidPassword(value.text)
                      ? IconAssets.correct2x
                      : IconAssets.incorrect2x,
                );
              }
            },
          ),
        ),
        const SizedBox(height: 8.0),
        const Text(
          '確認密碼',
          style: TextStyle(
            fontSize: 14.0,
            color: AppColors.grey2F,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 8.0),
        OutlineTextField(
          controller: data.passwordConfirm,
          focusNode: passwordConfirmFocusNode,
          obscureText: true,
          borderRadius: 26,
          enabledBorderColor: AppColors.greyC6,
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.done,
          autofillHints: const <String>[AutofillHints.password],
          hintText: '請再輸入一次密碼',
          onChanged: (_) => data.clearPasswordConfirmErrorText(),
          errorText: data.passwordConfirmErrorText,
          suffixIcon: ValueListenableBuilder<TextEditingValue>(
            valueListenable: data.passwordConfirm,
            builder: (_, TextEditingValue value, __) {
              if (value.text.isEmpty) {
                return Image.asset(
                  IconAssets.checkNormal2x,
                );
              } else {
                return Image.asset(
                  value.text == data.password.text
                      ? IconAssets.correct2x
                      : IconAssets.incorrect2x,
                );
              }
            },
          ),
        ),
      ],
    );
  }
}

class MemDataScreen extends StatefulWidget {
  const MemDataScreen({Key? key}) : super(key: key);

  @override
  _MemDataScreenState createState() => _MemDataScreenState();
}

class _MemDataScreenState extends State<MemDataScreen> {
  AdvancedSegmentController? segmentController;

  final FocusNode nameFocusNode = FocusNode();

  @override
  void initState() {
    segmentController = AdvancedSegmentController(
      context.read<RegisterData>().gender.toString(),
    );
    segmentController?.addListener(() {
      final String value = segmentController?.value ?? '';
      if (value == Gender.female.toString()) {
        context.read<RegisterData>().gender = Gender.female;
      } else if (value == Gender.male.toString()) {
        context.read<RegisterData>().gender = Gender.male;
      } else if (value == Gender.notSet.toString()) {
        context.read<RegisterData>().gender = Gender.notSet;
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 38.0),
      child: KeyboardDismissOnTap(
        child: Consumer<RegisterData>(
          builder: (_, RegisterData data, __) {
            return _content(data);
          },
        ),
      ),
    );
  }

  Widget _content(RegisterData data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Text(
          '生理性別',
          style: TextStyle(
            color: AppColors.grey2F,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 14.0),
        AdvancedSegment(
          controller: segmentController,
          segments: <String, String>{
            for (Gender value in <Gender>[
              Gender.female,
              Gender.male,
              Gender.notSet,
            ])
              value.toString(): value.text
          },
          itemPadding: const EdgeInsets.symmetric(
            vertical: 8.0,
            horizontal: 16.0,
          ),
          borderRadius: const BorderRadius.all(
            Radius.circular(30.0),
          ),
          backgroundColor: AppColors.greyF5,
          sliderColor: AppColors.muddyGreen,
          sliderOffset: 0.0,
          activeStyle: const TextStyle(
            fontSize: 14.0,
            color: AppColors.white,
          ),
          inactiveStyle: const TextStyle(
            fontSize: 14.0,
            color: AppColors.grey2F,
          ),
        ),
        const SizedBox(height: 20.0),
        Row(
          children: const <Widget>[
            Text(
              '出生年月日',
              style: TextStyle(
                color: AppColors.grey2F,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(width: 10.0),
            Text(
              '請正確填寫，註冊後將無法更改',
              style: TextStyle(
                color: AppColors.muddyGreen,
                fontSize: 11,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
        const SizedBox(height: 14.0),
        GestureDetector(
          onTap: () {
            showCupertinoModalPopup(
              context: context,
              barrierColor: Colors.transparent,
              builder: (BuildContext context) {
                return CustomDatePicker(
                  initialDateTime: DateTime.now().subtract(
                    Duration(
                      days: (365.25 * 30).ceil(),
                    ),
                  ),
                  maximumDate: DateTime.now(),
                  onConfirmClick: (DateTime datetime) {
                    data.setBirthday(datetime);
                  },
                );
              },
            );
          },
          child: Container(
            height: 36,
            width: double.infinity,
            padding: const EdgeInsets.only(
              right: 8.0,
              left: 28.0,
            ),
            decoration: BoxDecoration(
              border: Border.all(
                color: AppColors.greyC6,
              ),
              borderRadius: BorderRadius.circular(26),
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    data.birthday == null
                        ? ''
                        : DateFormat('yyyy-MM-dd').format(data.birthday!),
                    style: const TextStyle(
                      color: AppColors.grey2F,
                      fontSize: 18,
                      fontFamily: 'SFProText',
                    ),
                  ),
                ),
                const Icon(
                  Icons.arrow_drop_down,
                  color: AppColors.mediumGrey,
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 20.0),
        const Text(
          '姓名',
          style: TextStyle(
            color: AppColors.grey2F,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 14.0),
        OutlineTextField(
          controller: data.name,
          focusNode: nameFocusNode,
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.done,
          autofillHints: const <String>[AutofillHints.name],
          borderRadius: 26,
          enabledBorderColor: AppColors.greyC6,
        ),
        const SizedBox(height: 8),
        GestureDetector(
          onTap: () {
            setState(() {
              if (data.confirmPrivacy) {
                data.confirmPrivacy = false;
              } else {
                data.confirmPrivacy = true;
              }
            });
            data.checkStep2();
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 24,
                width: 24,
                padding: EdgeInsets.zero,
                child: FittedBox(
                  child: Checkbox(
                      value: data.confirmPrivacy,
                      onChanged: (bool? value) {
                        setState(() {
                          data.confirmPrivacy = value!;
                        });
                        data.checkStep2();
                      }),
                ),
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.only(top: 2),
                child: RichText(
                  text: TextSpan(
                      style: const TextStyle(
                        color: AppColors.black,
                        fontSize: 12,
                        fontFamily: 'SFProText',
                      ),
                      children: <TextSpan>[
                        const TextSpan(text: '我已閱讀、了解並同意接受 '),
                        TextSpan(
                            text: '會員隱私權條款',
                            style: const TextStyle(
                              color: AppColors.muddyGreen,
                              fontFamily: 'SFProText',
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                final AppMetaData data =
                                    context.read<AppMetaData>();
                                Navigator.of(context).push(
                                  MaterialPageRoute<void>(
                                    builder: (BuildContext context) =>
                                        WebViewPage(
                                      title: '會員隱私權條款',
                                      url: data.privacyPolicyUrl!,
                                    ),
                                  ),
                                );
                                // launch(data.privacyPolicyUrl!);
                              }),
                        const TextSpan(text: ' 之所有內容及其後之修改變更。'),
                      ]),
                ),
              )),
            ],
          ),
        ),
      ],
    );
  }
}

class PickInterestedScreen extends StatefulWidget {
  const PickInterestedScreen({Key? key}) : super(key: key);

  @override
  _PickInterestedScreenState createState() => _PickInterestedScreenState();
}

class _PickInterestedScreenState extends State<PickInterestedScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 30.0),
      child: KeyboardDismissOnTap(
        child: Consumer<RegisterData>(
          builder: (_, RegisterData data, __) {
            return _content(data);
          },
        ),
      ),
    );
  }

  Widget _content(RegisterData data) {
    return Column(
      children: <Widget>[
        const Text(
          '依照您感興趣的內容，提供最新消息以及專屬的個人化健康內容（請至少選擇三樣）',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: AppColors.grey2F,
            fontSize: 12,
            fontFamily: 'SFProText',
          ),
        ),
        const SizedBox(height: 15.0),
        GridView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.symmetric(
            horizontal: 8.0,
          ),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: (MediaQuery.of(context).size.width / 2) / 75,
          ),
          itemCount: data.categoryList.length,
          itemBuilder: (_, int index) {
            return InterestedCard(
              data: data.categoryList[index],
              onTap: () {
                data.updateCategoryList(index);
              },
            );
          },
        ),
      ],
    );
  }
}

class InterestedCard extends StatelessWidget {
  final InterestedCategory data;
  final VoidCallback? onTap;

  const InterestedCard({
    Key? key,
    required this.data,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Stack(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.symmetric(
              vertical: 5.0,
              horizontal: 6.0,
            ),
            decoration: BoxDecoration(
              color: Colors.transparent,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(data.imagePath),
              ),
              borderRadius: BorderRadius.circular(5),
            ),
          ),
          if (!data.isCheck)
            Container(
              margin: const EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 6.0,
              ),
              decoration: BoxDecoration(
                color: AppColors.black80,
                borderRadius: BorderRadius.circular(5),
              ),
            ),
          if (data.isCheck)
            Align(
              alignment: Alignment.topRight,
              child: Image.asset(
                IconAssets.checkDarkGreen2x,
                width: 20.0,
                height: 20.0,
              ),
            ),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 10.0,
            ),
            child: Text(
              data.title,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 14,
                fontFamily: 'SFProText',
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class NumberTitle extends StatelessWidget {
  final bool? previousActive;
  final bool active;
  final bool? nextActive;
  final int number;
  final String title;

  const NumberTitle({
    Key? key,
    required this.active,
    required this.number,
    required this.title,
    this.previousActive,
    this.nextActive,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicWidth(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ExpandedLine(
                active: active,
                top: 1.0,
              ),
              Container(
                height: 30.0,
                width: 30.0,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: active ? AppColors.muddyGreen : AppColors.greyE9,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Text(
                  '$number',
                  style: TextStyle(
                    color: active ? Colors.white : AppColors.greyC6,
                    fontSize: 18,
                    fontFamily: 'SFProDisplay',
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              if (nextActive != null)
                ExpandedLine(
                  active: nextActive!,
                  top: 1.0,
                )
              else
                const Spacer(),
            ],
          ),
          const SizedBox(height: 6.0),
          Text(
            title,
            style: TextStyle(
              color: active ? AppColors.grey2F : AppColors.greyC6,
              fontSize: 12,
              fontFamily: 'SFProDisplay',
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }
}

class ExpandedLine extends StatelessWidget {
  final bool active;
  final double? top;
  final int? flex;

  const ExpandedLine({
    Key? key,
    required this.active,
    this.top,
    this.flex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex ?? 1,
      child: Container(
        height: 1,
        margin: EdgeInsets.only(top: top ?? 15.0),
        color: active ? AppColors.muddyGreen : AppColors.greyC6,
      ),
    );
  }
}
