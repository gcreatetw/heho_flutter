import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/counter.dart';
import 'package:hepo_app/models/general_response.dart';
import 'package:hepo_app/pages/register/forget_password_page.dart';
import 'package:hepo_app/pages/register/reset_password_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:provider/provider.dart';

class ForgetPasswordValidatePage extends StatefulWidget {
  final ForgetPasswordMode mode;
  final String account;
  final String code;

  const ForgetPasswordValidatePage({
    Key? key,
    required this.mode,
    required this.account,
    required this.code,
  }) : super(key: key);

  @override
  _ForgetPasswordValidatePageState createState() =>
      _ForgetPasswordValidatePageState();
}

class _ForgetPasswordValidatePageState
    extends State<ForgetPasswordValidatePage> {
  final TextEditingController validationCode = TextEditingController();
  final FocusNode validationCodeFocusNode = FocusNode();

  CounterData counter = CounterData();

  String code = '';

  String get subMessage {
    switch (widget.mode) {
      case ForgetPasswordMode.email:
        return '若無收到信箱驗證碼，請嘗試以下方式';
      case ForgetPasswordMode.cellphoneNumber:
        return '若無收到簡訊驗證碼，請嘗試以下方式';
    }
  }

  @override
  void initState() {
    code = widget.code;
    counter.start();
    if (kDebugMode) {
      debugPrint('code = $code');
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: ChangeNotifierProvider<CounterData>.value(
        value: counter,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.white,
            iconTheme: const IconThemeData(
              color: AppColors.grey2F,
            ),
            title: const Text(
              '忘記密碼',
              style: TextStyle(
                color: AppColors.grey2F,
              ),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(height: 30.0),
                const Text(
                  '請輸入您收到的驗證碼，完成身分驗證流程。',
                  style: TextStyle(
                    color: AppColors.grey2F,
                    fontSize: 16,
                    fontFamily: 'SFProDisplay',
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(height: 23.0),
                const Text(
                  '請輸入您的驗證碼',
                  style: TextStyle(
                    color: AppColors.grey2F,
                    fontSize: 14,
                    fontFamily: 'SFProDisplay',
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(height: 10.0),
                SizedBox(
                  height: 50.0,
                  child: TextField(
                    controller: validationCode,
                    focusNode: validationCodeFocusNode,
                    obscuringCharacter: '*',
                    keyboardType: TextInputType.number,
                    autofillHints: const <String>[AutofillHints.oneTimeCode],
                    onEditingComplete: () {
                      validationCodeFocusNode.unfocus();
                      _finishValidation();
                    },
                    textInputAction: TextInputAction.send,
                    cursorColor: AppColors.azure,
                    style: const TextStyle(
                      color: AppColors.grey2F,
                      fontSize: 20,
                      fontFamily: 'Montserrat',
                    ),
                    decoration: InputDecoration(
                      counter: const Offstage(),
                      contentPadding: const EdgeInsets.symmetric(
                        vertical: 8.0,
                        horizontal: 23.0,
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(
                          color: AppColors.greyC6,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(
                          color: AppColors.greyC6,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(
                          color: AppColors.muddyGreen,
                        ),
                      ),
                      disabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(
                          color: AppColors.greyC6,
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(
                          color: AppColors.greyC6,
                        ),
                      ),
                      filled: true,
                      fillColor: AppColors.white,
                      focusColor: AppColors.white,
                      hoverColor: AppColors.azure,
                      hintStyle: const TextStyle(
                        color: AppColors.grey79,
                        fontSize: 14,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                Center(
                  child: Text(
                    subMessage,
                    style: const TextStyle(
                      fontSize: 14.0,
                      color: AppColors.grey2F,
                      fontFamily: 'SFProText',
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                Center(
                  child: Consumer<CounterData>(
                    builder: (_, CounterData data, __) {
                      return FractionallySizedBox(
                        widthFactor: 0.5,
                        child: GreenButton(
                          text: '重新發送驗證碼'
                              '${data.canReSend ? '' : ' (${data.seconds}秒)'}',
                          radius: 30.0,
                          padding: const EdgeInsets.symmetric(vertical: 13.0),
                          onTap: data.canReSend
                              ? () {
                                  _sendValidationCode();
                                }
                              : null,
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: Container(
            height: 108.0,
            color: Colors.transparent,
            padding:
                const EdgeInsets.symmetric(horizontal: 24.0, vertical: 8.0),
            child: Column(
              children: <Widget>[
                ValueListenableBuilder<TextEditingValue>(
                  valueListenable: validationCode,
                  builder: (_, TextEditingValue value, __) {
                    return GreenButton(
                      text: '驗證',
                      radius: 30.0,
                      padding: const EdgeInsets.symmetric(vertical: 13.0),
                      onTap: value.text.isEmpty ? null : _finishValidation,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _sendValidationCode() {
    Helper.instance.sendForgetPassword(
      account: widget.account,
      callback: GeneralCallback<String>(
        onSuccess: (String data) {
          counter.start();
          CenterToast.show(context, '已重新發送');
          code = data;
          if (kDebugMode) {
            debugPrint('new code = $code');
          }
        },
        onError: (GeneralResponse response) {
          CenterToast.show(context, response.message);
        },
      ),
    );
  }

  void _finishValidation() {
    if (code == validationCode.text) {
      CenterToast.show(context, '驗證成功');
      Navigator.of(context).pop();
      Navigator.of(context).push(
        MaterialPageRoute<void>(
          builder: (_) => ResetPasswordPage(
            code: code,
            account: widget.account,
          ),
        ),
      );
    } else {
      CenterToast.show(context, '驗證碼錯誤');
    }
  }
}
