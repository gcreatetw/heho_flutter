import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/api/forget_password_response.dart';
import 'package:hepo_app/models/app_meta_data.dart';
import 'package:hepo_app/models/counter.dart';
import 'package:hepo_app/models/general_callback.dart';
import 'package:hepo_app/models/user_data.dart';
import 'package:hepo_app/pages/common/web_view_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

enum ValidateType { update, register }

class PhoneValidatePage extends StatefulWidget {
  final String cellPhoneNumber;
  final String? code;
  final ValidateType validateType;

  const PhoneValidatePage({
    Key? key,
    required this.cellPhoneNumber,
    this.code,
    required this.validateType,
  }) : super(key: key);

  @override
  _PhoneValidatePageState createState() => _PhoneValidatePageState();
}

class _PhoneValidatePageState extends State<PhoneValidatePage> {
  final TextEditingController validationCode = TextEditingController();
  final FocusNode validationCodeFocusNode = FocusNode();

  CounterData counter = CounterData();

  bool isSending = false;

  String? newCode;

  @override
  void initState() {
    counter.start();
    if (kDebugMode) {
      debugPrint('Send code to cellphone number = ${widget.cellPhoneNumber}');
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final String text = widget.cellPhoneNumber;
    final int len = widget.cellPhoneNumber.length;
    return ChangeNotifierProvider<CounterData>.value(
      value: counter,
      child: Scaffold(
        body: KeyboardDismissOnTap(
          child: Container(
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + 8,
            ),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(34),
              ),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Color(0x4d000000),
                  offset: Offset(-3, 3),
                  blurRadius: 16,
                ),
              ],
            ),
            child: Column(
              children: <Widget>[
                const SizedBox(height: 30.0),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Image.asset(
                          IconAssets.backGreen2x,
                          width: 20.0,
                          height: 20.0,
                        ),
                      ),
                    ),
                  ),
                ),
                const Text(
                  '手機號碼驗證',
                  style: TextStyle(
                    color: AppColors.grey2F,
                    fontSize: 28,
                    fontFamily: 'SFProDisplay',
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(height: 34.0),
                Text.rich(
                  TextSpan(
                    children: <TextSpan>[
                      const TextSpan(
                        text: '已將手機安全驗證簡訊寄至您的手機\n',
                        style: TextStyle(
                          color: AppColors.grey2F,
                          fontSize: 16,
                          fontFamily: 'SFProText',
                        ),
                      ),
                      TextSpan(
                        text: '${text.substring(0, 5)}'
                            '*****'
                            '${text.substring(len - 3, len)}',
                        style: const TextStyle(
                          color: AppColors.muddyGreen,
                          fontSize: 18,
                          fontFamily: 'SFProText',
                        ),
                      ),
                      const TextSpan(
                        text: '\n請輸入簡訊傳送的驗證碼',
                        style: TextStyle(
                          color: AppColors.grey2F,
                          fontSize: 16,
                          fontFamily: 'SFProText',
                        ),
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: AppColors.muddyGreen,
                    fontSize: 18,
                    fontFamily: 'SFProText',
                  ),
                ),
                const SizedBox(height: 16.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 38.0),
                  child: SizedBox(
                    height: 50.0,
                    child: TextField(
                      controller: validationCode,
                      focusNode: validationCodeFocusNode,
                      obscuringCharacter: '*',
                      keyboardType: TextInputType.number,
                      autofillHints: const <String>[AutofillHints.oneTimeCode],
                      onEditingComplete: () {
                        validationCodeFocusNode.unfocus();
                      },
                      textInputAction: TextInputAction.send,
                      cursorColor: AppColors.azure,
                      style: const TextStyle(
                        color: AppColors.grey2F,
                        fontSize: 20,
                        fontFamily: 'Montserrat',
                      ),
                      decoration: InputDecoration(
                        counter: const Offstage(),
                        contentPadding: const EdgeInsets.symmetric(
                          vertical: 8.0,
                          horizontal: 23.0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: const BorderSide(
                            color: AppColors.greyC6,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: const BorderSide(
                            color: AppColors.greyC6,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: const BorderSide(
                            color: AppColors.muddyGreen,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: const BorderSide(
                            color: AppColors.greyC6,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: const BorderSide(
                            color: AppColors.greyC6,
                          ),
                        ),
                        filled: true,
                        fillColor: AppColors.white,
                        focusColor: AppColors.white,
                        hoverColor: AppColors.azure,
                        hintStyle: const TextStyle(
                          color: AppColors.grey79,
                          fontSize: 14,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                const Text(
                  '若無收到簡訊驗證碼，請嘗試以下方式',
                  style: TextStyle(
                    fontSize: 14.0,
                    color: AppColors.grey2F,
                    fontFamily: 'SFProText',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 16.0),
                Consumer<CounterData>(
                  builder: (_, CounterData data, __) {
                    return FractionallySizedBox(
                      widthFactor: 0.5,
                      child: GreenButton(
                        text: '重新發送驗證碼'
                            '${data.canReSend ? '' : ' (${data.seconds}秒)'}',
                        radius: 30.0,
                        padding: const EdgeInsets.symmetric(vertical: 13.0),
                        onTap: data.canReSend
                            ? () {
                                _sendValidationCode();
                              }
                            : null,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Container(
          height: 108.0,
          color: Colors.white,
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 8.0),
          child: Column(
            children: <Widget>[
              ValueListenableBuilder<TextEditingValue>(
                valueListenable: validationCode,
                builder: (_, TextEditingValue value, __) {
                  return GreenButton(
                    text: '驗證',
                    radius: 30.0,
                    padding: const EdgeInsets.symmetric(vertical: 13.0),
                    onTap: value.text.isEmpty ? null : _finishValidation,
                  );
                },
              ),
              const SizedBox(height: 16.0),
              GestureDetector(
                onTap: () {
                  final AppMetaData data = context.read<AppMetaData>();
                  Navigator.of(context).push(
                    MaterialPageRoute<void>(
                      builder: (BuildContext context) =>
                          WebViewPage(
                            title: '隱私權政策與服務條款',
                            url: data.privacyPolicyUrl!,
                          ),
                    ),
                  );
                  // launch(data.privacyPolicyUrl!);
                },
                child: const Text(
                  '隱私權政策與服務條款',
                  style: TextStyle(
                    color: AppColors.greyC6,
                    fontFamily: 'SFProText',
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _sendValidationCode() {
    if (!isSending) {
      isSending = true;
      Helper.instance.sendValidationCode(
        cellPhoneNumber: widget.cellPhoneNumber,
        callback: GeneralCallback<ForgetPasswordResponse>(
          onError: (GeneralResponse e) {
            CenterToast.show(context, e.message);
            isSending = false;
          },
          onSuccess: (ForgetPasswordResponse r) async {
            counter.start();
            newCode = r.code;
            CenterToast.show(context, '已重新發送');
            isSending = false;
          },
        ),
      );
    } else {
      isSending = true;
    }
  }

  Future<void> _finishValidation() async {
    switch (widget.validateType) {
      case ValidateType.update:
        context.read<UserData>().setCellphoneNumber(widget.cellPhoneNumber);
        await context.read<UserData>().updateData(
              context: context,
              updateType: UpdateType.phone,
              code: validationCode.text,
            );
        Navigator.of(context).pop(true);
        break;
      case ValidateType.register:
        if (validationCode.text == widget.code || validationCode.text == newCode) {
          CenterToast.show(context, '驗證成功');
          Navigator.of(context).pop(true);
        } else {
          CenterToast.show(context, '驗證碼錯誤');
        }
        break;
    }
  }
}
