import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hepo_app/api/helper.dart';
import 'package:hepo_app/models/general_callback.dart';
import 'package:hepo_app/models/general_response.dart';
import 'package:hepo_app/models/phone_area_code.dart';
import 'package:hepo_app/pages/common/phone_area_code_picker.dart';
import 'package:hepo_app/pages/register/forget_password_validate_page.dart';
import 'package:hepo_app/pages/register/register_hone_page.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';
import 'package:hepo_app/utils/center_toast.dart';
import 'package:hepo_app/utils/utils.dart';
import 'package:hepo_app/widgets/green_button.dart';
import 'package:hepo_app/widgets/outline_text_field.dart';
import 'package:hepo_app/widgets/yes_no_dialog.dart';

enum ForgetPasswordMode {
  email,
  cellphoneNumber,
}

class ForgetPasswordPage extends StatefulWidget {
  final ForgetPasswordMode mode;

  const ForgetPasswordPage({
    Key? key,
    required this.mode,
  }) : super(key: key);

  @override
  _ForgetPasswordPageState createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  final TextEditingController _controller = TextEditingController();
  final FocusNode _focusNode = FocusNode();

  PhoneAreaCodeData phoneAreaCodeData = PhoneAreaCodeData.load();

  late List<PhoneAreaCode> phoneAreaCodeList;

  int phoneAreaCodeIndex = 0;

  String errorText = '';

  bool isSending = false;

  String get description {
    switch (widget.mode) {
      case ForgetPasswordMode.email:
        return '為保障您的帳戶安全，請輸入您的 Email 信箱進行身分驗證，系統將會發送六位數驗證碼至您的信箱。';
      case ForgetPasswordMode.cellphoneNumber:
        return '為保障您的帳戶安全，請輸入您的手機號碼進行身分驗證，系統將會發送六位數驗證碼簡訊至您的手機。';
    }
  }

  String get title {
    switch (widget.mode) {
      case ForgetPasswordMode.email:
        return 'Email 地址';
      case ForgetPasswordMode.cellphoneNumber:
        return '手機號碼';
    }
  }

  String get hintText {
    switch (widget.mode) {
      case ForgetPasswordMode.email:
        return '請輸入您的信箱';
      case ForgetPasswordMode.cellphoneNumber:
        return '';
    }
  }

  TextInputType get keyboardType {
    switch (widget.mode) {
      case ForgetPasswordMode.email:
        return TextInputType.emailAddress;
      case ForgetPasswordMode.cellphoneNumber:
        return TextInputType.phone;
    }
  }

  @override
  void initState() {
    phoneAreaCodeList = phoneAreaCodeData.phoneCodeData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.white,
          iconTheme: const IconThemeData(
            color: AppColors.grey2F,
          ),
          title: const Text(
            '忘記密碼',
            style: TextStyle(
              color: AppColors.grey2F,
            ),
          ),
        ),
        backgroundColor: AppColors.greyF5,
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 26.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              const SizedBox(height: 30.0),
              Text(
                description,
                style: const TextStyle(
                  color: AppColors.grey2F,
                  fontSize: 16,
                  fontFamily: 'SFProText',
                ),
              ),
              const SizedBox(height: 16.0),
              Text(
                title,
                style: const TextStyle(
                  color: AppColors.grey2F,
                  fontSize: 14,
                  fontFamily: 'SFProDisplay',
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: 16.0),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  if (widget.mode == ForgetPasswordMode.cellphoneNumber)
                    SizedBox(
                      width: 80.0,
                      height: 42.0,
                      child: Center(
                        child: PhoneAreaCodePicker(
                          index: phoneAreaCodeIndex,
                          phoneAreaCodeList: phoneAreaCodeList,
                          onChanged: (int value) {
                            setState(() {
                              phoneAreaCodeIndex = value;
                            });
                          },
                        ),
                      ),
                    ),
                  Expanded(
                    child: OutlineTextField(
                      height: 50.0,
                      controller: _controller,
                      focusNode: _focusNode,
                      borderRadius: 26,
                      enabledBorderColor: AppColors.greyC6,
                      keyboardType: keyboardType,
                      textInputAction: TextInputAction.send,
                      onEditingComplete: () {
                        _send();
                      },
                      autofillHints: <String>[
                        if (widget.mode == ForgetPasswordMode.email)
                          AutofillHints.email,
                        if (widget.mode == ForgetPasswordMode.cellphoneNumber)
                          AutofillHints.telephoneNumber,
                      ],
                      hintText: hintText,
                      onChanged: (_) {
                        setState(() {
                          errorText = '';
                        });
                      },
                      errorText: errorText,
                      suffixIcon: ValueListenableBuilder<TextEditingValue>(
                        valueListenable: _controller,
                        builder: (_, TextEditingValue value, __) {
                          if (value.text.isEmpty) {
                            return Image.asset(
                              IconAssets.checkNormal2x,
                            );
                          } else {
                            return Image.asset(
                              isValid(value.text)
                                  ? IconAssets.correct2x
                                  : IconAssets.incorrect2x,
                            );
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          height: 100.0,
          color: AppColors.greyF5,
          padding: const EdgeInsets.only(
            left: 50.0,
            right: 50.0,
            bottom: 50.0,
          ),
          child: ValueListenableBuilder<TextEditingValue>(
            valueListenable: _controller,
            builder: (_, TextEditingValue value, __) {
              return GreenButton(
                text: '取得驗證碼',
                radius: 30.0,
                padding: const EdgeInsets.symmetric(vertical: 13.0),
                onTap: value.text.isEmpty || isSending ? null : _send,
              );
            },
          ),
        ),
      ),
    );
  }

  bool isValid(String text) {
    switch (widget.mode) {
      case ForgetPasswordMode.email:
        return Utils.isValidEmail(text);
      case ForgetPasswordMode.cellphoneNumber:
        return Utils.isValidCellPhoneNumber(text);
    }
  }

  void _send() {
    String text = _controller.text;
    switch (widget.mode) {
      case ForgetPasswordMode.email:
        if (!Utils.isValidEmail(text)) {
          setState(() {
            errorText = '電子信箱格式錯誤';
          });
          return;
        }
        break;
      case ForgetPasswordMode.cellphoneNumber:
        if (!Utils.isValidCellPhoneNumber(text)) {
          setState(() {
            errorText = '手機號碼格式錯誤';
          });
          return;
        }
        text = Utils.contactPhoneNumber(
          areaCode: phoneAreaCodeList[phoneAreaCodeIndex].code,
          number: text,
        );
        break;
    }
    setState(() {
      isSending = true;
    });
    Helper.instance.sendForgetPassword(
      account: text,
      callback: GeneralCallback<String>(
        onSuccess: (String data) {
          Navigator.of(context).pop();
          Navigator.of(context).push(
            MaterialPageRoute<void>(
              builder: (_) => ForgetPasswordValidatePage(
                mode: widget.mode,
                account: _controller.text,
                code: data,
              ),
            ),
          );
        },
        onError: (GeneralResponse response) {
          if (response.statusCode == '403') {
            _controller.text = '';
            YesNoDialog.show(
              context,
              '查無會員資料',
              message: '是否前往註冊？',
              rightActionText: '註冊',
              onRightActionClick: () {
                Navigator.pop(context);
                Navigator.of(context).push(
                  MaterialPageRoute<void>(
                    builder: (_) => const RegisterHomePage(),
                  ),
                );
              },
            );
          } else {
            CenterToast.show(context, response.message);
          }
          setState(() {
            errorText = '';
            isSending = false;
          });
        },
      ),
    );
  }
}
