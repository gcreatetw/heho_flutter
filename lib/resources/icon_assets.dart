part of 'resources.dart';

class IconAssets {
  IconAssets._();

  static const String hospitalType2x = 'assets/icons/hospital_type@2x.png';
  static const String calendar2x = 'assets/icons/calendar@2x.png';
  static const String categoryAskExpert2x =
      'assets/icons/category_ask_expert@2x.png';
  static const String categoryNPower2x =
      'assets/icons/home-page_icon_n-power@2x.png';
  static const String search2x = 'assets/icons/search@2x.png';
  static const String welcomeSun2x = 'assets/icons/welcome_sun@2x.png';
  static const String commentsVeryLow2x =
      'assets/icons/comments_very_low@2x.png';
  static const String hospitalDepartment2x =
      'assets/icons/hospital_department@2x.png';
  static const String arrowRightBlack2x =
      'assets/icons/arrow_right_black@2x.png';
  static const String edit2x = 'assets/icons/edit@2x.png';
  static const String correct2x = 'assets/icons/correct@2x.png';
  static const String home3x = 'assets/icons/home@3x.png';
  static const String checkDarkGreen2x = 'assets/icons/check_dark_green@2x.png';
  static const String lockGreen2x = 'assets/icons/lock_green@2x.png';
  static const String clear = 'assets/icons/clear.png';
  static const String welcomeCalendar2x =
      'assets/icons/welcome_calendar@2x.png';
  static const String collectIconSymptom2x =
      'assets/icons/collectIcon_symptom@2x.png';
  static const String categoryHealthLife2x =
      'assets/icons/category_health_life@2x.png';
  static const String collect = 'assets/icons/collect.svg';
  static const String arrowRightGrey2x = 'assets/icons/arrow_right_grey@2x.png';
  static const String videoSelected3x = 'assets/icons/video_selected@3x.png';
  static const String setting2x = 'assets/icons/setting@2x.png';
  static const String collectIconHospital2x =
      'assets/icons/collectIcon_hospital@2x.png';
  static const String backGreen2x = 'assets/icons/back_green@2x.png';
  static const String apple2x = 'assets/icons/apple@2x.png';
  static const String memberSelected3x = 'assets/icons/member_selected@3x.png';
  static const String welcomeRain2x = 'assets/icons/welcome_rain@2x.png';
  static const String arrowRight = 'assets/icons/arrow_right.svg';
  static const String facebookWhite2x = 'assets/icons/facebook_white@2x.png';
  static const String google2x = 'assets/icons/google@2x.png';
  static const String welcomeGo2x = 'assets/icons/welcome_go@2x.png';
  static const String cancel2x = 'assets/icons/cancel@2x.png';
  static const String facebookGreen2x = 'assets/icons/facebook_green@2x.png';
  static const String newsNavIconShare2x =
      'assets/icons/news_nav_icon_share@2x.png';
  static const String googleMap2x = 'assets/icons/google_map@2x.png';
  static const String search = 'assets/icons/search.png';
  static const String commentsMedium2x = 'assets/icons/comments_medium@2x.png';
  static const String line2x = 'assets/icons/line@2x.png';
  static const String order2x = 'assets/icons/order@2x.png';
  static const String newsNavLine2x = 'assets/icons/news_nav_line@2x.png';
  static const String bodymap3x = 'assets/icons/bodymap@3x.png';
  static const String askExpert3x = 'assets/icons/ask_expert@3x.png';
  static const String light2x = 'assets/icons/light@2x.png';
  static const String categoryHopeStore2x =
      'assets/icons/category_hope_store@2x.png';
  static const String askExpertSelected3x =
      'assets/icons/ask_expert_selected@3x.png';
  static const String userGreen2x = 'assets/icons/user_green@2x.png';
  static const String newsNavCollectSelected2x =
      'assets/icons/news_nav_collect_selected@2x.png';
  static const String homeCategoryList2x =
      'assets/icons/home_category_list@2x.png';
  static const String commentsVeryHeight2x =
      'assets/icons/comments_very_height@2x.png';
  static const String cancelSmall2x = 'assets/icons/cancel_small@2x.png';
  static const String check2x = 'assets/icons/check@2x.png';
  static const String categoryMedicineSearch2x =
      'assets/icons/category_medicine_search@2x.png';
  static const String categoryKids2x = 'assets/icons/category_kids@2x.png';
  static const String member3x = 'assets/icons/member@3x.png';
  static const String categoryHospitalSearch2x =
      'assets/icons/category_hospital_search@2x.png';
  static const String collectGreen2x = 'assets/icons/collect_green@2x.png';
  static const String incorrect2x = 'assets/icons/incorrect@2x.png';
  static const String youtube2x = 'assets/icons/youtube@2x.png';
  static const String categoryHealthTools2x =
      'assets/icons/category_health_tools@2x.png';
  static const String information2x = 'assets/icons/information@2x.png';
  static const String memberBlack2x = 'assets/icons/member_black@2x.png';
  static const String collectIconDisease2x =
      'assets/icons/collectIcon_disease@2x.png';
  static const String welcomeAirQuality2x =
      'assets/icons/welcome_air_quality@2x.png';
  static const String instagram2x = 'assets/icons/instagram@2x.png';
  static const String categoryBodyMap2x =
      'assets/icons/category_body_map@2x.png';
  static const String commentsHeight2x = 'assets/icons/comments_height@2x.png';
  static const String eyeOpen2x = 'assets/icons/eye_open@2x.png';
  static const String favorite2x = 'assets/icons/favorite@2x.png';
  static const String commentsLow2x = 'assets/icons/comments_low@2x.png';
  static const String checkNormal2x = 'assets/icons/check_normal@2x.png';
  static const String newsNavCollectUnselected2x =
      'assets/icons/news_nav_collect_unselected@2x.png';
  static const String newsNavWordLevel2x =
      'assets/icons/news_nav_word_level@2x.png';
  static const String lineGreen2x = 'assets/icons/line_green@2x.png';
  static const String eyeOff2x = 'assets/icons/eye_off@2x.png';
  static const String close2x = 'assets/icons/close@2x.png';
  static const String video3x = 'assets/icons/video@3x.png';
  static const String facebook2x = 'assets/icons/facebook@2x.png';
  static const String flutterDash = 'assets/icons/flutter_dash.svg';
  static const String homeSelected3x = 'assets/icons/home_selected@3x.png';
  static const String bodymapSelected3x =
      'assets/icons/bodymap_selected@3x.png';
  static const String person2x = 'assets/icons/person@2x.png';
  static const String lineWhite2x = 'assets/icons/line_white@2x.png';
  static const String categorySymptomSearch2x =
      'assets/icons/category_symptom_search@2x.png';
  static const String shoppingBag2x = 'assets/icons/shopping_bag@2x.png';
}
