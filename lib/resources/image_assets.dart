part of 'resources.dart';

class ImageAssets {
  ImageAssets._();

  static const String loginBackground2x =
      'assets/images/login_background@2x.png';
  static const String chineseMedicineBackground2x =
      'assets/images/chinese_medicine_background@2x.png';
  static const String loginForeground2x =
      'assets/images/login_foreground@2x.png';
  static const String symptomSearchBackground2x =
      'assets/images/symptom_search_background@2x.png';
  static const String newsNavBanner2x = 'assets/images/news_nav_banner@2x.png';
  static const String cryCancelLoveNoText =
      'assets/images/cry_cancel_love_no_text.svg';
  static const String askExpertBackground2x =
      'assets/images/ask_expert_background@2x.png';
  static const String newsImage23x = 'assets/images/newsImage2@3x.png';
  static const String commentsBackground2x =
      'assets/images/comments_background@2x.png';
  static const String foodBackground2x = 'assets/images/food_background@2x.png';
  static const String submited = 'assets/images/submited.svg';
  static const String sleepBackground2x =
      'assets/images/sleep_background@2x.png';
  static const String noFollowItem = 'assets/images/no_follow_item.svg';
  static const String sportBackground2x =
      'assets/images/sport_background@2x.png';
  static const String newsImage13x = 'assets/images/newsImage1@3x.png';
  static const String healthLifeBackground2x =
      'assets/images/health_life_background@2x.png';
  static const String hospitalSearchBackground2x =
      'assets/images/hospital_search_background@2x.png';
  static const String flutterDash = 'assets/images/flutter_dash.png';
  static const String noOrder = 'assets/images/no_order.svg';
  static const String noCollect = 'assets/images/no_collect.svg';
  static const String memberScreenBackground2x =
      'assets/images/member_screen_background@2x.png';
  static const String expertPersonalBackground2x =
      'assets/images/expert_personal_background@2x.png';
  static const String goToLink = 'assets/images/go_to_link.svg';
  static const String headlinesImage3x = 'assets/images/headlinesImage@3x.png';
}
