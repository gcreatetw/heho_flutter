import 'package:flutter/cupertino.dart';

class AppColors {
  AppColors._();

  static const Color white = Color(0xffffffff);
  static const Color whiteSmoke = Color(0xfff5f5f5);
  static const Color paleLilac = Color(0xffe9e9ea);
  static const Color lightGrey = Color(0xffe4ebd0);
  static const Color offWhite = Color(0xfff3f8f2);

  static const Color black = Color(0xff000000);
  static const Color black6 = Color(0x0F000000);
  static const Color black12 = Color(0x1F000000);
  static const Color black16 = Color(0x29000000);
  static const Color black40 = Color(0x66000000);
  static const Color black45 = Color(0x74000000);
  static const Color black50 = Color(0x80000000);
  static const Color black77 = Color(0xc4000000);
  static const Color black80 = Color(0xcc000000);
  static const Color black87 = Color(0xdd1f1f1d);
  static const Color dodgerBlue0 = Color(0x003497fd);
  static const Color charcoalGrey36 = Color(0x5c3c3c43);

  static const Color grey26 = Color(0xff262626);
  static const Color grey2F = Color(0xff2f2f2f);
  static const Color grey2f50 = Color(0x802f2f2f);
  static const Color grey33 = Color(0xff333333);
  static const Color grey36 = Color(0xff363636);
  static const Color grey41 = Color(0xff414141);
  static const Color grey4E = Color(0xff4e4e4e);
  static const Color grey70 = Color(0xff707070);
  static const Color grey7F = Color(0xff7f7f7f);
  static const Color grey75 = Color(0xff757575);
  static const Color grey79 = Color(0xff797979);
  static const Color grey8B = Color(0xff8b8b8b);
  static const Color greyC3 = Color(0xffc3c3c3);
  static const Color greyC6 = Color(0xffc6c6c6);
  static const Color greyCE = Color(0xffCECECE);
  static const Color greyD0 = Color(0xffd0d0d0);
  static const Color greyD3 = Color(0xffd3d3d3);
  static const Color greyDE = Color(0xffdedede);
  static const Color greyDD = Color(0xffDDDDDD);
  static const Color greyE5 = Color(0xffE5E5E5);
  static const Color greyE9 = Color(0xffE9E9E9);
  static const Color greyEF = Color(0xffEFEFEF);
  static const Color greyF2 = Color(0xfff2f2f2);
  static const Color greyF5 = Color(0xfff5f5f5);
  static const Color greyF7 = Color(0xfff7f7f7);
  static const Color greyF8 = Color(0xfff8f8f8);
  static const Color greyFA = Color(0xfffAfAfA);
  static const Color greyishBrown = Color(0xff4e4c4c);
  static const Color mediumGrey = Color(0xff555752);
  static const Color slateGrey = Color(0xff555869);
  static const Color charcoalGrey = Color(0xff2f2e2f);

  static const Color redBrown = Color(0xff811e11);
  static const Color darkRed = Color(0xff8a0000);
  static const Color terracota = Color(0xffdd6c41);

  static const Color lightGreen = Color(0xffcfe5ca);
  static const Color parchment = Color(0xffe3f2b7);
  static const Color lightKhaki = Color(0xffdfefb3);
  static const Color paleOliveGreen = Color(0xffbfe077);
  static const Color lightOlive = Color(0xffa3ba67);
  static const Color drabGreen = Color(0xff728d4a);
  static const Color drabGreen7D = Color(0xff7d9b51);
  static const Color militaryGreen = Color(0xff607a39);
  static const Color forestGreen = Color(0xff66a683);
  static const Color muddyGreen = Color(0xff566f31);
  static const Color darkSage = Color(0xff486448);
  static const Color darkOliveGreen = Color(0xff255701);
  static const Color dustyTeal = Color(0xff3e7f73);
  static const Color darkGreyBlue = Color(0xff2b5d54);
  static const Color green200 = Color(0xFF4A9C9B);
  static const Color green300 = Color(0xFF57A5A1);
  static const Color green600 = Color(0xFF8DAD60);
  static const Color green700 = Color(0xFF9FC363);

  static const Color lightPale = Color(0xffedebdc);
  static const Color pale = Color(0xffffe2c6);
  static const Color eggShell = Color(0xfff8f4cc);
  static const Color lightTan = Color(0xfffaecb0);
  static const Color beige = Color(0xfff4da96);
  static const Color paleGold = Color(0xfffadb72);
  static const Color sandy = Color(0xfff6d273);
  static const Color orangeYellow = Color(0xfff0c01d);
  static const Color paleOrange = Color(0xffffb957);
  static const Color fadedOrange = Color(0xffea9d5b);
  static const Color clayBrown = Color(0xffbc673a);

  static const Color lightPurple = Color(0xffe0d8ef);
  static const Color powderBlue = Color(0xffbcd9ef);
  static const Color lightGreyBlue = Color(0xff8ab7cb);
  static const Color blueGrey = Color(0xff79abd0);
  static const Color carolinaBlue = Color(0xff8fb1f9);
  static const Color azure = Color(0xff0093ff);
  static const Color mutedBlue = Color(0xff366199);
  static const Color leafyGreen = Color(0xff57be37);

  static const Color shimmerBaseColor = AppColors.greyE5;
  static const Color shimmerHighlightColor = AppColors.greyF2;
}
