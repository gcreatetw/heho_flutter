import 'dart:io';

import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:flutter/foundation.dart';

///Refer from app_tracking_transparency TrackingStatus
enum AppTrackingState {
  /// The user has not yet received an authorization request dialog
  notDetermined,

  /// The device is restricted,
  /// tracking is disabled and the system can't show a request dialog
  restricted,

  /// The user denies authorization for tracking
  denied,

  /// The user authorizes access to tracking
  authorized,

  /// The platform is not iOS or the iOS version is below 14.0
  notSupported,
}

extension _TrackingStatusEx on TrackingStatus {
  AppTrackingState get appTrackingState {
    switch (this) {
      case TrackingStatus.notDetermined:
        return AppTrackingState.notDetermined;
      case TrackingStatus.restricted:
        return AppTrackingState.restricted;
      case TrackingStatus.denied:
        return AppTrackingState.denied;
      case TrackingStatus.authorized:
        return AppTrackingState.authorized;
      case TrackingStatus.notSupported:
        return AppTrackingState.notSupported;
    }
  }
}

class AppTrackingUtils {
  static AppTrackingUtils? _instance;

  // ignore: prefer_constructors_over_static_methods
  static AppTrackingUtils get instance {
    return _instance ??= AppTrackingUtils();
  }

  Future<AppTrackingState> get state async {
    if (!kIsWeb && Platform.isIOS) {
      return (await AppTrackingTransparency.requestTrackingAuthorization())
          .appTrackingState;
    } else {
      return AppTrackingState.notSupported;
    }
  }

  ///Only iOS need
  Future<void> checkPermissionStatus() async {
    if (!kIsWeb && Platform.isIOS) {
      if (await AppTrackingTransparency.trackingAuthorizationStatus ==
          TrackingStatus.notDetermined) {
        await AppTrackingTransparency.requestTrackingAuthorization();
      }
    }
  }

  ///Advertising Identifier
  Future<String?> get uuid async {
    if (!kIsWeb && Platform.isIOS) {
      return AppTrackingTransparency.getAdvertisingIdentifier();
    } else {
      return null;
    }
  }
}
