import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/foundation.dart';

export 'package:firebase_analytics/observer.dart';

class FirebaseAnalyticsUtils {
  static FirebaseAnalyticsUtils? _instance;

  // ignore: prefer_constructors_over_static_methods
  static FirebaseAnalyticsUtils get instance {
    return _instance ??= FirebaseAnalyticsUtils();
  }

  static bool get isSupported => kIsWeb || Platform.isAndroid || Platform.isIOS;

  FirebaseAnalyticsUtils() {
    if (isSupported) {
      analytics = FirebaseAnalytics();
    }
  }

  FirebaseAnalytics? analytics;

  Future<void> logEvent(
    String name, {
    Map<String, Object>? parameters,
  }) async {
    await analytics?.logEvent(name: name, parameters: parameters);
  }

  Future<void> setUserProperty(String name, String value) async {
    await analytics?.setUserProperty(name: name, value: value);
  }

  Future<void> setCurrentScreen(
    String screenName, {
    required String screenClassOverride,
  }) async {
    await analytics?.setCurrentScreen(
      screenName: screenName,
      screenClassOverride: screenClassOverride,
    );
  }

  Future<void> setUserId(String id) async {
    await analytics?.setUserId(id);
  }
}
