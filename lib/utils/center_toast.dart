import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:hepo_app/resources/colors.dart';

class CenterToast {
  static void show(
    BuildContext context,
    String msg, {
    String? prefixIcon,
    String? actionText,
    VoidCallback? onActionClick,
    Duration? duration = const Duration(seconds: 2),
  }) {
    final double top = MediaQuery.of(context).size.height / 4;
    showToastWidget(
      Padding(
        padding: EdgeInsets.only(top: top),
        child: Container(
          height: 50.0,
          padding: const EdgeInsets.symmetric(
            vertical: 14.0,
            horizontal: 42.0,
          ),
          decoration: const BoxDecoration(
            color: Color(0xE02F2F2F),
            borderRadius: BorderRadius.all(
              Radius.circular(6.0),
            ),
          ),
          child: Text(
            msg,
            style: const TextStyle(
              color: AppColors.white,
              fontSize: 16,
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
      duration: duration,
      context: context,
      isIgnoring: false,
      animation: StyledToastAnimation.fade,
      reverseAnimation: StyledToastAnimation.fade,
      position: StyledToastPosition.center,
    );
  }
}
