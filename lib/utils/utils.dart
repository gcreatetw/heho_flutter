import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/widgets.dart';

class Utils {
  Utils._();

  //source: https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
  static double measure(double lat1, double lon1, double lat2, double lon2) {
    // generally used geo measurement function
    const double r = 6378.137; // Radius of earth in KM
    final double dLat = lat2 * math.pi / 180 - lat1 * math.pi / 180;
    final double dLon = lon2 * math.pi / 180 - lon1 * math.pi / 180;
    final double a = math.sin(dLat / 2) * math.sin(dLat / 2) +
        math.cos(lat1 * math.pi / 180) *
            math.cos(lat2 * math.pi / 180) *
            math.sin(dLon / 2) *
            math.sin(dLon / 2);
    final double c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a));
    final double d = r * c;
    return d * 1000; // meters
  }

  //source: https://stackoverflow.com/a/67482567
  static double textHeight(String text, TextStyle style, double textWidth) {
    final TextPainter textPainter = TextPainter(
      text: TextSpan(text: text, style: style),
      textDirection: TextDirection.ltr,
      maxLines: 1,
    )..layout();

    final int countLines = (textPainter.size.width / textWidth).ceil();
    final double height = countLines * textPainter.size.height;
    return height;
  }

  static bool isValidCellPhoneNumber(String text) {
    return RegExp(r'^(?:[+0]9)?[0-9]{10}$').hasMatch(text);
  }

  static bool isValidEmail(String text) {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(text);
  }

  static bool isValidPassword(String text) {
    return RegExp(r'^(?=.*?[A-Z,a-z,0-9,!@#\$&*~]).{8,20}$').hasMatch(text);
  }

  static String contactPhoneNumber({
    required String areaCode,
    required String number,
  }) {
    String text = '';
    text = number[0] == '0' ? number.substring(1) : number;
    return '$areaCode$text';
  }
}
