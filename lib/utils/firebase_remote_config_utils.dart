import 'dart:io';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/foundation.dart';

export 'package:firebase_analytics/observer.dart';

class FirebaseRemoteConfigUtils {
  static FirebaseRemoteConfigUtils? _instance;

  // ignore: prefer_constructors_over_static_methods
  static FirebaseRemoteConfigUtils get instance {
    return _instance ??= FirebaseRemoteConfigUtils();
  }

  static bool get isSupported =>
      !kIsWeb && (Platform.isAndroid || Platform.isIOS || Platform.isMacOS);

  FirebaseRemoteConfigUtils() {
    if (isSupported) {
      config = RemoteConfig.instance;
    }
  }

  RemoteConfig? config;

  Future<bool?> fetchAndActivate() async {
    if (config != null) {
      return config!.fetchAndActivate();
    } else {
      return null;
    }
  }
}
