import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hepo_app/resources/colors.dart';
import 'package:hepo_app/resources/resources.dart';

class Toast {
  static void show(
    BuildContext context,
    String msg, {
    String? prefixIcon,
    String? actionText,
    VoidCallback? onActionClick,
    Duration? duration = const Duration(seconds: 3),
  }) {
    showToastWidget(
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(
            vertical: 14.0,
            horizontal: 19.0,
          ),
          decoration: const BoxDecoration(
            color: AppColors.greyF5,
            borderRadius: BorderRadius.all(
              Radius.circular(12.0),
            ),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: AppColors.black16,
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
          child: Row(
            children: <Widget>[
              if (prefixIcon != null) ...<Widget>[
                const SizedBox(width: 5.5),
                Padding(
                  padding: const EdgeInsets.only(top: 3.0),
                  child: SvgPicture.asset(
                    prefixIcon,
                    width: 14.0,
                    height: 14.0,
                  ),
                ),
                const SizedBox(width: 10.5),
              ],
              Expanded(
                child: Text(
                  msg,
                  style: const TextStyle(
                    color: Color(0xb2000000),
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              if (actionText != null)
                GestureDetector(
                  onTap: onActionClick,
                  child: Row(
                    children: <Widget>[
                      Text(
                        actionText,
                        style: const TextStyle(
                          color: AppColors.mediumGrey,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      const SizedBox(width: 5.5),
                      Padding(
                        padding: const EdgeInsets.only(top: 3.0),
                        child: SvgPicture.asset(
                          IconAssets.arrowRight,
                          width: 12.0,
                          height: 12.0,
                        ),
                      ),
                    ],
                  ),
                )
            ],
          ),
        ),
      ),
      duration: duration,
      context: context,
      isIgnoring: false,
      animation: StyledToastAnimation.slideFromBottomFade,
      reverseAnimation: StyledToastAnimation.slideFromBottomFade,
      position: StyledToastPosition.bottom,
    );
  }
}
